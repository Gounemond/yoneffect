// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:8611,x:32719,y:32712,varname:node_8611,prsc:2|emission-1159-OUT;n:type:ShaderForge.SFN_Lerp,id:1159,x:32431,y:32686,varname:node_1159,prsc:2|A-3079-RGB,B-9238-OUT,T-7120-OUT;n:type:ShaderForge.SFN_Multiply,id:9238,x:32242,y:32729,varname:node_9238,prsc:2|A-7733-RGB,B-9797-OUT;n:type:ShaderForge.SFN_Color,id:7733,x:32053,y:32605,ptovrint:False,ptlb:node_7733,ptin:_node_7733,varname:node_7733,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:0,c4:1;n:type:ShaderForge.SFN_Vector1,id:9797,x:32016,y:32799,varname:node_9797,prsc:2,v1:2;n:type:ShaderForge.SFN_Color,id:3079,x:32210,y:32536,ptovrint:False,ptlb:node_3079,ptin:_node_3079,varname:node_3079,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0,c3:0.07450981,c4:1;n:type:ShaderForge.SFN_OneMinus,id:7070,x:32337,y:32913,varname:node_7070,prsc:2|IN-1777-OUT;n:type:ShaderForge.SFN_Fresnel,id:1777,x:32144,y:32913,varname:node_1777,prsc:2|NRM-994-OUT,EXP-1436-OUT;n:type:ShaderForge.SFN_NormalVector,id:994,x:31861,y:32885,prsc:2,pt:False;n:type:ShaderForge.SFN_Vector1,id:1436,x:31902,y:33076,varname:node_1436,prsc:2,v1:3;n:type:ShaderForge.SFN_Sin,id:7662,x:32290,y:33082,varname:node_7662,prsc:2|IN-8803-OUT;n:type:ShaderForge.SFN_Time,id:8249,x:31888,y:33145,varname:node_8249,prsc:2;n:type:ShaderForge.SFN_Multiply,id:8803,x:32107,y:33122,varname:node_8803,prsc:2|A-8249-T,B-5491-OUT;n:type:ShaderForge.SFN_Vector1,id:5491,x:32010,y:33292,varname:node_5491,prsc:2,v1:1.5;n:type:ShaderForge.SFN_Add,id:7120,x:32499,y:33006,varname:node_7120,prsc:2|A-7070-OUT,B-7662-OUT;proporder:7733-3079;pass:END;sub:END;*/

Shader "Custom/AstroPilar" {
    Properties {
        _node_7733 ("node_7733", Color) = (1,1,0,1)
        _node_3079 ("node_3079", Color) = (0,0,0.07450981,1)
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform float4 _node_7733;
            uniform float4 _node_3079;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 posWorld : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
                UNITY_FOG_COORDS(2)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float4 node_8249 = _Time + _TimeEditor;
                float3 emissive = lerp(_node_3079.rgb,(_node_7733.rgb*2.0),((1.0 - pow(1.0-max(0,dot(i.normalDir, viewDirection)),3.0))+sin((node_8249.g*1.5))));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
