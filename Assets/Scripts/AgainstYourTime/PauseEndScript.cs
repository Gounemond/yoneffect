﻿using UnityEngine;
using System.Collections;


public class PauseEndScript : MonoBehaviour {


    public GameObject pauseUI;
    public GameObject endUI;


	// Use this for initialization
	void Start ()
    {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
	    if (Input.GetButtonDown("Menu"))
        {
            if (!YonEffectJRController.Self.IsPause)
            {
                YonEffectJRController.Self.IsPause = true;
                Time.timeScale = 0;
                pauseUI.SetActive(true);
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
            }
            else
            {
                YonEffectJRController.Self.IsPause = false;
                Time.timeScale = 1;
                pauseUI.SetActive(false);
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.Locked;
            }
        }
	}
}
