// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:2784,x:35980,y:32624,varname:node_2784,prsc:2|emission-1293-OUT,alpha-2473-OUT,clip-7656-OUT;n:type:ShaderForge.SFN_Color,id:9650,x:34552,y:32480,ptovrint:False,ptlb:node_9650,ptin:_node_9650,varname:node_9650,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0.347,c3:1,c4:1;n:type:ShaderForge.SFN_Append,id:210,x:34522,y:32285,varname:node_210,prsc:2|A-383-OUT,B-9411-OUT;n:type:ShaderForge.SFN_Vector1,id:9411,x:34245,y:32458,varname:node_9411,prsc:2,v1:1;n:type:ShaderForge.SFN_Tex2d,id:4105,x:33524,y:33340,ptovrint:False,ptlb:noise,ptin:_noise,varname:node_4625,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:28c7aad1372ff114b90d330f8a2dd938,ntxv:0,isnm:False|UVIN-8402-UVOUT;n:type:ShaderForge.SFN_Add,id:1807,x:33888,y:33153,varname:node_1807,prsc:2|A-4540-OUT,B-4105-RGB;n:type:ShaderForge.SFN_RemapRange,id:4540,x:33231,y:33047,varname:node_4540,prsc:2,frmn:1,frmx:0,tomn:0.1,tomx:-0.1|IN-8205-OUT;n:type:ShaderForge.SFN_RemapRange,id:2178,x:33785,y:32590,varname:node_2178,prsc:2,frmn:1,frmx:0,tomn:6,tomx:-6|IN-1807-OUT;n:type:ShaderForge.SFN_Clamp01,id:2459,x:33976,y:32396,varname:node_2459,prsc:2|IN-2178-OUT;n:type:ShaderForge.SFN_OneMinus,id:383,x:34177,y:32273,varname:node_383,prsc:2|IN-2459-OUT;n:type:ShaderForge.SFN_ComponentMask,id:7656,x:34991,y:33126,varname:node_7656,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-1807-OUT;n:type:ShaderForge.SFN_Sin,id:3424,x:32658,y:33070,varname:node_3424,prsc:2|IN-5939-OUT;n:type:ShaderForge.SFN_Slider,id:5096,x:32469,y:32866,ptovrint:False,ptlb:DoNoTouchPLZ,ptin:_DoNoTouchPLZ,varname:node_291,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-10,cur:10,max:10;n:type:ShaderForge.SFN_Multiply,id:8205,x:32872,y:32975,varname:node_8205,prsc:2|A-5096-OUT,B-3424-OUT;n:type:ShaderForge.SFN_Slider,id:5939,x:32099,y:33208,ptovrint:False,ptlb:time,ptin:_time,varname:node_731,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-1,cur:1,max:1;n:type:ShaderForge.SFN_Add,id:5937,x:35069,y:32457,varname:node_5937,prsc:2|A-210-OUT,B-5653-OUT;n:type:ShaderForge.SFN_TexCoord,id:8402,x:33283,y:33417,varname:node_8402,prsc:2,uv:0;n:type:ShaderForge.SFN_Fresnel,id:2473,x:34560,y:32837,varname:node_2473,prsc:2|NRM-5406-OUT,EXP-7664-OUT;n:type:ShaderForge.SFN_ValueProperty,id:7664,x:34377,y:33054,ptovrint:False,ptlb:fresnel,ptin:_fresnel,varname:node_7664,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_NormalVector,id:5406,x:34279,y:32823,prsc:2,pt:False;n:type:ShaderForge.SFN_OneMinus,id:7001,x:34800,y:32887,varname:node_7001,prsc:2|IN-2473-OUT;n:type:ShaderForge.SFN_Lerp,id:5653,x:34945,y:32607,varname:node_5653,prsc:2|A-9650-RGB,B-8297-OUT,T-2473-OUT;n:type:ShaderForge.SFN_Color,id:4667,x:34439,y:32626,ptovrint:False,ptlb:node_4667,ptin:_node_4667,varname:node_4667,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0.345098,c3:1,c4:1;n:type:ShaderForge.SFN_Multiply,id:8297,x:34663,y:32659,varname:node_8297,prsc:2|A-4667-RGB,B-8905-OUT;n:type:ShaderForge.SFN_Vector1,id:8905,x:34354,y:32767,varname:node_8905,prsc:2,v1:2;n:type:ShaderForge.SFN_DepthBlend,id:8083,x:35064,y:32977,varname:node_8083,prsc:2|DIST-6444-OUT;n:type:ShaderForge.SFN_Vector1,id:6444,x:34880,y:33056,varname:node_6444,prsc:2,v1:1;n:type:ShaderForge.SFN_Add,id:6601,x:35220,y:32935,varname:node_6601,prsc:2|A-7001-OUT,B-8083-OUT;n:type:ShaderForge.SFN_SwitchProperty,id:1293,x:35418,y:32516,ptovrint:False,ptlb:node_1293,ptin:_node_1293,varname:node_1293,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:False|A-5937-OUT,B-5653-OUT;proporder:9650-4105-5096-5939-7664-4667-1293;pass:END;sub:END;*/

Shader "Custom/bubble" {
    Properties {
        _node_9650 ("node_9650", Color) = (1,0.347,1,1)
        _noise ("noise", 2D) = "white" {}
        _DoNoTouchPLZ ("DoNoTouchPLZ", Range(-10, 10)) = 10
        _time ("time", Range(-1, 1)) = 1
        _fresnel ("fresnel", Float ) = 1
        _node_4667 ("node_4667", Color) = (1,0.345098,1,1)
        [MaterialToggle] _node_1293 ("node_1293", Float ) = 2
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _node_9650;
            uniform sampler2D _noise; uniform float4 _noise_ST;
            uniform float _DoNoTouchPLZ;
            uniform float _time;
            uniform float _fresnel;
            uniform float4 _node_4667;
            uniform fixed _node_1293;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                UNITY_FOG_COORDS(3)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float4 _noise_var = tex2D(_noise,TRANSFORM_TEX(i.uv0, _noise));
                float3 node_1807 = (((_DoNoTouchPLZ*sin(_time))*0.2+-0.1)+_noise_var.rgb);
                clip(node_1807.r - 0.5);
////// Lighting:
////// Emissive:
                float node_2473 = pow(1.0-max(0,dot(i.normalDir, viewDirection)),_fresnel);
                float3 node_5653 = lerp(_node_9650.rgb,(_node_4667.rgb*2.0),node_2473);
                float3 emissive = lerp( (float4((1.0 - saturate((node_1807*12.0+-6.0))),1.0)+node_5653), node_5653, _node_1293 ).rgb;
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,node_2473);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform sampler2D _noise; uniform float4 _noise_ST;
            uniform float _DoNoTouchPLZ;
            uniform float _time;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float4 _noise_var = tex2D(_noise,TRANSFORM_TEX(i.uv0, _noise));
                float3 node_1807 = (((_DoNoTouchPLZ*sin(_time))*0.2+-0.1)+_noise_var.rgb);
                clip(node_1807.r - 0.5);
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
