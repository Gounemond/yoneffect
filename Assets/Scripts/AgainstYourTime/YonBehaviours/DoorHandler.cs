using UnityEngine;
using System.Collections;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System;
using System.Linq;

public class DoorHandler : YonBehaviour {
	
	public float speed = 0.1f;
	public float distance = 0;
	public int axis = 0;
	
	private bool active = false;
	private bool moving = false;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

	}
	
	override public void Activate()
	{
		StartCoroutine (move (1));
	}
	
	override public void deActivate()
	{
		StartCoroutine (move (-1));
	}
	
	private IEnumerator move(int dove)
	{
		float spazio = 0.0f;
		while (spazio*spazio < distance*distance)
		{
			if (axis == 0)
			{
				transform.Translate (Vector3.left * Time.deltaTime * speed*dove);
				spazio+= speed*Time.deltaTime;
				yield return 0;
			}
			if (axis == 1)
			{
				transform.Translate (Vector3.up * Time.deltaTime * speed*dove);
				spazio+= speed*Time.deltaTime;
				yield return 0;
			}
			if (axis == 2)
			{
				transform.Translate (Vector3.forward * Time.deltaTime * speed*dove);
				spazio+= speed*Time.deltaTime;
				yield return 0;	
			}
		}
	}
}
