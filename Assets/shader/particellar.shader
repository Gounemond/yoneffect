// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:1,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:8713,x:32719,y:32712,varname:node_8713,prsc:2|emission-7391-OUT;n:type:ShaderForge.SFN_Tex2d,id:9072,x:32122,y:32514,ptovrint:False,ptlb:ologram_3,ptin:_ologram_3,varname:node_9072,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:7beeecf6babef464791faeb7693ed85d,ntxv:2,isnm:False|UVIN-5757-UVOUT;n:type:ShaderForge.SFN_Tex2d,id:3529,x:32258,y:32900,ptovrint:False,ptlb:ologram_1,ptin:_ologram_1,varname:node_3529,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:c01cd32a05805574bb7648740fb37dab,ntxv:0,isnm:False|UVIN-167-UVOUT;n:type:ShaderForge.SFN_Parallax,id:5757,x:31902,y:32582,varname:node_5757,prsc:2|UVIN-4525-UVOUT,HEI-955-OUT;n:type:ShaderForge.SFN_Vector1,id:955,x:31638,y:32568,varname:node_955,prsc:2,v1:3;n:type:ShaderForge.SFN_Multiply,id:6441,x:32258,y:32690,varname:node_6441,prsc:2|A-9072-RGB,B-4578-RGB;n:type:ShaderForge.SFN_Color,id:4578,x:32003,y:32755,ptovrint:False,ptlb:olograma_2color,ptin:_olograma_2color,varname:node_4578,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.466317,c2:0.862908,c3:0.9191176,c4:1;n:type:ShaderForge.SFN_Vector1,id:2800,x:31845,y:32958,varname:node_2800,prsc:2,v1:1;n:type:ShaderForge.SFN_Parallax,id:167,x:32068,y:32944,varname:node_167,prsc:2|UVIN-993-UVOUT,HEI-2800-OUT;n:type:ShaderForge.SFN_Rotator,id:993,x:31924,y:33055,varname:node_993,prsc:2|UVIN-7685-UVOUT;n:type:ShaderForge.SFN_Panner,id:7685,x:31745,y:33055,varname:node_7685,prsc:2,spu:0,spv:0.1|UVIN-2536-UVOUT,DIST-4005-OUT;n:type:ShaderForge.SFN_Time,id:3390,x:31457,y:33005,varname:node_3390,prsc:2;n:type:ShaderForge.SFN_Multiply,id:4005,x:31585,y:33119,varname:node_4005,prsc:2|A-3390-T,B-9835-OUT;n:type:ShaderForge.SFN_Vector1,id:9835,x:31372,y:33228,varname:node_9835,prsc:2,v1:0.001;n:type:ShaderForge.SFN_Panner,id:4525,x:31723,y:32634,varname:node_4525,prsc:2,spu:1,spv:0|UVIN-2536-UVOUT,DIST-5571-OUT;n:type:ShaderForge.SFN_Time,id:1015,x:31306,y:32538,varname:node_1015,prsc:2;n:type:ShaderForge.SFN_Multiply,id:5571,x:31433,y:32664,varname:node_5571,prsc:2|A-1015-T,B-8763-OUT;n:type:ShaderForge.SFN_Vector1,id:8763,x:31258,y:32785,varname:node_8763,prsc:2,v1:1;n:type:ShaderForge.SFN_Slider,id:508,x:32101,y:33195,ptovrint:False,ptlb:slidevfx,ptin:_slidevfx,varname:node_508,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Add,id:3777,x:32470,y:32837,varname:node_3777,prsc:2|A-6441-OUT,B-3529-RGB;n:type:ShaderForge.SFN_Lerp,id:7391,x:32537,y:32967,varname:node_7391,prsc:2|A-3777-OUT,B-3643-OUT,T-508-OUT;n:type:ShaderForge.SFN_Vector1,id:3643,x:32241,y:33129,varname:node_3643,prsc:2,v1:0;n:type:ShaderForge.SFN_TexCoord,id:2536,x:31471,y:32431,varname:node_2536,prsc:2,uv:0;proporder:9072-3529-4578-508;pass:END;sub:END;*/

Shader "Custom/particellar" {
    Properties {
        _ologram_3 ("ologram_3", 2D) = "black" {}
        _ologram_1 ("ologram_1", 2D) = "white" {}
        _olograma_2color ("olograma_2color", Color) = (0.466317,0.862908,0.9191176,1)
        _slidevfx ("slidevfx", Range(0, 1)) = 0
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Cull Front
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform sampler2D _ologram_3; uniform float4 _ologram_3_ST;
            uniform sampler2D _ologram_1; uniform float4 _ologram_1_ST;
            uniform float4 _olograma_2color;
            uniform float _slidevfx;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 bitangentDir : TEXCOORD4;
                UNITY_FOG_COORDS(5)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(-v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float4 node_1015 = _Time + _TimeEditor;
                float2 node_5757 = (0.05*(3.0 - 0.5)*mul(tangentTransform, viewDirection).xy + (i.uv0+(node_1015.g*1.0)*float2(1,0)));
                float4 _ologram_3_var = tex2D(_ologram_3,TRANSFORM_TEX(node_5757.rg, _ologram_3));
                float4 node_6024 = _Time + _TimeEditor;
                float node_993_ang = node_6024.g;
                float node_993_spd = 1.0;
                float node_993_cos = cos(node_993_spd*node_993_ang);
                float node_993_sin = sin(node_993_spd*node_993_ang);
                float2 node_993_piv = float2(0.5,0.5);
                float4 node_3390 = _Time + _TimeEditor;
                float2 node_993 = (mul((i.uv0+(node_3390.g*0.001)*float2(0,0.1))-node_993_piv,float2x2( node_993_cos, -node_993_sin, node_993_sin, node_993_cos))+node_993_piv);
                float2 node_167 = (0.05*(1.0 - 0.5)*mul(tangentTransform, viewDirection).xy + node_993);
                float4 _ologram_1_var = tex2D(_ologram_1,TRANSFORM_TEX(node_167.rg, _ologram_1));
                float node_3643 = 0.0;
                float3 emissive = lerp(((_ologram_3_var.rgb*_olograma_2color.rgb)+_ologram_1_var.rgb),float3(node_3643,node_3643,node_3643),_slidevfx);
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
