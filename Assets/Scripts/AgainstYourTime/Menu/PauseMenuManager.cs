﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using VRStandardAssets.Common;
using VRStandardAssets.Utils;

public class PauseMenuManager : MonoBehaviour {

    public Transform OVRPlayerController;
    public VRCameraFade PlayerCameraFader;

    public Transform OVRCameraRigPause;
    public VRCameraFade CameraPauseFader;

    public YonEffectJRController gameplayController;

    // Use this for initialization
    void Start ()
    {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetButtonDown("Menu"))
        {
            if (!YonEffectJRController.Self.IsPause)
            {
                PauseActivate();
            }
            else
            {
                PauseDeactivate();
            }
        }
    }

    public void PauseActivate()
    {
        YonEffectJRController.Self.IsPause = true;
        YonEffectJRController.Self.PausePlayerRecording();

        PlayerCameraFader.OnFadeComplete += SwitchToPauseCamera;
        PlayerCameraFader.FadeOut(false);
    }

    public void PauseDeactivate()
    {
        CameraPauseFader.OnFadeComplete += SwitchToGameCamera;
        CameraPauseFader.FadeOut(false);
    }

    public void SwitchToPauseCamera()
    {
        OVRPlayerController.gameObject.SetActive(false);
        OVRCameraRigPause.gameObject.SetActive(true);
        CameraPauseFader.FadeIn(false);

        PlayerCameraFader.OnFadeComplete -= SwitchToPauseCamera;
    }

    public void SwitchToGameCamera()
    {
        OVRCameraRigPause.gameObject.SetActive(false);
        OVRPlayerController.gameObject.SetActive(true);

        YonEffectJRController.Self.IsPause = false;
        YonEffectJRController.Self.ResumePlayerRecording();
        PlayerCameraFader.FadeIn(false);

        CameraPauseFader.OnFadeComplete -= SwitchToGameCamera;

    }
}
