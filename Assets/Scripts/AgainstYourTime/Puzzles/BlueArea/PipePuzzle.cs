﻿using UnityEngine;
using System.Collections;

public class PipePuzzle : MonoBehaviour
{
    public Pipe generatorPipe;
    public Pipe[] endPipes;
    public Pipe[] pipes;

    public GameObject[] objectsToActivate;

    public AudioSource audioPlayer;
    public AudioClip turningElement;
    public AudioClip poweringUp;

	// Use this for initialization
	IEnumerator Start ()
    {
        yield return new WaitForSeconds(1);
        ResetPower();
        Propagate();
	}

    public void ResetPower()
    {
        audioPlayer.PlayOneShot(turningElement);
        for (int i = 0; i < pipes.Length; i++)
        {
            pipes[i].TurnOff();
            pipes[i].RechargeAll();
        }
    }

    public void Propagate()
    {
        audioPlayer.PlayOneShot(poweringUp);
        generatorPipe.RechargeAll();
        ResetPower();
        generatorPipe.Propagate();
        CheckWinning();
    }

    bool CheckWinning()
    {
        bool won = true;
        for (int i = 0; i < endPipes.Length; i++)
        {
            if (endPipes[i].isPowered == false)
            {
                won = false;
            }
        }
        if (won)
        {
            for (int i = 0; i < objectsToActivate.Length; i++)
            {
                objectsToActivate[i].SetActive(true);
            }
            return true;
        }
        else
        {
            for (int i = 0; i < objectsToActivate.Length; i++)
            {
                objectsToActivate[i].SetActive(false);
            }
            return false;
        }
    }
}
