// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:6051,x:33312,y:32914,varname:node_6051,prsc:2|emission-7488-OUT,voffset-3091-OUT,disp-3091-OUT,tess-8809-OUT;n:type:ShaderForge.SFN_Add,id:3091,x:32777,y:33157,varname:node_3091,prsc:2|A-7770-OUT,B-2208-OUT;n:type:ShaderForge.SFN_Frac,id:7770,x:32605,y:33012,varname:node_7770,prsc:2|IN-1588-XYZ;n:type:ShaderForge.SFN_FragmentPosition,id:1588,x:32397,y:32944,varname:node_1588,prsc:2;n:type:ShaderForge.SFN_Multiply,id:2208,x:32528,y:33215,varname:node_2208,prsc:2|A-1113-OUT,B-2699-OUT,C-7913-OUT;n:type:ShaderForge.SFN_Vector1,id:1113,x:32246,y:33191,varname:node_1113,prsc:2,v1:0.3;n:type:ShaderForge.SFN_NormalVector,id:2699,x:32222,y:33279,prsc:2,pt:False;n:type:ShaderForge.SFN_Vector1,id:8809,x:32866,y:33290,varname:node_8809,prsc:2,v1:1.5;n:type:ShaderForge.SFN_Multiply,id:7913,x:32151,y:32932,varname:node_7913,prsc:2|A-8746-OUT,B-4382-OUT;n:type:ShaderForge.SFN_Sin,id:8746,x:31948,y:32881,varname:node_8746,prsc:2|IN-4586-UVOUT;n:type:ShaderForge.SFN_Rotator,id:4586,x:31698,y:32807,varname:node_4586,prsc:2|UVIN-2278-UVOUT;n:type:ShaderForge.SFN_TexCoord,id:2278,x:31537,y:32693,varname:node_2278,prsc:2,uv:0;n:type:ShaderForge.SFN_Vector1,id:4382,x:31978,y:33160,varname:node_4382,prsc:2,v1:1;n:type:ShaderForge.SFN_Add,id:7488,x:32588,y:32766,varname:node_7488,prsc:2|A-2488-RGB,B-7913-OUT;n:type:ShaderForge.SFN_Color,id:2488,x:32311,y:32655,ptovrint:False,ptlb:node_2488,ptin:_node_2488,varname:node_2488,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0,c3:1,c4:1;proporder:2488;pass:END;sub:END;*/

Shader "Custom/idontknowsomthing" {
    Properties {
        _node_2488 ("node_2488", Color) = (0,0,1,1)
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma hull hull
            #pragma domain domain
            #pragma vertex tessvert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "Tessellation.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 5.0
            uniform float4 _TimeEditor;
            uniform float4 _node_2488;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                UNITY_FOG_COORDS(3)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                float4 node_8099 = _Time + _TimeEditor;
                float node_4586_ang = node_8099.g;
                float node_4586_spd = 1.0;
                float node_4586_cos = cos(node_4586_spd*node_4586_ang);
                float node_4586_sin = sin(node_4586_spd*node_4586_ang);
                float2 node_4586_piv = float2(0.5,0.5);
                float2 node_4586 = (mul(o.uv0-node_4586_piv,float2x2( node_4586_cos, -node_4586_sin, node_4586_sin, node_4586_cos))+node_4586_piv);
                float2 node_7913 = (sin(node_4586)*1.0);
                float3 node_3091 = (frac(mul(unity_ObjectToWorld, v.vertex).rgb)+(0.3*v.normal*float3(node_7913,0.0)));
                v.vertex.xyz += node_3091;
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            #ifdef UNITY_CAN_COMPILE_TESSELLATION
                struct TessVertex {
                    float4 vertex : INTERNALTESSPOS;
                    float3 normal : NORMAL;
                    float4 tangent : TANGENT;
                    float2 texcoord0 : TEXCOORD0;
                };
                struct OutputPatchConstant {
                    float edge[3]         : SV_TessFactor;
                    float inside          : SV_InsideTessFactor;
                    float3 vTangent[4]    : TANGENT;
                    float2 vUV[4]         : TEXCOORD;
                    float3 vTanUCorner[4] : TANUCORNER;
                    float3 vTanVCorner[4] : TANVCORNER;
                    float4 vCWts          : TANWEIGHTS;
                };
                TessVertex tessvert (VertexInput v) {
                    TessVertex o;
                    o.vertex = v.vertex;
                    o.normal = v.normal;
                    o.tangent = v.tangent;
                    o.texcoord0 = v.texcoord0;
                    return o;
                }
                void displacement (inout VertexInput v){
                    float4 node_8099 = _Time + _TimeEditor;
                    float node_4586_ang = node_8099.g;
                    float node_4586_spd = 1.0;
                    float node_4586_cos = cos(node_4586_spd*node_4586_ang);
                    float node_4586_sin = sin(node_4586_spd*node_4586_ang);
                    float2 node_4586_piv = float2(0.5,0.5);
                    float2 node_4586 = (mul(v.texcoord0-node_4586_piv,float2x2( node_4586_cos, -node_4586_sin, node_4586_sin, node_4586_cos))+node_4586_piv);
                    float2 node_7913 = (sin(node_4586)*1.0);
                    float3 node_3091 = (frac(mul(unity_ObjectToWorld, v.vertex).rgb)+(0.3*v.normal*float3(node_7913,0.0)));
                    v.vertex.xyz += node_3091;
                }
                float Tessellation(TessVertex v){
                    return 1.5;
                }
                float4 Tessellation(TessVertex v, TessVertex v1, TessVertex v2){
                    float tv = Tessellation(v);
                    float tv1 = Tessellation(v1);
                    float tv2 = Tessellation(v2);
                    return float4( tv1+tv2, tv2+tv, tv+tv1, tv+tv1+tv2 ) / float4(2,2,2,3);
                }
                OutputPatchConstant hullconst (InputPatch<TessVertex,3> v) {
                    OutputPatchConstant o = (OutputPatchConstant)0;
                    float4 ts = Tessellation( v[0], v[1], v[2] );
                    o.edge[0] = ts.x;
                    o.edge[1] = ts.y;
                    o.edge[2] = ts.z;
                    o.inside = ts.w;
                    return o;
                }
                [domain("tri")]
                [partitioning("fractional_odd")]
                [outputtopology("triangle_cw")]
                [patchconstantfunc("hullconst")]
                [outputcontrolpoints(3)]
                TessVertex hull (InputPatch<TessVertex,3> v, uint id : SV_OutputControlPointID) {
                    return v[id];
                }
                [domain("tri")]
                VertexOutput domain (OutputPatchConstant tessFactors, const OutputPatch<TessVertex,3> vi, float3 bary : SV_DomainLocation) {
                    VertexInput v = (VertexInput)0;
                    v.vertex = vi[0].vertex*bary.x + vi[1].vertex*bary.y + vi[2].vertex*bary.z;
                    v.normal = vi[0].normal*bary.x + vi[1].normal*bary.y + vi[2].normal*bary.z;
                    v.tangent = vi[0].tangent*bary.x + vi[1].tangent*bary.y + vi[2].tangent*bary.z;
                    v.texcoord0 = vi[0].texcoord0*bary.x + vi[1].texcoord0*bary.y + vi[2].texcoord0*bary.z;
                    displacement(v);
                    VertexOutput o = vert(v);
                    return o;
                }
            #endif
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float4 node_8099 = _Time + _TimeEditor;
                float node_4586_ang = node_8099.g;
                float node_4586_spd = 1.0;
                float node_4586_cos = cos(node_4586_spd*node_4586_ang);
                float node_4586_sin = sin(node_4586_spd*node_4586_ang);
                float2 node_4586_piv = float2(0.5,0.5);
                float2 node_4586 = (mul(i.uv0-node_4586_piv,float2x2( node_4586_cos, -node_4586_sin, node_4586_sin, node_4586_cos))+node_4586_piv);
                float2 node_7913 = (sin(node_4586)*1.0);
                float3 emissive = (_node_2488.rgb+float3(node_7913,0.0));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            
            CGPROGRAM
            #pragma hull hull
            #pragma domain domain
            #pragma vertex tessvert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #include "Tessellation.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 5.0
            uniform float4 _TimeEditor;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
                float4 posWorld : TEXCOORD2;
                float3 normalDir : TEXCOORD3;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                float4 node_8023 = _Time + _TimeEditor;
                float node_4586_ang = node_8023.g;
                float node_4586_spd = 1.0;
                float node_4586_cos = cos(node_4586_spd*node_4586_ang);
                float node_4586_sin = sin(node_4586_spd*node_4586_ang);
                float2 node_4586_piv = float2(0.5,0.5);
                float2 node_4586 = (mul(o.uv0-node_4586_piv,float2x2( node_4586_cos, -node_4586_sin, node_4586_sin, node_4586_cos))+node_4586_piv);
                float2 node_7913 = (sin(node_4586)*1.0);
                float3 node_3091 = (frac(mul(unity_ObjectToWorld, v.vertex).rgb)+(0.3*v.normal*float3(node_7913,0.0)));
                v.vertex.xyz += node_3091;
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            #ifdef UNITY_CAN_COMPILE_TESSELLATION
                struct TessVertex {
                    float4 vertex : INTERNALTESSPOS;
                    float3 normal : NORMAL;
                    float4 tangent : TANGENT;
                    float2 texcoord0 : TEXCOORD0;
                };
                struct OutputPatchConstant {
                    float edge[3]         : SV_TessFactor;
                    float inside          : SV_InsideTessFactor;
                    float3 vTangent[4]    : TANGENT;
                    float2 vUV[4]         : TEXCOORD;
                    float3 vTanUCorner[4] : TANUCORNER;
                    float3 vTanVCorner[4] : TANVCORNER;
                    float4 vCWts          : TANWEIGHTS;
                };
                TessVertex tessvert (VertexInput v) {
                    TessVertex o;
                    o.vertex = v.vertex;
                    o.normal = v.normal;
                    o.tangent = v.tangent;
                    o.texcoord0 = v.texcoord0;
                    return o;
                }
                void displacement (inout VertexInput v){
                    float4 node_8023 = _Time + _TimeEditor;
                    float node_4586_ang = node_8023.g;
                    float node_4586_spd = 1.0;
                    float node_4586_cos = cos(node_4586_spd*node_4586_ang);
                    float node_4586_sin = sin(node_4586_spd*node_4586_ang);
                    float2 node_4586_piv = float2(0.5,0.5);
                    float2 node_4586 = (mul(v.texcoord0-node_4586_piv,float2x2( node_4586_cos, -node_4586_sin, node_4586_sin, node_4586_cos))+node_4586_piv);
                    float2 node_7913 = (sin(node_4586)*1.0);
                    float3 node_3091 = (frac(mul(unity_ObjectToWorld, v.vertex).rgb)+(0.3*v.normal*float3(node_7913,0.0)));
                    v.vertex.xyz += node_3091;
                }
                float Tessellation(TessVertex v){
                    return 1.5;
                }
                float4 Tessellation(TessVertex v, TessVertex v1, TessVertex v2){
                    float tv = Tessellation(v);
                    float tv1 = Tessellation(v1);
                    float tv2 = Tessellation(v2);
                    return float4( tv1+tv2, tv2+tv, tv+tv1, tv+tv1+tv2 ) / float4(2,2,2,3);
                }
                OutputPatchConstant hullconst (InputPatch<TessVertex,3> v) {
                    OutputPatchConstant o = (OutputPatchConstant)0;
                    float4 ts = Tessellation( v[0], v[1], v[2] );
                    o.edge[0] = ts.x;
                    o.edge[1] = ts.y;
                    o.edge[2] = ts.z;
                    o.inside = ts.w;
                    return o;
                }
                [domain("tri")]
                [partitioning("fractional_odd")]
                [outputtopology("triangle_cw")]
                [patchconstantfunc("hullconst")]
                [outputcontrolpoints(3)]
                TessVertex hull (InputPatch<TessVertex,3> v, uint id : SV_OutputControlPointID) {
                    return v[id];
                }
                [domain("tri")]
                VertexOutput domain (OutputPatchConstant tessFactors, const OutputPatch<TessVertex,3> vi, float3 bary : SV_DomainLocation) {
                    VertexInput v = (VertexInput)0;
                    v.vertex = vi[0].vertex*bary.x + vi[1].vertex*bary.y + vi[2].vertex*bary.z;
                    v.normal = vi[0].normal*bary.x + vi[1].normal*bary.y + vi[2].normal*bary.z;
                    v.tangent = vi[0].tangent*bary.x + vi[1].tangent*bary.y + vi[2].tangent*bary.z;
                    v.texcoord0 = vi[0].texcoord0*bary.x + vi[1].texcoord0*bary.y + vi[2].texcoord0*bary.z;
                    displacement(v);
                    VertexOutput o = vert(v);
                    return o;
                }
            #endif
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 normalDirection = i.normalDir;
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
