﻿using UnityEngine;
using System.Collections;
using System;
using DG.Tweening;

public class CVRingPuzzle : PuzzleManager
{
    [Header ("Parameters")]
    public float rotationAngle = 30;                            // The angle which will rotate a ring for every single interaction

    [Header ("Puzzle Inputs")]
    public RingActivator[] panelRingActivator;                  // Controller panels which will operate the puzzle

    [Header ("Puzzle Components")]
    public Transform innerRing;                                 // internal ring
    public Transform middleRing;                                // middle ring
    public Transform outerRing;                                 // outer ring

    [Header("Puzzle Rewards")]
    public AudioSource successAudio;                            // AudioSource to play when winning
    public AudioSource ringMoving;                              // AudioSource to play when rotating a ring

    // Use this for initialization
    void Awake ()
    {

    }

    IEnumerator Start()
    {
        for (int i = 0; i < panelRingActivator.Length; i++)
        {
            panelRingActivator[i].OnInnerRotate += innerRotate;
            panelRingActivator[i].OnMiddleRotate += middleRotate;
            panelRingActivator[i].OnOuterRotate += outerRotate;
        }

        yield return new WaitForSeconds(1);
        yield return StartCoroutine(RandomBeginning());
    }

    void OnEnable()
    {

    }

    void OnDisable()
    {

    }
	
	// Update is called once per frame
	void Update ()
    {

	}

    public void laserInterrupted()
    {

    }

    public void laserFreed()
    {

    }

    public void innerRotate()
    {
        innerRing.DORotate(new Vector3(0, 0, rotationAngle), 2f, RotateMode.LocalAxisAdd);
        middleRing.DORotate(new Vector3(0, 0, -rotationAngle), 2f, RotateMode.LocalAxisAdd);
        for (int i = 0; i < panelRingActivator.Length; i++)
        {
            panelRingActivator[i].innerRing.DORotate(new Vector3(0, 0, rotationAngle), 2f, RotateMode.LocalAxisAdd);
            panelRingActivator[i].middleRing.DORotate(new Vector3(0, 0, -rotationAngle), 2f, RotateMode.LocalAxisAdd);
        }
        ringMoving.Play();
    }

    public void middleRotate()
    {
        innerRing.DORotate(new Vector3(0, 0, -rotationAngle), 2f, RotateMode.LocalAxisAdd);
        middleRing.DORotate(new Vector3(0, 0, rotationAngle), 2f, RotateMode.LocalAxisAdd);
        outerRing.DORotate(new Vector3(0, 0, -rotationAngle), 2f, RotateMode.LocalAxisAdd);
        for (int i = 0; i < panelRingActivator.Length; i++)
        {
            panelRingActivator[i].innerRing.DORotate(new Vector3(0, 0, -rotationAngle), 2f, RotateMode.LocalAxisAdd);
            panelRingActivator[i].middleRing.DORotate(new Vector3(0, 0, rotationAngle), 2f, RotateMode.LocalAxisAdd);
            panelRingActivator[i].outerRing.DORotate(new Vector3(0, 0, -rotationAngle), 2f, RotateMode.LocalAxisAdd);
        }
        ringMoving.Play();
    }

    public void outerRotate()
    {
        middleRing.DORotate(new Vector3(0, 0, -rotationAngle), 2f, RotateMode.LocalAxisAdd);
        outerRing.DORotate(new Vector3(0, 0, rotationAngle), 2f, RotateMode.LocalAxisAdd);
        for (int i = 0; i < panelRingActivator.Length; i++)
        {
            panelRingActivator[i].middleRing.DORotate(new Vector3(0, 0, -rotationAngle), 2f, RotateMode.LocalAxisAdd);
            panelRingActivator[i].outerRing.DORotate(new Vector3(0, 0, rotationAngle), 2f, RotateMode.LocalAxisAdd);
        }
        ringMoving.Play();
    }



    public override void ResetPuzzle()
    {
        throw new NotImplementedException();
    }

    public override void CompletePuzzle()
    {
        throw new NotImplementedException();
    }

    private IEnumerator TimerWaitToWin()
    {
        float timer = 0;

        while (timer < 1.5f)
        {
            timer += Time.deltaTime;
            yield return null;
        }

        if (true)
        {

        }
        else
        {
            timer = 0;
            yield break;
        }

    }

    IEnumerator RandomBeginning()
    {
        int choice = UnityEngine.Random.Range(0, 4);
        switch (choice)
        {
            case 0:
                middleRotate();
                yield return new WaitForSeconds(3);
                middleRotate();
                yield return new WaitForSeconds(3);
                outerRotate();
                yield return new WaitForSeconds(3);
                innerRotate();
                yield return new WaitForSeconds(3);
                innerRotate();
                break;
            case 1:
                outerRotate();
                yield return new WaitForSeconds(3);
                middleRotate();
                yield return new WaitForSeconds(3);
                outerRotate();
                yield return new WaitForSeconds(3);
                innerRotate();
                yield return new WaitForSeconds(3);
                outerRotate();
                break;
            case 2:
                outerRotate();
                yield return new WaitForSeconds(3);
                middleRotate();
                yield return new WaitForSeconds(3);
                innerRotate();
                yield return new WaitForSeconds(3);
                outerRotate();
                yield return new WaitForSeconds(3);
                middleRotate();
                break;
            case 3:
                middleRotate();
                yield return new WaitForSeconds(3);
                innerRotate();
                yield return new WaitForSeconds(3);
                outerRotate();
                yield return new WaitForSeconds(3);
                innerRotate();
                yield return new WaitForSeconds(3);
                innerRotate();
                break;
        }
    }
}
