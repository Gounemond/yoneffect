﻿using UnityEngine;
using System.Collections;

public class TimeMachineActivator : MonoBehaviour 
{
    // Singleton implementation
    protected static TimeMachineActivator _self;
    public static TimeMachineActivator Self
    {
        get
        {
            if (_self == null)
                _self = FindObjectOfType(typeof(TimeMachineActivator)) as TimeMachineActivator;
            return _self;
        }
    }

    // Reference to the attached animator
    private Animator animator;
    [Header ("animationStates")]
    public AnimationClip yellowRepair;
    public AnimationClip redRepair;
    public AnimationClip greenRepair;
    public AnimationClip blueRepair;

    [Header ("materials")]
    public Material timeMachineChiaro;
    public Material timeMachineLight;
    public Material timeMachineScuro;
    public Material timeMachineFracture;

    [Header ("Original shaders")]
    public Shader originalChiaro;
    public Shader originalLight;
    public Shader originalScuro;
    public Shader originalFracture;

    [Header ("Repairing shaders")]
    public Shader repairingChiaro;
    public Shader repairingLight;
    public Shader repairingScuro;
    public Shader repairingFracture;

    void Awake()
    {
        // Getting attached animator
        animator = GetComponent<Animator>();
    }

    void Start()
    {
        timeMachineChiaro.shader = originalChiaro;
        timeMachineLight.shader = originalLight;
        timeMachineScuro.shader = originalScuro;
        timeMachineFracture.shader = originalFracture;
    }

    // Set transition to Reparation state
    public void ReparationTransition()
    {
        // Check for animator
        if (animator != null)
        {
            // Set Reparation parameter to true
            animator.SetBool("Reparation", true);
            timeMachineFracture.shader = repairingFracture;
            StartCoroutine(SwitchShaders(5));
        }
        else
        {
            Debug.Log("No animator attached to the object");
        }
        
    }

    // Set transition to Yellow state
    public void YellowTransition()
    {
        // Check for animator
        if (animator != null)
        {
            // Set Yellow parameter to true
            animator.SetBool("Yellow", true);
            StartCoroutine(SwitchShaders(yellowRepair.length));
        }
        else
        {
            Debug.Log("No animator attached to the object");
        }
    }

    // Set transition to Red state
    public void RedTransition()
    {
        // Check for animator
        if (animator != null)
        {
            // Set Red parameter to true
            animator.SetBool("Red", true);
            StartCoroutine(SwitchShaders(redRepair.length));
        }
        else
        {
            Debug.Log("No animator attached to the object");
        }
    }

    // Set transition to Green state
    public void GreenTransition()
    {
        // Check for animator
        if (animator != null)
        {
            // Set Green parameter to true
            animator.SetBool("Green", true);
            StartCoroutine(SwitchShaders(greenRepair.length));
        }
        else
        {
            Debug.Log("No animator attached to the object");
        }
    }

    // Set transition to Blue state
    public void BlueTransition()
    {
        // Check for animator
        if (animator != null)
        {
            // Set Blue parameter to true
            animator.SetBool("Blue", true);
            StartCoroutine(SwitchShaders(blueRepair.length));
        }
        else
        {
            Debug.Log("No animator attached to the object");
        }
    }

    /// <summary>
    /// Called to switch the time machine shaders for a fixed amound of time
    /// </summary>
    /// <param name="duration">Ideally the duration of repairing animation</param>
    /// <returns></returns>
    public IEnumerator SwitchShaders(float duration)
    {
        float timer = 0;            // Initialize the timer

        // Switching the shaders on the time machine materials
        timeMachineChiaro.shader = repairingChiaro;
        timeMachineLight.shader = repairingLight;
        timeMachineScuro.shader = repairingScuro;
        //timeMachineFracture.shader = repairingFracture;

        while (timer <= duration)
        {
            timer += Time.deltaTime;
            yield return null;
        }

        timeMachineChiaro.shader = originalChiaro;
        timeMachineLight.shader = originalLight;
        timeMachineScuro.shader = originalScuro;
        //timeMachineFracture.shader = originalFracture;
        yield return null;
    }
}
