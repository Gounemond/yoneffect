﻿using UnityEngine;
using System.Collections;
using System;

public class ConnectTheDotsPuzzle : PuzzleManager
{
    [Header ("Paths")]
    public EndPathCollider[] paths;                 // Paths composing the puzzle

    [Header("Sphere pieces")]
    public GameObject objectGrabbed;

    [Header("Puzzle Reward")]
    public AudioSource myAudioSource;               // Audio of success when placing an object correctly
    public AudioClip   pathCompletedAudio;          // Audio of success upon completing one path
    public AudioClip puzzleCompletedAudio;          // Audio of success upon completing all the paths
    public GameObject  starGate;                    // The stargate that will be opened

    // Various counters
    // Counters which memorize the total number of sensors

    // Counters that memorize how many sensors are active
    private int pathsCompleted = 0;

    // Use this for initialization
    void Awake ()
    {

    }

    void OnEnable()
    {
        for (int i = 0; i < paths.Length; i++)
        {
            paths[i].pathCompleted += PathCompleted;
        }

    }

    void OnDisable()
    {
        for (int i = 0; i < paths.Length; i++)
        {
            paths[i].pathCompleted -= PathCompleted;
        }
    }
	
    void PathCompleted()
    {
        pathsCompleted++;
        //myAudioSource.PlayOneShot(pathCompletedAudio);
        CheckIfPuzzleIsCompleted();
    }

    public void CheckIfPuzzleIsCompleted()
    {
        if (pathsCompleted >= paths.Length)
        {
            starGate.SetActive(true);
            myAudioSource.PlayOneShot(puzzleCompletedAudio);
        }
    }

    public void switchObjectGrabbed(GameObject spherePiece)
    {
        if (spherePiece == null)
        {
            objectGrabbed = spherePiece;
            return;
        }

        if (objectGrabbed == null)
        {
            objectGrabbed = spherePiece;
            return;
        }

        if (spherePiece != objectGrabbed)
        {
            objectGrabbed.GetComponent<VRBallDotsGrab>().GrabSwitchState();
            objectGrabbed = spherePiece;
            return;
        }
    }

    public override void ResetPuzzle()
    {
        throw new NotImplementedException();
    }

    public override void CompletePuzzle()
    {
        throw new NotImplementedException();
    }
}
