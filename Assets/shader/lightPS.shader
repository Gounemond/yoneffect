// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:6976,x:32719,y:32712,varname:node_6976,prsc:2|emission-2184-OUT;n:type:ShaderForge.SFN_Color,id:7475,x:32281,y:32669,ptovrint:False,ptlb:node_7475,ptin:_node_7475,varname:node_7475,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Lerp,id:2184,x:32539,y:32800,varname:node_2184,prsc:2|A-7475-RGB,B-3465-OUT,T-7569-OUT;n:type:ShaderForge.SFN_Multiply,id:3465,x:32329,y:32827,varname:node_3465,prsc:2|A-790-RGB,B-545-OUT;n:type:ShaderForge.SFN_Multiply,id:7569,x:32538,y:32981,varname:node_7569,prsc:2|A-1501-OUT,B-1504-OUT;n:type:ShaderForge.SFN_Color,id:790,x:32141,y:32784,ptovrint:False,ptlb:node_790,ptin:_node_790,varname:node_790,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:0,c4:1;n:type:ShaderForge.SFN_Vector1,id:545,x:32188,y:32939,varname:node_545,prsc:2,v1:2;n:type:ShaderForge.SFN_OneMinus,id:1501,x:32320,y:32988,varname:node_1501,prsc:2|IN-8533-OUT;n:type:ShaderForge.SFN_Slider,id:1504,x:32276,y:33148,ptovrint:False,ptlb:SliderPS,ptin:_SliderPS,varname:node_1504,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Fresnel,id:8533,x:32114,y:33008,varname:node_8533,prsc:2|NRM-2481-OUT,EXP-6524-OUT;n:type:ShaderForge.SFN_NormalVector,id:2481,x:31936,y:32928,prsc:2,pt:False;n:type:ShaderForge.SFN_Vector1,id:6524,x:31952,y:33120,varname:node_6524,prsc:2,v1:2;proporder:7475-790-1504;pass:END;sub:END;*/

Shader "Custom/lightPS" {
    Properties {
        _node_7475 ("node_7475", Color) = (0.5,0.5,0.5,1)
        _node_790 ("node_790", Color) = (1,1,0,1)
        _SliderPS ("SliderPS", Range(0, 1)) = 0
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _node_7475;
            uniform float4 _node_790;
            uniform float _SliderPS;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 posWorld : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
                UNITY_FOG_COORDS(2)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float3 emissive = lerp(_node_7475.rgb,(_node_790.rgb*2.0),((1.0 - pow(1.0-max(0,dot(i.normalDir, viewDirection)),2.0))*_SliderPS));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
