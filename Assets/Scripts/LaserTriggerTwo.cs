using UnityEngine;
using System.Collections;

public class LaserTriggerTwo : MonoBehaviour {
	
	public GameObject generator;
	public GameObject laserTwo;
	//publich GameObject mechanismTwo
	
	private GenTwoLaserBehaviour genEngine;
	private LaserTwoPosition scriptLaserTwo;
	//private MecOneScript mecTwoEngine
	
	// Use this for initialization
	void Start () {
		
		genEngine = generator.GetComponent<GenTwoLaserBehaviour>();
		scriptLaserTwo = laserTwo.GetComponent<LaserTwoPosition>();
		//mecTwoEngine = mechanismTwo.GetComponent<MecTwoScript>();
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnTriggerEnter(Collider collision) {   
		
		genEngine.deactiveGen();
		//mecTwoEngine.activate()
		
		if(collision.CompareTag("Player")) {
			
		    //The player collided	
		    scriptLaserTwo.modifyPos(collision.transform.position);
			
		}
		else {
			
			//An object collided
			scriptLaserTwo.modifyPos(collision.attachedRigidbody.position);
			
		}
		
	}
	
	void OnTriggerExit(Collider collision) {
		
		genEngine.activeGen();
		//mecTwoEngine.deactivate()
		scriptLaserTwo.resetPos();
		
	}
	
}
