﻿using UnityEngine;
using System;
using VRStandardAssets.Utils;
using System.Collections;

[RequireComponent (typeof(BoxCollider))]
public class HanoiTriggerEnd : MonoBehaviour
{

    public event Action onCollisionEnter;              // Called when a Hanoi Object enters the collider
    public event Action onCollisionExit;               // Called when a Hanoi Object enters the collider

    private int objectsInCollider = 0;

    // Use this for initialization
    void Awake ()
    {

    }

    // Update is called once per frame
    void Update () {
	
	}

    void OnTriggerEnter(Collider other)
    { 
        if (other.tag =="Hanoi")
        {
            onCollisionEnter();
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Hanoi")
        {
            onCollisionExit();
        }
    }
}
