// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:2,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:2,rntp:3,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:957,x:34044,y:32948,varname:node_957,prsc:2|emission-3749-RGB,clip-4052-OUT;n:type:ShaderForge.SFN_Tex2d,id:2216,x:33446,y:33090,ptovrint:False,ptlb:node_2216,ptin:_node_2216,varname:node_2216,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:122722ed3a82a264c86c60ba82600fbd,ntxv:0,isnm:False|UVIN-4038-OUT;n:type:ShaderForge.SFN_Vector4Property,id:2113,x:31266,y:33109,ptovrint:False,ptlb:vector 5,ptin:_vector5,varname:_vector5,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:3,v2:3,v3:4,v4:0;n:type:ShaderForge.SFN_Relay,id:1705,x:31694,y:33057,varname:node_1705,prsc:2|IN-2113-X;n:type:ShaderForge.SFN_Relay,id:3960,x:31563,y:33297,varname:node_3960,prsc:2|IN-2113-Y;n:type:ShaderForge.SFN_Relay,id:9693,x:31317,y:33434,varname:node_9693,prsc:2|IN-2113-Z;n:type:ShaderForge.SFN_Multiply,id:6734,x:31504,y:33452,varname:node_6734,prsc:2|A-9693-OUT,B-8957-OUT;n:type:ShaderForge.SFN_Time,id:1390,x:31091,y:33503,varname:node_1390,prsc:2;n:type:ShaderForge.SFN_Frac,id:6456,x:31737,y:33562,varname:node_6456,prsc:2|IN-6734-OUT;n:type:ShaderForge.SFN_Multiply,id:3080,x:31699,y:33368,varname:node_3080,prsc:2|A-1705-OUT,B-3960-OUT;n:type:ShaderForge.SFN_Multiply,id:5552,x:31919,y:33419,varname:node_5552,prsc:2|A-3080-OUT,B-6456-OUT;n:type:ShaderForge.SFN_Round,id:7619,x:32076,y:33542,varname:node_7619,prsc:2|IN-5552-OUT;n:type:ShaderForge.SFN_Divide,id:9525,x:32190,y:33394,varname:node_9525,prsc:2|A-7619-OUT,B-1705-OUT;n:type:ShaderForge.SFN_Divide,id:5338,x:32345,y:32979,varname:node_5338,prsc:2|A-7579-OUT,B-1705-OUT;n:type:ShaderForge.SFN_Divide,id:1190,x:32390,y:33233,varname:node_1190,prsc:2|A-8522-OUT,B-3960-OUT;n:type:ShaderForge.SFN_Floor,id:8522,x:32378,y:33394,varname:node_8522,prsc:2|IN-9525-OUT;n:type:ShaderForge.SFN_Fmod,id:7579,x:32373,y:33098,varname:node_7579,prsc:2|A-7619-OUT,B-1705-OUT;n:type:ShaderForge.SFN_OneMinus,id:9155,x:32619,y:33199,varname:node_9155,prsc:2|IN-1190-OUT;n:type:ShaderForge.SFN_Append,id:9722,x:32798,y:33088,varname:node_9722,prsc:2|A-5338-OUT,B-9155-OUT;n:type:ShaderForge.SFN_Append,id:4688,x:32723,y:32905,varname:node_4688,prsc:2|A-1705-OUT,B-3960-OUT;n:type:ShaderForge.SFN_TexCoord,id:8483,x:32932,y:32748,varname:node_8483,prsc:2,uv:0;n:type:ShaderForge.SFN_Divide,id:8151,x:33188,y:33050,varname:node_8151,prsc:2|A-8483-UVOUT,B-4688-OUT;n:type:ShaderForge.SFN_Add,id:4038,x:33082,y:33187,varname:node_4038,prsc:2|A-8151-OUT,B-9722-OUT;n:type:ShaderForge.SFN_Multiply,id:8957,x:31371,y:33596,varname:node_8957,prsc:2|A-1390-T,B-7865-OUT;n:type:ShaderForge.SFN_Slider,id:7865,x:31023,y:33734,ptovrint:False,ptlb:time_copy,ptin:_time_copy,varname:_time_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Tex2d,id:2190,x:33623,y:33256,ptovrint:False,ptlb:node_2190,ptin:_node_2190,varname:node_2190,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:122722ed3a82a264c86c60ba82600fbd,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Multiply,id:4052,x:33840,y:33206,varname:node_4052,prsc:2|A-2216-R,B-2190-A;n:type:ShaderForge.SFN_Color,id:3749,x:33453,y:32834,ptovrint:False,ptlb:node_3749,ptin:_node_3749,varname:node_3749,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0,c3:0,c4:1;proporder:2216-2113-7865-2190-3749;pass:END;sub:END;*/

Shader "Custom/warp" {
    Properties {
        _node_2216 ("node_2216", 2D) = "white" {}
        _vector5 ("vector 5", Vector) = (3,3,4,0)
        _time_copy ("time_copy", Range(0, 1)) = 1
        _node_2190 ("node_2190", 2D) = "white" {}
        _node_3749 ("node_3749", Color) = (0,0,0,1)
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "Queue"="AlphaTest"
            "RenderType"="TransparentCutout"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Cull Off
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform sampler2D _node_2216; uniform float4 _node_2216_ST;
            uniform float4 _vector5;
            uniform float _time_copy;
            uniform sampler2D _node_2190; uniform float4 _node_2190_ST;
            uniform float4 _node_3749;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                UNITY_FOG_COORDS(1)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                float node_1705 = _vector5.r;
                float node_3960 = _vector5.g;
                float4 node_1390 = _Time + _TimeEditor;
                float node_7619 = round(((node_1705*node_3960)*frac((_vector5.b*(node_1390.g*_time_copy)))));
                float2 node_4038 = ((i.uv0/float2(node_1705,node_3960))+float2((fmod(node_7619,node_1705)/node_1705),(1.0 - (floor((node_7619/node_1705))/node_3960))));
                float4 _node_2216_var = tex2D(_node_2216,TRANSFORM_TEX(node_4038, _node_2216));
                float4 _node_2190_var = tex2D(_node_2190,TRANSFORM_TEX(i.uv0, _node_2190));
                clip((_node_2216_var.r*_node_2190_var.a) - 0.5);
////// Lighting:
////// Emissive:
                float3 emissive = _node_3749.rgb;
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform sampler2D _node_2216; uniform float4 _node_2216_ST;
            uniform float4 _vector5;
            uniform float _time_copy;
            uniform sampler2D _node_2190; uniform float4 _node_2190_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                float node_1705 = _vector5.r;
                float node_3960 = _vector5.g;
                float4 node_1390 = _Time + _TimeEditor;
                float node_7619 = round(((node_1705*node_3960)*frac((_vector5.b*(node_1390.g*_time_copy)))));
                float2 node_4038 = ((i.uv0/float2(node_1705,node_3960))+float2((fmod(node_7619,node_1705)/node_1705),(1.0 - (floor((node_7619/node_1705))/node_3960))));
                float4 _node_2216_var = tex2D(_node_2216,TRANSFORM_TEX(node_4038, _node_2216));
                float4 _node_2190_var = tex2D(_node_2190,TRANSFORM_TEX(i.uv0, _node_2190));
                clip((_node_2216_var.r*_node_2190_var.a) - 0.5);
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
