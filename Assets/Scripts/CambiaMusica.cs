using UnityEngine;
using System.Collections;

public class CambiaMusica : MonoBehaviour {
	
	public AudioSource gestore;
	public AudioClip musica;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnTriggerEnter(Collider ObjectCollision)
	{
		if (ObjectCollision.tag=="Player")
		{
				gestore.clip = musica;
				gestore.Play ();
		}
	}
}
