using UnityEngine;
using System.Collections;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System;
using System.Linq;

public class AnimationHandler : YonBehaviour {
	
	public float activeSpeed = 1f;
	public float restSpeed = -1;

    public Animator animatorController;
	
	// Use this for initialization
	void Start ()
    {

	}
	
	// Update is called once per frame
	void Update () {

	}
	
	override public void Activate()
	{
        animatorController.SetFloat("Speed", activeSpeed);
    }
	
	override public void deActivate()
	{
        animatorController.SetFloat("Speed", restSpeed);

    }
}
