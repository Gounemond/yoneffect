﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class GameDifficulty : MonoBehaviour
{
    [SerializeField]public static int maxDifficulty = 0;
    [SerializeField]public static int selectedDifficulty = 0;


    public static void SaveDifficulty()
    {
        BinaryFormatter bf = new BinaryFormatter();
        //Application.persistentDataPath is a string, so if you wanted you can put that into debug.log if you want to know where save games are located
        FileStream file = File.Create(Application.persistentDataPath + "/savedDifficulty.gd"); //you can call it anything you want
        bf.Serialize(file, GameDifficulty.maxDifficulty);
        file.Close();
    }

    public static void LoadDifficulty()
    {
        if (File.Exists(Application.persistentDataPath + "/savedDifficulty.gd"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/savedDifficulty.gd", FileMode.Open);
            GameDifficulty.maxDifficulty = (int)bf.Deserialize(file);
            file.Close();
        }
    }

    public static void UnlockNextDifficulty()
    {
        if (selectedDifficulty == maxDifficulty)
        {
            if (maxDifficulty < 2)
            {
                maxDifficulty++;
            }
        }
    }
}
