using UnityEngine;
using System.Collections;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System;
using System.Linq;

public class PositionDoorTrigger : MonoBehaviour {

	public YonBehaviour[] obj;
	private int triggerCounter = 0;
    public int gearsToUnlock = 0;
    public AnimationClip doorAnimation;

    private OcclusionPortal _occlusionPortal;
	// Use this for initialization
	void Awake ()
    {
        _occlusionPortal = this.GetComponent<OcclusionPortal>();
        _occlusionPortal.open = false;
	}
	
	// Update is called once per frame
	void Update () {
	}
	
	void OnTriggerEnter(Collider ObjectCollision)
	{
		if (YonEffectJRController.Self.numberGEARSCollected >= gearsToUnlock)
        {
            if (triggerCounter == 0)
            {
                _occlusionPortal.open = true;
                //Debug.Log("Occlusion portal is open: " + _occlusionPortal.open);
                for (int i = 0; i < obj.Length; i++)
                {
                    obj[i].Activate();
                }
            }
            triggerCounter++;
        }
    }
	
	void OnTriggerExit(Collider ObjectCollision)
	{
        if (YonEffectJRController.Self.numberGEARSCollected >= gearsToUnlock)
        {
            if (triggerCounter == 1)
		    {
                StartCoroutine(CloseOcclusionPortal());
			    for (int i = 0; i < obj.Length; i++)
			    {
				    obj[i].deActivate();
			    }
			    triggerCounter--;
		    }
		    else
		    {
			    triggerCounter--;
		    }
        }
	}

    IEnumerator CloseOcclusionPortal()
    {
        yield return new WaitForSeconds(doorAnimation.length);
        _occlusionPortal.open = true;
    }
	
}
