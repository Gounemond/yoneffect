// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:2,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:9664,x:34918,y:32172,varname:node_9664,prsc:2|emission-3475-OUT,voffset-4031-OUT;n:type:ShaderForge.SFN_TexCoord,id:2088,x:31062,y:32126,varname:node_2088,prsc:2,uv:0;n:type:ShaderForge.SFN_Color,id:2346,x:33418,y:32143,ptovrint:False,ptlb:node_2346,ptin:_node_2346,varname:node_2346,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Multiply,id:4031,x:33949,y:32712,varname:node_4031,prsc:2|A-9412-OUT,B-9212-OUT,C-5097-OUT;n:type:ShaderForge.SFN_NormalVector,id:9212,x:33677,y:32836,prsc:2,pt:True;n:type:ShaderForge.SFN_Vector1,id:9412,x:33588,y:32686,varname:node_9412,prsc:2,v1:0.1;n:type:ShaderForge.SFN_Sin,id:5097,x:33517,y:32491,varname:node_5097,prsc:2|IN-2836-OUT;n:type:ShaderForge.SFN_Add,id:6314,x:33861,y:32332,varname:node_6314,prsc:2|A-2346-RGB,B-5097-OUT;n:type:ShaderForge.SFN_Distance,id:5556,x:31396,y:32299,varname:node_5556,prsc:2|A-2088-UVOUT,B-1008-OUT;n:type:ShaderForge.SFN_Vector2,id:1008,x:30932,y:32425,varname:node_1008,prsc:2,v1:0.5,v2:0.5;n:type:ShaderForge.SFN_Multiply,id:3960,x:31589,y:32356,varname:node_3960,prsc:2|A-5556-OUT,B-6411-OUT;n:type:ShaderForge.SFN_Vector1,id:6411,x:30976,y:32550,varname:node_6411,prsc:2,v1:2;n:type:ShaderForge.SFN_OneMinus,id:5418,x:31740,y:32407,varname:node_5418,prsc:2|IN-3960-OUT;n:type:ShaderForge.SFN_Multiply,id:2836,x:33030,y:32344,varname:node_2836,prsc:2|A-6412-UVOUT,B-569-OUT;n:type:ShaderForge.SFN_Tau,id:569,x:32388,y:32638,varname:node_569,prsc:2;n:type:ShaderForge.SFN_Time,id:8563,x:31128,y:32651,varname:node_8563,prsc:2;n:type:ShaderForge.SFN_Multiply,id:2799,x:31402,y:32622,varname:node_2799,prsc:2|A-8563-T,B-736-OUT;n:type:ShaderForge.SFN_Vector1,id:736,x:31204,y:32841,varname:node_736,prsc:2,v1:0;n:type:ShaderForge.SFN_Panner,id:2961,x:32406,y:32107,varname:node_2961,prsc:2,spu:1,spv:1|UVIN-2088-UVOUT,DIST-7172-OUT;n:type:ShaderForge.SFN_Multiply,id:7172,x:32043,y:32478,varname:node_7172,prsc:2|A-5418-OUT,B-2964-OUT;n:type:ShaderForge.SFN_Vector1,id:2964,x:31876,y:32612,varname:node_2964,prsc:2,v1:2;n:type:ShaderForge.SFN_Rotator,id:6412,x:32733,y:32192,varname:node_6412,prsc:2|UVIN-2961-UVOUT;n:type:ShaderForge.SFN_Multiply,id:348,x:31690,y:32615,varname:node_348,prsc:2|A-3960-OUT,B-2799-OUT;n:type:ShaderForge.SFN_Tex2d,id:1888,x:33912,y:31953,ptovrint:False,ptlb:node_1888,ptin:_node_1888,varname:node_1888,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:c7a5e1937cfd86d4bab16211d6faa818,ntxv:0,isnm:False|UVIN-5440-UVOUT;n:type:ShaderForge.SFN_Add,id:5321,x:34224,y:32252,varname:node_5321,prsc:2|A-1888-RGB,B-6314-OUT;n:type:ShaderForge.SFN_Panner,id:783,x:33389,y:31886,varname:node_783,prsc:2,spu:2,spv:2|UVIN-2484-UVOUT,DIST-7346-OUT;n:type:ShaderForge.SFN_TexCoord,id:2484,x:32797,y:31779,varname:node_2484,prsc:2,uv:0;n:type:ShaderForge.SFN_Rotator,id:5440,x:33683,y:31893,varname:node_5440,prsc:2|UVIN-783-UVOUT;n:type:ShaderForge.SFN_Distance,id:7346,x:33091,y:31905,varname:node_7346,prsc:2|A-2484-UVOUT,B-4206-OUT;n:type:ShaderForge.SFN_Vector2,id:4206,x:32797,y:31977,varname:node_4206,prsc:2,v1:0.5,v2:0.5;n:type:ShaderForge.SFN_Multiply,id:3475,x:34489,y:32011,varname:node_3475,prsc:2|A-5055-RGB,B-5321-OUT;n:type:ShaderForge.SFN_Color,id:5055,x:34290,y:31799,ptovrint:False,ptlb:node_5055,ptin:_node_5055,varname:node_5055,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0,c3:0,c4:1;proporder:2346-1888-5055;pass:END;sub:END;*/

Shader "Custom/stargate" {
    Properties {
        _node_2346 ("node_2346", Color) = (0,1,1,1)
        _node_1888 ("node_1888", 2D) = "white" {}
        _node_5055 ("node_5055", Color) = (1,0,0,1)
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Cull Off
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform float4 _node_2346;
            uniform sampler2D _node_1888; uniform float4 _node_1888_ST;
            uniform float4 _node_5055;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                UNITY_FOG_COORDS(3)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                float4 node_609 = _Time + _TimeEditor;
                float node_6412_ang = node_609.g;
                float node_6412_spd = 1.0;
                float node_6412_cos = cos(node_6412_spd*node_6412_ang);
                float node_6412_sin = sin(node_6412_spd*node_6412_ang);
                float2 node_6412_piv = float2(0.5,0.5);
                float node_3960 = (distance(o.uv0,float2(0.5,0.5))*2.0);
                float2 node_6412 = (mul((o.uv0+((1.0 - node_3960)*2.0)*float2(1,1))-node_6412_piv,float2x2( node_6412_cos, -node_6412_sin, node_6412_sin, node_6412_cos))+node_6412_piv);
                float2 node_5097 = sin((node_6412*6.28318530718));
                v.vertex.xyz += (0.1*v.normal*float3(node_5097,0.0));
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                i.normalDir = normalize(i.normalDir);
                i.normalDir *= faceSign;
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float4 node_609 = _Time + _TimeEditor;
                float node_5440_ang = node_609.g;
                float node_5440_spd = 1.0;
                float node_5440_cos = cos(node_5440_spd*node_5440_ang);
                float node_5440_sin = sin(node_5440_spd*node_5440_ang);
                float2 node_5440_piv = float2(0.5,0.5);
                float2 node_5440 = (mul((i.uv0+distance(i.uv0,float2(0.5,0.5))*float2(2,2))-node_5440_piv,float2x2( node_5440_cos, -node_5440_sin, node_5440_sin, node_5440_cos))+node_5440_piv);
                float4 _node_1888_var = tex2D(_node_1888,TRANSFORM_TEX(node_5440, _node_1888));
                float node_6412_ang = node_609.g;
                float node_6412_spd = 1.0;
                float node_6412_cos = cos(node_6412_spd*node_6412_ang);
                float node_6412_sin = sin(node_6412_spd*node_6412_ang);
                float2 node_6412_piv = float2(0.5,0.5);
                float node_3960 = (distance(i.uv0,float2(0.5,0.5))*2.0);
                float2 node_6412 = (mul((i.uv0+((1.0 - node_3960)*2.0)*float2(1,1))-node_6412_piv,float2x2( node_6412_cos, -node_6412_sin, node_6412_sin, node_6412_cos))+node_6412_piv);
                float2 node_5097 = sin((node_6412*6.28318530718));
                float3 emissive = (_node_5055.rgb*(_node_1888_var.rgb+(_node_2346.rgb+float3(node_5097,0.0))));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
                float4 posWorld : TEXCOORD2;
                float3 normalDir : TEXCOORD3;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                float4 node_1999 = _Time + _TimeEditor;
                float node_6412_ang = node_1999.g;
                float node_6412_spd = 1.0;
                float node_6412_cos = cos(node_6412_spd*node_6412_ang);
                float node_6412_sin = sin(node_6412_spd*node_6412_ang);
                float2 node_6412_piv = float2(0.5,0.5);
                float node_3960 = (distance(o.uv0,float2(0.5,0.5))*2.0);
                float2 node_6412 = (mul((o.uv0+((1.0 - node_3960)*2.0)*float2(1,1))-node_6412_piv,float2x2( node_6412_cos, -node_6412_sin, node_6412_sin, node_6412_cos))+node_6412_piv);
                float2 node_5097 = sin((node_6412*6.28318530718));
                v.vertex.xyz += (0.1*v.normal*float3(node_5097,0.0));
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                i.normalDir = normalize(i.normalDir);
                i.normalDir *= faceSign;
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
