using UnityEngine;
using System.Collections;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System;
using System.Linq;

public class PlatePuzzleTrigger : MonoBehaviour {

	public YonBehaviour[] obj;
    public Animator YonGearGate;
	private int triggerCounter = 0;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	}
	
	void OnTriggerEnter(Collider ObjectCollision)
	{
		if(triggerCounter == 0)
		{
			for (int i = 0; i < obj.Length; i++)
			{
				obj[i].Activate();
			}
            YonGearGate.SetBool("HasWon", true);
        }

			triggerCounter++;

	}
	
	void OnTriggerExit(Collider ObjectCollision)
	{
		if(triggerCounter == 1)
		{
			for (int i = 0; i < obj.Length; i++)
			{
				obj[i].deActivate();
			}
            YonGearGate.SetBool("HasWon", false);
            triggerCounter--;
		}
		else
		{
			triggerCounter--;
		}
	}
	
}
