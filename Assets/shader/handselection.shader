// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:9085,x:32857,y:32705,varname:node_9085,prsc:2|emission-7-OUT;n:type:ShaderForge.SFN_Color,id:6223,x:32185,y:32593,ptovrint:False,ptlb:fresn,ptin:_fresn,varname:node_6223,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.6264706,c2:1,c3:0.5620689,c4:1;n:type:ShaderForge.SFN_Lerp,id:6960,x:32448,y:32724,varname:node_6960,prsc:2|A-6223-RGB,B-5098-OUT,T-5751-OUT;n:type:ShaderForge.SFN_Vector1,id:5098,x:32169,y:32785,varname:node_5098,prsc:2,v1:1;n:type:ShaderForge.SFN_OneMinus,id:5751,x:32296,y:32950,varname:node_5751,prsc:2|IN-9338-OUT;n:type:ShaderForge.SFN_Fresnel,id:9338,x:32073,y:32977,varname:node_9338,prsc:2|NRM-9874-OUT,EXP-7413-OUT;n:type:ShaderForge.SFN_NormalVector,id:9874,x:31864,y:32894,prsc:2,pt:False;n:type:ShaderForge.SFN_ValueProperty,id:7413,x:31922,y:33098,ptovrint:False,ptlb:fresnel,ptin:_fresnel,varname:node_7413,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Color,id:9247,x:32450,y:32472,ptovrint:False,ptlb:main,ptin:_main,varname:node_9247,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:1,c3:0,c4:1;n:type:ShaderForge.SFN_Multiply,id:7,x:32616,y:32680,varname:node_7,prsc:2|A-9247-RGB,B-6960-OUT;proporder:6223-7413-9247;pass:END;sub:END;*/

Shader "Custom/handselection" {
    Properties {
        _fresn ("fresn", Color) = (0.6264706,1,0.5620689,1)
        _fresnel ("fresnel", Float ) = 1
        _main ("main", Color) = (0,1,0,1)
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _fresn;
            uniform float _fresnel;
            uniform float4 _main;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 posWorld : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
                UNITY_FOG_COORDS(2)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float node_5098 = 1.0;
                float3 emissive = (_main.rgb*lerp(_fresn.rgb,float3(node_5098,node_5098,node_5098),(1.0 - pow(1.0-max(0,dot(i.normalDir, viewDirection)),_fresnel))));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
