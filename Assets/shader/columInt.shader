// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:1,bsrc:0,bdst:1,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:497,x:36282,y:32596,varname:node_497,prsc:2|emission-5925-OUT;n:type:ShaderForge.SFN_SceneDepth,id:5793,x:34167,y:33014,varname:node_5793,prsc:2;n:type:ShaderForge.SFN_Depth,id:7661,x:34140,y:33154,varname:node_7661,prsc:2;n:type:ShaderForge.SFN_Subtract,id:1724,x:34400,y:33089,varname:node_1724,prsc:2|A-5793-OUT,B-7661-OUT;n:type:ShaderForge.SFN_Multiply,id:7155,x:34627,y:33069,varname:node_7155,prsc:2|A-2843-OUT,B-1724-OUT;n:type:ShaderForge.SFN_ValueProperty,id:2843,x:34400,y:33035,ptovrint:False,ptlb:density_copy_copy,ptin:_density_copy_copy,varname:_density_copy_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Vector4Property,id:8918,x:34346,y:33212,ptovrint:False,ptlb:node_940_copy_copy,ptin:_node_940_copy_copy,varname:_node_940_copy_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0,v2:0,v3:0,v4:-0.1;n:type:ShaderForge.SFN_Add,id:8950,x:34590,y:33236,varname:node_8950,prsc:2|A-8918-XYZ,B-8918-W;n:type:ShaderForge.SFN_RemapRangeAdvanced,id:1567,x:35002,y:33172,varname:node_1567,prsc:2|IN-7155-OUT,IMIN-8950-OUT,IMAX-6483-OUT,OMIN-9291-OUT,OMAX-9170-OUT;n:type:ShaderForge.SFN_Add,id:6483,x:34629,y:33393,varname:node_6483,prsc:2|A-4059-XYZ,B-4059-W;n:type:ShaderForge.SFN_Vector4Property,id:4059,x:34340,y:33485,ptovrint:False,ptlb:node_2263_copy_copy,ptin:_node_2263_copy_copy,varname:_node_2263_copy_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.6172414,v2:1,v3:0.9324544,v4:0.8;n:type:ShaderForge.SFN_Vector1,id:9291,x:34784,y:33308,varname:node_9291,prsc:2,v1:1;n:type:ShaderForge.SFN_Vector1,id:9170,x:34804,y:33437,varname:node_9170,prsc:2,v1:0;n:type:ShaderForge.SFN_Clamp01,id:8116,x:35146,y:33129,varname:node_8116,prsc:2|IN-1567-OUT;n:type:ShaderForge.SFN_Power,id:4446,x:35412,y:33089,varname:node_4446,prsc:2|VAL-8116-OUT,EXP-5109-OUT;n:type:ShaderForge.SFN_ValueProperty,id:5109,x:35146,y:33330,ptovrint:False,ptlb:fade power_copy_copy,ptin:_fadepower_copy_copy,varname:_fadepower_copy_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:5;n:type:ShaderForge.SFN_SceneColor,id:6241,x:34866,y:33012,varname:node_6241,prsc:2;n:type:ShaderForge.SFN_Multiply,id:529,x:35445,y:32964,varname:node_529,prsc:2|A-6241-RGB,B-4446-OUT;n:type:ShaderForge.SFN_Color,id:5097,x:34917,y:32421,ptovrint:False,ptlb:node_5097,ptin:_node_5097,varname:node_5097,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0,c3:0,c4:1;n:type:ShaderForge.SFN_Add,id:5925,x:35823,y:32666,varname:node_5925,prsc:2|A-4319-OUT,B-529-OUT;n:type:ShaderForge.SFN_ScreenPos,id:5883,x:34888,y:32601,varname:node_5883,prsc:2,sctp:0;n:type:ShaderForge.SFN_Multiply,id:4762,x:35280,y:32549,varname:node_4762,prsc:2|A-5097-RGB,B-5883-UVOUT;n:type:ShaderForge.SFN_ScreenPos,id:4195,x:34912,y:32746,varname:node_4195,prsc:2,sctp:0;n:type:ShaderForge.SFN_OneMinus,id:7148,x:35082,y:32746,varname:node_7148,prsc:2|IN-4195-UVOUT;n:type:ShaderForge.SFN_Multiply,id:7589,x:35259,y:32685,varname:node_7589,prsc:2|A-5097-RGB,B-7148-OUT;n:type:ShaderForge.SFN_Multiply,id:3883,x:35536,y:32622,varname:node_3883,prsc:2|A-4762-OUT,B-7589-OUT;n:type:ShaderForge.SFN_Blend,id:4319,x:35665,y:32427,varname:node_4319,prsc:2,blmd:17,clmp:True|SRC-4762-OUT,DST-3883-OUT;proporder:2843-8918-4059-5109-5097;pass:END;sub:END;*/

Shader "Custom/columInt" {
    Properties {
        _density_copy_copy ("density_copy_copy", Float ) = 1
        _node_940_copy_copy ("node_940_copy_copy", Vector) = (0,0,0,-0.1)
        _node_2263_copy_copy ("node_2263_copy_copy", Vector) = (0.6172414,1,0.9324544,0.8)
        _fadepower_copy_copy ("fade power_copy_copy", Float ) = 5
        _node_5097 ("node_5097", Color) = (1,0,0,1)
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        LOD 200
        GrabPass{ }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Cull Front
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform sampler2D _GrabTexture;
            uniform sampler2D _CameraDepthTexture;
            uniform float _density_copy_copy;
            uniform float4 _node_940_copy_copy;
            uniform float4 _node_2263_copy_copy;
            uniform float _fadepower_copy_copy;
            uniform float4 _node_5097;
            struct VertexInput {
                float4 vertex : POSITION;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 posWorld : TEXCOORD0;
                float4 screenPos : TEXCOORD1;
                float4 projPos : TEXCOORD2;
                UNITY_FOG_COORDS(3)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                o.projPos = ComputeScreenPos (o.pos);
                COMPUTE_EYEDEPTH(o.projPos.z);
                o.screenPos = o.pos;
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                #if UNITY_UV_STARTS_AT_TOP
                    float grabSign = -_ProjectionParams.x;
                #else
                    float grabSign = _ProjectionParams.x;
                #endif
                i.screenPos = float4( i.screenPos.xy / i.screenPos.w, 0, 0 );
                i.screenPos.y *= _ProjectionParams.x;
                float sceneZ = max(0,LinearEyeDepth (UNITY_SAMPLE_DEPTH(tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(i.projPos)))) - _ProjectionParams.g);
                float partZ = max(0,i.projPos.z - _ProjectionParams.g);
                float2 sceneUVs = float2(1,grabSign)*i.screenPos.xy*0.5+0.5;
                float4 sceneColor = tex2D(_GrabTexture, sceneUVs);
////// Lighting:
////// Emissive:
                float3 node_4762 = (_node_5097.rgb*float3(i.screenPos.rg,0.0));
                float3 node_7589 = (_node_5097.rgb*float3((1.0 - i.screenPos.rg),0.0));
                float3 node_3883 = (node_4762*node_7589);
                float3 node_8950 = (_node_940_copy_copy.rgb+_node_940_copy_copy.a);
                float node_9291 = 1.0;
                float3 node_5925 = (saturate(abs(node_4762-node_3883))+(sceneColor.rgb*pow(saturate((node_9291 + ( ((_density_copy_copy*(sceneZ-partZ)) - node_8950) * (0.0 - node_9291) ) / ((_node_2263_copy_copy.rgb+_node_2263_copy_copy.a) - node_8950))),_fadepower_copy_copy)));
                float3 emissive = node_5925;
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
