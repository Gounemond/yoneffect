// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:7540,x:33521,y:32666,varname:node_7540,prsc:2|emission-7244-OUT,alpha-1135-OUT;n:type:ShaderForge.SFN_Color,id:2529,x:32551,y:32410,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_2529,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Multiply,id:7244,x:32800,y:32468,varname:node_7244,prsc:2|A-2529-RGB,B-5578-OUT;n:type:ShaderForge.SFN_Vector1,id:5578,x:32551,y:32570,varname:node_5578,prsc:2,v1:2;n:type:ShaderForge.SFN_TexCoord,id:373,x:31990,y:32966,varname:node_373,prsc:2,uv:0;n:type:ShaderForge.SFN_Multiply,id:3997,x:32666,y:32726,varname:node_3997,prsc:2|A-3449-OUT,B-2022-OUT;n:type:ShaderForge.SFN_Vector1,id:2022,x:32435,y:32817,varname:node_2022,prsc:2,v1:1;n:type:ShaderForge.SFN_OneMinus,id:3449,x:32435,y:32687,varname:node_3449,prsc:2|IN-373-V;n:type:ShaderForge.SFN_Time,id:426,x:32647,y:33069,varname:node_426,prsc:2;n:type:ShaderForge.SFN_Add,id:1135,x:33298,y:32953,varname:node_1135,prsc:2|A-3997-OUT,B-3917-OUT;n:type:ShaderForge.SFN_Sin,id:5151,x:32860,y:33109,varname:node_5151,prsc:2|IN-426-TDB;n:type:ShaderForge.SFN_Multiply,id:3917,x:33069,y:33128,varname:node_3917,prsc:2|A-5151-OUT,B-762-OUT;n:type:ShaderForge.SFN_Vector1,id:762,x:32856,y:33260,varname:node_762,prsc:2,v1:0.1;proporder:2529;pass:END;sub:END;*/

Shader "Custom/Crystal" {
    Properties {
        _Color ("Color", Color) = (0,1,1,1)
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform float4 _Color;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                UNITY_FOG_COORDS(1)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
////// Lighting:
////// Emissive:
                float3 emissive = (_Color.rgb*2.0);
                float3 finalColor = emissive;
                float4 node_426 = _Time + _TimeEditor;
                fixed4 finalRGBA = fixed4(finalColor,(((1.0 - i.uv0.g)*1.0)+(sin(node_426.b)*0.1)));
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
