using UnityEngine;
using System.Collections;

public class LaserPosition : MonoBehaviour {
	
	//Variables which represent the inital and the final position of the laser
	public GameObject laserGen;
	public GameObject generator;
	public int choice=0;
	
	//This represents the lineRenderer itself
	private LineRenderer laser;
	
	//Variables which save the position of the lineRenderer (the laser)
	private Vector3 initialPos;
	private Vector3 finalPos;
	private bool initialized = false;
	
	// Use this for initialization
	void Start () {
	
        //Modifico la Y delle posizioni così il laser è posizionato meglio (al centro, dato che la scala dei cubi è 1)
		initialPos = new Vector3(laserGen.transform.position.x, laserGen.transform.position.y , laserGen.transform.position.z);
		finalPos = new Vector3(generator.transform.position.x, generator.transform.position.y , generator.transform.position.z);
		
		//This sets the position and the color of the lineRenderer
		laser = this.GetComponent<LineRenderer>();
		laser.SetPosition(0, initialPos);
		laser.SetPosition(1, finalPos);
		initialized = true;
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	//Function which modify the position of the laser when there is a collision
	public void modifyPos(Vector3 posizione) {
		
		//Modification of the x position of the laser
		//Vector3 pos = new Vector3(posizione.x, finalPos.y, finalPos.z);
		if (choice ==0)
		{	
			Vector3 pos = new Vector3(finalPos.x, finalPos.y, posizione.z);
			laser.SetPosition(1, pos);
		}
		if (choice ==1)
		{
			Vector3 pos = new Vector3(finalPos.x, posizione.y, finalPos.z);
			laser.SetPosition(1, pos);
		}
		if (choice ==2)
		{
			Vector3 pos = new Vector3(posizione.x, finalPos.y, finalPos.z);
			laser.SetPosition(1, pos);
		}
		
	}
	
	//Reset of the position of the laser when the collision is over
	public void resetPos() {
		if (initialized)
		{
			laser.SetPosition(1, finalPos);
		}
		
	}
	
	public void offPos()
	{
		laser.SetPosition (1,laserGen.transform.position);	
	}
	
}
