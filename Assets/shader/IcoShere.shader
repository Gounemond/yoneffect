// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:2,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:8361,x:34022,y:32687,varname:node_8361,prsc:2|spec-2132-OUT,emission-8707-OUT,transm-3150-OUT,lwrap-3150-OUT,voffset-6859-OUT,disp-7382-XYZ;n:type:ShaderForge.SFN_Multiply,id:822,x:32845,y:32891,varname:node_822,prsc:2|A-9323-RGB,B-101-OUT,C-3150-OUT;n:type:ShaderForge.SFN_Vector1,id:101,x:32594,y:33046,varname:node_101,prsc:2,v1:10;n:type:ShaderForge.SFN_Color,id:9323,x:32569,y:32821,ptovrint:False,ptlb:main,ptin:_main,varname:node_9323,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0,c3:0.06206894,c4:1;n:type:ShaderForge.SFN_Power,id:3150,x:32372,y:32921,varname:node_3150,prsc:2|VAL-7822-OUT,EXP-9258-OUT;n:type:ShaderForge.SFN_Color,id:1812,x:32590,y:32517,ptovrint:False,ptlb:glow,ptin:_glow,varname:node_1812,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0,c3:1,c4:1;n:type:ShaderForge.SFN_Vector1,id:2132,x:32856,y:32810,varname:node_2132,prsc:2,v1:1;n:type:ShaderForge.SFN_Lerp,id:892,x:32906,y:32596,varname:node_892,prsc:2|A-1812-RGB,B-7756-OUT,T-3150-OUT;n:type:ShaderForge.SFN_Vector1,id:7756,x:32535,y:32680,varname:node_7756,prsc:2,v1:0.5;n:type:ShaderForge.SFN_Multiply,id:744,x:32845,y:33142,varname:node_744,prsc:2|A-3150-OUT,B-3180-OUT,C-6607-OUT;n:type:ShaderForge.SFN_NormalVector,id:6607,x:32619,y:33272,prsc:2,pt:False;n:type:ShaderForge.SFN_Multiply,id:7822,x:32180,y:32846,varname:node_7822,prsc:2|A-1071-OUT,B-9177-OUT;n:type:ShaderForge.SFN_Abs,id:1071,x:32039,y:32740,varname:node_1071,prsc:2|IN-6097-OUT;n:type:ShaderForge.SFN_Subtract,id:6097,x:31877,y:32705,varname:node_6097,prsc:2|A-4592-OUT,B-5830-OUT;n:type:ShaderForge.SFN_Frac,id:4592,x:31655,y:32597,varname:node_4592,prsc:2|IN-7731-OUT;n:type:ShaderForge.SFN_ComponentMask,id:7731,x:31436,y:32571,varname:node_7731,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-1260-UVOUT;n:type:ShaderForge.SFN_Panner,id:1260,x:31180,y:32481,varname:node_1260,prsc:2,spu:0.25,spv:0|UVIN-8848-OUT;n:type:ShaderForge.SFN_Frac,id:3726,x:33228,y:33000,varname:node_3726,prsc:2|IN-7382-XYZ;n:type:ShaderForge.SFN_FragmentPosition,id:7382,x:33002,y:33000,varname:node_7382,prsc:2;n:type:ShaderForge.SFN_Add,id:6859,x:33440,y:33172,varname:node_6859,prsc:2|A-3726-OUT,B-744-OUT;n:type:ShaderForge.SFN_TexCoord,id:1522,x:30119,y:32313,varname:node_1522,prsc:2,uv:0;n:type:ShaderForge.SFN_ValueProperty,id:9258,x:32180,y:33073,ptovrint:False,ptlb:power,ptin:_power,varname:node_9258,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:2.5;n:type:ShaderForge.SFN_ValueProperty,id:9177,x:31981,y:32955,ptovrint:False,ptlb:abs,ptin:_abs,varname:node_9177,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1.5;n:type:ShaderForge.SFN_ValueProperty,id:5830,x:31670,y:32846,ptovrint:False,ptlb:substract,ptin:_substract,varname:node_5830,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.5;n:type:ShaderForge.SFN_ValueProperty,id:3180,x:32493,y:33188,ptovrint:False,ptlb:movimento,ptin:_movimento,varname:node_3180,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.2;n:type:ShaderForge.SFN_Multiply,id:8848,x:30943,y:32386,varname:node_8848,prsc:2|A-4021-OUT,B-276-OUT;n:type:ShaderForge.SFN_ValueProperty,id:276,x:30674,y:32574,ptovrint:False,ptlb:quantiti,ptin:_quantiti,varname:node_276,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:3;n:type:ShaderForge.SFN_Multiply,id:9948,x:30500,y:32478,varname:node_9948,prsc:2|A-1522-V,B-7134-OUT;n:type:ShaderForge.SFN_Multiply,id:9332,x:30511,y:32249,varname:node_9332,prsc:2|A-1522-U,B-7593-OUT;n:type:ShaderForge.SFN_ValueProperty,id:7593,x:30262,y:32219,ptovrint:False,ptlb:U Scale,ptin:_UScale,varname:node_7593,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_ValueProperty,id:7134,x:30270,y:32532,ptovrint:False,ptlb:V Scale,ptin:_VScale,varname:node_7134,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Append,id:4021,x:30726,y:32359,varname:node_4021,prsc:2|A-9332-OUT,B-9948-OUT;n:type:ShaderForge.SFN_Lerp,id:9502,x:33190,y:32537,varname:node_9502,prsc:2|A-892-OUT,B-4617-RGB,T-3150-OUT;n:type:ShaderForge.SFN_Color,id:4617,x:32894,y:32405,ptovrint:False,ptlb:node_4617,ptin:_node_4617,varname:node_4617,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Lerp,id:8707,x:33354,y:32640,varname:node_8707,prsc:2|A-9502-OUT,B-822-OUT,T-4109-OUT;n:type:ShaderForge.SFN_Power,id:4109,x:33298,y:32810,varname:node_4109,prsc:2|VAL-3150-OUT,EXP-299-OUT;n:type:ShaderForge.SFN_ValueProperty,id:299,x:33121,y:32855,ptovrint:False,ptlb:node_299,ptin:_node_299,varname:node_299,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1.2;proporder:9323-1812-9258-9177-5830-3180-276-7593-7134-4617-299;pass:END;sub:END;*/

Shader "Custom/IcoShere" {
    Properties {
        _main ("main", Color) = (1,0,0.06206894,1)
        _glow ("glow", Color) = (0,0,1,1)
        _power ("power", Float ) = 2.5
        _abs ("abs", Float ) = 1.5
        _substract ("substract", Float ) = 0.5
        _movimento ("movimento", Float ) = 0.2
        _quantiti ("quantiti", Float ) = 3
        _UScale ("U Scale", Float ) = 1
        _VScale ("V Scale", Float ) = 1
        _node_4617 ("node_4617", Color) = (1,1,1,1)
        _node_299 ("node_299", Float ) = 1.2
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Cull Off
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform float4 _TimeEditor;
            uniform float4 _main;
            uniform float4 _glow;
            uniform float _power;
            uniform float _abs;
            uniform float _substract;
            uniform float _movimento;
            uniform float _quantiti;
            uniform float _UScale;
            uniform float _VScale;
            uniform float4 _node_4617;
            uniform float _node_299;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                LIGHTING_COORDS(3,4)
                UNITY_FOG_COORDS(5)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                float4 node_4288 = _Time + _TimeEditor;
                float node_3150 = pow((abs((frac(((float2((o.uv0.r*_UScale),(o.uv0.g*_VScale))*_quantiti)+node_4288.g*float2(0.25,0)).r)-_substract))*_abs),_power);
                v.vertex.xyz += (frac(mul(unity_ObjectToWorld, v.vertex).rgb)+(node_3150*_movimento*v.normal));
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                i.normalDir = normalize(i.normalDir);
                i.normalDir *= faceSign;
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
///////// Gloss:
                float gloss = 0.5;
                float specPow = exp2( gloss * 10.0+1.0);
////// Specular:
                float NdotL = max(0, dot( normalDirection, lightDirection ));
                float node_2132 = 1.0;
                float3 specularColor = float3(node_2132,node_2132,node_2132);
                float3 directSpecular = (floor(attenuation) * _LightColor0.xyz) * pow(max(0,dot(halfDirection,normalDirection)),specPow)*specularColor;
                float3 specular = directSpecular;
////// Emissive:
                float node_7756 = 0.5;
                float4 node_4288 = _Time + _TimeEditor;
                float node_3150 = pow((abs((frac(((float2((i.uv0.r*_UScale),(i.uv0.g*_VScale))*_quantiti)+node_4288.g*float2(0.25,0)).r)-_substract))*_abs),_power);
                float3 emissive = lerp(lerp(lerp(_glow.rgb,float3(node_7756,node_7756,node_7756),node_3150),_node_4617.rgb,node_3150),(_main.rgb*10.0*node_3150),pow(node_3150,_node_299));
/// Final Color:
                float3 finalColor = specular + emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            Cull Off
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform float4 _TimeEditor;
            uniform float4 _main;
            uniform float4 _glow;
            uniform float _power;
            uniform float _abs;
            uniform float _substract;
            uniform float _movimento;
            uniform float _quantiti;
            uniform float _UScale;
            uniform float _VScale;
            uniform float4 _node_4617;
            uniform float _node_299;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                LIGHTING_COORDS(3,4)
                UNITY_FOG_COORDS(5)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                float4 node_2063 = _Time + _TimeEditor;
                float node_3150 = pow((abs((frac(((float2((o.uv0.r*_UScale),(o.uv0.g*_VScale))*_quantiti)+node_2063.g*float2(0.25,0)).r)-_substract))*_abs),_power);
                v.vertex.xyz += (frac(mul(unity_ObjectToWorld, v.vertex).rgb)+(node_3150*_movimento*v.normal));
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                i.normalDir = normalize(i.normalDir);
                i.normalDir *= faceSign;
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
///////// Gloss:
                float gloss = 0.5;
                float specPow = exp2( gloss * 10.0+1.0);
////// Specular:
                float NdotL = max(0, dot( normalDirection, lightDirection ));
                float node_2132 = 1.0;
                float3 specularColor = float3(node_2132,node_2132,node_2132);
                float3 directSpecular = attenColor * pow(max(0,dot(halfDirection,normalDirection)),specPow)*specularColor;
                float3 specular = directSpecular;
/// Final Color:
                float3 finalColor = specular;
                fixed4 finalRGBA = fixed4(finalColor * 1,0);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform float _power;
            uniform float _abs;
            uniform float _substract;
            uniform float _movimento;
            uniform float _quantiti;
            uniform float _UScale;
            uniform float _VScale;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
                float4 posWorld : TEXCOORD2;
                float3 normalDir : TEXCOORD3;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                float4 node_4445 = _Time + _TimeEditor;
                float node_3150 = pow((abs((frac(((float2((o.uv0.r*_UScale),(o.uv0.g*_VScale))*_quantiti)+node_4445.g*float2(0.25,0)).r)-_substract))*_abs),_power);
                v.vertex.xyz += (frac(mul(unity_ObjectToWorld, v.vertex).rgb)+(node_3150*_movimento*v.normal));
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                i.normalDir = normalize(i.normalDir);
                i.normalDir *= faceSign;
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
