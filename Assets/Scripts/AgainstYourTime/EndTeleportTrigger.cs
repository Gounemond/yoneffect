﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System;
using System.Linq;

public class EndTeleportTrigger : MonoBehaviour
{

    public AudioSource teleportAudio;
    private int triggerCounter = 0;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
    }

    void OnTriggerEnter(Collider ObjectCollision)
    {
        if (ObjectCollision.tag == "Player" || ObjectCollision.tag == "ClonePlayer")
        {
            if (triggerCounter == 0)
            {
                YonEffectJRController.Self.winGame();
                teleportAudio.Play();
            }
            triggerCounter++;
        }
    }

    void OnTriggerExit(Collider ObjectCollision)
    {
        if (ObjectCollision.tag == "Player" || ObjectCollision.tag == "ClonePlayer")
        {
            if (triggerCounter == 1)
            {
                /*if (m_TeleportRoutine != null)
                {
                    StopCoroutine(m_TeleportRoutine);
                    StartCoroutine(CameraEffectsManager.Self.EndBlurEffect(timeToTeleport));
                    m_Timer = 0;
                }*/
                triggerCounter--;
            }
            else
            {
                triggerCounter--;
            }
        }
    }
}
