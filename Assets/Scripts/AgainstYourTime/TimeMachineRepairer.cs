﻿using UnityEngine;
using UnityEngine.Audio;
using System.Collections;
using DG.Tweening;

public class TimeMachineRepairer : MonoBehaviour {

    public AudioSource tikTokAudio;
    public AudioSource timeMachineRepairingAudio;
    public AudioMixerSnapshot timeRunningSnap;
    public YonEffectJRController gamestate;

    [Header("Ending elemenets")]
    public GameObject[] triggerDoors;
    public GameObject[] endingPortals;

    private bool m_firstRepair = false;
    private bool m_yellowRepair = false;
    private bool m_redRepair = false;
    private bool m_greenRepair = false;
    private bool m_blueRepair = false;

    // Use this for initialization
    void Start ()
    {
        if (gamestate == null)
        {
            gamestate = FindObjectOfType<YonEffectJRController>().GetComponent<YonEffectJRController>();
        }
	}
	
	// Update is called once per frame
	void Update ()
    {
	
	}

    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if (!m_firstRepair && gamestate.numberGEARSCollected > 0)
            {
                gamestate.timerStarted = true;
                gamestate.startPlayerRecording();
                tikTokAudio.Play();
                timeMachineRepairingAudio.Play();
                timeRunningSnap.TransitionTo(0.2f);
                TimeMachineActivator.Self.ReparationTransition();
                m_firstRepair = true;
                return;
            }
            if (!m_yellowRepair && gamestate.numberGEARSCollected >= 6)
            {
                timeMachineRepairingAudio.Play();
                TimeMachineActivator.Self.YellowTransition();
                m_yellowRepair = true;
                return;
            }
            if (!m_redRepair && gamestate.numberGEARSCollected >= 12)
            {
                timeMachineRepairingAudio.Play();
                TimeMachineActivator.Self.RedTransition();
                m_redRepair = true;
                return;
            }
            if (!m_greenRepair && gamestate.numberGEARSCollected >= 18)
            {
                timeMachineRepairingAudio.Play();
                TimeMachineActivator.Self.GreenTransition();
                m_greenRepair = true;
                return;
            }
            if (!m_blueRepair && gamestate.numberGEARSCollected >= 19)
            {
                timeMachineRepairingAudio.Play();
                StartCoroutine(GameCompleted());
                TimeMachineActivator.Self.BlueTransition();
                m_blueRepair = true;
                return;
            }
        }
    }

    public IEnumerator GameCompleted()
    {
        gamestate.timerStarted = false;
        gamestate.stopClones();
        yield return new WaitForSeconds(TimeMachineActivator.Self.blueRepair.length);

        for (int i = 0; i < triggerDoors.Length; i++)
        {
            triggerDoors[i].transform.DOScale(100, 0.1f);
            endingPortals[i].SetActive(true);
        }
    }
}
