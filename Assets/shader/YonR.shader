// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:8361,x:33400,y:32572,varname:node_8361,prsc:2|spec-2132-OUT,emission-1019-OUT,transm-3150-OUT,lwrap-3150-OUT,olwid-4264-OUT,olcol-416-RGB;n:type:ShaderForge.SFN_Multiply,id:822,x:32863,y:32707,varname:node_822,prsc:2|A-9323-RGB,B-101-OUT,C-3150-OUT;n:type:ShaderForge.SFN_Vector1,id:101,x:32463,y:32767,varname:node_101,prsc:2,v1:5;n:type:ShaderForge.SFN_Color,id:9323,x:32587,y:32628,ptovrint:False,ptlb:glow,ptin:_glow,varname:node_9323,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.2313726,c2:0.9803922,c3:0.7215686,c4:1;n:type:ShaderForge.SFN_Power,id:3150,x:32398,y:32942,varname:node_3150,prsc:2|VAL-7822-OUT,EXP-8829-OUT;n:type:ShaderForge.SFN_Color,id:1812,x:32550,y:32441,ptovrint:False,ptlb:base,ptin:_base,varname:node_1812,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0,c3:1,c4:1;n:type:ShaderForge.SFN_Vector1,id:2132,x:32863,y:32609,varname:node_2132,prsc:2,v1:1;n:type:ShaderForge.SFN_Lerp,id:892,x:32973,y:32494,varname:node_892,prsc:2|A-1812-RGB,B-7756-OUT,T-3150-OUT;n:type:ShaderForge.SFN_Vector1,id:7756,x:32483,y:32579,varname:node_7756,prsc:2,v1:0.1;n:type:ShaderForge.SFN_Multiply,id:7822,x:32165,y:32868,varname:node_7822,prsc:2|A-1071-OUT,B-9584-OUT;n:type:ShaderForge.SFN_Vector1,id:8829,x:32176,y:33047,varname:node_8829,prsc:2,v1:5;n:type:ShaderForge.SFN_Abs,id:1071,x:32039,y:32740,varname:node_1071,prsc:2|IN-6097-OUT;n:type:ShaderForge.SFN_Vector1,id:9584,x:31896,y:32977,varname:node_9584,prsc:2,v1:1.5;n:type:ShaderForge.SFN_Subtract,id:6097,x:31877,y:32705,varname:node_6097,prsc:2|A-4592-OUT,B-155-OUT;n:type:ShaderForge.SFN_Vector1,id:155,x:31713,y:32831,varname:node_155,prsc:2,v1:0.5;n:type:ShaderForge.SFN_Frac,id:4592,x:31655,y:32597,varname:node_4592,prsc:2|IN-7731-OUT;n:type:ShaderForge.SFN_ComponentMask,id:7731,x:31528,y:32481,varname:node_7731,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-1260-UVOUT;n:type:ShaderForge.SFN_Panner,id:1260,x:31196,y:32464,varname:node_1260,prsc:2,spu:0.25,spv:0|UVIN-7714-OUT,DIST-2090-OUT;n:type:ShaderForge.SFN_Slider,id:4264,x:32989,y:32965,ptovrint:False,ptlb:slideroutline,ptin:_slideroutline,varname:node_4264,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.09932142,max:1;n:type:ShaderForge.SFN_Color,id:416,x:33146,y:33179,ptovrint:False,ptlb:coloroutline,ptin:_coloroutline,varname:node_416,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0,c3:0,c4:1;n:type:ShaderForge.SFN_Add,id:1019,x:33024,y:32638,varname:node_1019,prsc:2|A-892-OUT,B-822-OUT;n:type:ShaderForge.SFN_Time,id:3446,x:30951,y:32645,varname:node_3446,prsc:2;n:type:ShaderForge.SFN_Multiply,id:2090,x:31133,y:32659,varname:node_2090,prsc:2|A-3446-T,B-9079-OUT;n:type:ShaderForge.SFN_Slider,id:9079,x:30891,y:32786,ptovrint:False,ptlb:gottagofast,ptin:_gottagofast,varname:node_9079,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-10,cur:2.960048,max:10;n:type:ShaderForge.SFN_TexCoord,id:7066,x:30770,y:32480,varname:node_7066,prsc:2,uv:0;n:type:ShaderForge.SFN_Multiply,id:7714,x:30951,y:32480,varname:node_7714,prsc:2|A-7066-UVOUT,B-7374-OUT;n:type:ShaderForge.SFN_Slider,id:7374,x:30724,y:32387,ptovrint:False,ptlb:numberoflight,ptin:_numberoflight,varname:node_7374,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:1,cur:1,max:10;proporder:9323-1812-4264-416-9079-7374;pass:END;sub:END;*/

Shader "Custom/YonR" {
    Properties {
        _glow ("glow", Color) = (0.2313726,0.9803922,0.7215686,1)
        _base ("base", Color) = (0,0,1,1)
        _slideroutline ("slideroutline", Range(0, 1)) = 0.09932142
        _coloroutline ("coloroutline", Color) = (1,0,0,1)
        _gottagofast ("gottagofast", Range(-10, 10)) = 2.960048
        _numberoflight ("numberoflight", Range(1, 10)) = 1
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "Outline"
            Tags {
            }
            Cull Front
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float _slideroutline;
            uniform float4 _coloroutline;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                UNITY_FOG_COORDS(0)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.pos = mul(UNITY_MATRIX_MVP, float4(v.vertex.xyz + v.normal*_slideroutline,1) );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                return fixed4(_coloroutline.rgb,0);
            }
            ENDCG
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform float4 _TimeEditor;
            uniform float4 _glow;
            uniform float4 _base;
            uniform float _gottagofast;
            uniform float _numberoflight;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                LIGHTING_COORDS(3,4)
                UNITY_FOG_COORDS(5)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
///////// Gloss:
                float gloss = 0.5;
                float specPow = exp2( gloss * 10.0+1.0);
////// Specular:
                float NdotL = max(0, dot( normalDirection, lightDirection ));
                float node_2132 = 1.0;
                float3 specularColor = float3(node_2132,node_2132,node_2132);
                float3 directSpecular = (floor(attenuation) * _LightColor0.xyz) * pow(max(0,dot(halfDirection,normalDirection)),specPow)*specularColor;
                float3 specular = directSpecular;
////// Emissive:
                float node_7756 = 0.1;
                float4 node_3446 = _Time + _TimeEditor;
                float node_3150 = pow((abs((frac(((i.uv0*_numberoflight)+(node_3446.g*_gottagofast)*float2(0.25,0)).r)-0.5))*1.5),5.0);
                float3 emissive = (lerp(_base.rgb,float3(node_7756,node_7756,node_7756),node_3150)+(_glow.rgb*5.0*node_3150));
/// Final Color:
                float3 finalColor = specular + emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform float4 _TimeEditor;
            uniform float4 _glow;
            uniform float4 _base;
            uniform float _gottagofast;
            uniform float _numberoflight;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                LIGHTING_COORDS(3,4)
                UNITY_FOG_COORDS(5)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
///////// Gloss:
                float gloss = 0.5;
                float specPow = exp2( gloss * 10.0+1.0);
////// Specular:
                float NdotL = max(0, dot( normalDirection, lightDirection ));
                float node_2132 = 1.0;
                float3 specularColor = float3(node_2132,node_2132,node_2132);
                float3 directSpecular = attenColor * pow(max(0,dot(halfDirection,normalDirection)),specPow)*specularColor;
                float3 specular = directSpecular;
/// Final Color:
                float3 finalColor = specular;
                fixed4 finalRGBA = fixed4(finalColor * 1,0);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
