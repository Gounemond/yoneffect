﻿using UnityEngine;
using System.Collections;
using System;
using DG.Tweening;

public class FinalPuzzle : PuzzleManager
{
    [Header ("Plate sensors")]
    public FinalPlateCollider yellowPlateCollider;      // yellow plate sensor
    public FinalPlateCollider redPlateCollider;         // red plate sensor
    public FinalPlateCollider greenPlateCollider;       // green plate sensor



    [Header("Puzzle Reward")]
    public AudioSource levelCompleteAudio;          // Audio of success upon completing all the Tangram
    //public Animator YonGearGate;
    public GameObject yellowRing;
    public GameObject redRing;
    public GameObject greenRing;
    public Animator finalYonGear;
    public GameObject bubble;
    public GameObject finalStartGate;

    // Various counters
    // Counters which memorize the total number of sensors
    private int collidersNumber = 0;


    // Counters that memorize how many sensors are active
    private int collidersTouched = 0;


    // Use this for initialization
    void Awake ()
    {
        collidersNumber = 3;
    }

    void OnEnable()
    {
        yellowPlateCollider.onCollisionEnter += topTriggerTouched;
        yellowPlateCollider.onCollisionExit += topTriggerUntouched;
        yellowPlateCollider.onCollisionEnter += yellowTriggerTouched;
        yellowPlateCollider.onCollisionExit += yellowTriggerUntouched;
        redPlateCollider.onCollisionEnter += topTriggerTouched;
        redPlateCollider.onCollisionExit += topTriggerUntouched;
        redPlateCollider.onCollisionEnter += redTriggerTouched;
        redPlateCollider.onCollisionExit += redTriggerUntouched;
        greenPlateCollider.onCollisionEnter += topTriggerTouched;
        greenPlateCollider.onCollisionExit += topTriggerUntouched;
        greenPlateCollider.onCollisionEnter += greenTriggerTouched;
        greenPlateCollider.onCollisionExit += greenTriggerUntouched;
    }

    void OnDisable()
    {
        yellowPlateCollider.onCollisionEnter -= topTriggerTouched;
        yellowPlateCollider.onCollisionExit -= topTriggerUntouched;
        yellowPlateCollider.onCollisionEnter -= yellowTriggerTouched;
        yellowPlateCollider.onCollisionExit -= yellowTriggerUntouched;
        redPlateCollider.onCollisionEnter -= topTriggerTouched;
        redPlateCollider.onCollisionExit -= topTriggerUntouched;
        redPlateCollider.onCollisionEnter -= redTriggerTouched;
        redPlateCollider.onCollisionExit -= redTriggerUntouched;
        greenPlateCollider.onCollisionEnter -= topTriggerTouched;
        greenPlateCollider.onCollisionExit -= topTriggerUntouched;
        greenPlateCollider.onCollisionEnter -= greenTriggerTouched;
        greenPlateCollider.onCollisionExit -= greenTriggerUntouched;
    }
	
	// Update is called once per frame
	void Update ()
    {
	
	}

    public void topTriggerTouched()
    {
        Debug.Log("Ok?");
        collidersTouched++;
        Debug.Log(collidersTouched);
        if (collidersTouched == collidersNumber)
        {
            bubble.GetComponent<Renderer>().material.DOFloat(-1, "_time", 1);
            levelCompleteAudio.Play();
            finalYonGear.SetBool("HasWon", true);
            finalStartGate.SetActive(true);
        }
    }

    public void topTriggerUntouched()
    {
        collidersTouched--;
    }

    public void yellowTriggerTouched()
    {
        StartCoroutine(RingProtectorDisappear(yellowRing));
    }

    public void redTriggerTouched()
    {
        Debug.Log("Meh");
        StartCoroutine(RingProtectorDisappear(redRing));
    }

    public void greenTriggerTouched()
    {
        StartCoroutine(RingProtectorDisappear(greenRing));
    }

    public void yellowTriggerUntouched()
    {
        StartCoroutine(RingProtectorAppear(yellowRing));
    }

    public void redTriggerUntouched()
    {
        StartCoroutine(RingProtectorAppear(redRing));
    }

    public void greenTriggerUntouched()
    {
        StartCoroutine(RingProtectorAppear(greenRing));
    }

    public override void ResetPuzzle()
    {
        throw new NotImplementedException();
    }

    public override void CompletePuzzle()
    {
        throw new NotImplementedException();
    }

    public IEnumerator RingProtectorDisappear(GameObject ring)
    {
        ring.transform.DOScale(3.5f, 1);
        yield return new WaitForSeconds(1);
        ring.transform.DOScale(0, 1);
        yield return new WaitForSeconds(1);
    }

    public IEnumerator RingProtectorAppear(GameObject ring)
    {
        ring.transform.DOScale(3.5f, 1);
        yield return new WaitForSeconds(1);
        ring.transform.DOScale(1, 1);
        yield return new WaitForSeconds(1);
    }
}
