// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.16 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.16;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,culm:1,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,ufog:True,aust:True,igpj:False,qofs:0,qpre:2,rntp:3,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:8713,x:33318,y:32713,varname:node_8713,prsc:2|emission-7391-OUT,clip-4072-OUT;n:type:ShaderForge.SFN_Tex2d,id:9072,x:32458,y:32316,ptovrint:False,ptlb:ologram_3,ptin:_ologram_3,varname:node_9072,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:7beeecf6babef464791faeb7693ed85d,ntxv:3,isnm:False|UVIN-5757-UVOUT;n:type:ShaderForge.SFN_Tex2d,id:3529,x:32546,y:32768,ptovrint:False,ptlb:ologram_1,ptin:_ologram_1,varname:node_3529,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:c01cd32a05805574bb7648740fb37dab,ntxv:0,isnm:False|UVIN-167-UVOUT;n:type:ShaderForge.SFN_Parallax,id:5757,x:32217,y:32368,varname:node_5757,prsc:2|UVIN-6262-OUT,HEI-955-OUT;n:type:ShaderForge.SFN_Vector1,id:955,x:31944,y:32370,varname:node_955,prsc:2,v1:3;n:type:ShaderForge.SFN_Multiply,id:6441,x:32719,y:32441,varname:node_6441,prsc:2|A-9072-RGB,B-4578-RGB;n:type:ShaderForge.SFN_Color,id:4578,x:32320,y:32595,ptovrint:False,ptlb:olograma_2color,ptin:_olograma_2color,varname:node_4578,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.466317,c2:0.862908,c3:0.9191176,c4:1;n:type:ShaderForge.SFN_Vector1,id:2800,x:32133,y:32826,varname:node_2800,prsc:2,v1:1;n:type:ShaderForge.SFN_Parallax,id:167,x:32356,y:32812,varname:node_167,prsc:2|UVIN-5566-UVOUT,HEI-2800-OUT;n:type:ShaderForge.SFN_Panner,id:7685,x:32052,y:32915,varname:node_7685,prsc:2,spu:0,spv:0.1|DIST-4005-OUT;n:type:ShaderForge.SFN_Time,id:3390,x:31660,y:32843,varname:node_3390,prsc:2;n:type:ShaderForge.SFN_Multiply,id:4005,x:31873,y:32987,varname:node_4005,prsc:2|A-3390-T,B-9835-OUT;n:type:ShaderForge.SFN_Vector1,id:9835,x:31660,y:33096,varname:node_9835,prsc:2,v1:0.1;n:type:ShaderForge.SFN_Slider,id:508,x:32367,y:33036,ptovrint:False,ptlb:fade_out,ptin:_fade_out,varname:node_508,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Add,id:3777,x:32758,y:32705,varname:node_3777,prsc:2|A-6441-OUT,B-3529-RGB;n:type:ShaderForge.SFN_Lerp,id:7391,x:32825,y:32835,varname:node_7391,prsc:2|A-3777-OUT,B-3643-OUT,T-508-OUT;n:type:ShaderForge.SFN_Vector1,id:3643,x:32546,y:32949,varname:node_3643,prsc:2,v1:0;n:type:ShaderForge.SFN_ScreenPos,id:3921,x:31453,y:32482,varname:node_3921,prsc:2,sctp:2;n:type:ShaderForge.SFN_Rotator,id:5566,x:32201,y:33036,varname:node_5566,prsc:2|UVIN-7685-UVOUT,SPD-671-OUT;n:type:ShaderForge.SFN_Vector1,id:671,x:32010,y:33160,varname:node_671,prsc:2,v1:0.2;n:type:ShaderForge.SFN_Rotator,id:4876,x:31844,y:32522,varname:node_4876,prsc:2|UVIN-3921-UVOUT,SPD-9799-OUT;n:type:ShaderForge.SFN_Vector1,id:9799,x:31648,y:32684,varname:node_9799,prsc:2,v1:5;n:type:ShaderForge.SFN_OneMinus,id:6262,x:32010,y:32525,varname:node_6262,prsc:2|IN-4876-UVOUT;n:type:ShaderForge.SFN_Tex2d,id:9687,x:32359,y:33242,ptovrint:False,ptlb:ologram_2,ptin:_ologram_2,varname:_ologram_2,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:c01cd32a05805574bb7648740fb37dab,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Power,id:6290,x:32644,y:33336,varname:node_6290,prsc:2|VAL-9687-RGB,EXP-7365-OUT;n:type:ShaderForge.SFN_Slider,id:3744,x:32014,y:33442,ptovrint:False,ptlb:Fade_in,ptin:_Fade_in,varname:node_3744,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_ComponentMask,id:4072,x:33118,y:33067,varname:node_4072,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-8646-OUT;n:type:ShaderForge.SFN_OneMinus,id:7365,x:32359,y:33419,varname:node_7365,prsc:2|IN-3744-OUT;n:type:ShaderForge.SFN_If,id:8646,x:33007,y:33308,varname:node_8646,prsc:2|A-3744-OUT,B-5367-OUT,GT-6290-OUT,EQ-5367-OUT,LT-5367-OUT;n:type:ShaderForge.SFN_Vector1,id:5367,x:32644,y:33474,varname:node_5367,prsc:2,v1:0;proporder:9072-3529-4578-508-9687-3744;pass:END;sub:END;*/

Shader "Custom/particellar" {
    Properties {
        _ologram_3 ("ologram_3", 2D) = "bump" {}
        _ologram_1 ("ologram_1", 2D) = "white" {}
        _olograma_2color ("olograma_2color", Color) = (0.466317,0.862908,0.9191176,1)
        _fade_out ("fade_out", Range(0, 1)) = 0
        _ologram_2 ("ologram_2", 2D) = "white" {}
        _Fade_in ("Fade_in", Range(0, 1)) = 1
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "Queue"="AlphaTest"
            "RenderType"="TransparentCutout"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Cull Front
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform sampler2D _ologram_3; uniform float4 _ologram_3_ST;
            uniform sampler2D _ologram_1; uniform float4 _ologram_1_ST;
            uniform float4 _olograma_2color;
            uniform float _fade_out;
            uniform sampler2D _ologram_2; uniform float4 _ologram_2_ST;
            uniform float _Fade_in;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 bitangentDir : TEXCOORD4;
                float4 screenPos : TEXCOORD5;
                UNITY_FOG_COORDS(6)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(-v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                UNITY_TRANSFER_FOG(o,o.pos);
                o.screenPos = o.pos;
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                #if UNITY_UV_STARTS_AT_TOP
                    float grabSign = -_ProjectionParams.x;
                #else
                    float grabSign = _ProjectionParams.x;
                #endif
                i.normalDir = normalize(i.normalDir);
                i.screenPos = float4( i.screenPos.xy / i.screenPos.w, 0, 0 );
                i.screenPos.y *= _ProjectionParams.x;
                float2 sceneUVs = float2(1,grabSign)*i.screenPos.xy*0.5+0.5;
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
/////// Vectors:
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float node_5367 = 0.0;
                float node_8646_if_leA = step(_Fade_in,node_5367);
                float node_8646_if_leB = step(node_5367,_Fade_in);
                float4 _ologram_2_var = tex2D(_ologram_2,TRANSFORM_TEX(i.uv0, _ologram_2));
                clip(lerp((node_8646_if_leA*node_5367)+(node_8646_if_leB*pow(_ologram_2_var.rgb,(1.0 - _Fade_in))),node_5367,node_8646_if_leA*node_8646_if_leB).r - 0.5);
////// Lighting:
////// Emissive:
                float4 node_6104 = _Time + _TimeEditor;
                float node_4876_ang = node_6104.g;
                float node_4876_spd = 5.0;
                float node_4876_cos = cos(node_4876_spd*node_4876_ang);
                float node_4876_sin = sin(node_4876_spd*node_4876_ang);
                float2 node_4876_piv = float2(0.5,0.5);
                float2 node_4876 = (mul(sceneUVs.rg-node_4876_piv,float2x2( node_4876_cos, -node_4876_sin, node_4876_sin, node_4876_cos))+node_4876_piv);
                float2 node_5757 = (0.05*(3.0 - 0.5)*mul(tangentTransform, viewDirection).xy + (1.0 - node_4876));
                float4 _ologram_3_var = tex2D(_ologram_3,TRANSFORM_TEX(node_5757.rg, _ologram_3));
                float node_5566_ang = node_6104.g;
                float node_5566_spd = 0.2;
                float node_5566_cos = cos(node_5566_spd*node_5566_ang);
                float node_5566_sin = sin(node_5566_spd*node_5566_ang);
                float2 node_5566_piv = float2(0.5,0.5);
                float4 node_3390 = _Time + _TimeEditor;
                float2 node_5566 = (mul((i.uv0+(node_3390.g*0.1)*float2(0,0.1))-node_5566_piv,float2x2( node_5566_cos, -node_5566_sin, node_5566_sin, node_5566_cos))+node_5566_piv);
                float2 node_167 = (0.05*(1.0 - 0.5)*mul(tangentTransform, viewDirection).xy + node_5566);
                float4 _ologram_1_var = tex2D(_ologram_1,TRANSFORM_TEX(node_167.rg, _ologram_1));
                float node_3643 = 0.0;
                float3 emissive = lerp(((_ologram_3_var.rgb*_olograma_2color.rgb)+_ologram_1_var.rgb),float3(node_3643,node_3643,node_3643),_fade_out);
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform sampler2D _ologram_2; uniform float4 _ologram_2_ST;
            uniform float _Fade_in;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
                float node_5367 = 0.0;
                float node_8646_if_leA = step(_Fade_in,node_5367);
                float node_8646_if_leB = step(node_5367,_Fade_in);
                float4 _ologram_2_var = tex2D(_ologram_2,TRANSFORM_TEX(i.uv0, _ologram_2));
                clip(lerp((node_8646_if_leA*node_5367)+(node_8646_if_leB*pow(_ologram_2_var.rgb,(1.0 - _Fade_in))),node_5367,node_8646_if_leA*node_8646_if_leB).r - 0.5);
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
