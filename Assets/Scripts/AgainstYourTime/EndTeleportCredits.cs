﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using VRStandardAssets.Utils;

public class EndTeleportCredits : MonoBehaviour
{
    public AudioSource teleportAudio;
    private int triggerCounter = 0;
    public VRCameraFade cameraFader;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider ObjectCollision)
    {
        if (ObjectCollision.tag == "Player" || ObjectCollision.tag == "ClonePlayer")
        {
            if (triggerCounter == 0)
            {
                cameraFader.OnFadeComplete += LoadGameScene;
                teleportAudio.Play();
                StartCoroutine(cameraFader.BeginFadeOut(false));
            }
            triggerCounter++;
        }
    }

    void OnTriggerExit(Collider ObjectCollision)
    {
        if (ObjectCollision.tag == "Player" || ObjectCollision.tag == "ClonePlayer")
        {
            if (triggerCounter == 1)
            {
                /*if (m_TeleportRoutine != null)
                {
                    StopCoroutine(m_TeleportRoutine);
                    StartCoroutine(CameraEffectsManager.Self.EndBlurEffect(timeToTeleport));
                    m_Timer = 0;
                }*/
                triggerCounter--;
            }
            else
            {
                triggerCounter--;
            }
        }
    }

    public void LoadGameScene()
    {
        SceneManager.LoadScene("StartMenu_CHR&SONIA");
    }
}
