﻿using UnityEngine;
using System.Collections;
using System;

namespace UI.StepsVR
{
    public class Return : Pulsante
    {

        public GameObject uiPausa;

        public bool hideMouse;

        public override void azione_click()
        {
            uiPausa.SetActive(false);
            YonEffectJRController.Self.IsPause = false;
            Time.timeScale = 1.0f;

            if (hideMouse == true)
            {
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.Locked;
            }
        }
    }
}