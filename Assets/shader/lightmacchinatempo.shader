// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:5277,x:33707,y:32692,varname:node_5277,prsc:2|emission-3066-OUT,voffset-8935-OUT;n:type:ShaderForge.SFN_Time,id:9839,x:32273,y:32753,varname:node_9839,prsc:2;n:type:ShaderForge.SFN_Sin,id:6356,x:32555,y:32806,varname:node_6356,prsc:2|IN-1564-OUT;n:type:ShaderForge.SFN_Multiply,id:1564,x:32441,y:32877,varname:node_1564,prsc:2|A-9839-T,B-2165-OUT;n:type:ShaderForge.SFN_Color,id:9239,x:32811,y:32630,ptovrint:False,ptlb:maincolor,ptin:_maincolor,varname:node_9239,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5254902,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Add,id:3066,x:33059,y:32795,varname:node_3066,prsc:2|A-9239-RGB,B-7371-OUT;n:type:ShaderForge.SFN_Multiply,id:7371,x:32875,y:32908,varname:node_7371,prsc:2|A-8627-OUT,B-3104-OUT;n:type:ShaderForge.SFN_Vector1,id:3104,x:32707,y:33072,varname:node_3104,prsc:2,v1:1;n:type:ShaderForge.SFN_Frac,id:8627,x:32689,y:32888,varname:node_8627,prsc:2|IN-6356-OUT;n:type:ShaderForge.SFN_Multiply,id:8935,x:33374,y:33077,varname:node_8935,prsc:2|A-2161-UVOUT,B-44-OUT;n:type:ShaderForge.SFN_Rotator,id:2161,x:33034,y:33029,varname:node_2161,prsc:2|UVIN-3612-UVOUT,SPD-8226-OUT;n:type:ShaderForge.SFN_TexCoord,id:3612,x:32814,y:33072,varname:node_3612,prsc:2,uv:0;n:type:ShaderForge.SFN_Slider,id:44,x:33064,y:33226,ptovrint:False,ptlb:rotator,ptin:_rotator,varname:node_44,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.1,max:1;n:type:ShaderForge.SFN_Slider,id:2165,x:32146,y:32974,ptovrint:False,ptlb:time,ptin:_time,varname:node_2165,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.5,max:1;n:type:ShaderForge.SFN_Slider,id:8226,x:32991,y:33417,ptovrint:False,ptlb:timerotator,ptin:_timerotator,varname:node_8226,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.4017094,max:10;proporder:9239-44-2165-8226;pass:END;sub:END;*/

Shader "Custom/lightmacchinatempo" {
    Properties {
        _maincolor ("maincolor", Color) = (0.5254902,1,1,1)
        _rotator ("rotator", Range(0, 1)) = 0.1
        _time ("time", Range(0, 1)) = 0.5
        _timerotator ("timerotator", Range(0, 10)) = 0.4017094
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform float4 _maincolor;
            uniform float _rotator;
            uniform float _time;
            uniform float _timerotator;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                UNITY_FOG_COORDS(1)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                float4 node_6444 = _Time + _TimeEditor;
                float node_2161_ang = node_6444.g;
                float node_2161_spd = _timerotator;
                float node_2161_cos = cos(node_2161_spd*node_2161_ang);
                float node_2161_sin = sin(node_2161_spd*node_2161_ang);
                float2 node_2161_piv = float2(0.5,0.5);
                float2 node_2161 = (mul(o.uv0-node_2161_piv,float2x2( node_2161_cos, -node_2161_sin, node_2161_sin, node_2161_cos))+node_2161_piv);
                float2 node_8935 = (node_2161*_rotator);
                v.vertex.xyz += float3(node_8935,0.0);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
////// Lighting:
////// Emissive:
                float4 node_9839 = _Time + _TimeEditor;
                float3 emissive = (_maincolor.rgb+(frac(sin((node_9839.g*_time)))*1.0));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform float _rotator;
            uniform float _timerotator;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                float4 node_2091 = _Time + _TimeEditor;
                float node_2161_ang = node_2091.g;
                float node_2161_spd = _timerotator;
                float node_2161_cos = cos(node_2161_spd*node_2161_ang);
                float node_2161_sin = sin(node_2161_spd*node_2161_ang);
                float2 node_2161_piv = float2(0.5,0.5);
                float2 node_2161 = (mul(o.uv0-node_2161_piv,float2x2( node_2161_cos, -node_2161_sin, node_2161_sin, node_2161_cos))+node_2161_piv);
                float2 node_8935 = (node_2161*_rotator);
                v.vertex.xyz += float3(node_8935,0.0);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
