using UnityEngine;
using VRStandardAssets.Utils;
using System.Collections;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System;
using System.Linq;
using DG.Tweening;

[RequireComponent (typeof(VRInteractiveItem))]
[RequireComponent (typeof(Rigidbody))]
public class VRGrabFinalCube : MonoBehaviour 
{
	public float  correctionforce = 50.0f;
	public Transform beacon;
    public GameObject graphicalElement;
    public int whichMaterialSwitch = 0;
    public Material switchMaterial;

    [Header("AudioSfx")]
    public AudioSource myAudioSource;
    public AudioClip hoverInAudio;
    public AudioClip hoverOutAudio;
    public AudioClip grabAudio;

    private bool  grabbed  = false;
    private bool  onHover = false;

    private Rigidbody _myRigidbody;
    private VRInteractiveItem _myVRitem;
    private Material _myMaterial;


    void OnEnable()
    {
        _myVRitem.OnClick += GrabSwitchState;
        _myVRitem.OnOver += HoverIn;
        _myVRitem.OnOut += HoverOut;
    }

    void OnDisable()
    {
        _myVRitem.OnClick -= GrabSwitchState;
        _myVRitem.OnOver -= HoverIn;
        _myVRitem.OnOut -= HoverOut;
    }

	void Awake () 
	{
        _myRigidbody = GetComponent<Rigidbody>();
        _myVRitem = GetComponent<VRInteractiveItem>();
        _myMaterial = graphicalElement.GetComponent<Renderer>().material;
    }

    void Update () 
	{
        if (grabbed && !onHover && Input.GetButton("Fire1"))
        {
            GrabSwitchState();
        }

        // Update gravity force around the GrabBeacon if you actually have grabbed an object
        // The grab method is inspired from the Half Life series
        if (grabbed)
		{
            if (Vector3.Distance(this.transform.position,beacon.position) > 10)
            {
                GrabSwitchState();
                return;
            }
			Vector3 force = beacon.transform.position - transform.position;
            _myRigidbody.velocity = force.normalized * _myRigidbody.velocity.magnitude;
            _myRigidbody.AddForce(force * correctionforce);
            _myRigidbody.velocity *= Mathf.Min(1.0f, force.magnitude / 2 );
		}

        
	}

    public void GrabSwitchState()
    {
        if (grabbed)
        {
            grabbed = false;
            this.transform.DOScale(0.7f, 0.2f);
            _myRigidbody.useGravity = true;
            _myRigidbody.constraints = RigidbodyConstraints.FreezeRotation;
            //_myRigidbody.freezeRotation = false;
        }
        else
        {
            grabbed = true;
            this.transform.DOScale(0.4f, 0.2f);
            myAudioSource.PlayOneShot(grabAudio);
            _myRigidbody.useGravity = false;
            _myRigidbody.constraints = RigidbodyConstraints.FreezeRotation;

        }
    }

    public void HoverIn()
    {
        graphicalElement.GetComponent<Renderer>().materials[whichMaterialSwitch] = switchMaterial;
        myAudioSource.PlayOneShot(hoverInAudio);
        onHover = true;
    }

    public void HoverOut()
    {
        graphicalElement.GetComponent<Renderer>().materials[whichMaterialSwitch] = _myMaterial;
        myAudioSource.PlayOneShot(hoverOutAudio);
        onHover = false;
    }
}