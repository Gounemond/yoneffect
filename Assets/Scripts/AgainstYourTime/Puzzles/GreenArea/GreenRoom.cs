﻿using UnityEngine;
using System.Collections;
using System;
using VRStandardAssets.Utils;


public class GreenRoom : MonoBehaviour
{
    public HologramRoom[] hologramRooms;
    public PanelActivator[] panelActivator;
    public GeneratorActivator generator;
    public GreenSequencerPuzzle GreenPuzzleManager;

    public float timeTurnOnRoom;
    public float timeBetweenSequence;

    public event Action GeneratorActivated;                   // This event is triggered when the target needs to be removed.

    private bool m_isSequencePlaying = false;

    // Use this for initialization
    void Start ()
    {
        for (int i = 0; i < panelActivator.Length; i++)
        {
            panelActivator[i].myVRItem.OnClick += PlaySequence;
        }
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public IEnumerator PlayHologramSequence()
    {
        m_isSequencePlaying = true;
        for (int i = 0; i < panelActivator.Length; i++)
        {
            panelActivator[i].DisableInteractivity();
        }

        for (int i = 0; i < GreenPuzzleManager.puzzleSequence[GreenPuzzleManager.currentPlayingSequence].Count; i ++)
        {
            for (int j = 0; j < panelActivator.Length; j++)
            {
                panelActivator[j].IndexPlaying = i;
            }

            hologramRooms[GreenPuzzleManager.puzzleSequence[GreenPuzzleManager.currentPlayingSequence][i]].TurnOn();
            for (int j = 0; j < panelActivator.Length; j++)
            {
                panelActivator[j].TurnOnNumber();
            }

            yield return new WaitForSeconds(timeTurnOnRoom);
            hologramRooms[GreenPuzzleManager.puzzleSequence[GreenPuzzleManager.currentPlayingSequence][i]].TurnOff();
            for (int j = 0; j < panelActivator.Length; j++)
            {
                panelActivator[j].TurnOffNumber();
            }

            yield return new WaitForSeconds(timeBetweenSequence);
        }

        m_isSequencePlaying = false;
        for (int i = 0; i < panelActivator.Length; i++)
        {
            panelActivator[i].ResetStatus();
            panelActivator[i].EnableInteractivity();
        }

    }

    void PlaySequence()
    {
        if (!m_isSequencePlaying)
        {
            StartCoroutine(PlayHologramSequence());
        }
    }

    public void EnableGenerator()
    {

    }

    public void DisableGenerator()
    {

    }
}
