// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:4720,x:33163,y:32775,varname:node_4720,prsc:2|emission-1361-OUT;n:type:ShaderForge.SFN_Color,id:5994,x:32328,y:32707,ptovrint:False,ptlb:node_5994,ptin:_node_5994,varname:node_5994,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.4449799,c2:0,c3:0.9926471,c4:1;n:type:ShaderForge.SFN_Bitangent,id:1232,x:32095,y:32984,varname:node_1232,prsc:2;n:type:ShaderForge.SFN_Blend,id:6399,x:32598,y:32829,varname:node_6399,prsc:2,blmd:17,clmp:True|SRC-5994-RGB,DST-3462-OUT;n:type:ShaderForge.SFN_Frac,id:1361,x:32839,y:32783,varname:node_1361,prsc:2|IN-6399-OUT;n:type:ShaderForge.SFN_Add,id:3462,x:32299,y:32909,varname:node_3462,prsc:2|A-4554-UVOUT,B-1232-OUT;n:type:ShaderForge.SFN_Parallax,id:4554,x:32109,y:32808,varname:node_4554,prsc:2|UVIN-2617-UVOUT,HEI-1283-A,DEP-9027-OUT;n:type:ShaderForge.SFN_Color,id:1283,x:31799,y:32871,ptovrint:False,ptlb:node_1283,ptin:_node_1283,varname:node_1283,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0.1310344,c3:1,c4:1;n:type:ShaderForge.SFN_TexCoord,id:2617,x:31851,y:32697,varname:node_2617,prsc:2,uv:0;n:type:ShaderForge.SFN_Vector1,id:9027,x:31860,y:33061,varname:node_9027,prsc:2,v1:1;proporder:5994-1283;pass:END;sub:END;*/

Shader "Custom/timemachine_fracture" {
    Properties {
        _node_5994 ("node_5994", Color) = (0.4449799,0,0.9926471,1)
        _node_1283 ("node_1283", Color) = (0,0.1310344,1,1)
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _node_5994;
            uniform float4 _node_1283;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 bitangentDir : TEXCOORD4;
                UNITY_FOG_COORDS(5)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float3 emissive = frac(saturate(abs(_node_5994.rgb-(float3((1.0*(_node_1283.a - 0.5)*mul(tangentTransform, viewDirection).xy + i.uv0).rg,0.0)+i.bitangentDir))));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
