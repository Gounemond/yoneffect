using UnityEngine;
using System.Collections;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System;
using System.Linq;

public class TimerPlateTrigger : MonoBehaviour {

	public YonBehaviour[] obj;
    public float timerCountdown = 10f;

	private int triggerCounter = 0;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	}
	
	void OnTriggerEnter(Collider ObjectCollision)
	{

		if(triggerCounter == 0)
		{
			for (int i = 0; i < obj.Length; i++)
			{
				obj[i].Activate();
			}
		}

		triggerCounter++;
	}
	
	void OnTriggerExit(Collider ObjectCollision)
	{
		if(triggerCounter == 1)
		{
			for (int i = 0; i < obj.Length; i++)
			{
                StartCoroutine(DeactivateCountdown());
			}
		}
		triggerCounter--;
	}

    public IEnumerator DeactivateCountdown()
    {
        float time = 0;

        while (time < timerCountdown)
        {
            time += Time.deltaTime;
            yield return null;
        }

        for (int i = 0; i < obj.Length; i++)
        {
            obj[i].deActivate();
        }
    }
	
}
