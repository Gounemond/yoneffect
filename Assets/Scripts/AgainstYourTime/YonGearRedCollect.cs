﻿using UnityEngine;
using System.Collections;

public class YonGearRedCollect : MonoBehaviour {

    public AudioSource pickupAudio;                 // AudioSource used to play the pickUp sound
    public GameObject indicatorArrow;               // Indicator arrow object
    public GameObject indicatorAura;                // Indicator aura
    public Material turnedOffArrow;                     
    public Material turnedOffAura;                     
    private Color m_OriginalColor;                  
    private YonEffectJRController gamestate;        // Reference to the main gameController


	// Use this for initialization
	void Start ()
    {
        // If, for some reason, I forgot to assign the variable, goes and gets it by himself
        if (pickupAudio == null)
        {
            pickupAudio = GameObject.Find("PickUpYonGearAudioSource").GetComponent<AudioSource>();
        }

        // Binds to the controller
        gamestate = FindObjectOfType<YonEffectJRController>().GetComponent<YonEffectJRController>();
	}


    public void OnTriggerEnter(Collider other)
    {
        // If the player picked up the YonGear
        if (other.tag == "Player")
        {
            gamestate.numberGEARSCollected++;       // Increases the YonGear count
            pickupAudio.Play();                     // Plays the pick up audio
            indicatorArrow.GetComponent<Renderer>().material = turnedOffArrow;
            indicatorAura.GetComponent<Renderer>().material = turnedOffAura;
            transform.gameObject.SetActive(false);  // Make the YonGear disappear
        }        
    }
}
