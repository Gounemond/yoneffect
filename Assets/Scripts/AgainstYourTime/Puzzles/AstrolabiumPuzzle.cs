﻿using UnityEngine;
using System.Collections;
using System;
using DG.Tweening;

public class AstrolabiumPuzzle : PuzzleManager
{
    [Header ("Parameters")]
    public float rotationSpeed = 100;                           // Rotating speed of floor and ceiling

    [Header ("Puzzle Inputs")]
    public AstrolabiumCollider lowerClockwiseArrow;             // Arrow to rotate the floor clockwise
    public AstrolabiumCollider lowerCounterclockwiseArrow;      // Arrow to rotate the floor counterclockwise
    public AstrolabiumCollider upperClockwiseArrow;             // Arrow to rotate the ceiling clockwise
    public AstrolabiumCollider upperCounterclockwiseArrow;      // Arrow to rotate the ceiling counterclockwise

    [Header ("Puzzle Components")]
    public LaserTriggerOne[] lasersGenerators;                  // All the laser generators on the ceiling
    public Transform lowerSection;                              // Lower section to rotate
    public Transform upperSection;                              // Upper section to rotate

    [Header("Puzzle Rewards")]
    public Animator sunAnimator;
    public Transform yonGear;
    public Transform yonGear_Destination;
    public AudioSource successAudio;                            // AudioSource to play when winning

    private int lasersCount = 0;                                // How many lasers there are in total
    private int lasersFreed = 0;                                // How many lasers are actually reaching their receiver

    private Vector3 m_initialFloorPos;
    private Quaternion m_initialFloorRot;
    private Vector3 m_initialCeilingPos;
    private Quaternion m_initialCeilingRot;
    private float m_acceleration = 0.3f;

    // Use this for initialization
    void Awake ()
    {
        lasersCount = lasersGenerators.Length;
        lasersFreed = lasersCount;

        // Saving the initial positions / rotations of the elements 
        m_initialFloorPos = lowerSection.transform.position;
        m_initialFloorRot = lowerSection.transform.rotation;
        m_initialCeilingPos = upperSection.transform.position;
        m_initialCeilingRot = upperSection.transform.rotation;

    }

    void OnEnable()
    {
        for (int i = 0; i < lasersCount; i++)
        {
            lasersGenerators[i].onTriggerEnter += laserInterrupted;
            lasersGenerators[i].onTriggerExit += laserFreed;
        }

        lowerClockwiseArrow.elementActivated += lowerRotateRight;
        lowerCounterclockwiseArrow.elementActivated += lowerRotateLeft;
        upperClockwiseArrow.elementActivated += upperRotateRight;
        upperCounterclockwiseArrow.elementActivated += upperRotateLeft;
        lowerClockwiseArrow.onTriggerExit += resetAcceleration;
        lowerCounterclockwiseArrow.onTriggerExit += resetAcceleration;
        upperClockwiseArrow.onTriggerExit += resetAcceleration;
        upperCounterclockwiseArrow.onTriggerExit += resetAcceleration;
    }

    void OnDisable()
    {
        for (int i = 0; i < lasersCount; i++)
        {
            lasersGenerators[i].onTriggerEnter -= laserInterrupted;
            lasersGenerators[i].onTriggerExit -= laserFreed;
        }

        lowerClockwiseArrow.elementActivated -= lowerRotateRight;
        lowerCounterclockwiseArrow.elementActivated -= lowerRotateLeft;
        upperClockwiseArrow.elementActivated -= upperRotateRight;
        upperCounterclockwiseArrow.elementActivated -= upperRotateLeft;
        lowerClockwiseArrow.onTriggerExit -= resetAcceleration;
        lowerCounterclockwiseArrow.onTriggerExit -= resetAcceleration;
        upperClockwiseArrow.onTriggerExit -= resetAcceleration;
        upperCounterclockwiseArrow.onTriggerExit -= resetAcceleration;
    }
	
	// Update is called once per frame
	void Update ()
    {

	}

    public void laserInterrupted()
    {
        lasersFreed--;

    }

    public void laserFreed()
    {
        lasersFreed++;
        if (lasersFreed == lasersCount)
        {
            StartCoroutine(TimerWaitToWin());
        }
    }

    public void upperRotateLeft()
    {
        upperSection.Rotate(0, 0, -Time.deltaTime * rotationSpeed * m_acceleration);
        increaseAcceleration();
    }

    public void upperRotateRight()
    {
        upperSection.Rotate(0, 0, Time.deltaTime * rotationSpeed * m_acceleration);

        increaseAcceleration();
    }

    public void lowerRotateLeft()
    {
        lowerSection.Rotate(0, 0, -Time.deltaTime * rotationSpeed * m_acceleration);
        increaseAcceleration();
    }

    public void lowerRotateRight()
    {
        lowerSection.Rotate(0, 0, Time.deltaTime * rotationSpeed * m_acceleration);
        increaseAcceleration();
    }

    public void resetAcceleration()
    {
        m_acceleration = 0.1f;
    }

    public void increaseAcceleration()
    {
        if (m_acceleration < 3)
        {
            m_acceleration += 0.3f * Time.deltaTime;
        }
    }

    public override void ResetPuzzle()
    {
        lowerSection.transform.position = m_initialFloorPos;
        lowerSection.transform.rotation = m_initialFloorRot;
        upperSection.transform.position = m_initialCeilingPos;
        upperSection.transform.rotation = m_initialCeilingRot;
    }

    public override void CompletePuzzle()
    {
        throw new NotImplementedException();
    }

    private IEnumerator TimerWaitToWin()
    {
        float timer = 0;

        while (timer < 1.5f && lasersFreed == lasersCount)
        {
            timer += Time.deltaTime;
            yield return null;
        }

        if (lasersFreed == lasersCount)
        {
            sunAnimator.SetBool("HasWon", true);
            successAudio.Play();
            yonGear.DOMove(yonGear_Destination.position, 8);
        }
        else
        {
            timer = 0;
            yield break;
        }

    }
}
