using UnityEngine;
using System.Collections;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System;
using System.Linq;


public class FootMoving : MonoBehaviour 
{
    public AudioSource footAudioSource;
    public AudioClip footStep1;
	float bias = 0.0f;

	
	// Use this for initialization
	void Start () 
	{

	}

    void OnTriggerEnter(Collider other)
    {
        footAudioSource.pitch = UnityEngine.Random.Range(0.98f, 1.02f);
        footAudioSource.PlayOneShot(footStep1);
        Debug.Log("Orca!");
    }

    public void UpdateFootStep(float timeMultiplier)
    {
        transform.localPosition += new Vector3(0, 0.5f*Mathf.Sin(bias) * timeMultiplier, 0);
        bias = bias + 1f * timeMultiplier;
    }
}
