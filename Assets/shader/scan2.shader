// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:2,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:False,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:8529,x:33066,y:32714,varname:node_8529,prsc:2|emission-1122-OUT,alpha-6359-OUT;n:type:ShaderForge.SFN_Color,id:1132,x:32421,y:32708,ptovrint:False,ptlb:node_1132,ptin:_node_1132,varname:node_1132,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Multiply,id:5731,x:32795,y:32727,varname:node_5731,prsc:2|A-1132-RGB,B-7367-OUT;n:type:ShaderForge.SFN_Slider,id:7367,x:32384,y:32856,ptovrint:False,ptlb:node_7367,ptin:_node_7367,varname:node_7367,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:10,max:10;n:type:ShaderForge.SFN_Slider,id:6359,x:32369,y:32955,ptovrint:False,ptlb:node_6359,ptin:_node_6359,varname:node_6359,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-1,cur:0.04273504,max:1;n:type:ShaderForge.SFN_Fresnel,id:3883,x:32570,y:33133,varname:node_3883,prsc:2|NRM-3659-OUT,EXP-5907-OUT;n:type:ShaderForge.SFN_NormalVector,id:3659,x:32161,y:33074,prsc:2,pt:False;n:type:ShaderForge.SFN_OneMinus,id:804,x:32762,y:33086,varname:node_804,prsc:2|IN-3883-OUT;n:type:ShaderForge.SFN_Add,id:1122,x:32900,y:33048,varname:node_1122,prsc:2|A-5731-OUT,B-804-OUT;n:type:ShaderForge.SFN_Slider,id:5907,x:32289,y:33245,ptovrint:False,ptlb:node_5907,ptin:_node_5907,varname:node_5907,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-1,cur:5,max:5;proporder:1132-7367-6359-5907;pass:END;sub:END;*/

Shader "Custom/scan2" {
    Properties {
        _node_1132 ("node_1132", Color) = (1,1,1,1)
        _node_7367 ("node_7367", Range(0, 10)) = 10
        _node_6359 ("node_6359", Range(-1, 1)) = 0.04273504
        _node_5907 ("node_5907", Range(-1, 5)) = 5
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            Cull Off
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _node_1132;
            uniform float _node_7367;
            uniform float _node_6359;
            uniform float _node_5907;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 posWorld : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
                UNITY_FOG_COORDS(2)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                i.normalDir = normalize(i.normalDir);
                i.normalDir *= faceSign;
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float3 emissive = ((_node_1132.rgb*_node_7367)+(1.0 - pow(1.0-max(0,dot(i.normalDir, viewDirection)),_node_5907)));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,_node_6359);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
