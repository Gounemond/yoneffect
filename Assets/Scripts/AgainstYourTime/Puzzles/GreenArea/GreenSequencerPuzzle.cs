﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using DG.Tweening;

public class GreenSequencerPuzzle : PuzzleManager
{

    public GreenRoom[] greenRooms;
    public int sequenceLength = 5;

    public AudioClip successAudio;
    public GameObject yonGearPrefab;

    [HideInInspector]
    public List<int>[] puzzleSequence;

    [HideInInspector]
    public int currentPlayingSequence = 0;

    [HideInInspector]
    public int sequenceCounter = 0;

    private bool m_GreenPuzzleCompleted = false;

    private bool m_WaitCorrectGenerator = false;


    // Use this for initialization
    public IEnumerator Start ()
    {
        // Declaring four sequences and generating them
        puzzleSequence = new List<int>[3];
        GenerateSequences();

        // For all the sequences

        // Initialize the current playing sequence
        currentPlayingSequence = 0;

        while (currentPlayingSequence < puzzleSequence.Length && !m_GreenPuzzleCompleted)
        {
            // Starting from the first element
            sequenceCounter = 0;

            // Until the sequence is finished
            while (sequenceCounter < puzzleSequence[currentPlayingSequence].Count && !m_GreenPuzzleCompleted)
            {
                // Determining the next correct generator
                //float pitchRooms = 0.85f;


                // Iterating through all the rooms to assign which one correspond to the correct step in the sequence
                for (int j = 0; j < greenRooms.Length; j++)
                {
                    // If the room is the correct one in the sequence, set his generator as the correct one, while setting the others as the wrong ones
                    if (puzzleSequence[currentPlayingSequence][sequenceCounter] == j)
                    {
                        greenRooms[j].generator.onGeneratorActivate += greenRooms[j].generator.CorrectActivation;       // Graphical appearance of the correct activation
                        greenRooms[j].generator.onGeneratorActivate += CorrectGeneratorActivated;                       // Data method of the correct activation
                    }
                    else
                    {
                        greenRooms[j].generator.onGeneratorActivate += greenRooms[j].generator.WrongActivation;         // Graphical appearance of the wrong activation
                    }

                    greenRooms[j].generator.EnableGenerator();                      // Enable the generator in the greenroom (applying his interaction methods
                }

                // Once all the generator are ready, wait until the player select the correct one
                m_WaitCorrectGenerator = false;
                while (!m_WaitCorrectGenerator)
                {
                    yield return null;
                }

                // When the player activates the correct generator, remove all the methods from the generators
                for (int j = 0; j < greenRooms.Length; j++)
                {
                    if (puzzleSequence[currentPlayingSequence][sequenceCounter] == j)
                    {
                        greenRooms[j].generator.onGeneratorActivate -= greenRooms[j].generator.CorrectActivation;
                        greenRooms[j].generator.onGeneratorActivate -= CorrectGeneratorActivated;
                    }
                    else
                    {
                        greenRooms[j].generator.onGeneratorActivate -= greenRooms[j].generator.WrongActivation;
                    }
                }

                // Evaluate the next step of the sequence
                NextStepSequence();
                
            }

            yield return new WaitForSeconds(0.5f);

            if (!m_GreenPuzzleCompleted)
            {
                // Spawn two YonGears to give as a reward to the player
                GameObject yonGear1 = (GameObject)Instantiate(yonGearPrefab, greenRooms[puzzleSequence[currentPlayingSequence - 1][sequenceCounter - 1]].generator.spawnYonGear.position, Quaternion.identity);
                GameObject yonGear2 = (GameObject)Instantiate(yonGearPrefab, greenRooms[puzzleSequence[currentPlayingSequence - 1][sequenceCounter - 1]].generator.spawnYonGear.position, Quaternion.identity);

                // Handles scaling and animation of spawning and moving
                yonGear1.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
                yonGear2.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);

                yonGear1.transform.DOMove(greenRooms[puzzleSequence[currentPlayingSequence - 1][sequenceCounter - 1]].generator.destionationYonGear1.position, 3);
                yonGear2.transform.DOMove(greenRooms[puzzleSequence[currentPlayingSequence - 1][sequenceCounter - 1]].generator.destionationYonGear2.position, 3);
                yonGear1.transform.DOScale(1.25f, 2);
                yonGear2.transform.DOScale(1.25f, 2);

                // Plays victorious audio
                greenRooms[puzzleSequence[currentPlayingSequence - 1][sequenceCounter - 1]].generator.audioEmitter.PlayOneShot(successAudio);
            }
            else
            {
                GameObject yonGear1 = (GameObject)Instantiate(yonGearPrefab, greenRooms[puzzleSequence[currentPlayingSequence][sequenceCounter - 1]].generator.spawnYonGear.position, Quaternion.identity);
                GameObject yonGear2 = (GameObject)Instantiate(yonGearPrefab, greenRooms[puzzleSequence[currentPlayingSequence][sequenceCounter - 1]].generator.spawnYonGear.position, Quaternion.identity);

                // Handles scaling and animation of spawning and moving
                yonGear1.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
                yonGear2.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);

                yonGear1.transform.DOMove(greenRooms[puzzleSequence[currentPlayingSequence][sequenceCounter - 1]].generator.destionationYonGear1.position, 3);
                yonGear2.transform.DOMove(greenRooms[puzzleSequence[currentPlayingSequence][sequenceCounter - 1]].generator.destionationYonGear2.position, 3);
                yonGear1.transform.DOScale(1.25f, 2);
                yonGear2.transform.DOScale(1.25f, 2);

                // Plays victorious audio
                greenRooms[puzzleSequence[currentPlayingSequence][sequenceCounter - 1]].generator.audioEmitter.PlayOneShot(successAudio);
            }
            for (int j = 0; j < greenRooms.Length; j++)
            {
                for (int k = 0; k < greenRooms[j].panelActivator.Length; k++)
                {
                    greenRooms[j].panelActivator[k].SetActiveGears(currentPlayingSequence * 2);
                }
            }

                // Spawn two YonGears to give as a reward to the player

        }

        for (int i = 0; i < greenRooms.Length; i++)
        {
            for (int k = 0; k < greenRooms[i].panelActivator.Length; k++)
            {
                greenRooms[i].panelActivator[k].DisableInteractivity();
                greenRooms[i].panelActivator[k].Disappear();
            }
            greenRooms[i].generator.DisableGenerator();
        }
        
    }

    public void GenerateSequences()
    {
        int lastNumber = -1;
        int temp;

        for (int i = 0; i < puzzleSequence.Length; i++)
        {
            puzzleSequence[i] = new List<int>();

            for (int j = 0; j < sequenceLength; j++)
            {
                do
                {
                    temp = UnityEngine.Random.Range(0, greenRooms.Length);
                } while (temp == lastNumber);
                lastNumber = temp;

                puzzleSequence[i].Add(temp);
            }
        }

    }

    public void CorrectGeneratorActivated()
    {
        m_WaitCorrectGenerator = true;
    }

    public void NextStepSequence()
    {
        sequenceCounter++;                      // Increase the progress in the sequence

        if (sequenceCounter >= sequenceLength)
        {
            // Give the YonGears;
            currentPlayingSequence++;
        }

        if (currentPlayingSequence >= puzzleSequence.Length)
        {
            m_GreenPuzzleCompleted = true;
            currentPlayingSequence--;
            // Deactivate the puzzle
        }

    }

    public override void ResetPuzzle()
    {
        throw new NotImplementedException();
    }

    public override void CompletePuzzle()
    {
        throw new NotImplementedException();
    }
}
