// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:6546,x:33095,y:32275,varname:node_6546,prsc:2|emission-531-OUT,olwid-9344-OUT,olcol-604-RGB;n:type:ShaderForge.SFN_Tex2d,id:1334,x:32218,y:32739,ptovrint:False,ptlb:node_1334,ptin:_node_1334,varname:node_1334,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:9b038171a9a5d6c45a483bbb484c9e6f,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Lerp,id:531,x:32561,y:32554,varname:node_531,prsc:2|A-5861-OUT,B-8955-OUT,T-1334-RGB;n:type:ShaderForge.SFN_Color,id:8719,x:31491,y:32142,ptovrint:False,ptlb:node_8719,ptin:_node_8719,varname:node_8719,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0,c3:1,c4:1;n:type:ShaderForge.SFN_Multiply,id:8955,x:32048,y:32499,varname:node_8955,prsc:2|A-2617-OUT,B-8195-OUT;n:type:ShaderForge.SFN_Lerp,id:2617,x:31818,y:32369,varname:node_2617,prsc:2|A-2743-OUT,B-9066-OUT,T-4253-OUT;n:type:ShaderForge.SFN_Multiply,id:9066,x:31578,y:32429,varname:node_9066,prsc:2|A-5210-RGB,B-3515-OUT;n:type:ShaderForge.SFN_Color,id:5210,x:31343,y:32319,ptovrint:False,ptlb:node_5210,ptin:_node_5210,varname:node_5210,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.2449606,c2:0.9000682,c3:1,c4:1;n:type:ShaderForge.SFN_Vector1,id:3515,x:31356,y:32494,varname:node_3515,prsc:2,v1:2;n:type:ShaderForge.SFN_OneMinus,id:4253,x:31657,y:32554,varname:node_4253,prsc:2|IN-7069-OUT;n:type:ShaderForge.SFN_Fresnel,id:7069,x:31456,y:32595,varname:node_7069,prsc:2|NRM-6726-OUT,EXP-2231-OUT;n:type:ShaderForge.SFN_NormalVector,id:6726,x:31203,y:32565,prsc:2,pt:False;n:type:ShaderForge.SFN_Vector1,id:2231,x:31337,y:32721,varname:node_2231,prsc:2,v1:3;n:type:ShaderForge.SFN_Multiply,id:1128,x:31671,y:32751,varname:node_1128,prsc:2|A-2827-T,B-8683-OUT;n:type:ShaderForge.SFN_Time,id:2827,x:31512,y:32708,varname:node_2827,prsc:2;n:type:ShaderForge.SFN_Slider,id:8683,x:31406,y:32919,ptovrint:False,ptlb:node_8683,ptin:_node_8683,varname:node_8683,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.2886472,max:2;n:type:ShaderForge.SFN_Sin,id:5915,x:31846,y:32751,varname:node_5915,prsc:2|IN-1128-OUT;n:type:ShaderForge.SFN_Blend,id:8195,x:32011,y:32778,varname:node_8195,prsc:2,blmd:7,clmp:True|SRC-5915-OUT,DST-3279-OUT;n:type:ShaderForge.SFN_Vector1,id:3279,x:31853,y:32939,varname:node_3279,prsc:2,v1:1;n:type:ShaderForge.SFN_Parallax,id:6475,x:31283,y:32178,varname:node_6475,prsc:2|HEI-7019-A,DEP-454-OUT;n:type:ShaderForge.SFN_Add,id:2743,x:31631,y:32233,varname:node_2743,prsc:2|A-8719-RGB,B-6475-UVOUT;n:type:ShaderForge.SFN_Tex2d,id:7019,x:31046,y:32231,ptovrint:False,ptlb:node_7019,ptin:_node_7019,varname:node_7019,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:9b038171a9a5d6c45a483bbb484c9e6f,ntxv:0,isnm:False;n:type:ShaderForge.SFN_TexCoord,id:5486,x:30846,y:32205,varname:node_5486,prsc:2,uv:0;n:type:ShaderForge.SFN_Slider,id:454,x:30965,y:32415,ptovrint:False,ptlb:node_454,ptin:_node_454,varname:node_454,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Blend,id:5861,x:32436,y:31858,varname:node_5861,prsc:2,blmd:7,clmp:True|SRC-11-OUT,DST-6188-OUT;n:type:ShaderForge.SFN_Add,id:6188,x:31897,y:31841,varname:node_6188,prsc:2|A-288-OUT,B-5308-RGB;n:type:ShaderForge.SFN_Power,id:11,x:32202,y:31639,varname:node_11,prsc:2|VAL-6188-OUT,EXP-7039-OUT;n:type:ShaderForge.SFN_Vector1,id:7039,x:32085,y:31809,varname:node_7039,prsc:2,v1:5;n:type:ShaderForge.SFN_Multiply,id:288,x:31680,y:31675,varname:node_288,prsc:2|A-8200-OUT,B-1591-OUT;n:type:ShaderForge.SFN_Multiply,id:8200,x:31504,y:31593,varname:node_8200,prsc:2|A-640-RGB,B-1591-OUT;n:type:ShaderForge.SFN_Color,id:640,x:31289,y:31411,ptovrint:False,ptlb:node_640,ptin:_node_640,varname:node_640,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0.07417818,c3:0.3602941,c4:1;n:type:ShaderForge.SFN_Fresnel,id:1591,x:31305,y:31710,varname:node_1591,prsc:2|NRM-8178-OUT,EXP-2252-OUT;n:type:ShaderForge.SFN_ValueProperty,id:2252,x:31102,y:31737,ptovrint:False,ptlb:fresnel,ptin:_fresnel,varname:node_2252,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.5;n:type:ShaderForge.SFN_NormalVector,id:8178,x:31072,y:31541,prsc:2,pt:False;n:type:ShaderForge.SFN_Color,id:5308,x:31165,y:31834,ptovrint:False,ptlb:node_5308,ptin:_node_5308,varname:node_5308,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.07094117,c2:0.3066004,c3:0.4926471,c4:1;n:type:ShaderForge.SFN_Color,id:604,x:32867,y:32652,ptovrint:False,ptlb:node_604,ptin:_node_604,varname:node_604,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0,c3:1,c4:1;n:type:ShaderForge.SFN_ValueProperty,id:9344,x:32862,y:32544,ptovrint:False,ptlb:outline,ptin:_outline,varname:node_9344,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;proporder:1334-8719-5210-8683-7019-454-640-2252-5308-604-9344;pass:END;sub:END;*/

Shader "Custom/BlueStand 1" {
    Properties {
        _node_1334 ("node_1334", 2D) = "white" {}
        _node_8719 ("node_8719", Color) = (0,0,1,1)
        _node_5210 ("node_5210", Color) = (0.2449606,0.9000682,1,1)
        _node_8683 ("node_8683", Range(0, 2)) = 0.2886472
        _node_7019 ("node_7019", 2D) = "white" {}
        _node_454 ("node_454", Range(0, 1)) = 1
        _node_640 ("node_640", Color) = (0,0.07417818,0.3602941,1)
        _fresnel ("fresnel", Float ) = 0.5
        _node_5308 ("node_5308", Color) = (0.07094117,0.3066004,0.4926471,1)
        _node_604 ("node_604", Color) = (0,0,1,1)
        _outline ("outline", Float ) = 0
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "Outline"
            Tags {
            }
            Cull Front
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _node_604;
            uniform float _outline;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                UNITY_FOG_COORDS(0)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.pos = mul(UNITY_MATRIX_MVP, float4(v.vertex.xyz + v.normal*_outline,1) );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                return fixed4(_node_604.rgb,0);
            }
            ENDCG
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform sampler2D _node_1334; uniform float4 _node_1334_ST;
            uniform float4 _node_8719;
            uniform float4 _node_5210;
            uniform float _node_8683;
            uniform sampler2D _node_7019; uniform float4 _node_7019_ST;
            uniform float _node_454;
            uniform float4 _node_640;
            uniform float _fresnel;
            uniform float4 _node_5308;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 bitangentDir : TEXCOORD4;
                UNITY_FOG_COORDS(5)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float node_1591 = pow(1.0-max(0,dot(i.normalDir, viewDirection)),_fresnel);
                float3 node_6188 = (((_node_640.rgb*node_1591)*node_1591)+_node_5308.rgb);
                float4 _node_7019_var = tex2D(_node_7019,TRANSFORM_TEX(i.uv0, _node_7019));
                float4 node_2827 = _Time + _TimeEditor;
                float4 _node_1334_var = tex2D(_node_1334,TRANSFORM_TEX(i.uv0, _node_1334));
                float3 emissive = lerp(saturate((node_6188/(1.0-pow(node_6188,5.0)))),(lerp((_node_8719.rgb+float3((_node_454*(_node_7019_var.a - 0.5)*mul(tangentTransform, viewDirection).xy + i.uv0).rg,0.0)),(_node_5210.rgb*2.0),(1.0 - pow(1.0-max(0,dot(i.normalDir, viewDirection)),3.0)))*saturate((1.0/(1.0-sin((node_2827.g*_node_8683)))))),_node_1334_var.rgb);
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
