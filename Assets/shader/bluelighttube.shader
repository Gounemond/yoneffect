// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:1783,x:33898,y:32902,varname:node_1783,prsc:2|emission-8252-OUT,transm-6771-OUT,lwrap-6771-OUT;n:type:ShaderForge.SFN_Multiply,id:11,x:33120,y:33124,varname:node_11,prsc:2|A-4552-RGB,B-6621-OUT,C-6771-OUT;n:type:ShaderForge.SFN_Vector1,id:6621,x:32850,y:33302,varname:node_6621,prsc:2,v1:5;n:type:ShaderForge.SFN_Color,id:4552,x:32810,y:33001,ptovrint:False,ptlb:node_9323_copy,ptin:_node_9323_copy,varname:_node_9323_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.2313726,c2:0.9803922,c3:0.7215686,c4:1;n:type:ShaderForge.SFN_Power,id:6771,x:32628,y:33177,varname:node_6771,prsc:2|VAL-6620-OUT,EXP-6115-OUT;n:type:ShaderForge.SFN_Multiply,id:1721,x:33101,y:33398,varname:node_1721,prsc:2|A-6771-OUT,B-425-OUT,C-2833-OUT;n:type:ShaderForge.SFN_NormalVector,id:2833,x:32875,y:33528,prsc:2,pt:False;n:type:ShaderForge.SFN_Vector1,id:425,x:32875,y:33452,varname:node_425,prsc:2,v1:0.2;n:type:ShaderForge.SFN_Multiply,id:6620,x:32436,y:33102,varname:node_6620,prsc:2|A-1412-OUT,B-3111-OUT;n:type:ShaderForge.SFN_Vector1,id:6115,x:32432,y:33303,varname:node_6115,prsc:2,v1:2;n:type:ShaderForge.SFN_Abs,id:1412,x:32295,y:32996,varname:node_1412,prsc:2|IN-8569-OUT;n:type:ShaderForge.SFN_Vector1,id:3111,x:32295,y:33211,varname:node_3111,prsc:2,v1:1.5;n:type:ShaderForge.SFN_Subtract,id:8569,x:32133,y:32961,varname:node_8569,prsc:2|A-8304-OUT,B-9995-OUT;n:type:ShaderForge.SFN_Vector1,id:9995,x:31969,y:33087,varname:node_9995,prsc:2,v1:0.5;n:type:ShaderForge.SFN_Frac,id:8304,x:31911,y:32853,varname:node_8304,prsc:2|IN-5521-OUT;n:type:ShaderForge.SFN_ComponentMask,id:5521,x:31641,y:32797,varname:node_5521,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-5324-UVOUT;n:type:ShaderForge.SFN_Panner,id:5324,x:31452,y:32720,varname:node_5324,prsc:2,spu:0.25,spv:0|UVIN-6825-UVOUT;n:type:ShaderForge.SFN_TexCoord,id:6825,x:31200,y:32660,varname:node_6825,prsc:2,uv:0;n:type:ShaderForge.SFN_Color,id:2855,x:32928,y:32786,ptovrint:False,ptlb:node_2855,ptin:_node_2855,varname:node_2855,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0,c3:1,c4:1;n:type:ShaderForge.SFN_Lerp,id:8252,x:33197,y:32856,varname:node_8252,prsc:2|A-2855-RGB,B-11-OUT,T-6771-OUT;proporder:4552-2855;pass:END;sub:END;*/

Shader "Custom/bluelighttube" {
    Properties {
        _node_9323_copy ("node_9323_copy", Color) = (0.2313726,0.9803922,0.7215686,1)
        _node_2855 ("node_2855", Color) = (0,0,1,1)
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform float4 _node_9323_copy;
            uniform float4 _node_2855;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                UNITY_FOG_COORDS(1)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
////// Lighting:
////// Emissive:
                float4 node_1424 = _Time + _TimeEditor;
                float node_6771 = pow((abs((frac((i.uv0+node_1424.g*float2(0.25,0)).r)-0.5))*1.5),2.0);
                float3 emissive = lerp(_node_2855.rgb,(_node_9323_copy.rgb*5.0*node_6771),node_6771);
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
