﻿using UnityEngine;
using System.Collections;

namespace UI.StepsVR
{
    [RequireComponent(typeof(BoxCollider))]
    public abstract class Pulsante : MonoBehaviour
    {


        private SpriteRenderer spRenderer;

        public Sprite spriteNormale;
        public Sprite spriteOver;

        // Use this for initialization
        void Start()
        {
            spRenderer = this.GetComponent<SpriteRenderer>();
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void OnMouseEnter()
        {
            spRenderer.sprite = spriteOver;
        }

        public void OnMouseExit()
        {
            spRenderer.sprite = spriteNormale;
        }

        public void OnMouseUp()
        {
            azione_click();
        }


        public abstract void azione_click();
    }
}