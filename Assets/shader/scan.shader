// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:1,bsrc:0,bdst:1,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:False,igpj:False,qofs:0,qpre:2,rntp:3,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:7595,x:33858,y:32653,varname:node_7595,prsc:2|emission-6479-OUT,alpha-2513-OUT,disp-1617-OUT,tess-659-OUT;n:type:ShaderForge.SFN_Reflect,id:1617,x:33615,y:32972,varname:node_1617,prsc:2|A-9443-OUT,B-5307-OUT;n:type:ShaderForge.SFN_Reflect,id:9443,x:33416,y:32912,varname:node_9443,prsc:2|A-5307-OUT,B-2442-OUT;n:type:ShaderForge.SFN_Multiply,id:5307,x:33153,y:33037,varname:node_5307,prsc:2|A-9875-OUT,B-2442-OUT;n:type:ShaderForge.SFN_Vector1,id:659,x:33443,y:33139,varname:node_659,prsc:2,v1:8;n:type:ShaderForge.SFN_Multiply,id:9875,x:33132,y:32805,varname:node_9875,prsc:2|A-7136-OUT,B-8840-OUT;n:type:ShaderForge.SFN_Desaturate,id:7136,x:33222,y:32688,varname:node_7136,prsc:2|COL-5890-OUT;n:type:ShaderForge.SFN_Reflect,id:5890,x:33088,y:32674,varname:node_5890,prsc:2|A-2308-RGB,B-2442-OUT;n:type:ShaderForge.SFN_NormalVector,id:2442,x:32816,y:32970,prsc:2,pt:False;n:type:ShaderForge.SFN_Slider,id:8840,x:32577,y:32857,ptovrint:False,ptlb:node_8840,ptin:_node_8840,varname:node_8840,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.9116571,max:1;n:type:ShaderForge.SFN_Append,id:6549,x:32656,y:32570,varname:node_6549,prsc:2|A-8848-OUT,B-3030-OUT;n:type:ShaderForge.SFN_Add,id:3030,x:32482,y:32582,varname:node_3030,prsc:2|A-6801-OUT,B-907-V;n:type:ShaderForge.SFN_TexCoord,id:907,x:32290,y:32532,varname:node_907,prsc:2,uv:0;n:type:ShaderForge.SFN_Add,id:491,x:32509,y:32397,varname:node_491,prsc:2|A-4544-OUT,B-907-U;n:type:ShaderForge.SFN_Multiply,id:6801,x:32404,y:32735,varname:node_6801,prsc:2|A-3859-T,B-4409-OUT;n:type:ShaderForge.SFN_Slider,id:4409,x:32198,y:32854,ptovrint:False,ptlb:node_4409,ptin:_node_4409,varname:node_4409,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.5154346,max:1;n:type:ShaderForge.SFN_Time,id:3859,x:32092,y:32655,varname:node_3859,prsc:2;n:type:ShaderForge.SFN_Multiply,id:4544,x:32336,y:32344,varname:node_4544,prsc:2|A-4254-OUT,B-3859-T;n:type:ShaderForge.SFN_Slider,id:4254,x:32110,y:32266,ptovrint:False,ptlb:node_4254,ptin:_node_4254,varname:node_4254,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.5993428,max:1;n:type:ShaderForge.SFN_Add,id:8848,x:32758,y:32265,varname:node_8848,prsc:2|A-6542-OUT,B-491-OUT;n:type:ShaderForge.SFN_Fmod,id:6542,x:32539,y:32288,varname:node_6542,prsc:2|A-907-V,B-9523-OUT;n:type:ShaderForge.SFN_Vector1,id:9523,x:32460,y:32218,varname:node_9523,prsc:2,v1:1;n:type:ShaderForge.SFN_Tex2d,id:2308,x:32887,y:32553,ptovrint:False,ptlb:node_2308,ptin:_node_2308,varname:node_2308,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:ed022fea07aefa64192fa64c637f2690,ntxv:0,isnm:False|UVIN-6549-OUT;n:type:ShaderForge.SFN_ValueProperty,id:2513,x:33702,y:32867,ptovrint:False,ptlb:opaciti,ptin:_opaciti,varname:node_2513,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_Color,id:4874,x:33032,y:32306,ptovrint:False,ptlb:node_4874,ptin:_node_4874,varname:node_4874,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0,c3:0,c4:1;n:type:ShaderForge.SFN_Lerp,id:6479,x:33442,y:32372,varname:node_6479,prsc:2|A-4874-RGB,B-6311-RGB,T-2308-RGB;n:type:ShaderForge.SFN_Color,id:6311,x:33214,y:32235,ptovrint:False,ptlb:node_6311,ptin:_node_6311,varname:node_6311,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.07843138,c2:0.07843138,c3:0.2352941,c4:1;proporder:8840-2308-4409-4254-2513-4874-6311;pass:END;sub:END;*/

Shader "Custom/scan" {
    Properties {
        _node_8840 ("node_8840", Range(0, 1)) = 0.9116571
        _node_2308 ("node_2308", 2D) = "white" {}
        _node_4409 ("node_4409", Range(0, 1)) = 0.5154346
        _node_4254 ("node_4254", Range(0, 1)) = 0.5993428
        _opaciti ("opaciti", Float ) = 0
        _node_4874 ("node_4874", Color) = (1,0,0,1)
        _node_6311 ("node_6311", Color) = (0.07843138,0.07843138,0.2352941,1)
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "Queue"="AlphaTest"
            "RenderType"="TransparentCutout"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Cull Front
            ZWrite Off
            
            CGPROGRAM
            #pragma hull hull
            #pragma domain domain
            #pragma vertex tessvert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "Tessellation.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 5.0
            uniform float4 _TimeEditor;
            uniform float _node_8840;
            uniform float _node_4409;
            uniform float _node_4254;
            uniform sampler2D _node_2308; uniform float4 _node_2308_ST;
            uniform float _opaciti;
            uniform float4 _node_4874;
            uniform float4 _node_6311;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
                UNITY_FOG_COORDS(2)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(-v.normal);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            #ifdef UNITY_CAN_COMPILE_TESSELLATION
                struct TessVertex {
                    float4 vertex : INTERNALTESSPOS;
                    float3 normal : NORMAL;
                    float4 tangent : TANGENT;
                    float2 texcoord0 : TEXCOORD0;
                };
                struct OutputPatchConstant {
                    float edge[3]         : SV_TessFactor;
                    float inside          : SV_InsideTessFactor;
                    float3 vTangent[4]    : TANGENT;
                    float2 vUV[4]         : TEXCOORD;
                    float3 vTanUCorner[4] : TANUCORNER;
                    float3 vTanVCorner[4] : TANVCORNER;
                    float4 vCWts          : TANWEIGHTS;
                };
                TessVertex tessvert (VertexInput v) {
                    TessVertex o;
                    o.vertex = v.vertex;
                    o.normal = v.normal;
                    o.tangent = v.tangent;
                    o.texcoord0 = v.texcoord0;
                    return o;
                }
                void displacement (inout VertexInput v){
                    float4 node_3859 = _Time + _TimeEditor;
                    float2 node_6549 = float2((fmod(v.texcoord0.g,1.0)+((_node_4254*node_3859.g)+v.texcoord0.r)),((node_3859.g*_node_4409)+v.texcoord0.g));
                    float4 _node_2308_var = tex2Dlod(_node_2308,float4(TRANSFORM_TEX(node_6549, _node_2308),0.0,0));
                    float3 node_5307 = ((dot(reflect(_node_2308_var.rgb,v.normal),float3(0.3,0.59,0.11))*_node_8840)*v.normal);
                    v.vertex.xyz += reflect(reflect(node_5307,v.normal),node_5307);
                }
                float Tessellation(TessVertex v){
                    return 8.0;
                }
                float4 Tessellation(TessVertex v, TessVertex v1, TessVertex v2){
                    float tv = Tessellation(v);
                    float tv1 = Tessellation(v1);
                    float tv2 = Tessellation(v2);
                    return float4( tv1+tv2, tv2+tv, tv+tv1, tv+tv1+tv2 ) / float4(2,2,2,3);
                }
                OutputPatchConstant hullconst (InputPatch<TessVertex,3> v) {
                    OutputPatchConstant o = (OutputPatchConstant)0;
                    float4 ts = Tessellation( v[0], v[1], v[2] );
                    o.edge[0] = ts.x;
                    o.edge[1] = ts.y;
                    o.edge[2] = ts.z;
                    o.inside = ts.w;
                    return o;
                }
                [domain("tri")]
                [partitioning("fractional_odd")]
                [outputtopology("triangle_cw")]
                [patchconstantfunc("hullconst")]
                [outputcontrolpoints(3)]
                TessVertex hull (InputPatch<TessVertex,3> v, uint id : SV_OutputControlPointID) {
                    return v[id];
                }
                [domain("tri")]
                VertexOutput domain (OutputPatchConstant tessFactors, const OutputPatch<TessVertex,3> vi, float3 bary : SV_DomainLocation) {
                    VertexInput v = (VertexInput)0;
                    v.vertex = vi[0].vertex*bary.x + vi[1].vertex*bary.y + vi[2].vertex*bary.z;
                    v.normal = vi[0].normal*bary.x + vi[1].normal*bary.y + vi[2].normal*bary.z;
                    v.tangent = vi[0].tangent*bary.x + vi[1].tangent*bary.y + vi[2].tangent*bary.z;
                    v.texcoord0 = vi[0].texcoord0*bary.x + vi[1].texcoord0*bary.y + vi[2].texcoord0*bary.z;
                    displacement(v);
                    VertexOutput o = vert(v);
                    return o;
                }
            #endif
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float4 node_3859 = _Time + _TimeEditor;
                float2 node_6549 = float2((fmod(i.uv0.g,1.0)+((_node_4254*node_3859.g)+i.uv0.r)),((node_3859.g*_node_4409)+i.uv0.g));
                float4 _node_2308_var = tex2D(_node_2308,TRANSFORM_TEX(node_6549, _node_2308));
                float3 emissive = lerp(_node_4874.rgb,_node_6311.rgb,_node_2308_var.rgb);
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,_opaciti);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            
            CGPROGRAM
            #pragma hull hull
            #pragma domain domain
            #pragma vertex tessvert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #include "Tessellation.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 5.0
            uniform float4 _TimeEditor;
            uniform float _node_8840;
            uniform float _node_4409;
            uniform float _node_4254;
            uniform sampler2D _node_2308; uniform float4 _node_2308_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(-v.normal);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            #ifdef UNITY_CAN_COMPILE_TESSELLATION
                struct TessVertex {
                    float4 vertex : INTERNALTESSPOS;
                    float3 normal : NORMAL;
                    float4 tangent : TANGENT;
                    float2 texcoord0 : TEXCOORD0;
                };
                struct OutputPatchConstant {
                    float edge[3]         : SV_TessFactor;
                    float inside          : SV_InsideTessFactor;
                    float3 vTangent[4]    : TANGENT;
                    float2 vUV[4]         : TEXCOORD;
                    float3 vTanUCorner[4] : TANUCORNER;
                    float3 vTanVCorner[4] : TANVCORNER;
                    float4 vCWts          : TANWEIGHTS;
                };
                TessVertex tessvert (VertexInput v) {
                    TessVertex o;
                    o.vertex = v.vertex;
                    o.normal = v.normal;
                    o.tangent = v.tangent;
                    o.texcoord0 = v.texcoord0;
                    return o;
                }
                void displacement (inout VertexInput v){
                    float4 node_3859 = _Time + _TimeEditor;
                    float2 node_6549 = float2((fmod(v.texcoord0.g,1.0)+((_node_4254*node_3859.g)+v.texcoord0.r)),((node_3859.g*_node_4409)+v.texcoord0.g));
                    float4 _node_2308_var = tex2Dlod(_node_2308,float4(TRANSFORM_TEX(node_6549, _node_2308),0.0,0));
                    float3 node_5307 = ((dot(reflect(_node_2308_var.rgb,v.normal),float3(0.3,0.59,0.11))*_node_8840)*v.normal);
                    v.vertex.xyz += reflect(reflect(node_5307,v.normal),node_5307);
                }
                float Tessellation(TessVertex v){
                    return 8.0;
                }
                float4 Tessellation(TessVertex v, TessVertex v1, TessVertex v2){
                    float tv = Tessellation(v);
                    float tv1 = Tessellation(v1);
                    float tv2 = Tessellation(v2);
                    return float4( tv1+tv2, tv2+tv, tv+tv1, tv+tv1+tv2 ) / float4(2,2,2,3);
                }
                OutputPatchConstant hullconst (InputPatch<TessVertex,3> v) {
                    OutputPatchConstant o = (OutputPatchConstant)0;
                    float4 ts = Tessellation( v[0], v[1], v[2] );
                    o.edge[0] = ts.x;
                    o.edge[1] = ts.y;
                    o.edge[2] = ts.z;
                    o.inside = ts.w;
                    return o;
                }
                [domain("tri")]
                [partitioning("fractional_odd")]
                [outputtopology("triangle_cw")]
                [patchconstantfunc("hullconst")]
                [outputcontrolpoints(3)]
                TessVertex hull (InputPatch<TessVertex,3> v, uint id : SV_OutputControlPointID) {
                    return v[id];
                }
                [domain("tri")]
                VertexOutput domain (OutputPatchConstant tessFactors, const OutputPatch<TessVertex,3> vi, float3 bary : SV_DomainLocation) {
                    VertexInput v = (VertexInput)0;
                    v.vertex = vi[0].vertex*bary.x + vi[1].vertex*bary.y + vi[2].vertex*bary.z;
                    v.normal = vi[0].normal*bary.x + vi[1].normal*bary.y + vi[2].normal*bary.z;
                    v.tangent = vi[0].tangent*bary.x + vi[1].tangent*bary.y + vi[2].tangent*bary.z;
                    v.texcoord0 = vi[0].texcoord0*bary.x + vi[1].texcoord0*bary.y + vi[2].texcoord0*bary.z;
                    displacement(v);
                    VertexOutput o = vert(v);
                    return o;
                }
            #endif
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 normalDirection = i.normalDir;
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
