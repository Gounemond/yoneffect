// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:1457,x:32794,y:32692,varname:node_1457,prsc:2|emission-3582-OUT;n:type:ShaderForge.SFN_Tex2d,id:6893,x:32090,y:32684,ptovrint:False,ptlb:node_6893,ptin:_node_6893,varname:node_6893,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:c221062867ae30f44ad77f6f65b2132d,ntxv:0,isnm:False|UVIN-4292-UVOUT;n:type:ShaderForge.SFN_Color,id:6566,x:32165,y:32378,ptovrint:False,ptlb:background,ptin:_background,varname:node_6566,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0.04827571,c3:1,c4:1;n:type:ShaderForge.SFN_OneMinus,id:7299,x:32276,y:32671,varname:node_7299,prsc:2|IN-6893-B;n:type:ShaderForge.SFN_Panner,id:4292,x:31780,y:32596,varname:node_4292,prsc:2,spu:0,spv:1|UVIN-3831-UVOUT,DIST-5859-OUT;n:type:ShaderForge.SFN_TexCoord,id:3831,x:31531,y:32520,varname:node_3831,prsc:2,uv:0;n:type:ShaderForge.SFN_Time,id:112,x:31484,y:32718,varname:node_112,prsc:2;n:type:ShaderForge.SFN_Multiply,id:5859,x:31696,y:32799,varname:node_5859,prsc:2|A-112-T,B-4884-OUT;n:type:ShaderForge.SFN_Slider,id:4884,x:31392,y:32881,ptovrint:False,ptlb:time,ptin:_time,varname:node_4884,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.2419196,max:1;n:type:ShaderForge.SFN_Multiply,id:4689,x:32378,y:32449,varname:node_4689,prsc:2|A-6566-RGB,B-7299-OUT;n:type:ShaderForge.SFN_Add,id:3582,x:32615,y:32423,varname:node_3582,prsc:2|A-4689-OUT,B-5864-OUT;n:type:ShaderForge.SFN_Color,id:8295,x:32455,y:32650,ptovrint:False,ptlb:fluid,ptin:_fluid,varname:node_8295,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Multiply,id:5864,x:32609,y:32653,varname:node_5864,prsc:2|A-8295-RGB,B-6893-B;proporder:6893-6566-4884-8295;pass:END;sub:END;*/

Shader "Custom/waterfall" {
    Properties {
        _node_6893 ("node_6893", 2D) = "white" {}
        _background ("background", Color) = (0,0.04827571,1,1)
        _time ("time", Range(0, 1)) = 0.2419196
        _fluid ("fluid", Color) = (0,1,1,1)
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform sampler2D _node_6893; uniform float4 _node_6893_ST;
            uniform float4 _background;
            uniform float _time;
            uniform float4 _fluid;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                UNITY_FOG_COORDS(1)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
////// Lighting:
////// Emissive:
                float4 node_112 = _Time + _TimeEditor;
                float2 node_4292 = (i.uv0+(node_112.g*_time)*float2(0,1));
                float4 _node_6893_var = tex2D(_node_6893,TRANSFORM_TEX(node_4292, _node_6893));
                float node_7299 = (1.0 - _node_6893_var.b);
                float3 emissive = ((_background.rgb*node_7299)+(_fluid.rgb*_node_6893_var.b));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
