﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using VRStandardAssets.Utils;

public class VRTutorialButton : MonoBehaviour {

    private VRInteractiveItem _myVRitem;

    public VRCameraFade cameraFader;
    public Shader eyeHOverShader;               // The shader used when hovering on the object
    private Shader _myShader;
    private bool m_onHover = true;

    // Use this for initialization
    void Awake()
    {
        _myVRitem = GetComponent<VRInteractiveItem>();
        _myShader = GetComponent<Renderer>().material.shader;
    }

    void Start ()
    {
        _myVRitem.OnClick += Clicked;
        _myVRitem.OnOver += HoverIn;
        _myVRitem.OnOut += HoverOut;
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void HoverIn()
    {
        GetComponent<Renderer>().material.shader = eyeHOverShader;
        m_onHover = true;
    }

    public void HoverOut()
    {
        GetComponent<Renderer>().material.shader = _myShader;
        m_onHover = false;
    }

    public void Clicked()
    {
        if (m_onHover)
        {
            _myVRitem.OnClick -= Clicked;
            _myVRitem.OnOver -= HoverIn;
            _myVRitem.OnOut -= HoverOut;
            cameraFader.OnFadeComplete += LoadGameScene;
            StartCoroutine(cameraFader.BeginFadeIn(false));
        }
    }

    public void LoadGameScene()
    {
        SceneManager.LoadScene("AgainstYourTime");
    }
}
