// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:6518,x:33005,y:32903,varname:node_6518,prsc:2|emission-9212-OUT;n:type:ShaderForge.SFN_Tex2d,id:9596,x:32523,y:33011,ptovrint:False,ptlb:node_9596,ptin:_node_9596,varname:node_9596,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:62e76d73fdb5fbe43862dd7ab2baab75,ntxv:0,isnm:False|UVIN-6396-UVOUT;n:type:ShaderForge.SFN_Multiply,id:3426,x:31980,y:33077,varname:node_3426,prsc:2|A-5445-OUT,B-5301-OUT;n:type:ShaderForge.SFN_Tau,id:5301,x:31812,y:33130,varname:node_5301,prsc:2;n:type:ShaderForge.SFN_Multiply,id:5445,x:31759,y:32910,varname:node_5445,prsc:2|A-568-OUT,B-2857-OUT;n:type:ShaderForge.SFN_OneMinus,id:568,x:31558,y:32910,varname:node_568,prsc:2|IN-5139-OUT;n:type:ShaderForge.SFN_Multiply,id:5139,x:31425,y:32786,varname:node_5139,prsc:2|A-4715-OUT,B-4603-OUT;n:type:ShaderForge.SFN_Distance,id:4715,x:31246,y:32707,varname:node_4715,prsc:2|A-8777-UVOUT,B-7986-OUT;n:type:ShaderForge.SFN_TexCoord,id:8777,x:31049,y:32653,varname:node_8777,prsc:2,uv:0;n:type:ShaderForge.SFN_Vector1,id:4603,x:31284,y:32913,varname:node_4603,prsc:2,v1:2;n:type:ShaderForge.SFN_Vector2,id:7986,x:31079,y:32795,varname:node_7986,prsc:2,v1:0.5,v2:0.5;n:type:ShaderForge.SFN_Time,id:5606,x:31355,y:33015,varname:node_5606,prsc:2;n:type:ShaderForge.SFN_Rotator,id:6396,x:32339,y:33011,varname:node_6396,prsc:2|UVIN-149-OUT,SPD-408-OUT;n:type:ShaderForge.SFN_Vector1,id:408,x:32166,y:33126,varname:node_408,prsc:2,v1:0.25;n:type:ShaderForge.SFN_Multiply,id:2857,x:31562,y:33066,varname:node_2857,prsc:2|A-5606-T,B-8273-OUT;n:type:ShaderForge.SFN_Vector1,id:8273,x:31355,y:33147,varname:node_8273,prsc:2,v1:0.005;n:type:ShaderForge.SFN_Color,id:9407,x:32523,y:32836,ptovrint:False,ptlb:node_9407,ptin:_node_9407,varname:node_9407,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0,c3:1,c4:1;n:type:ShaderForge.SFN_Blend,id:9212,x:32762,y:32966,varname:node_9212,prsc:2,blmd:10,clmp:True|SRC-9407-RGB,DST-9596-RGB;n:type:ShaderForge.SFN_TexCoord,id:4330,x:31980,y:32922,varname:node_4330,prsc:2,uv:0;n:type:ShaderForge.SFN_Multiply,id:149,x:32166,y:32998,varname:node_149,prsc:2|A-4330-UVOUT,B-3426-OUT;proporder:9596-9407;pass:END;sub:END;*/

Shader "Custom/ring center" {
    Properties {
        _node_9596 ("node_9596", 2D) = "white" {}
        _node_9407 ("node_9407", Color) = (0,0,1,1)
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform sampler2D _node_9596; uniform float4 _node_9596_ST;
            uniform float4 _node_9407;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                UNITY_FOG_COORDS(1)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
////// Lighting:
////// Emissive:
                float4 node_9015 = _Time + _TimeEditor;
                float node_6396_ang = node_9015.g;
                float node_6396_spd = 0.25;
                float node_6396_cos = cos(node_6396_spd*node_6396_ang);
                float node_6396_sin = sin(node_6396_spd*node_6396_ang);
                float2 node_6396_piv = float2(0.5,0.5);
                float4 node_5606 = _Time + _TimeEditor;
                float2 node_6396 = (mul((i.uv0*(((1.0 - (distance(i.uv0,float2(0.5,0.5))*2.0))*(node_5606.g*0.005))*6.28318530718))-node_6396_piv,float2x2( node_6396_cos, -node_6396_sin, node_6396_sin, node_6396_cos))+node_6396_piv);
                float4 _node_9596_var = tex2D(_node_9596,TRANSFORM_TEX(node_6396, _node_9596));
                float3 emissive = saturate(( _node_9596_var.rgb > 0.5 ? (1.0-(1.0-2.0*(_node_9596_var.rgb-0.5))*(1.0-_node_9407.rgb)) : (2.0*_node_9596_var.rgb*_node_9407.rgb) ));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
