using UnityEngine;
using System.Collections;

public class ParadossoVisioneScript : MonoBehaviour {
	
	//Attributi per paradosso Giocatore
	public GameObject officialPlayer;                               // Reference to the official player (which if seen will cause the paradox)
    public YonEffectJRController gameState;                         // Reference to the gameplay controller, for shared methods

    [Header ("Clone paradox state")]
    public GameObject cloneBody;                                    // The body of the clone (used to apply materials and graphics)
    public Material materialIdle;                                   // Standard material applied to the cloneBody
    public Material materialWarning;                                // Material applied to the clone then the player is in proximity
    public Material materialParadox;                                // Material applied to the clone when a paradox is triggering

    [Header ("Paradox tolerance parameters")]
    public float paradoxMaxDistance;                                // The maximum distance to trigger the paradox. Over this distance, the clone won't be able to detect paradoxes
    public float paradoxProximityDistance = 2;                      // Proximity distance which will trigger a paradox in any case ( Even from behind )
    public float paradoxFOVAngle;                                   // The field of view (expressed in angle) of the clone to trigger paradoxes
    public float warningBonusDistance = 10;                         // The extra distance, on top of the maxDistance, which will trigger the warning state
    public float warningBonusAngle = 15;                            // The extra field of view, on top of the maxAngle, which will trigger the warning state

    private Coroutine m_paradoxRoutine;                             // Reference to the paradox routine (since only one routine of paradox should be running)
    private float m_Timer;                                          // Count to paradox timer
    private float m_timeToParadox = 2.0f;                           // Time needed to the player to being detected, to trigger a paradox
    private bool m_GazeOver = false;                                // Wether the clone is looking at the player or not
    private bool m_CloneWarning = false;                            // Wether the clone is in warning mode
    private bool m_CloneIsClose = false;                            // Wether the clone is in proximity to the player (to trigger intense audio and other status)


	
	
	void Start ()
    {
        cloneBody.GetComponent<Renderer>().material = materialIdle; // The initial material for the clone should be the idle material

        if (gameState == null)
        {
            gameState = FindObjectOfType<YonEffectJRController>().GetComponent<YonEffectJRController>();
        }
    }
	
    void FixedUpdate()
    {
        // Raycast that will cause paradox
        ParadoxRayCast();
    }

    private void ParadoxRayCast()
    {
        // Vector which connects player and clone
        Vector3 connectionVector = (officialPlayer.transform.position - transform.position).normalized;

        // Evaluates the angle between the clone line of sight and the vector
        float angle = Vector3.Angle(connectionVector, transform.forward);

        // Create a ray that points directly from the clone to the player.
        Ray ray = new Ray(transform.position, connectionVector);
        RaycastHit hit;

        // Checking if the raycast reaches the player ( considering the maximum distance plus warning distance, and if there aren't obstacles in between)
        if (Physics.Raycast(ray, out hit, paradoxMaxDistance + warningBonusDistance))
        {
            if (hit.collider.tag == "Player")
            {
                // If there isn't anything between clone and player, and the "CloneClose" state hasn't been triggered yet
                if (!m_CloneIsClose)
                {
                    m_CloneIsClose = true;                  // The clone is now close enough to trigger the CloneClose state
                    gameState.OnCloneRangeIn();             // Tells the manager that this clone is now close (the manager will handle what happens after)
                }

                // Checks if the player is in the field of the clone, or if it's too close. This methods will trigger a complete paradox
                if (hit.distance <= paradoxMaxDistance && angle < paradoxFOVAngle && angle > 0 || hit.distance <= paradoxProximityDistance)
                {
                    // If we just got seen
                    if (!m_GazeOver)
                    {
                        m_GazeOver = true;                      // Set the gaze on
                        gameState.OnCloneHoverInAtYou();        // Increases the amount of clones looking at you
                        cloneBody.GetComponent<Renderer>().material = materialParadox;  // Change the appearance of the clone according to the paradox status

                        // If no other clones were looking at you, start the paradox
                        if (gameState.paradoxRoutine == null)
                        {
                            gameState.paradoxRoutine = StartCoroutine(gameState.BeginParadox());
                        }
                    }
                    return; // Skip all the other checks since we triggered a paradox
                }
                // Checks if the player is in the extra field of view of the clone, which will trigger the warning status
                else if (angle < paradoxFOVAngle + warningBonusAngle && angle > -warningBonusAngle)
                {
                    if (!m_CloneWarning)
                    {
                        m_CloneWarning = true;                  // The clone is now close enough to trigger our warning
                        cloneBody.GetComponent<Renderer>().material = materialWarning;
                    }
                    return;
                }
                // If nor the paradox status or warning status has been triggered in this iteration
                else
                {
                    // If any status was triggered before
                    if (m_GazeOver || m_CloneWarning)
                    {
                        cloneBody.GetComponent<Renderer>().material = materialIdle;         // Switch the clone appearance to normal

                        // If previously the clone was creating a paradox, disable the flags
                        if (m_GazeOver)
                        {
                            m_GazeOver = false;
                            gameState.OnCloneHoverOutAtYou();
                        }
                        // If previously the clone was creating a warning, disable the flags
                        if (m_CloneWarning)
                        {
                            m_CloneWarning = false;
                        }
                    }
                }
            }
            // If an obstacle is now between the player and the clone
            else
            {
                // If any status was triggered before
                if (m_GazeOver || m_CloneWarning)
                {
                    cloneBody.GetComponent<Renderer>().material = materialIdle;         // Switch the clone appearance to normal

                    // If previously the clone was creating a paradox, disable the flags
                    if (m_GazeOver)
                    {
                        m_GazeOver = false;
                        gameState.OnCloneHoverOutAtYou();
                    }
                    // If previously the clone was creating a warning, disable the flags
                    if (m_CloneWarning)
                    {
                        m_CloneWarning = false;
                    }
                }


                if (Vector3.Distance(this.transform.position, officialPlayer.transform.position) < paradoxMaxDistance)
                {
                    if (!m_CloneIsClose)
                    {
                        m_CloneIsClose = true;
                        gameState.OnCloneRangeIn();
                    }
                }
                else
                {
                    if (m_CloneIsClose)
                    {
                        m_CloneIsClose = false;                  // The clone is now close enough to trigger our warning
                        gameState.OnCloneRangeOut();             // Tells the manager that this clone is in range to change the music
                    }
                }
            }
            
        }
        else
        {        
            if (m_CloneIsClose)
            {
                m_CloneIsClose = false;                  // The clone is now close enough to trigger our warning
                gameState.OnCloneRangeOut();             // Tells the manager that this clone is in range to change the music
            }
        }
    }

    IEnumerator BeginParadox()
    {
        m_Timer = 0;
        while (m_Timer < m_timeToParadox)
        {
            // ... add to the timer the difference between frames.
            m_Timer += Time.deltaTime;

            Debug.Log(m_Timer);
            yield return null;

            if (m_GazeOver)
            {
                continue;
            }

            m_Timer = 0f;
            yield break;

        }

        // Ok here you lose the game
        gameState.loseGame();
    }
}
