﻿using UnityEngine;
using System.Collections;

public class TimeMachineCollect : MonoBehaviour {

    public AudioSource pickupAudio;
    private YonEffectJRController gamestate;

	// Use this for initialization
	void Start ()
    {
        gamestate = FindObjectOfType<YonEffectJRController>().GetComponent<YonEffectJRController>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            gamestate.timerStarted = true;
            pickupAudio.Play();
            gamestate.startPlayerRecording();
            transform.gameObject.SetActive(false);
            //transform.position = new Vector3(3000, 3000, 3000);
        }

        // Bring the object outside the field of view 
        
    }
}
