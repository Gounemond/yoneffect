﻿using UnityEngine;
using System;
using VRStandardAssets.Utils;
using System.Collections;

[RequireComponent (typeof(Collider))]
public class EndPathCollider : MonoBehaviour
{

    public event Action pathCompleted;                  // Called when a Hanoi Object enters the collider

    private bool m_IsPathCompleted = false;

    // Use this for initialization
    void Awake ()
    {

    }

    // Update is called once per frame
    void Update () {
	
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.tag =="ConnectTheDotSphere" && !m_IsPathCompleted)
        {
            m_IsPathCompleted = true;
            if (pathCompleted != null)
            {
                pathCompleted();
            }
        }
    }
}
