using UnityEngine;
using System.Collections;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System;
using System.Linq;

public class PositionDoorTriggerNoOcclusion : MonoBehaviour {

	public YonBehaviour[] obj;
	private int triggerCounter = 0;
    public int gearsToUnlock = 0;

	// Use this for initialization
	void Awake ()
    {

	}
	
	// Update is called once per frame
	void Update () {
	}
	
	void OnTriggerEnter(Collider ObjectCollision)
	{
		if (YonEffectJRController.Self.numberGEARSCollected >= gearsToUnlock)
        {
            if (triggerCounter == 0)
            {
                //Debug.Log("Occlusion portal is open: " + _occlusionPortal.open);
                for (int i = 0; i < obj.Length; i++)
                {
                    obj[i].Activate();
                }
            }
            triggerCounter++;
        }
    }
	
	void OnTriggerExit(Collider ObjectCollision)
	{
        if (YonEffectJRController.Self.numberGEARSCollected >= gearsToUnlock)
        {
            if (triggerCounter == 1)
		    {
			    for (int i = 0; i < obj.Length; i++)
			    {
				    obj[i].deActivate();
			    }
			    triggerCounter--;
		    }
		    else
		    {
			    triggerCounter--;
		    }
        }
	}
	
}
