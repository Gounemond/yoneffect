// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.16 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.16;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:4486,x:32719,y:32712,varname:node_4486,prsc:2|emission-1646-OUT;n:type:ShaderForge.SFN_Lerp,id:1646,x:32490,y:32803,varname:node_1646,prsc:2|A-28-RGB,B-7941-OUT,T-7252-OUT;n:type:ShaderForge.SFN_Slider,id:8656,x:32211,y:33157,ptovrint:False,ptlb:SliderHanoi,ptin:_SliderHanoi,varname:node_8656,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Multiply,id:7252,x:32476,y:32991,varname:node_7252,prsc:2|A-6146-OUT,B-8656-OUT;n:type:ShaderForge.SFN_Fresnel,id:9130,x:32181,y:32991,varname:node_9130,prsc:2|NRM-7165-OUT,EXP-4303-OUT;n:type:ShaderForge.SFN_OneMinus,id:6146,x:32331,y:32965,varname:node_6146,prsc:2|IN-9130-OUT;n:type:ShaderForge.SFN_Vector1,id:4303,x:32011,y:33088,varname:node_4303,prsc:2,v1:2;n:type:ShaderForge.SFN_NormalVector,id:7165,x:31999,y:32928,prsc:2,pt:False;n:type:ShaderForge.SFN_Color,id:578,x:32067,y:32709,ptovrint:False,ptlb:node_578,ptin:_node_578,varname:node_578,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:0,c4:1;n:type:ShaderForge.SFN_Color,id:28,x:32385,y:32633,ptovrint:False,ptlb:node_28,ptin:_node_28,varname:node_28,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Multiply,id:7941,x:32249,y:32786,varname:node_7941,prsc:2|A-578-RGB,B-9050-OUT;n:type:ShaderForge.SFN_Vector1,id:9050,x:32123,y:32882,varname:node_9050,prsc:2,v1:2;proporder:8656-28-578;pass:END;sub:END;*/

Shader "Custom/lightHanoi" {
    Properties {
        _SliderHanoi ("SliderHanoi", Range(0, 1)) = 1
        _node_28 ("node_28", Color) = (0.5,0.5,0.5,1)
        _node_578 ("node_578", Color) = (1,1,0,1)
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float _SliderHanoi;
            uniform float4 _node_578;
            uniform float4 _node_28;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 posWorld : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
                UNITY_FOG_COORDS(2)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
/////// Vectors:
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float3 emissive = lerp(_node_28.rgb,(_node_578.rgb*2.0),((1.0 - pow(1.0-max(0,dot(i.normalDir, viewDirection)),2.0))*_SliderHanoi));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
