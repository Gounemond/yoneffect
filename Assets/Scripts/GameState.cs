﻿using UnityEngine;
using System.Collections;

public class GameState : MonoBehaviour 
{
	public int MAX_JUMP_TIME = 120;						// Defines the maximum amount of seconds you can jump back in time
	
	public ClonePoolManager clonePool;					// Reference to the clone pool manager
	//public GuiManager guiManager;						// Reference to guiManager component
	
	public float worldTime;								// Time in game, subject to time travel (different from unity time!)
	public bool isPause;								// If the game is paused or not
	
	public InputVCR playerVCR;							// Main player - instance of Kaerb VCR controller
	public InputVCR cameraVCR;							// Main player camera - instance of Maincamera VCR controller
		
	private bool _isRecording;							// If we are actually recording Kaerb or not
	
	public float maxJumpTimeAvailable = 0;				// Keeps how much time you're able to jump back (never exceeds MAX_JUMP_TIME)
		
	// Use this for initialization
	void Awake () 
	{
		// Getting a reference to the clonePool 
		clonePool = GameObject.Find("ClonePool").GetComponent<ClonePoolManager>();
		
		// Getting a reference to the guiManager 
//		guiManager = GameObject.Find("guiManager").GetComponent<GuiManager>();
		
		worldTime = 0.0f;								// Setting the world time to 0 (the beginning)
		maxJumpTimeAvailable = 0.0f;					// Setting time machine maximum jump to 0, to avoid negative time jumps
		
		// Starting to record our actions, to reproduce them when going back in the past
		playerVCR.NewRecording ();
		cameraVCR.NewRecording ();
		
		isPause = false;								
		_isRecording = true;								// Recording is on!
		
		// Hiding cursor
		Cursor.visible = false;
	}
	
	// Update is called once per frame
	void Update () 
	{
		// Keeps track of the actual time		
		worldTime += Time.deltaTime;
		
		if (maxJumpTimeAvailable <= MAX_JUMP_TIME)
		{
			maxJumpTimeAvailable += Time.deltaTime;
		}
	}
	
	#region methodsForGUI
	// Start recording the player - Start to record from the last record interrupted
	public void RecordingStart()
	{
		// Start again recording (Record will append the recording to the previous one, like the stop never happened!)
		playerVCR.Record ();
		cameraVCR.Record ();
		_isRecording = true;
	}
	
	// Stop recording the player
	public void RecordingStop()
	{
		// Stop recording
		playerVCR.Stop ();	
		cameraVCR.Stop ();
		_isRecording = false;
	}
	
	// Sets pause on
	public void PauseOn()
	{
		isPause = true;
		Time.timeScale = 0;
		Cursor.visible = true;
		Screen.lockCursor = false;
	}
	
	// Sets pause off
	public void PauseOff()
	{
		isPause = false;
		Time.timeScale = 1;
		Cursor.visible = false;
		Screen.lockCursor = true;
	}
	
	// Does a time travel in the past
	public void TimeTravel()
	{		
		// place here a coroutine that executes the special effect and waits 2 seconds
		
		//clonePool.addKaerbRecording(playerVCR.GetRecording(), cameraVCR.GetRecording (), guiManager.jumpTimeAmount);
		
		//worldTime = worldTime - guiManager.jumpTimeAmount;
		//maxJumpTimeAvailable = maxJumpTimeAvailable - guiManager.jumpTimeAmount;
		// Starts a new recording for the actual player
		playerVCR.NewRecording ();
		cameraVCR.NewRecording ();
		//guiManager.jumpTimeAmount = 0;
		//guiManager.timeLabel.text = "0";
	}
	
	#endregion
}
