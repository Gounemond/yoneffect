﻿using UnityEngine;
using System.Collections;
using VRStandardAssets.Utils;
using System;

public class RingActivator : MonoBehaviour
{
    public event Action OnInnerRotate;                          // Action called from the manager when clicking the inner activator
    public event Action OnMiddleRotate;                         // Action called from the manager when clicking the middle activator
    public event Action OnOuterRotate;                          // Action called from the manager when clicking the outer activator

    public VRInteractiveItem innerRingActivator;                // Inner activator element
    public VRInteractiveItem middleRingActivator;               // Middle activator element
    public VRInteractiveItem outerRingActivator;                // Outer activator element

    public Transform innerRing;                                 // Inner element of the panel                                 
    public Transform middleRing;                                // Middle element of the panel
    public Transform outerRing;                                 // Outer element of the panel

    public Color HoverInColor;
    private Color m_OriginalColor;

    // Use this for initialization
    void Start ()
    {
        m_OriginalColor = innerRing.GetComponent<Renderer>().materials[1].GetColor("_node_4194");
        EnableInteractivity();
    }

    public void EnableInteractivity()
    {
        innerRingActivator.OnOver   += InnerHoverIn;
        innerRingActivator.OnOut    += InnerHoverOut;
        innerRingActivator.OnClick  += InnerRotateClick;

        middleRingActivator.OnOver  += MiddleHoverIn;
        middleRingActivator.OnOut   += MiddleHoverOut;
        middleRingActivator.OnClick += MiddleRotateClick;

        outerRingActivator.OnOver   += OuterHoverIn;
        outerRingActivator.OnOut    += OuterHoverOut;
        outerRingActivator.OnClick  += OuterRotateClick;
    }

    public void DisableInteractivity()
    {
        InnerHoverOut();
        MiddleHoverOut();
        OuterHoverOut();

        innerRingActivator.OnOver   -= InnerHoverIn;
        innerRingActivator.OnOut    -= InnerHoverOut;
        innerRingActivator.OnClick  -= InnerRotateClick;

        middleRingActivator.OnOver  -= MiddleHoverIn;
        middleRingActivator.OnOut   -= MiddleHoverOut;
        middleRingActivator.OnClick -= MiddleRotateClick;

        outerRingActivator.OnOver   -= OuterHoverIn;
        outerRingActivator.OnOut    -= OuterHoverOut;
        outerRingActivator.OnClick  -= OuterRotateClick;
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void InnerHoverIn()
    {
        innerRing.GetComponent<Renderer>().materials[1].SetColor("_node_4194", HoverInColor);
    }

    public void InnerHoverOut()
    {
        innerRing.GetComponent<Renderer>().materials[1].SetColor("_node_4194", m_OriginalColor);
    }

    public void MiddleHoverIn()
    {
        middleRing.GetComponent<Renderer>().materials[1].SetColor("_node_4194", HoverInColor);
    }

    public void MiddleHoverOut()
    {
        middleRing.GetComponent<Renderer>().materials[1].SetColor("_node_4194", m_OriginalColor);
    }

    public void OuterHoverIn()
    {
        outerRing.GetComponent<Renderer>().materials[1].SetColor("_node_4194", HoverInColor);
    }

    public void OuterHoverOut()
    {
        outerRing.GetComponent<Renderer>().materials[1].SetColor("_node_4194", m_OriginalColor);
    }

    public void InnerRotateClick()
    {
        if (OnInnerRotate != null)
        {
            OnInnerRotate();
        }
        StartCoroutine(WaitAndRestoreInteractivity());
    }

    public void MiddleRotateClick()
    {
        if (OnMiddleRotate != null)
        {
            OnMiddleRotate();
        }
        StartCoroutine(WaitAndRestoreInteractivity());
    }

    public void OuterRotateClick()
    {
        if (OnOuterRotate != null)
        {
            OnOuterRotate();
        }
        StartCoroutine(WaitAndRestoreInteractivity());
    }

    public IEnumerator WaitAndRestoreInteractivity()
    {
        DisableInteractivity();
        yield return new WaitForSeconds(2);
        EnableInteractivity();
    }
}
