// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:5072,x:33528,y:32729,varname:node_5072,prsc:2|emission-9161-OUT,alpha-3947-OUT;n:type:ShaderForge.SFN_Multiply,id:5656,x:32909,y:32955,varname:node_5656,prsc:2|A-2551-RGB,B-7161-OUT,C-2352-OUT;n:type:ShaderForge.SFN_Vector1,id:7161,x:32658,y:33110,varname:node_7161,prsc:2,v1:5;n:type:ShaderForge.SFN_Color,id:2551,x:32581,y:32872,ptovrint:False,ptlb:glow,ptin:_glow,varname:node_9323,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.2313726,c2:0.9803922,c3:0.7215686,c4:1;n:type:ShaderForge.SFN_Power,id:2352,x:32436,y:32985,varname:node_2352,prsc:2|VAL-4378-OUT,EXP-6135-OUT;n:type:ShaderForge.SFN_Color,id:7221,x:32654,y:32581,ptovrint:False,ptlb:main,ptin:_main,varname:node_1812,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0,c3:0.07450981,c4:1;n:type:ShaderForge.SFN_Vector1,id:3847,x:32920,y:32874,varname:node_3847,prsc:2,v1:1;n:type:ShaderForge.SFN_Lerp,id:6259,x:32920,y:32746,varname:node_6259,prsc:2|A-7221-RGB,B-2603-OUT,T-2352-OUT;n:type:ShaderForge.SFN_Vector1,id:2603,x:32594,y:32756,varname:node_2603,prsc:2,v1:0.1;n:type:ShaderForge.SFN_Multiply,id:4378,x:32244,y:32910,varname:node_4378,prsc:2|A-3132-OUT,B-9712-OUT;n:type:ShaderForge.SFN_Vector1,id:6135,x:32240,y:33111,varname:node_6135,prsc:2,v1:6;n:type:ShaderForge.SFN_Abs,id:3132,x:32103,y:32804,varname:node_3132,prsc:2|IN-9721-OUT;n:type:ShaderForge.SFN_Vector1,id:9712,x:32067,y:33017,varname:node_9712,prsc:2,v1:1.5;n:type:ShaderForge.SFN_Subtract,id:9721,x:31941,y:32769,varname:node_9721,prsc:2|A-9136-OUT,B-1901-OUT;n:type:ShaderForge.SFN_Vector1,id:1901,x:31777,y:32895,varname:node_1901,prsc:2,v1:0.8;n:type:ShaderForge.SFN_Frac,id:9136,x:31719,y:32661,varname:node_9136,prsc:2|IN-1300-OUT;n:type:ShaderForge.SFN_ComponentMask,id:8743,x:31434,y:32558,varname:node_8743,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-5663-UVOUT;n:type:ShaderForge.SFN_Panner,id:5663,x:31260,y:32528,varname:node_5663,prsc:2,spu:0.25,spv:0|UVIN-8897-UVOUT;n:type:ShaderForge.SFN_Time,id:6799,x:30681,y:32699,varname:node_6799,prsc:2;n:type:ShaderForge.SFN_Multiply,id:359,x:30978,y:32728,varname:node_359,prsc:2|A-6799-T,B-7912-OUT;n:type:ShaderForge.SFN_Slider,id:7912,x:30614,y:32838,ptovrint:False,ptlb:gottagofast,ptin:_gottagofast,varname:node_6798,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:2,max:20;n:type:ShaderForge.SFN_TexCoord,id:8897,x:30786,y:32492,varname:node_8897,prsc:2,uv:0;n:type:ShaderForge.SFN_Multiply,id:5352,x:30976,y:32401,varname:node_5352,prsc:2|A-7115-OUT,B-8897-UVOUT;n:type:ShaderForge.SFN_Slider,id:7115,x:30577,y:32347,ptovrint:False,ptlb:numberoflight,ptin:_numberoflight,varname:node_5288,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:1,cur:5,max:10;n:type:ShaderForge.SFN_Add,id:9161,x:33097,y:32769,varname:node_9161,prsc:2|A-6259-OUT,B-5656-OUT;n:type:ShaderForge.SFN_SwitchProperty,id:1300,x:31581,y:32687,ptovrint:False,ptlb:switch direction,ptin:_switchdirection,varname:node_6531,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:True|A-8743-OUT,B-6589-OUT;n:type:ShaderForge.SFN_ComponentMask,id:6589,x:31422,y:32715,varname:node_6589,prsc:2,cc1:1,cc2:-1,cc3:-1,cc4:-1|IN-8117-UVOUT;n:type:ShaderForge.SFN_Panner,id:8117,x:31241,y:32715,varname:node_8117,prsc:2,spu:0,spv:0.25|UVIN-5352-OUT,DIST-359-OUT;n:type:ShaderForge.SFN_Multiply,id:3947,x:33063,y:33093,varname:node_3947,prsc:2|A-2352-OUT,B-7762-OUT;n:type:ShaderForge.SFN_ValueProperty,id:7762,x:32823,y:33227,ptovrint:False,ptlb:int opacity,ptin:_intopacity,varname:node_7762,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.25;proporder:2551-7221-7912-7115-1300-7762;pass:END;sub:END;*/

Shader "Custom/shadercollision" {
    Properties {
        _glow ("glow", Color) = (0.2313726,0.9803922,0.7215686,1)
        _main ("main", Color) = (0,0,0.07450981,1)
        _gottagofast ("gottagofast", Range(0, 20)) = 2
        _numberoflight ("numberoflight", Range(1, 10)) = 5
        [MaterialToggle] _switchdirection ("switch direction", Float ) = 0
        _intopacity ("int opacity", Float ) = 0.25
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform float4 _glow;
            uniform float4 _main;
            uniform float _gottagofast;
            uniform float _numberoflight;
            uniform fixed _switchdirection;
            uniform float _intopacity;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                UNITY_FOG_COORDS(1)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
////// Lighting:
////// Emissive:
                float node_2603 = 0.1;
                float4 node_2764 = _Time + _TimeEditor;
                float4 node_6799 = _Time + _TimeEditor;
                float node_2352 = pow((abs((frac(lerp( (i.uv0+node_2764.g*float2(0.25,0)).r, ((_numberoflight*i.uv0)+(node_6799.g*_gottagofast)*float2(0,0.25)).g, _switchdirection ))-0.8))*1.5),6.0);
                float3 emissive = (lerp(_main.rgb,float3(node_2603,node_2603,node_2603),node_2352)+(_glow.rgb*5.0*node_2352));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,(node_2352*_intopacity));
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
