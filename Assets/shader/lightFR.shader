// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.16 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.16;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:3024,x:32719,y:32712,varname:node_3024,prsc:2|emission-8305-OUT;n:type:ShaderForge.SFN_Lerp,id:8305,x:32515,y:32786,varname:node_8305,prsc:2|A-1902-RGB,B-9324-OUT,T-8342-OUT;n:type:ShaderForge.SFN_Multiply,id:8342,x:32526,y:32956,varname:node_8342,prsc:2|A-4251-OUT,B-9003-OUT;n:type:ShaderForge.SFN_OneMinus,id:4251,x:32336,y:32920,varname:node_4251,prsc:2|IN-4644-OUT;n:type:ShaderForge.SFN_Slider,id:9003,x:32257,y:33072,ptovrint:False,ptlb:SliderFR,ptin:_SliderFR,varname:node_9003,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Fresnel,id:4644,x:32165,y:32890,varname:node_4644,prsc:2|NRM-9966-OUT,EXP-8068-OUT;n:type:ShaderForge.SFN_NormalVector,id:9966,x:31997,y:32855,prsc:2,pt:False;n:type:ShaderForge.SFN_Vector1,id:8068,x:32040,y:33010,varname:node_8068,prsc:2,v1:2;n:type:ShaderForge.SFN_Multiply,id:9324,x:32305,y:32723,varname:node_9324,prsc:2|A-8509-RGB,B-5648-OUT;n:type:ShaderForge.SFN_Color,id:8509,x:32099,y:32584,ptovrint:False,ptlb:node_8509,ptin:_node_8509,varname:node_8509,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:0,c4:1;n:type:ShaderForge.SFN_Vector1,id:5648,x:32112,y:32786,varname:node_5648,prsc:2,v1:2;n:type:ShaderForge.SFN_Color,id:1902,x:32408,y:32582,ptovrint:False,ptlb:node_1902,ptin:_node_1902,varname:node_1902,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;proporder:9003-8509-1902;pass:END;sub:END;*/

Shader "Custom/lightFR" {
    Properties {
        _SliderFR ("SliderFR", Range(0, 1)) = 0
        _node_8509 ("node_8509", Color) = (1,1,0,1)
        _node_1902 ("node_1902", Color) = (0.5,0.5,0.5,1)
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float _SliderFR;
            uniform float4 _node_8509;
            uniform float4 _node_1902;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 posWorld : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
                UNITY_FOG_COORDS(2)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
/////// Vectors:
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float3 emissive = lerp(_node_1902.rgb,(_node_8509.rgb*2.0),((1.0 - pow(1.0-max(0,dot(i.normalDir, viewDirection)),2.0))*_SliderFR));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
