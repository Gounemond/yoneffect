// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:7324,x:35562,y:32192,varname:node_7324,prsc:2|emission-1569-OUT,voffset-7438-OUT;n:type:ShaderForge.SFN_TexCoord,id:67,x:31254,y:32318,varname:node_67,prsc:2,uv:0;n:type:ShaderForge.SFN_Multiply,id:7438,x:34356,y:32748,varname:node_7438,prsc:2|A-8515-OUT,B-44-OUT;n:type:ShaderForge.SFN_NormalVector,id:6583,x:33916,y:32861,prsc:2,pt:True;n:type:ShaderForge.SFN_Sin,id:8515,x:33443,y:32590,varname:node_8515,prsc:2|IN-5348-OUT;n:type:ShaderForge.SFN_Distance,id:3026,x:31588,y:32491,varname:node_3026,prsc:2|A-67-UVOUT,B-3283-OUT;n:type:ShaderForge.SFN_Vector2,id:3283,x:31124,y:32617,varname:node_3283,prsc:2,v1:0.5,v2:0.5;n:type:ShaderForge.SFN_Multiply,id:6867,x:31781,y:32548,varname:node_6867,prsc:2|A-3026-OUT,B-919-OUT;n:type:ShaderForge.SFN_Vector1,id:919,x:31168,y:32742,varname:node_919,prsc:2,v1:2;n:type:ShaderForge.SFN_OneMinus,id:9943,x:31932,y:32599,varname:node_9943,prsc:2|IN-6867-OUT;n:type:ShaderForge.SFN_Multiply,id:5348,x:33147,y:32399,varname:node_5348,prsc:2|A-4310-UVOUT,B-5407-OUT;n:type:ShaderForge.SFN_Tau,id:5407,x:32580,y:32830,varname:node_5407,prsc:2;n:type:ShaderForge.SFN_Time,id:3109,x:31320,y:32843,varname:node_3109,prsc:2;n:type:ShaderForge.SFN_Multiply,id:6293,x:31594,y:32814,varname:node_6293,prsc:2|A-3109-T,B-6812-OUT;n:type:ShaderForge.SFN_Vector1,id:6812,x:31396,y:33033,varname:node_6812,prsc:2,v1:0;n:type:ShaderForge.SFN_Panner,id:9858,x:32533,y:32361,varname:node_9858,prsc:2,spu:1,spv:1|UVIN-67-UVOUT,DIST-2796-OUT;n:type:ShaderForge.SFN_Multiply,id:2796,x:32279,y:32665,varname:node_2796,prsc:2|A-9943-OUT,B-2438-OUT;n:type:ShaderForge.SFN_Vector1,id:2438,x:32068,y:32804,varname:node_2438,prsc:2,v1:2;n:type:ShaderForge.SFN_Rotator,id:4310,x:32801,y:32423,varname:node_4310,prsc:2|UVIN-9858-UVOUT;n:type:ShaderForge.SFN_Multiply,id:3865,x:31882,y:32807,varname:node_3865,prsc:2|A-6867-OUT,B-6293-OUT;n:type:ShaderForge.SFN_Tex2d,id:8285,x:34210,y:31891,ptovrint:False,ptlb:node_1888_copy,ptin:_node_1888_copy,varname:_node_1888_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:c221062867ae30f44ad77f6f65b2132d,ntxv:2,isnm:False|UVIN-9995-UVOUT;n:type:ShaderForge.SFN_Add,id:1569,x:35135,y:32256,varname:node_1569,prsc:2|A-8285-RGB,B-6904-OUT;n:type:ShaderForge.SFN_Panner,id:8268,x:33681,y:31840,varname:node_8268,prsc:2,spu:2,spv:2|UVIN-2585-UVOUT,DIST-2881-OUT;n:type:ShaderForge.SFN_TexCoord,id:2585,x:32989,y:31971,varname:node_2585,prsc:2,uv:0;n:type:ShaderForge.SFN_Rotator,id:9995,x:33942,y:31878,varname:node_9995,prsc:2|UVIN-8268-UVOUT;n:type:ShaderForge.SFN_Distance,id:2881,x:33283,y:32097,varname:node_2881,prsc:2|A-2585-UVOUT,B-7556-OUT;n:type:ShaderForge.SFN_Vector2,id:7556,x:32989,y:32169,varname:node_7556,prsc:2,v1:0.5,v2:0.5;n:type:ShaderForge.SFN_SwitchProperty,id:768,x:33816,y:32529,ptovrint:False,ptlb:node_8956,ptin:_node_8956,varname:node_8956,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:True|A-5348-OUT,B-8515-OUT;n:type:ShaderForge.SFN_Desaturate,id:2199,x:34269,y:32435,varname:node_2199,prsc:2|COL-768-OUT,DES-4561-OUT;n:type:ShaderForge.SFN_ComponentMask,id:2097,x:34437,y:32391,varname:node_2097,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-2199-OUT;n:type:ShaderForge.SFN_Vector1,id:4561,x:34124,y:32591,varname:node_4561,prsc:2,v1:1;n:type:ShaderForge.SFN_Color,id:8713,x:33909,y:32120,ptovrint:False,ptlb:node_1979,ptin:_node_1979,varname:node_1979,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0,c3:0.9310346,c4:1;n:type:ShaderForge.SFN_Color,id:1560,x:33896,y:32319,ptovrint:False,ptlb:node_6248,ptin:_node_6248,varname:node_6248,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:1,c3:0.7517242,c4:1;n:type:ShaderForge.SFN_Lerp,id:6904,x:34389,y:32178,varname:node_6904,prsc:2|A-8713-RGB,B-1560-RGB,T-2097-OUT;n:type:ShaderForge.SFN_Vector1,id:44,x:34135,y:32797,varname:node_44,prsc:2,v1:0.3;proporder:8285-768-8713-1560;pass:END;sub:END;*/

Shader "Custom/ALBERI2" {
    Properties {
        _node_1888_copy ("node_1888_copy", 2D) = "black" {}
        [MaterialToggle] _node_8956 ("node_8956", Float ) = -0.9810169
        _node_1979 ("node_1979", Color) = (1,0,0.9310346,1)
        _node_6248 ("node_6248", Color) = (0,1,0.7517242,1)
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform sampler2D _node_1888_copy; uniform float4 _node_1888_copy_ST;
            uniform fixed _node_8956;
            uniform float4 _node_1979;
            uniform float4 _node_6248;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                UNITY_FOG_COORDS(1)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                float4 node_8497 = _Time + _TimeEditor;
                float node_4310_ang = node_8497.g;
                float node_4310_spd = 1.0;
                float node_4310_cos = cos(node_4310_spd*node_4310_ang);
                float node_4310_sin = sin(node_4310_spd*node_4310_ang);
                float2 node_4310_piv = float2(0.5,0.5);
                float node_6867 = (distance(o.uv0,float2(0.5,0.5))*2.0);
                float2 node_4310 = (mul((o.uv0+((1.0 - node_6867)*2.0)*float2(1,1))-node_4310_piv,float2x2( node_4310_cos, -node_4310_sin, node_4310_sin, node_4310_cos))+node_4310_piv);
                float2 node_5348 = (node_4310*6.28318530718);
                float2 node_8515 = sin(node_5348);
                v.vertex.xyz += float3((node_8515*0.3),0.0);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
////// Lighting:
////// Emissive:
                float4 node_8497 = _Time + _TimeEditor;
                float node_9995_ang = node_8497.g;
                float node_9995_spd = 1.0;
                float node_9995_cos = cos(node_9995_spd*node_9995_ang);
                float node_9995_sin = sin(node_9995_spd*node_9995_ang);
                float2 node_9995_piv = float2(0.5,0.5);
                float2 node_9995 = (mul((i.uv0+distance(i.uv0,float2(0.5,0.5))*float2(2,2))-node_9995_piv,float2x2( node_9995_cos, -node_9995_sin, node_9995_sin, node_9995_cos))+node_9995_piv);
                float4 _node_1888_copy_var = tex2D(_node_1888_copy,TRANSFORM_TEX(node_9995, _node_1888_copy));
                float node_4310_ang = node_8497.g;
                float node_4310_spd = 1.0;
                float node_4310_cos = cos(node_4310_spd*node_4310_ang);
                float node_4310_sin = sin(node_4310_spd*node_4310_ang);
                float2 node_4310_piv = float2(0.5,0.5);
                float node_6867 = (distance(i.uv0,float2(0.5,0.5))*2.0);
                float2 node_4310 = (mul((i.uv0+((1.0 - node_6867)*2.0)*float2(1,1))-node_4310_piv,float2x2( node_4310_cos, -node_4310_sin, node_4310_sin, node_4310_cos))+node_4310_piv);
                float2 node_5348 = (node_4310*6.28318530718);
                float2 node_8515 = sin(node_5348);
                float3 emissive = (_node_1888_copy_var.rgb+lerp(_node_1979.rgb,_node_6248.rgb,lerp(lerp( node_5348, node_8515, _node_8956 ),dot(lerp( node_5348, node_8515, _node_8956 ),float3(0.3,0.59,0.11)),1.0).r));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                float4 node_6328 = _Time + _TimeEditor;
                float node_4310_ang = node_6328.g;
                float node_4310_spd = 1.0;
                float node_4310_cos = cos(node_4310_spd*node_4310_ang);
                float node_4310_sin = sin(node_4310_spd*node_4310_ang);
                float2 node_4310_piv = float2(0.5,0.5);
                float node_6867 = (distance(o.uv0,float2(0.5,0.5))*2.0);
                float2 node_4310 = (mul((o.uv0+((1.0 - node_6867)*2.0)*float2(1,1))-node_4310_piv,float2x2( node_4310_cos, -node_4310_sin, node_4310_sin, node_4310_cos))+node_4310_piv);
                float2 node_5348 = (node_4310*6.28318530718);
                float2 node_8515 = sin(node_5348);
                v.vertex.xyz += float3((node_8515*0.3),0.0);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
