// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:2,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:4395,x:34439,y:32133,varname:node_4395,prsc:2|emission-6584-OUT;n:type:ShaderForge.SFN_Tex2d,id:9774,x:32163,y:32405,varname:node_9774,prsc:2,tex:c7a5e1937cfd86d4bab16211d6faa818,ntxv:0,isnm:False|UVIN-769-UVOUT,TEX-5256-TEX;n:type:ShaderForge.SFN_Panner,id:7628,x:31704,y:32824,varname:node_7628,prsc:2,spu:1,spv:1|UVIN-6345-OUT,DIST-5425-OUT;n:type:ShaderForge.SFN_TexCoord,id:9572,x:30777,y:32526,varname:node_9572,prsc:2,uv:0;n:type:ShaderForge.SFN_Tex2dAsset,id:5256,x:30777,y:32728,ptovrint:False,ptlb:node_5256,ptin:_node_5256,varname:node_5256,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:c7a5e1937cfd86d4bab16211d6faa818,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Panner,id:769,x:32002,y:32405,varname:node_769,prsc:2,spu:1,spv:1|UVIN-3340-OUT,DIST-9633-OUT;n:type:ShaderForge.SFN_Multiply,id:6345,x:31499,y:32755,varname:node_6345,prsc:2|A-9572-UVOUT,B-5741-OUT;n:type:ShaderForge.SFN_Multiply,id:3340,x:31792,y:32426,varname:node_3340,prsc:2|A-9572-UVOUT,B-6099-OUT;n:type:ShaderForge.SFN_Vector1,id:6099,x:31449,y:32604,varname:node_6099,prsc:2,v1:0.5;n:type:ShaderForge.SFN_Vector1,id:5741,x:31256,y:32954,varname:node_5741,prsc:2,v1:0.5;n:type:ShaderForge.SFN_Tex2d,id:7091,x:32134,y:32709,varname:node_7091,prsc:2,tex:c7a5e1937cfd86d4bab16211d6faa818,ntxv:0,isnm:False|UVIN-5410-OUT,TEX-5256-TEX;n:type:ShaderForge.SFN_Tex2d,id:9580,x:32246,y:33043,varname:node_9580,prsc:2,tex:c7a5e1937cfd86d4bab16211d6faa818,ntxv:0,isnm:False|UVIN-9716-UVOUT,TEX-5256-TEX;n:type:ShaderForge.SFN_Panner,id:9716,x:32035,y:33146,varname:node_9716,prsc:2,spu:0.2,spv:0.2|UVIN-719-OUT,DIST-589-OUT;n:type:ShaderForge.SFN_Multiply,id:719,x:31703,y:33194,varname:node_719,prsc:2|A-9572-UVOUT,B-6956-OUT;n:type:ShaderForge.SFN_Vector1,id:6956,x:31281,y:33187,varname:node_6956,prsc:2,v1:0.125;n:type:ShaderForge.SFN_Add,id:4682,x:32646,y:32327,varname:node_4682,prsc:2|A-9572-UVOUT,B-5531-OUT;n:type:ShaderForge.SFN_ComponentMask,id:9646,x:32356,y:32405,varname:node_9646,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-9774-RGB;n:type:ShaderForge.SFN_ComponentMask,id:1018,x:32328,y:32709,varname:node_1018,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-7091-RGB;n:type:ShaderForge.SFN_ComponentMask,id:5898,x:32439,y:33004,varname:node_5898,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-9580-RGB;n:type:ShaderForge.SFN_Multiply,id:5531,x:32535,y:32529,varname:node_5531,prsc:2|A-9646-OUT,B-4206-OUT;n:type:ShaderForge.SFN_Vector1,id:4206,x:32372,y:32653,varname:node_4206,prsc:2,v1:0.2;n:type:ShaderForge.SFN_Multiply,id:8921,x:32514,y:32710,varname:node_8921,prsc:2|A-1018-OUT,B-3300-OUT;n:type:ShaderForge.SFN_Multiply,id:2980,x:32602,y:32979,varname:node_2980,prsc:2|A-5898-OUT,B-7843-OUT;n:type:ShaderForge.SFN_Vector1,id:3300,x:32377,y:32867,varname:node_3300,prsc:2,v1:0.2;n:type:ShaderForge.SFN_Vector1,id:7843,x:32517,y:33169,varname:node_7843,prsc:2,v1:0.5;n:type:ShaderForge.SFN_Add,id:9682,x:32794,y:32566,varname:node_9682,prsc:2|A-4682-OUT,B-8921-OUT;n:type:ShaderForge.SFN_Add,id:203,x:32959,y:32765,varname:node_203,prsc:2|A-9682-OUT,B-2980-OUT;n:type:ShaderForge.SFN_Color,id:9110,x:32935,y:32294,ptovrint:False,ptlb:node_9110,ptin:_node_9110,varname:node_9110,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0.006896734,c3:1,c4:1;n:type:ShaderForge.SFN_Color,id:1641,x:33029,y:32499,ptovrint:False,ptlb:node_1641,ptin:_node_1641,varname:node_1641,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:1,c3:0.5034482,c4:1;n:type:ShaderForge.SFN_Lerp,id:5568,x:33419,y:32349,varname:node_5568,prsc:2|A-9110-RGB,B-1641-RGB,T-7761-OUT;n:type:ShaderForge.SFN_ComponentMask,id:7761,x:33332,y:32526,varname:node_7761,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-6282-OUT;n:type:ShaderForge.SFN_Desaturate,id:6282,x:33156,y:32710,varname:node_6282,prsc:2|COL-203-OUT,DES-8935-OUT;n:type:ShaderForge.SFN_Vector1,id:8935,x:33035,y:32921,varname:node_8935,prsc:2,v1:1;n:type:ShaderForge.SFN_Time,id:7758,x:31677,y:32548,varname:node_7758,prsc:2;n:type:ShaderForge.SFN_Multiply,id:9633,x:31897,y:32548,varname:node_9633,prsc:2|A-7758-T,B-9435-OUT;n:type:ShaderForge.SFN_Slider,id:9435,x:31689,y:32739,ptovrint:False,ptlb:slider1,ptin:_slider1,varname:node_9435,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-1,cur:-0.6918517,max:1;n:type:ShaderForge.SFN_Multiply,id:5425,x:31558,y:32901,varname:node_5425,prsc:2|A-7215-T,B-844-OUT;n:type:ShaderForge.SFN_Time,id:7215,x:31379,y:33007,varname:node_7215,prsc:2;n:type:ShaderForge.SFN_Slider,id:844,x:31639,y:33111,ptovrint:False,ptlb:slider2,ptin:_slider2,varname:node_844,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-2,cur:0.05921507,max:1;n:type:ShaderForge.SFN_Time,id:7730,x:31690,y:33379,varname:node_7730,prsc:2;n:type:ShaderForge.SFN_Multiply,id:589,x:31927,y:33363,varname:node_589,prsc:2|A-7730-T,B-1875-OUT;n:type:ShaderForge.SFN_Slider,id:1875,x:31680,y:33523,ptovrint:False,ptlb:slider3,ptin:_slider3,varname:node_1875,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.3528411,max:1;n:type:ShaderForge.SFN_RemapRange,id:5410,x:31887,y:32833,varname:node_5410,prsc:2,frmn:1,frmx:0,tomn:1,tomx:-1|IN-7628-UVOUT;n:type:ShaderForge.SFN_Fresnel,id:9414,x:33640,y:32420,varname:node_9414,prsc:2|NRM-9045-OUT,EXP-9410-OUT;n:type:ShaderForge.SFN_Sin,id:5937,x:33786,y:32348,varname:node_5937,prsc:2|IN-9414-OUT;n:type:ShaderForge.SFN_NormalVector,id:9045,x:33427,y:32652,prsc:2,pt:False;n:type:ShaderForge.SFN_Add,id:3815,x:34099,y:32215,varname:node_3815,prsc:2|A-5568-OUT,B-5937-OUT;n:type:ShaderForge.SFN_Slider,id:9410,x:33589,y:32570,ptovrint:False,ptlb:fresnel,ptin:_fresnel,varname:node_9410,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:2,max:2;n:type:ShaderForge.SFN_SwitchProperty,id:6584,x:34224,y:32108,ptovrint:False,ptlb:fresnel on/off,ptin:_fresnelonoff,varname:node_6584,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:False|A-5568-OUT,B-3815-OUT;proporder:5256-9110-1641-9435-844-1875-9410-6584;pass:END;sub:END;*/

Shader "Custom/caustic" {
    Properties {
        _node_5256 ("node_5256", 2D) = "white" {}
        _node_9110 ("node_9110", Color) = (0,0.006896734,1,1)
        _node_1641 ("node_1641", Color) = (0,1,0.5034482,1)
        _slider1 ("slider1", Range(-1, 1)) = -0.6918517
        _slider2 ("slider2", Range(-2, 1)) = 0.05921507
        _slider3 ("slider3", Range(0, 1)) = 0.3528411
        _fresnel ("fresnel", Range(0, 2)) = 2
        [MaterialToggle] _fresnelonoff ("fresnel on/off", Float ) = 0
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Cull Off
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform sampler2D _node_5256; uniform float4 _node_5256_ST;
            uniform float4 _node_9110;
            uniform float4 _node_1641;
            uniform float _slider1;
            uniform float _slider2;
            uniform float _slider3;
            uniform float _fresnel;
            uniform fixed _fresnelonoff;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                UNITY_FOG_COORDS(3)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                i.normalDir = normalize(i.normalDir);
                i.normalDir *= faceSign;
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float4 node_7758 = _Time + _TimeEditor;
                float2 node_769 = ((i.uv0*0.5)+(node_7758.g*_slider1)*float2(1,1));
                float4 node_9774 = tex2D(_node_5256,TRANSFORM_TEX(node_769, _node_5256));
                float4 node_7215 = _Time + _TimeEditor;
                float2 node_5410 = (((i.uv0*0.5)+(node_7215.g*_slider2)*float2(1,1))*2.0+-1.0);
                float4 node_7091 = tex2D(_node_5256,TRANSFORM_TEX(node_5410, _node_5256));
                float4 node_7730 = _Time + _TimeEditor;
                float2 node_9716 = ((i.uv0*0.125)+(node_7730.g*_slider3)*float2(0.2,0.2));
                float4 node_9580 = tex2D(_node_5256,TRANSFORM_TEX(node_9716, _node_5256));
                float3 node_5568 = lerp(_node_9110.rgb,_node_1641.rgb,lerp((((i.uv0+(node_9774.rgb.r*0.2))+(node_7091.rgb.r*0.2))+(node_9580.rgb.r*0.5)),dot((((i.uv0+(node_9774.rgb.r*0.2))+(node_7091.rgb.r*0.2))+(node_9580.rgb.r*0.5)),float3(0.3,0.59,0.11)),1.0).r);
                float3 emissive = lerp( node_5568, (node_5568+sin(pow(1.0-max(0,dot(i.normalDir, viewDirection)),_fresnel))), _fresnelonoff );
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
