// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:1,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:4249,x:36847,y:31976,varname:node_4249,prsc:2|emission-2602-OUT;n:type:ShaderForge.SFN_Lerp,id:6900,x:33356,y:32520,varname:node_6900,prsc:2|A-5516-RGB,B-5259-OUT,T-5384-RGB;n:type:ShaderForge.SFN_Multiply,id:5259,x:33066,y:32636,varname:node_5259,prsc:2|A-6084-RGB,B-5384-RGB;n:type:ShaderForge.SFN_Color,id:5516,x:32926,y:32352,ptovrint:False,ptlb:node_1429_copy,ptin:_node_1429_copy,varname:_node_1429_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0,c3:0.9310346,c4:1;n:type:ShaderForge.SFN_Tex2d,id:5384,x:32669,y:32521,ptovrint:False,ptlb:node_1859_copy,ptin:_node_1859_copy,varname:_node_1859_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:ae0049a4a1783f946ac225627cf79c28,ntxv:0,isnm:False|UVIN-3268-OUT;n:type:ShaderForge.SFN_Color,id:6084,x:32760,y:32210,ptovrint:False,ptlb:node_5189_copy,ptin:_node_5189_copy,varname:_node_5189_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0,c3:0,c4:1;n:type:ShaderForge.SFN_TexCoord,id:7008,x:31917,y:32057,varname:node_7008,prsc:2,uv:0;n:type:ShaderForge.SFN_Add,id:3268,x:32372,y:32196,varname:node_3268,prsc:2|A-7008-UVOUT,B-4211-OUT;n:type:ShaderForge.SFN_Time,id:9917,x:31871,y:32280,varname:node_9917,prsc:2;n:type:ShaderForge.SFN_Multiply,id:4211,x:32136,y:32271,varname:node_4211,prsc:2|A-9917-T,B-3613-OUT;n:type:ShaderForge.SFN_Slider,id:3613,x:31736,y:32511,ptovrint:False,ptlb:time nube_copy,ptin:_timenube_copy,varname:_timenube_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.04200784,max:1;n:type:ShaderForge.SFN_Add,id:4027,x:33803,y:32014,varname:node_4027,prsc:2|A-3625-OUT,B-6900-OUT;n:type:ShaderForge.SFN_Multiply,id:3625,x:33554,y:31728,varname:node_3625,prsc:2|A-1637-OUT,B-393-OUT;n:type:ShaderForge.SFN_OneMinus,id:6888,x:33310,y:32071,varname:node_6888,prsc:2|IN-393-OUT;n:type:ShaderForge.SFN_Fresnel,id:393,x:33007,y:31874,varname:node_393,prsc:2|NRM-5186-OUT,EXP-7323-OUT;n:type:ShaderForge.SFN_NormalVector,id:5186,x:32656,y:31829,prsc:2,pt:False;n:type:ShaderForge.SFN_ValueProperty,id:7323,x:32760,y:32019,ptovrint:False,ptlb:fresnel_copy,ptin:_fresnel_copy,varname:_fresnel_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.5;n:type:ShaderForge.SFN_Multiply,id:1637,x:33265,y:31815,varname:node_1637,prsc:2|A-1770-RGB,B-393-OUT;n:type:ShaderForge.SFN_Color,id:1770,x:32997,y:31544,ptovrint:False,ptlb:2color_copy,ptin:_2color_copy,varname:_2color_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0,c3:0,c4:1;n:type:ShaderForge.SFN_Code,id:9009,x:34649,y:32537,varname:node_9009,prsc:2,code:IAAgAGYAbABvAGEAdAAgAHIAZQB0ACAAPQAgADAAOwANACAAIABpAG4AdAAgAGkAdABlAHIAYQB0AGkAbwBuAHMAIAA9ACAANAA7AA0ACgAgACAAZgBvAHIAIAAoAGkAbgB0ACAAaQAgAD0AIAAwADsAIABpACAAPAAgAGkAdABlAHIAYQB0AGkAbwBuAHMAOwAgACsAKwBpACkADQAKACAAIAB7AA0ACgAgACAAIAAgACAAZgBsAG8AYQB0ADIAIABwACAAPQAgAGYAbABvAG8AcgAoAFUAVgAgACoAIAAoAGkAKwAxACkAKQA7AA0ACgAgACAAIAAgACAAZgBsAG8AYQB0ADIAIABmACAAPQAgAGYAcgBhAGMAKABVAFYAIAAqACAAKABpACsAMQApACkAOwANAAoAIAAgACAAIAAgAGYAIAA9ACAAZgAgACoAIABmACAAKgAgACgAMwAuADAAIAAtACAAMgAuADAAIAAqACAAZgApADsADQAKACAAIAAgACAAIABmAGwAbwBhAHQAIABuACAAPQAgAHAALgB4ACAAKwAgAHAALgB5ACAAKgAgADUANwAuADAAOwANAAoAIAAgACAAIAAgAGYAbABvAGEAdAA0ACAAbgBvAGkAcwBlACAAPQAgAGYAbABvAGEAdAA0ACgAbgAsACAAbgAgACsAIAAxACwAIABuACAAKwAgADUANwAuADAALAAgAG4AIAArACAANQA4AC4AMAApADsADQAKACAAIAAgACAAIABuAG8AaQBzAGUAIAA9ACAAZgByAGEAYwAoAHMAaQBuACgAbgBvAGkAcwBlACkAKgA0ADMANwAuADUAOAA1ADQANQAzACkAOwANAAoAIAAgACAAIAAgAHIAZQB0ACAAKwA9ACAAbABlAHIAcAAoAGwAZQByAHAAKABuAG8AaQBzAGUALgB4ACwAIABuAG8AaQBzAGUALgB5ACwAIABmAC4AeAApACwAIABsAGUAcgBwACgAbgBvAGkAcwBlAC4AegAsACAAbgBvAGkAcwBlAC4AdwAsACAAZgAuAHgAKQAsACAAZgAuAHkAKQAgACoAIAAoACAAaQB0AGUAcgBhAHQAaQBvAG4AcwAgAC8AIAAoAGkAKwAxACkAKQA7AA0ACgAgACAAfQANAAoAIAAgAHIAZQB0AHUAcgBuACAAcgBlAHQALwBpAHQAZQByAGEAdABpAG8AbgBzADsA,output:2,fname:noise,width:247,height:112,input:1,input_1_label:UV|A-9556-OUT;n:type:ShaderForge.SFN_Clamp01,id:7559,x:35038,y:32522,varname:node_7559,prsc:2|IN-9009-OUT;n:type:ShaderForge.SFN_OneMinus,id:1806,x:35362,y:32607,varname:node_1806,prsc:2|IN-7559-OUT;n:type:ShaderForge.SFN_Panner,id:370,x:34004,y:32406,varname:node_370,prsc:2,spu:0.4,spv:0.4|UVIN-442-UVOUT;n:type:ShaderForge.SFN_TexCoord,id:442,x:33761,y:32406,varname:node_442,prsc:2,uv:0;n:type:ShaderForge.SFN_Rotator,id:7334,x:34209,y:32503,varname:node_7334,prsc:2|UVIN-370-UVOUT;n:type:ShaderForge.SFN_Tau,id:7713,x:34076,y:32698,varname:node_7713,prsc:2;n:type:ShaderForge.SFN_Multiply,id:9556,x:34439,y:32534,varname:node_9556,prsc:2|A-7334-UVOUT,B-7713-OUT;n:type:ShaderForge.SFN_OneMinus,id:3807,x:35847,y:32652,varname:node_3807,prsc:2|IN-1806-OUT;n:type:ShaderForge.SFN_SwitchProperty,id:2166,x:36227,y:32576,ptovrint:False,ptlb:You_Wrong_VO_copy,ptin:_You_Wrong_VO_copy,varname:_You_Wrong_VO_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:True|A-9489-OUT,B-1806-OUT;n:type:ShaderForge.SFN_Vector1,id:9489,x:35828,y:32543,varname:node_9489,prsc:2,v1:0;n:type:ShaderForge.SFN_Multiply,id:3306,x:36575,y:32318,varname:node_3306,prsc:2|A-7105-UVOUT,B-2166-OUT,C-2166-OUT;n:type:ShaderForge.SFN_TexCoord,id:7105,x:36310,y:32255,varname:node_7105,prsc:2,uv:0;n:type:ShaderForge.SFN_Multiply,id:6509,x:33895,y:32604,varname:node_6509,prsc:2|A-5097-T,B-395-OUT;n:type:ShaderForge.SFN_Time,id:5097,x:33571,y:32630,varname:node_5097,prsc:2;n:type:ShaderForge.SFN_ValueProperty,id:395,x:33764,y:32729,ptovrint:False,ptlb:time_copy,ptin:_time_copy,varname:_time_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Power,id:597,x:34792,y:31706,varname:node_597,prsc:2|VAL-4027-OUT,EXP-6975-OUT;n:type:ShaderForge.SFN_Vector1,id:6975,x:34502,y:31829,varname:node_6975,prsc:2,v1:5;n:type:ShaderForge.SFN_Blend,id:2602,x:35208,y:31780,varname:node_2602,prsc:2,blmd:7,clmp:True|SRC-597-OUT,DST-4027-OUT;n:type:ShaderForge.SFN_ValueProperty,id:2341,x:36636,y:32171,ptovrint:False,ptlb:opacity,ptin:_opacity,varname:node_2341,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;proporder:5516-5384-6084-3613-7323-1770-2166-2341;pass:END;sub:END;*/

Shader "Custom/CygnusSecret" {
    Properties {
        _node_1429_copy ("node_1429_copy", Color) = (1,0,0.9310346,1)
        _node_1859_copy ("node_1859_copy", 2D) = "white" {}
        _node_5189_copy ("node_5189_copy", Color) = (1,0,0,1)
        _timenube_copy ("time nube_copy", Range(0, 1)) = 0.04200784
        _fresnel_copy ("fresnel_copy", Float ) = 0.5
        _2color_copy ("2color_copy", Color) = (1,0,0,1)
        [MaterialToggle] _You_Wrong_VO_copy ("You_Wrong_VO_copy", Float ) = 1
        _opacity ("opacity", Float ) = 1
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Cull Front
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform float4 _node_1429_copy;
            uniform sampler2D _node_1859_copy; uniform float4 _node_1859_copy_ST;
            uniform float4 _node_5189_copy;
            uniform float _timenube_copy;
            uniform float _fresnel_copy;
            uniform float4 _2color_copy;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                UNITY_FOG_COORDS(3)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(-v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float node_393 = pow(1.0-max(0,dot(i.normalDir, viewDirection)),_fresnel_copy);
                float4 node_9917 = _Time + _TimeEditor;
                float2 node_3268 = (i.uv0+(node_9917.g*_timenube_copy));
                float4 _node_1859_copy_var = tex2D(_node_1859_copy,TRANSFORM_TEX(node_3268, _node_1859_copy));
                float3 node_4027 = (((_2color_copy.rgb*node_393)*node_393)+lerp(_node_1429_copy.rgb,(_node_5189_copy.rgb*_node_1859_copy_var.rgb),_node_1859_copy_var.rgb));
                float3 emissive = saturate((node_4027/(1.0-pow(node_4027,5.0))));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
