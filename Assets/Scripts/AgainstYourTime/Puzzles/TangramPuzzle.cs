﻿using UnityEngine;
using System.Collections;
using System;

public class TangramPuzzle : PuzzleManager
{
    [Header ("Tangram sensors")]
    public TangramCollider[] topColliders;          // Top collider sensors
    public TangramCollider[] leftColliders;         // Left collider sensors
    public TangramCollider[] rightColliders;        // Right collider sensors
    public TangramCollider[] bottomColliders;       // Bottom collider sensors
    public TangramCollider[] floorColliders;
    public Transform[] snapPositions;
    private int indexSnapPosition;
    public Transform[] placementPositions;

    [Header("Tangram pieces")]
    public GameObject[] tangramPieces;                  // Pieces of the tangram you can move
    public GameObject objectGrabbed;

    [Header("Puzzle Reward")]
    public AudioSource successAudio;                // Audio of success when placing an object correctly
    public AudioSource levelCompleteAudio;          // Audio of success upon completing all the Tangram
    public Animator YonGearGate;

    // Various counters
    // Counters which memorize the total number of sensors
    private int topCollidersNumber = 0;
    private int leftCollidersNumber = 0;
    private int rightCollidersNumber = 0;
    private int bottomCollidersNumber = 0;
    private int floorCollidersNumber = 0;

    // Counters that memorize how many sensors are active
    private int topCollidersTouched = 0;
    private int leftCollidersTouched = 0;
    private int rightCollidersTouched = 0;
    private int bottomCollidersTouched = 0;
    private int floorCollidersTouched = 0;

    // Use this for initialization
    void Awake ()
    {
        // Determining how many sensors do we have
        topCollidersNumber = topColliders.Length;
        leftCollidersNumber = leftColliders.Length;
        rightCollidersNumber = rightColliders.Length;
        bottomCollidersNumber = bottomColliders.Length;
        floorCollidersNumber = floorColliders.Length;
    }

    void OnEnable()
    {
        for (int i = 0; i < topCollidersNumber; i++)
        {
            topColliders[i].onCollisionEnter += topTriggerTouched;
            topColliders[i].onCollisionExit += topTriggerUntouched;
        }
        for (int i = 0; i < leftCollidersNumber; i++)
        {
            leftColliders[i].onCollisionEnter += leftTriggerTouched;
            leftColliders[i].onCollisionExit += leftTriggerUntouched;
        }
        for (int i = 0; i < rightCollidersNumber; i++)
        {
            rightColliders[i].onCollisionEnter += rightTriggerTouched;
            rightColliders[i].onCollisionExit += rightTriggerUntouched;
        }
        for (int i = 0; i < bottomCollidersNumber; i++)
        {
            bottomColliders[i].onCollisionEnter += bottomTriggerTouched;
            bottomColliders[i].onCollisionExit += bottomTriggerUntouched;
        }
        for (int i = 0; i < floorCollidersNumber; i++)
        {
            floorColliders[i].onCollisionEnter += floorTriggerTouched;
            floorColliders[i].onCollisionExit += floorTriggerUntouched;
        }
    }

    void OnDisable()
    {
        for (int i = 0; i < topCollidersNumber; i++)
        {
            topColliders[i].onCollisionEnter -= topTriggerTouched;
            topColliders[i].onCollisionExit -= topTriggerUntouched;
        }
        for (int i = 0; i < leftCollidersNumber; i++)
        {
            leftColliders[i].onCollisionEnter -= leftTriggerTouched;
            leftColliders[i].onCollisionExit -= leftTriggerUntouched;
        }
        for (int i = 0; i < rightCollidersNumber; i++)
        {
            rightColliders[i].onCollisionEnter -= rightTriggerTouched;
            rightColliders[i].onCollisionExit -= rightTriggerUntouched;
        }
        for (int i = 0; i < bottomCollidersNumber; i++)
        {
            bottomColliders[i].onCollisionEnter -= bottomTriggerTouched;
            bottomColliders[i].onCollisionExit -= bottomTriggerUntouched;
        }
        for (int i = 0; i < floorCollidersNumber; i++)
        {
            floorColliders[i].onCollisionEnter -= floorTriggerTouched;
            floorColliders[i].onCollisionExit -= floorTriggerUntouched;
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
	
	}

    public void topTriggerTouched()
    {
        topCollidersTouched++;
        if (topCollidersTouched == topCollidersNumber)
        {
            successAudio.Play() ;
            checkIfTangramCompleted();
        }
    }

    public void leftTriggerTouched()
    {
        leftCollidersTouched++;
        if (leftCollidersTouched == leftCollidersNumber)
        {
            successAudio.Play();
            checkIfTangramCompleted();

        }
    }

    public void rightTriggerTouched()
    {
        rightCollidersTouched++;
        if (rightCollidersTouched == rightCollidersNumber)
        {
            successAudio.Play();
            checkIfTangramCompleted();

        }
    }

    public void bottomTriggerTouched()
    {
        bottomCollidersTouched++;
        if (bottomCollidersTouched == bottomCollidersNumber)
        {
            successAudio.Play();
            checkIfTangramCompleted();

        }
    }

    public void floorTriggerTouched()
    {
        floorCollidersTouched++;
        if (floorCollidersTouched == floorCollidersNumber)
        {
            successAudio.Play();
            checkIfTangramCompleted();

        }
    }

    public void topTriggerUntouched()
    {
        topCollidersTouched--;
    }

    public void leftTriggerUntouched()
    {
        leftCollidersTouched--;
    }

    public void rightTriggerUntouched()
    {
        rightCollidersTouched--;
    }

    public void bottomTriggerUntouched()
    {
        bottomCollidersTouched--;
    }

    public void floorTriggerUntouched()
    {
        floorCollidersTouched--;
    }


    public void checkIfTangramCompleted()
    {
        if (topCollidersTouched == topCollidersNumber &&
            leftCollidersTouched == leftCollidersNumber &&
            rightCollidersTouched == rightCollidersNumber &&
            bottomCollidersTouched == bottomCollidersNumber &&
            floorCollidersTouched == floorCollidersNumber)
        {
            YonGearGate.SetBool("HasWon", true);
            levelCompleteAudio.Play();
        }
    }

    public void switchObjectGrabbed(GameObject tangramPiece)
    {
        if (tangramPiece == null)
        {
            objectGrabbed = tangramPiece;
            return;
        }

        if (objectGrabbed == null)
        {
            objectGrabbed = tangramPiece;
            return;
        }

        if (tangramPiece != objectGrabbed)
        {
            objectGrabbed.GetComponent<VRTangramGrab>().GrabSwitchState();
            objectGrabbed = tangramPiece;
            return;
        }
    }

    /// <summary>
    /// This function is called by Tangram grab elements to determine which is the closest snap position 
    /// </summary>
    /// <param name="objectPosition">Usually the transform of the grab beacon</param>
    /// <returns>The closest snap position, or the beacon position if snap is too far</returns>
    public Transform GetClosestSnapPosition(Transform objectPosition)
    {
        Transform bestTarget = null;                                                    // Memorize the best snap position
        float closestDistanceSqr = Mathf.Infinity;                                      // Distance from the snap position
        Vector3 currentPosition = objectPosition.transform.position;                    // Current position of the beacon
        indexSnapPosition = -1;                                                          // Index of the snap position selected
        int tempIndex = 0;                                                              // Current index in the cycle

        // Cycle through all the snap positions
        foreach (Transform potentialTarget in snapPositions)
        {
            Vector3 directionToTarget = potentialTarget.position - currentPosition;     // Gets direction vector between beacon and snap

            float dSqrToTarget = directionToTarget.sqrMagnitude;

            // Evaluates if the distance is smaller that than the previous ones
            if (dSqrToTarget < closestDistanceSqr && dSqrToTarget < 1.5f)
            {
                // Saves the index
                indexSnapPosition = tempIndex;

                // If the distance is smaller, saves information about the new snap position
                closestDistanceSqr = dSqrToTarget;
                bestTarget = potentialTarget;
            }
            tempIndex++;
        }
        if (bestTarget == null)
        {
            return objectPosition;
        }
        else
        {
            return bestTarget;
        }
    }

    /// <summary>
    /// Returns the correspondend placement position after getting the snap position
    /// </summary>
    /// <param name="objectPosition"></param>
    /// <returns></returns>
    public Transform GetCorrectPlacementPosition()
    {
        if (indexSnapPosition == -1)
        {
            return null;
        }
        else
        {
            return placementPositions[indexSnapPosition];
        }
    }

    public override void ResetPuzzle()
    {
        throw new NotImplementedException();
    }

    public override void CompletePuzzle()
    {
        throw new NotImplementedException();
    }
}
