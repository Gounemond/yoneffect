// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:1,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:6935,x:33329,y:32862,varname:node_6935,prsc:2|emission-7107-OUT,transm-4874-OUT,lwrap-4874-OUT,voffset-1577-OUT;n:type:ShaderForge.SFN_Multiply,id:1713,x:32876,y:32926,varname:node_1713,prsc:2|A-3659-RGB,B-1464-OUT,C-4874-OUT;n:type:ShaderForge.SFN_Vector1,id:1464,x:32722,y:33174,varname:node_1464,prsc:2,v1:5;n:type:ShaderForge.SFN_Color,id:3659,x:32635,y:32908,ptovrint:False,ptlb:node_9323_copy,ptin:_node_9323_copy,varname:_node_9323_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.2313726,c2:0.9803922,c3:0.7215686,c4:1;n:type:ShaderForge.SFN_Power,id:4874,x:32500,y:33049,varname:node_4874,prsc:2|VAL-6052-OUT,EXP-8552-OUT;n:type:ShaderForge.SFN_Multiply,id:1577,x:32973,y:33270,varname:node_1577,prsc:2|A-4874-OUT,B-1849-OUT,C-6536-OUT;n:type:ShaderForge.SFN_NormalVector,id:6536,x:32747,y:33400,prsc:2,pt:False;n:type:ShaderForge.SFN_Vector1,id:1849,x:32747,y:33324,varname:node_1849,prsc:2,v1:0.2;n:type:ShaderForge.SFN_Multiply,id:6052,x:32308,y:32974,varname:node_6052,prsc:2|A-6661-OUT,B-4232-OUT;n:type:ShaderForge.SFN_Vector1,id:8552,x:32304,y:33175,varname:node_8552,prsc:2,v1:3;n:type:ShaderForge.SFN_Abs,id:6661,x:32167,y:32868,varname:node_6661,prsc:2|IN-224-OUT;n:type:ShaderForge.SFN_Vector1,id:4232,x:32167,y:33083,varname:node_4232,prsc:2,v1:1.5;n:type:ShaderForge.SFN_Subtract,id:224,x:32005,y:32833,varname:node_224,prsc:2|A-7640-OUT,B-3417-OUT;n:type:ShaderForge.SFN_Vector1,id:3417,x:31841,y:32959,varname:node_3417,prsc:2,v1:0.5;n:type:ShaderForge.SFN_Frac,id:7640,x:31783,y:32725,varname:node_7640,prsc:2|IN-4754-OUT;n:type:ShaderForge.SFN_ComponentMask,id:4754,x:31513,y:32669,varname:node_4754,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-6985-UVOUT;n:type:ShaderForge.SFN_Panner,id:6985,x:31324,y:32592,varname:node_6985,prsc:2,spu:0.25,spv:0|UVIN-1083-UVOUT;n:type:ShaderForge.SFN_TexCoord,id:1083,x:31128,y:32508,varname:node_1083,prsc:2,uv:0;n:type:ShaderForge.SFN_Lerp,id:7107,x:33053,y:32741,varname:node_7107,prsc:2|A-8255-RGB,B-1713-OUT,T-4874-OUT;n:type:ShaderForge.SFN_Color,id:8255,x:32805,y:32635,ptovrint:False,ptlb:node_8255,ptin:_node_8255,varname:node_8255,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5808823,c2:0,c3:0.5637975,c4:1;proporder:3659-8255;pass:END;sub:END;*/

Shader "Custom/greencristalsecret" {
    Properties {
        _node_9323_copy ("node_9323_copy", Color) = (0.2313726,0.9803922,0.7215686,1)
        _node_8255 ("node_8255", Color) = (0.5808823,0,0.5637975,1)
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Cull Front
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform float4 _node_9323_copy;
            uniform float4 _node_8255;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
                UNITY_FOG_COORDS(2)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(-v.normal);
                float4 node_8774 = _Time + _TimeEditor;
                float node_4874 = pow((abs((frac((o.uv0+node_8774.g*float2(0.25,0)).r)-0.5))*1.5),3.0);
                v.vertex.xyz += (node_4874*0.2*v.normal);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float4 node_8774 = _Time + _TimeEditor;
                float node_4874 = pow((abs((frac((i.uv0+node_8774.g*float2(0.25,0)).r)-0.5))*1.5),3.0);
                float3 emissive = lerp(_node_8255.rgb,(_node_9323_copy.rgb*5.0*node_4874),node_4874);
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(-v.normal);
                float4 node_2425 = _Time + _TimeEditor;
                float node_4874 = pow((abs((frac((o.uv0+node_2425.g*float2(0.25,0)).r)-0.5))*1.5),3.0);
                v.vertex.xyz += (node_4874*0.2*v.normal);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 normalDirection = i.normalDir;
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
