using UnityEngine;
using VRStandardAssets.Utils;
using System.Collections;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System;
using System.Linq;

[RequireComponent (typeof(VRInteractiveItem))]
[RequireComponent (typeof(Rigidbody))]
[RequireComponent (typeof(HanoiCollider))]
public class VRHanoiGrab : MonoBehaviour 
{
	public float  correctionforce = 50.0f;              // The intensity of the gravity force toward the grabbing beacon
	public Transform beacon;                            // The beacon used as centre of gravity to grab and move around the object
    public HanoiPuzzle hanoiManager;                    // Reference to the hanoi puzzle manager

    private Material m_MyMaterial;                      // The current material which will be modified upon various conditions

    private Transform m_SelectedBeacon;                 // The currently selected posizion where the piece will be placed
    private Vector3 m_beforeGrabPosition;               // The starting position of the piece before being placed, used to reset the piece in case of wrong move


	private bool  grabbed  = false;                     // Flag that indicates if the object is grabbed
    private bool  onHover = false;                      // Flag that indicates if the object is currently targeted by the player

    private Rigidbody m_MyRigidbody;                    // Reference to own rigidbody
    private VRInteractiveItem _myVRitem;                // Reference to the interactable script

    public void OnEnable()
    {
        _myVRitem.OnClick += GrabSwitchState;
        _myVRitem.OnOver += HoverIn;
        _myVRitem.OnOut += HoverOut;

        GetComponent<HanoiCollider>().onCollisionEnter += ResetPosition;
    }

    public void OnDisable()
    {
        _myVRitem.OnClick -= GrabSwitchState;
        _myVRitem.OnOver -= HoverIn;
        _myVRitem.OnOut -= HoverOut;

        GetComponent<HanoiCollider>().onCollisionEnter -= ResetPosition;
    }

    void Awake () 
	{

        m_MyRigidbody = GetComponent<Rigidbody>();
        _myVRitem = GetComponent<VRInteractiveItem>();
        m_MyMaterial = GetComponent<Renderer>().material;
        m_SelectedBeacon = hanoiManager.snapPositions[0];
    }

    void Update () 
	{
        if (grabbed)
        {
            m_SelectedBeacon = hanoiManager.GetClosestSnapPosition(beacon);
        }


        if (grabbed && Input.GetButtonDown("Fire1"))
        {
            GrabSwitchState();
            StartCoroutine(DisableAMoment());
            hanoiManager.switchObjectGrabbed(null);     // Tells the manager to set the object grabbed as null
        }
        // Update gravity force around the GrabBeacon if you actually have grabbed an object
        // The grab method is inspired from the Half Life series
        if (grabbed)
		{
			Vector3 force = new Vector3(m_SelectedBeacon.transform.position.x - transform.position.x, beacon.transform.position.y - transform.position.y, m_SelectedBeacon.transform.position.z - transform.position.z);
            m_MyRigidbody.velocity = force.normalized * m_MyRigidbody.velocity.magnitude;
            m_MyRigidbody.AddForce(force * correctionforce);
            m_MyRigidbody.velocity *= Mathf.Min(1.0f, force.magnitude / 2 );
		}
    }

    public void GrabSwitchState()
    {
        if (onHover)
        {
            if (!grabbed)
            {
                hanoiManager.switchObjectGrabbed(this.gameObject);      // Tells the manages to set this object as grabbed
                grabbed = true;                                         // Sets grabbed state at true
                m_MyRigidbody.useGravity = false;                       // Removes gravity force
                m_beforeGrabPosition = transform.position;              // Sets the new originalgrab position
                m_MyRigidbody.constraints = RigidbodyConstraints.FreezeRotation;
            }
            else
            {
                hanoiManager.switchObjectGrabbed(null);     // Tells the manager to set the object grabbed as null
                grabbed = false;                            // The object is no more grabbed
                transform.position = new Vector3(m_SelectedBeacon.transform.position.x, transform.position.y, m_SelectedBeacon.transform.position.z);
                m_MyRigidbody.velocity = Vector3.zero;
                m_MyRigidbody.angularVelocity = Vector3.zero;
                m_MyRigidbody.useGravity = true;            // Reapplies gravity force
                m_MyRigidbody.constraints = RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezeRotation;


            }
        }
        else
        {
            if (grabbed)
            {
                grabbed = false;                            // The object is no more grabbed
                transform.position = new Vector3(m_SelectedBeacon.transform.position.x, transform.position.y, m_SelectedBeacon.transform.position.z);
                m_MyRigidbody.velocity = Vector3.zero;
                m_MyRigidbody.angularVelocity = Vector3.zero;
                m_MyRigidbody.useGravity = true;             // Reapplies gravity force
                m_MyRigidbody.constraints = RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezeRotation;
            }
        }
    }

    public void HoverIn()
    {
        if (hanoiManager.objectGrabbed == null)
        {
            m_MyMaterial.SetColor("_main", Color.green);
            onHover = true;
        }
    }

    public void HoverOut()
    {
        m_MyMaterial.SetColor("_main", Color.black);
        onHover = false;
    }

    public void AlignRight()
    {
        m_MyRigidbody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
        m_MyRigidbody.constraints = RigidbodyConstraints.FreezeRotation;

    }

    public void AlignLeft()
    {
        m_MyRigidbody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
        m_MyRigidbody.constraints = RigidbodyConstraints.FreezeRotation;
    }

    public void ResetPosition()
    {
        transform.position = m_beforeGrabPosition;
    }

    public IEnumerator DisableAMoment()
    {
        _myVRitem.OnClick -= GrabSwitchState;
        yield return new WaitForSeconds(0.1f);
        _myVRitem.OnClick += GrabSwitchState;
    }
}