// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:2,rntp:3,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:5402,x:35936,y:31941,varname:node_5402,prsc:2|emission-7078-OUT,clip-5431-OUT;n:type:ShaderForge.SFN_Lerp,id:1727,x:33850,y:32224,varname:node_1727,prsc:2|A-4115-RGB,B-8575-UVOUT,T-5804-RGB;n:type:ShaderForge.SFN_Color,id:4115,x:32967,y:32079,ptovrint:False,ptlb:node_1429_copy,ptin:_node_1429_copy,varname:_node_1429_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0,c3:0.9310346,c4:1;n:type:ShaderForge.SFN_Tex2d,id:5804,x:32710,y:32830,ptovrint:False,ptlb:node_1859_copy,ptin:_node_1859_copy,varname:_node_1859_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:ae0049a4a1783f946ac225627cf79c28,ntxv:0,isnm:False|UVIN-3928-UVOUT;n:type:ShaderForge.SFN_Multiply,id:7366,x:33502,y:31856,varname:node_7366,prsc:2|A-2815-OUT,B-2815-OUT;n:type:ShaderForge.SFN_OneMinus,id:3235,x:33310,y:32071,varname:node_3235,prsc:2|IN-2815-OUT;n:type:ShaderForge.SFN_Fresnel,id:2815,x:33007,y:31874,varname:node_2815,prsc:2|NRM-6289-OUT,EXP-8845-OUT;n:type:ShaderForge.SFN_NormalVector,id:6289,x:32656,y:31829,prsc:2,pt:False;n:type:ShaderForge.SFN_ValueProperty,id:8845,x:32760,y:32019,ptovrint:False,ptlb:fresnel_copy,ptin:_fresnel_copy,varname:_fresnel_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.5;n:type:ShaderForge.SFN_Add,id:7078,x:34039,y:31839,varname:node_7078,prsc:2|A-7366-OUT,B-1727-OUT;n:type:ShaderForge.SFN_ScreenPos,id:8575,x:33102,y:32469,varname:node_8575,prsc:2,sctp:0;n:type:ShaderForge.SFN_Panner,id:3928,x:32460,y:32865,varname:node_3928,prsc:2,spu:1,spv:1|UVIN-336-UVOUT,DIST-3432-OUT;n:type:ShaderForge.SFN_TexCoord,id:336,x:32167,y:32701,varname:node_336,prsc:2,uv:0;n:type:ShaderForge.SFN_Time,id:534,x:31846,y:32709,varname:node_534,prsc:2;n:type:ShaderForge.SFN_Multiply,id:3432,x:32310,y:32958,varname:node_3432,prsc:2|A-534-T,B-9439-OUT;n:type:ShaderForge.SFN_ValueProperty,id:9439,x:31989,y:32990,ptovrint:False,ptlb:time,ptin:_time,varname:node_9439,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_Vector1,id:7914,x:34049,y:32359,varname:node_7914,prsc:2,v1:1;n:type:ShaderForge.SFN_ComponentMask,id:5431,x:34811,y:32211,varname:node_5431,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-6488-OUT;n:type:ShaderForge.SFN_SwitchProperty,id:6488,x:34367,y:32202,ptovrint:False,ptlb:switch opacity,ptin:_switchopacity,varname:node_6488,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:False|A-7914-OUT,B-7078-OUT;n:type:ShaderForge.SFN_TexCoord,id:5976,x:32408,y:32250,varname:node_5976,prsc:2,uv:0;n:type:ShaderForge.SFN_Multiply,id:4334,x:33467,y:32385,varname:node_4334,prsc:2|A-551-OUT,B-8575-UVOUT;n:type:ShaderForge.SFN_Multiply,id:8715,x:32913,y:32299,varname:node_8715,prsc:2|A-5976-U,B-2234-OUT;n:type:ShaderForge.SFN_Multiply,id:8352,x:32908,y:32411,varname:node_8352,prsc:2|A-5976-V,B-5758-OUT;n:type:ShaderForge.SFN_ValueProperty,id:2234,x:32727,y:32347,ptovrint:False,ptlb:u,ptin:_u,varname:node_2234,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_ValueProperty,id:5758,x:32718,y:32477,ptovrint:False,ptlb:v,ptin:_v,varname:node_5758,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.2;n:type:ShaderForge.SFN_Multiply,id:2093,x:33091,y:32303,varname:node_2093,prsc:2|A-8715-OUT,B-8352-OUT;n:type:ShaderForge.SFN_OneMinus,id:551,x:33310,y:32284,varname:node_551,prsc:2|IN-2093-OUT;proporder:4115-5804-8845-9439-6488-2234-5758;pass:END;sub:END;*/

Shader "Custom/Cynusinthemiddle" {
    Properties {
        _node_1429_copy ("node_1429_copy", Color) = (1,0,0.9310346,1)
        _node_1859_copy ("node_1859_copy", 2D) = "white" {}
        _fresnel_copy ("fresnel_copy", Float ) = 0.5
        _time ("time", Float ) = 0
        [MaterialToggle] _switchopacity ("switch opacity", Float ) = 1
        _u ("u", Float ) = 1
        _v ("v", Float ) = 0.2
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "Queue"="AlphaTest"
            "RenderType"="TransparentCutout"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform float4 _node_1429_copy;
            uniform sampler2D _node_1859_copy; uniform float4 _node_1859_copy_ST;
            uniform float _fresnel_copy;
            uniform float _time;
            uniform fixed _switchopacity;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float4 screenPos : TEXCOORD3;
                UNITY_FOG_COORDS(4)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                o.screenPos = o.pos;
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                i.screenPos = float4( i.screenPos.xy / i.screenPos.w, 0, 0 );
                i.screenPos.y *= _ProjectionParams.x;
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float node_2815 = pow(1.0-max(0,dot(i.normalDir, viewDirection)),_fresnel_copy);
                float4 node_534 = _Time + _TimeEditor;
                float2 node_3928 = (i.uv0+(node_534.g*_time)*float2(1,1));
                float4 _node_1859_copy_var = tex2D(_node_1859_copy,TRANSFORM_TEX(node_3928, _node_1859_copy));
                float3 node_7078 = ((node_2815*node_2815)+lerp(_node_1429_copy.rgb,float3(i.screenPos.rg,0.0),_node_1859_copy_var.rgb));
                clip(lerp( 1.0, node_7078, _switchopacity ).r - 0.5);
////// Lighting:
////// Emissive:
                float3 emissive = node_7078;
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform float4 _node_1429_copy;
            uniform sampler2D _node_1859_copy; uniform float4 _node_1859_copy_ST;
            uniform float _fresnel_copy;
            uniform float _time;
            uniform fixed _switchopacity;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
                float4 posWorld : TEXCOORD2;
                float3 normalDir : TEXCOORD3;
                float4 screenPos : TEXCOORD4;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                o.screenPos = o.pos;
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                i.screenPos = float4( i.screenPos.xy / i.screenPos.w, 0, 0 );
                i.screenPos.y *= _ProjectionParams.x;
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float node_2815 = pow(1.0-max(0,dot(i.normalDir, viewDirection)),_fresnel_copy);
                float4 node_534 = _Time + _TimeEditor;
                float2 node_3928 = (i.uv0+(node_534.g*_time)*float2(1,1));
                float4 _node_1859_copy_var = tex2D(_node_1859_copy,TRANSFORM_TEX(node_3928, _node_1859_copy));
                float3 node_7078 = ((node_2815*node_2815)+lerp(_node_1429_copy.rgb,float3(i.screenPos.rg,0.0),_node_1859_copy_var.rgb));
                clip(lerp( 1.0, node_7078, _switchopacity ).r - 0.5);
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
