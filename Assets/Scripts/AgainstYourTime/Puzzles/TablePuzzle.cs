﻿using UnityEngine;
using System.Collections;
using System;

public class TablePuzzle : PuzzleManager
{
    [Header ("Plate sensors")]
    public TablePlateCollider[] plateColliders;          // Top collider sensors


    [Header("Puzzle Reward")]
    public AudioSource levelCompleteAudio;          // Audio of success upon completing all the Tangram
    public Animator YonGearGate;

    // Various counters
    // Counters which memorize the total number of sensors
    private int collidersNumber = 0;


    // Counters that memorize how many sensors are active
    private int collidersTouched = 0;


    // Use this for initialization
    void Awake ()
    {
        // Determining how many sensors do we have
        collidersNumber = plateColliders.Length;
    }

    void OnEnable()
    {
        for (int i = 0; i < collidersNumber; i++)
        {
            plateColliders[i].onCollisionEnter += topTriggerTouched;
            plateColliders[i].onCollisionExit += topTriggerUntouched;
        }
    }

    void OnDisable()
    {
        for (int i = 0; i < collidersNumber; i++)
        {
            plateColliders[i].onCollisionEnter -= topTriggerTouched;
            plateColliders[i].onCollisionExit -= topTriggerUntouched;
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
	
	}

    public void topTriggerTouched()
    {
        collidersTouched++;
        if (collidersTouched == collidersNumber)
        {
            YonGearGate.SetBool("HasWon", true);
            levelCompleteAudio.Play();
        }
    }

    public void topTriggerUntouched()
    {
        collidersTouched--;
    }

    /*public void switchObjectGrabbed(GameObject tangramPiece)
    {
        if (tangramPiece == null)
        {
            objectGrabbed = tangramPiece;
            return;
        }

        if (objectGrabbed == null)
        {
            objectGrabbed = tangramPiece;
            return;
        }

        if (tangramPiece != objectGrabbed)
        {
            objectGrabbed.GetComponent<VRTangramGrab>().GrabSwitchState();
            objectGrabbed = tangramPiece;
            return;
        }
    }*/

    public override void ResetPuzzle()
    {
        throw new NotImplementedException();
    }

    public override void CompletePuzzle()
    {
        throw new NotImplementedException();
    }
}
