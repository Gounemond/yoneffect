﻿using UnityEngine;
using System;
using System.Collections;

[RequireComponent (typeof(BoxCollider))]
public class TablePlateCollider : MonoBehaviour
{

    public event Action onCollisionEnter;              // Called when a Tangram Object enters the collider
    public event Action onCollisionExit;               // Called when a Tangram Object enters the collider


    private int objectsInCollider = 0;

    public YonBehaviour[] obj;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider ObjectCollision)
    {
        if (objectsInCollider == 0)
        {
            for (int i = 0; i < obj.Length; i++)
            {
                obj[i].Activate();
            }

            if (onCollisionEnter != null)
            {
                onCollisionEnter();
            }

        }
        objectsInCollider++;
    }

    void OnTriggerExit(Collider ObjectCollision)
    {
        if (objectsInCollider == 1)
        {
            for (int i = 0; i < obj.Length; i++)
            {
                obj[i].deActivate();
            }
            if (onCollisionExit != null)
            {
                onCollisionExit();
            }
            objectsInCollider--;
        }
        else
        {
            objectsInCollider--;
        }
    }
}
