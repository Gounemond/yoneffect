// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:4824,x:34775,y:32890,varname:node_4824,prsc:2|emission-9085-OUT;n:type:ShaderForge.SFN_Tex2d,id:73,x:32856,y:32658,ptovrint:False,ptlb:Diffuse/Alpha TEX,ptin:_DiffuseAlphaTEX,varname:node_73,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:05f8de7f980ec4f4b972c38ad3db0ebb,ntxv:0,isnm:False|UVIN-8882-UVOUT;n:type:ShaderForge.SFN_Parallax,id:9315,x:32210,y:32628,varname:node_9315,prsc:2|UVIN-4993-UVOUT,HEI-3281-OUT,DEP-4642-OUT;n:type:ShaderForge.SFN_Slider,id:4642,x:31793,y:32800,ptovrint:False,ptlb:DepR,ptin:_DepR,varname:node_4642,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-10,cur:10,max:10;n:type:ShaderForge.SFN_Slider,id:3281,x:31793,y:32656,ptovrint:False,ptlb:HeiR,ptin:_HeiR,varname:node_3281,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-10,cur:0,max:10;n:type:ShaderForge.SFN_Tex2d,id:4543,x:32843,y:32974,ptovrint:False,ptlb:Diffuse/Alpha TEX_copy,ptin:_DiffuseAlphaTEX_copy,varname:_DiffuseAlphaTEX_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:05f8de7f980ec4f4b972c38ad3db0ebb,ntxv:0,isnm:False|UVIN-3929-UVOUT;n:type:ShaderForge.SFN_Parallax,id:8124,x:32168,y:33163,varname:node_8124,prsc:2|UVIN-21-UVOUT,HEI-9100-OUT,DEP-5686-OUT;n:type:ShaderForge.SFN_TexCoord,id:21,x:31751,y:33059,varname:node_21,prsc:2,uv:0;n:type:ShaderForge.SFN_Slider,id:5686,x:31751,y:33316,ptovrint:False,ptlb:DepR_copy,ptin:_DepR_copy,varname:_DepR_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-10,cur:8,max:10;n:type:ShaderForge.SFN_Slider,id:9100,x:31751,y:33225,ptovrint:False,ptlb:HeiR_copy,ptin:_HeiR_copy,varname:_HeiR_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-10,cur:0,max:10;n:type:ShaderForge.SFN_Tex2d,id:6250,x:32843,y:33250,ptovrint:False,ptlb:Diffuse/Alpha TEX_copy_copy,ptin:_DiffuseAlphaTEX_copy_copy,varname:_DiffuseAlphaTEX_copy_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:05f8de7f980ec4f4b972c38ad3db0ebb,ntxv:0,isnm:False|UVIN-1021-UVOUT;n:type:ShaderForge.SFN_Parallax,id:5902,x:32166,y:33516,varname:node_5902,prsc:2|UVIN-5952-UVOUT,HEI-1963-OUT,DEP-2766-OUT;n:type:ShaderForge.SFN_TexCoord,id:5952,x:31749,y:33412,varname:node_5952,prsc:2,uv:0;n:type:ShaderForge.SFN_Slider,id:2766,x:31749,y:33669,ptovrint:False,ptlb:DepR_copy_copy,ptin:_DepR_copy_copy,varname:_DepR_copy_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-10,cur:7,max:10;n:type:ShaderForge.SFN_Slider,id:1963,x:31749,y:33578,ptovrint:False,ptlb:HeiR_copy_copy,ptin:_HeiR_copy_copy,varname:_HeiR_copy_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-10,cur:0,max:10;n:type:ShaderForge.SFN_Tex2d,id:4352,x:32867,y:33633,ptovrint:False,ptlb:Diffuse/Alpha TEX_copy_copy_copy,ptin:_DiffuseAlphaTEX_copy_copy_copy,varname:_DiffuseAlphaTEX_copy_copy_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:05f8de7f980ec4f4b972c38ad3db0ebb,ntxv:0,isnm:False|UVIN-496-UVOUT;n:type:ShaderForge.SFN_Parallax,id:961,x:32169,y:33865,varname:node_961,prsc:2|UVIN-6323-UVOUT,HEI-113-OUT,DEP-9921-OUT;n:type:ShaderForge.SFN_TexCoord,id:6323,x:31752,y:33761,varname:node_6323,prsc:2,uv:0;n:type:ShaderForge.SFN_Slider,id:9921,x:31752,y:34018,ptovrint:False,ptlb:DepR_copy_copy_copy,ptin:_DepR_copy_copy_copy,varname:_DepR_copy_copy_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-10,cur:6,max:10;n:type:ShaderForge.SFN_Slider,id:113,x:31752,y:33927,ptovrint:False,ptlb:HeiR_copy_copy_copy,ptin:_HeiR_copy_copy_copy,varname:_HeiR_copy_copy_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-10,cur:0,max:10;n:type:ShaderForge.SFN_Add,id:6399,x:33121,y:33053,varname:node_6399,prsc:2|A-73-R,B-4543-G,C-6250-B,D-4352-A;n:type:ShaderForge.SFN_TexCoord,id:4993,x:31917,y:32372,varname:node_4993,prsc:2,uv:0;n:type:ShaderForge.SFN_Rotator,id:8882,x:32487,y:32640,varname:node_8882,prsc:2|UVIN-9315-UVOUT,SPD-7929-OUT;n:type:ShaderForge.SFN_Slider,id:7929,x:32291,y:32870,ptovrint:False,ptlb:rotator_1,ptin:_rotator_1,varname:node_7929,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-1,cur:0.4988696,max:1;n:type:ShaderForge.SFN_Rotator,id:3929,x:32498,y:33028,varname:node_3929,prsc:2|UVIN-8124-UVOUT,SPD-1229-OUT;n:type:ShaderForge.SFN_Slider,id:1229,x:32326,y:33224,ptovrint:False,ptlb:rotator_2,ptin:_rotator_2,varname:node_1229,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-1,cur:0,max:1;n:type:ShaderForge.SFN_Rotator,id:1021,x:32501,y:33405,varname:node_1021,prsc:2|UVIN-5902-UVOUT,SPD-2202-OUT;n:type:ShaderForge.SFN_Slider,id:2202,x:32326,y:33556,ptovrint:False,ptlb:rotator_3,ptin:_rotator_3,varname:node_2202,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-1,cur:0,max:1;n:type:ShaderForge.SFN_Rotator,id:496,x:32575,y:33719,varname:node_496,prsc:2|UVIN-961-UVOUT,SPD-3995-OUT;n:type:ShaderForge.SFN_Slider,id:3995,x:32367,y:33927,ptovrint:False,ptlb:rotator_4,ptin:_rotator_4,varname:node_3995,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-1,cur:0,max:1;n:type:ShaderForge.SFN_Tex2d,id:7943,x:33471,y:32594,ptovrint:False,ptlb:node_7943,ptin:_node_7943,varname:node_7943,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:600e185cf40947845b6220297740efbb,ntxv:0,isnm:False|UVIN-2053-UVOUT;n:type:ShaderForge.SFN_Add,id:9085,x:33522,y:32889,varname:node_9085,prsc:2|A-3492-OUT,B-6399-OUT;n:type:ShaderForge.SFN_Parallax,id:154,x:33071,y:32378,varname:node_154,prsc:2|UVIN-2780-UVOUT,HEI-2480-OUT,DEP-4820-OUT;n:type:ShaderForge.SFN_TexCoord,id:2780,x:32814,y:32320,varname:node_2780,prsc:2,uv:0;n:type:ShaderForge.SFN_Vector1,id:2480,x:32738,y:32511,varname:node_2480,prsc:2,v1:0.9;n:type:ShaderForge.SFN_Vector1,id:4820,x:32814,y:32570,varname:node_4820,prsc:2,v1:1;n:type:ShaderForge.SFN_Rotator,id:2053,x:33326,y:32450,varname:node_2053,prsc:2|UVIN-154-UVOUT,SPD-3403-OUT;n:type:ShaderForge.SFN_Slider,id:3403,x:33047,y:32628,ptovrint:False,ptlb:node_3403,ptin:_node_3403,varname:node_3403,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.02907673,max:1;n:type:ShaderForge.SFN_Multiply,id:3492,x:33653,y:32400,varname:node_3492,prsc:2|A-4314-OUT,B-7943-RGB;n:type:ShaderForge.SFN_Vector1,id:4314,x:33495,y:32342,varname:node_4314,prsc:2,v1:0.3;proporder:73-4642-3281-4543-5686-9100-6250-2766-1963-4352-9921-113-7929-1229-2202-3995-7943-3403;pass:END;sub:END;*/

Shader "Custom/AstroStars" {
    Properties {
        _DiffuseAlphaTEX ("Diffuse/Alpha TEX", 2D) = "white" {}
        _DepR ("DepR", Range(-10, 10)) = 10
        _HeiR ("HeiR", Range(-10, 10)) = 0
        _DiffuseAlphaTEX_copy ("Diffuse/Alpha TEX_copy", 2D) = "white" {}
        _DepR_copy ("DepR_copy", Range(-10, 10)) = 8
        _HeiR_copy ("HeiR_copy", Range(-10, 10)) = 0
        _DiffuseAlphaTEX_copy_copy ("Diffuse/Alpha TEX_copy_copy", 2D) = "white" {}
        _DepR_copy_copy ("DepR_copy_copy", Range(-10, 10)) = 7
        _HeiR_copy_copy ("HeiR_copy_copy", Range(-10, 10)) = 0
        _DiffuseAlphaTEX_copy_copy_copy ("Diffuse/Alpha TEX_copy_copy_copy", 2D) = "white" {}
        _DepR_copy_copy_copy ("DepR_copy_copy_copy", Range(-10, 10)) = 6
        _HeiR_copy_copy_copy ("HeiR_copy_copy_copy", Range(-10, 10)) = 0
        _rotator_1 ("rotator_1", Range(-1, 1)) = 0.4988696
        _rotator_2 ("rotator_2", Range(-1, 1)) = 0
        _rotator_3 ("rotator_3", Range(-1, 1)) = 0
        _rotator_4 ("rotator_4", Range(-1, 1)) = 0
        _node_7943 ("node_7943", 2D) = "white" {}
        _node_3403 ("node_3403", Range(0, 1)) = 0.02907673
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform sampler2D _DiffuseAlphaTEX; uniform float4 _DiffuseAlphaTEX_ST;
            uniform float _DepR;
            uniform float _HeiR;
            uniform sampler2D _DiffuseAlphaTEX_copy; uniform float4 _DiffuseAlphaTEX_copy_ST;
            uniform float _DepR_copy;
            uniform float _HeiR_copy;
            uniform sampler2D _DiffuseAlphaTEX_copy_copy; uniform float4 _DiffuseAlphaTEX_copy_copy_ST;
            uniform float _DepR_copy_copy;
            uniform float _HeiR_copy_copy;
            uniform sampler2D _DiffuseAlphaTEX_copy_copy_copy; uniform float4 _DiffuseAlphaTEX_copy_copy_copy_ST;
            uniform float _DepR_copy_copy_copy;
            uniform float _HeiR_copy_copy_copy;
            uniform float _rotator_1;
            uniform float _rotator_2;
            uniform float _rotator_3;
            uniform float _rotator_4;
            uniform sampler2D _node_7943; uniform float4 _node_7943_ST;
            uniform float _node_3403;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 bitangentDir : TEXCOORD4;
                UNITY_FOG_COORDS(5)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float4 node_8513 = _Time + _TimeEditor;
                float node_2053_ang = node_8513.g;
                float node_2053_spd = _node_3403;
                float node_2053_cos = cos(node_2053_spd*node_2053_ang);
                float node_2053_sin = sin(node_2053_spd*node_2053_ang);
                float2 node_2053_piv = float2(0.5,0.5);
                float2 node_2053 = (mul((1.0*(0.9 - 0.5)*mul(tangentTransform, viewDirection).xy + i.uv0).rg-node_2053_piv,float2x2( node_2053_cos, -node_2053_sin, node_2053_sin, node_2053_cos))+node_2053_piv);
                float4 _node_7943_var = tex2D(_node_7943,TRANSFORM_TEX(node_2053, _node_7943));
                float node_8882_ang = node_8513.g;
                float node_8882_spd = _rotator_1;
                float node_8882_cos = cos(node_8882_spd*node_8882_ang);
                float node_8882_sin = sin(node_8882_spd*node_8882_ang);
                float2 node_8882_piv = float2(0.5,0.5);
                float2 node_8882 = (mul((_DepR*(_HeiR - 0.5)*mul(tangentTransform, viewDirection).xy + i.uv0).rg-node_8882_piv,float2x2( node_8882_cos, -node_8882_sin, node_8882_sin, node_8882_cos))+node_8882_piv);
                float4 _DiffuseAlphaTEX_var = tex2D(_DiffuseAlphaTEX,TRANSFORM_TEX(node_8882, _DiffuseAlphaTEX));
                float node_3929_ang = node_8513.g;
                float node_3929_spd = _rotator_2;
                float node_3929_cos = cos(node_3929_spd*node_3929_ang);
                float node_3929_sin = sin(node_3929_spd*node_3929_ang);
                float2 node_3929_piv = float2(0.5,0.5);
                float2 node_3929 = (mul((_DepR_copy*(_HeiR_copy - 0.5)*mul(tangentTransform, viewDirection).xy + i.uv0).rg-node_3929_piv,float2x2( node_3929_cos, -node_3929_sin, node_3929_sin, node_3929_cos))+node_3929_piv);
                float4 _DiffuseAlphaTEX_copy_var = tex2D(_DiffuseAlphaTEX_copy,TRANSFORM_TEX(node_3929, _DiffuseAlphaTEX_copy));
                float node_1021_ang = node_8513.g;
                float node_1021_spd = _rotator_3;
                float node_1021_cos = cos(node_1021_spd*node_1021_ang);
                float node_1021_sin = sin(node_1021_spd*node_1021_ang);
                float2 node_1021_piv = float2(0.5,0.5);
                float2 node_1021 = (mul((_DepR_copy_copy*(_HeiR_copy_copy - 0.5)*mul(tangentTransform, viewDirection).xy + i.uv0).rg-node_1021_piv,float2x2( node_1021_cos, -node_1021_sin, node_1021_sin, node_1021_cos))+node_1021_piv);
                float4 _DiffuseAlphaTEX_copy_copy_var = tex2D(_DiffuseAlphaTEX_copy_copy,TRANSFORM_TEX(node_1021, _DiffuseAlphaTEX_copy_copy));
                float node_496_ang = node_8513.g;
                float node_496_spd = _rotator_4;
                float node_496_cos = cos(node_496_spd*node_496_ang);
                float node_496_sin = sin(node_496_spd*node_496_ang);
                float2 node_496_piv = float2(0.5,0.5);
                float2 node_496 = (mul((_DepR_copy_copy_copy*(_HeiR_copy_copy_copy - 0.5)*mul(tangentTransform, viewDirection).xy + i.uv0).rg-node_496_piv,float2x2( node_496_cos, -node_496_sin, node_496_sin, node_496_cos))+node_496_piv);
                float4 _DiffuseAlphaTEX_copy_copy_copy_var = tex2D(_DiffuseAlphaTEX_copy_copy_copy,TRANSFORM_TEX(node_496, _DiffuseAlphaTEX_copy_copy_copy));
                float3 emissive = ((0.3*_node_7943_var.rgb)+(_DiffuseAlphaTEX_var.r+_DiffuseAlphaTEX_copy_var.g+_DiffuseAlphaTEX_copy_copy_var.b+_DiffuseAlphaTEX_copy_copy_copy_var.a));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
