﻿using UnityEngine;
using System.Collections;

public abstract class PuzzleManager : MonoBehaviour {

    abstract public void ResetPuzzle();

    abstract public void CompletePuzzle();
}
