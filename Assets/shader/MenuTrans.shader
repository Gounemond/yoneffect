// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:7979,x:35809,y:32683,varname:node_7979,prsc:2|emission-4554-OUT,alpha-2204-OUT;n:type:ShaderForge.SFN_Color,id:6778,x:34680,y:32608,ptovrint:False,ptlb:node_9650_copy,ptin:_node_9650_copy,varname:_node_9650_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.2627451,c2:0.6941177,c3:1,c4:1;n:type:ShaderForge.SFN_Fresnel,id:2204,x:34688,y:32965,varname:node_2204,prsc:2|NRM-9138-OUT,EXP-3476-OUT;n:type:ShaderForge.SFN_OneMinus,id:489,x:34928,y:33015,varname:node_489,prsc:2|IN-2204-OUT;n:type:ShaderForge.SFN_Lerp,id:4554,x:35073,y:32735,varname:node_4554,prsc:2|A-6778-RGB,B-1152-OUT,T-2204-OUT;n:type:ShaderForge.SFN_Color,id:1388,x:34567,y:32754,ptovrint:False,ptlb:node_4667_copy,ptin:_node_4667_copy,varname:_node_4667_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.2627451,c2:0.6941177,c3:1,c4:1;n:type:ShaderForge.SFN_Multiply,id:1152,x:34791,y:32787,varname:node_1152,prsc:2|A-1388-RGB,B-4883-OUT;n:type:ShaderForge.SFN_DepthBlend,id:6361,x:35192,y:33105,varname:node_6361,prsc:2|DIST-3006-OUT;n:type:ShaderForge.SFN_Vector1,id:3006,x:35008,y:33184,varname:node_3006,prsc:2,v1:1;n:type:ShaderForge.SFN_Add,id:9312,x:35348,y:33063,varname:node_9312,prsc:2|A-489-OUT,B-6361-OUT;n:type:ShaderForge.SFN_Vector1,id:4883,x:34513,y:32927,varname:node_4883,prsc:2,v1:2;n:type:ShaderForge.SFN_NormalVector,id:9138,x:34410,y:32991,prsc:2,pt:False;n:type:ShaderForge.SFN_Vector1,id:3476,x:34560,y:33088,varname:node_3476,prsc:2,v1:2;proporder:6778-1388;pass:END;sub:END;*/

Shader "Custom/MenuTrans" {
    Properties {
        _node_9650_copy ("node_9650_copy", Color) = (0.2627451,0.6941177,1,1)
        _node_4667_copy ("node_4667_copy", Color) = (0.2627451,0.6941177,1,1)
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _node_9650_copy;
            uniform float4 _node_4667_copy;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 posWorld : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
                UNITY_FOG_COORDS(2)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float node_2204 = pow(1.0-max(0,dot(i.normalDir, viewDirection)),2.0);
                float3 node_4554 = lerp(_node_9650_copy.rgb,(_node_4667_copy.rgb*2.0),node_2204);
                float3 emissive = node_4554;
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,node_2204);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
