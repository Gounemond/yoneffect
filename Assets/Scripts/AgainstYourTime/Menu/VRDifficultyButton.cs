﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using VRStandardAssets.Utils;
using System;

public class VRDifficultyButton : MonoBehaviour {

    public event Action onClicked;

    private VRInteractiveItem _myVRitem;

    public AudioClip soundClick;
    public AudioClip soundHoverIn;
    public AudioClip soundHoverOut;
    public Material energyLaneMaterial;
    public Material scuroMat;
    public Material lightMat;
    public Material chiaroMat;
    public Material selectedMaterial;
    public Material eyeHoverMaterial;               // The shader used when hovering on the object
    private bool m_onHover = true;
    private bool m_selected = false;
    private Material[] listMaterials;

    // Use this for initialization
    void Awake()
    {
        _myVRitem = GetComponent<VRInteractiveItem>();
        listMaterials = new Material[GetComponent<Renderer>().materials.Length];
        listMaterials[0] = scuroMat;
        listMaterials[1] = lightMat;
        listMaterials[2] = chiaroMat;
    }

    void Start ()
    {

    }

    public void UnlockDifficulty()
    {
        _myVRitem.OnClick += Clicked;
        _myVRitem.OnOver += HoverIn;
        _myVRitem.OnOut += HoverOut;

        GetComponent<Renderer>().materials = listMaterials;
    }

    public void LockDifficulty()
    {
        _myVRitem.OnClick -= Clicked;
        _myVRitem.OnOver -= HoverIn;
        _myVRitem.OnOut -= HoverOut;
    }
	

    public void SelectedDifficulty()
    {
        m_selected = true;
        energyLaneMaterial.SetFloat("_onoff", 1);
        GetComponent<Renderer>().material = selectedMaterial;
    }

    public void DeselectedDifficulty()
    {
        m_selected = false;
        energyLaneMaterial.SetFloat("_onoff", 0);
        GetComponent<Renderer>().material = scuroMat;
    }

    public void HoverIn()
    {
        if (!m_selected)
        {
            GetComponent<Renderer>().material = eyeHoverMaterial;
            GetComponent<AudioSource>().PlayOneShot(soundHoverIn);
        }
        m_onHover = true;

    }

    public void HoverOut()
    {
        if (!m_selected)
        {
            GetComponent<Renderer>().material = scuroMat;
            GetComponent<AudioSource>().PlayOneShot(soundHoverOut);
        }
        m_onHover = false;
    }

    public void Clicked()
    {
        GetComponent<AudioSource>().PlayOneShot(soundClick);
        if (onClicked != null)
        {
            onClicked();
        }
    }
}
