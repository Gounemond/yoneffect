using UnityEngine;
using System.Collections;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System;
using System.Linq;

public class BlueTeleportTrigger : MonoBehaviour {

    public GameObject[] objectToActivate;
    public Transform teleportDestination;
    public AudioSource teleportAudio;
    public float timeToTeleport = 2f;
	private int triggerCounter = 0;
    private Coroutine m_TeleportRoutine;
    private Coroutine m_CameraEffectRoutine;
    private float m_Timer = 0;
    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	}
	
	void OnTriggerEnter(Collider ObjectCollision)
	{
		if (ObjectCollision.tag == "Player" || ObjectCollision.tag=="ClonePlayer")
        {
            if (triggerCounter == 0)
            {
                ObjectCollision.gameObject.transform.position = teleportDestination.position;
                ObjectCollision.gameObject.transform.rotation = teleportDestination.rotation;
                for (int i = 0; i < objectToActivate.Length; i++)
                {
                    objectToActivate[i].SetActive(true);
                }
                teleportAudio.Play();
                //m_TeleportRoutine = StartCoroutine(BeginTeleportation(ObjectCollision));
                //m_CameraEffectRoutine = StartCoroutine(CameraEffectsManager.Self.StartBlurEffect(timeToTeleport));
            }
            triggerCounter++;
        }
    }
	
	void OnTriggerExit(Collider ObjectCollision)
	{
        if (ObjectCollision.tag == "Player" || ObjectCollision.tag == "ClonePlayer")
        {
            if (triggerCounter == 1)
		    {
                /*if (m_TeleportRoutine != null)
                {
                    StopCoroutine(m_TeleportRoutine);
                    StartCoroutine(CameraEffectsManager.Self.EndBlurEffect(timeToTeleport));
                    m_Timer = 0;
                }*/
                triggerCounter--;
		    }
		    else
		    {
			    triggerCounter--;
		    }
        }
	}

    IEnumerator BeginTeleportation(Collider objectColl)
    {
        m_Timer = 0;
        teleportAudio.Play();
        while (m_Timer < timeToTeleport)
        {
            // ... add to the timer the difference between frames.
            m_Timer += Time.deltaTime;

            yield return null;

            if (triggerCounter >= 1)
            {
                continue;
            }

            m_Timer = 0f;
            yield break;

        }

        // Teleport the designated player
        objectColl.gameObject.transform.position = teleportDestination.position;
        objectColl.gameObject.transform.rotation = teleportDestination.rotation;
    }

}
