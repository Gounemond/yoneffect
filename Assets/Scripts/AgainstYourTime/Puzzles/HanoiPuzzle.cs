﻿using UnityEngine;
using System.Collections;

public class HanoiPuzzle : MonoBehaviour
{
    public Transform[] snapPositions;

    public HanoiTriggerEnd endZone;

    private int numberEndZone = 0;


    public HanoiCollider[] hanoiDiscs;
    public GameObject objectGrabbed;

    public AudioSource successAudio;
    public Animator YonGearGate;


    // Use this for initialization
    void Start ()
    {
        
    }

    void OnEnable()
    {
        endZone.onCollisionEnter += positioned;
        endZone.onCollisionExit += removed;
    }

    void OnDisable()
    {
        endZone.onCollisionEnter -= positioned;
        endZone.onCollisionExit -= removed;
    }
	// Update is called once per frame
	void Update ()
    {
	
	}

    public void positioned()
    {
        numberEndZone++;
        StartCoroutine(HasWonCheck());
    }

    public void removed()
    {
        numberEndZone--;
    }

    public void switchObjectGrabbed(GameObject hanoiPiece)
    {
        if (hanoiPiece == null)
        {
            objectGrabbed = hanoiPiece;
            return;
        }

        if (objectGrabbed == null)
        {
            objectGrabbed = hanoiPiece;
            return;
        }

        if (hanoiPiece != objectGrabbed)
        {
            objectGrabbed.GetComponent<VRHanoiGrab>().GrabSwitchState();
            objectGrabbed = hanoiPiece;
            return;
        }
    }

    /// <summary>
    /// This function is called by Hanoi grab elements to determine which is the closest snap position 
    /// </summary>
    /// <param name="objectPosition">Usually the transform of the grab beacon</param>
    /// <returns>The closest snap position, or the beacon position if snap is too far</returns>
    public Transform GetClosestSnapPosition(Transform objectPosition)
    {
        Transform bestTarget = null;                                                    // Memorize the best snap position
        float closestDistanceSqr = Mathf.Infinity;                                      // Distance from the snap position, initialized to infinity since the lower the better
        Vector3 currentPosition = objectPosition.transform.position;                    // Current position of the beacon
        int tempIndex = 0;                                                              // Current index in the cycle

        // Cycle through all the snap positions
        foreach (Transform potentialTarget in snapPositions)
        {
            Vector3 directionToTarget = potentialTarget.position - currentPosition;     // Gets direction vector between beacon and snap

            float dSqrToTarget = directionToTarget.sqrMagnitude;

            // Evaluates if the distance is smaller that than the previous ones
            if (dSqrToTarget < closestDistanceSqr)
            {
                // If the distance is smaller, saves information about the new snap position
                closestDistanceSqr = dSqrToTarget;
                bestTarget = potentialTarget;
            }
            tempIndex++;
        }
        if (bestTarget == null)
        {
            return objectPosition;
        }
        else
        {
            return bestTarget;
        }
    }

    IEnumerator HasWonCheck()
    {
        float timer = 0;

        while (timer < 0.3f && numberEndZone == hanoiDiscs.Length)
        {
            timer += Time.deltaTime;
            yield return null;
        }
        if (numberEndZone == hanoiDiscs.Length)
        {
            successAudio.Play();
            YonGearGate.SetBool("HasWon", true);
            yield return null;
        }

    }
}
