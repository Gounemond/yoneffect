using UnityEngine;
using System.Collections;

public class GenBehaviour : MonoBehaviour {
	
	private Color active = Color.red;
	private Color deactivated = Color.black;
	
	// Use this for initialization
	void Start () {
		
		GetComponent<Renderer>().material.color = active;
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	//Function which activete the generator
	public void activeGen() {
	
		GetComponent<Renderer>().material.color = active;
		
	}
	
	//Function which disactivet the generator
	public void deactiveGen() {
	
		GetComponent<Renderer>().material.color = deactivated;
		
	}
	
}
