// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.16 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.16;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:1759,x:32719,y:32712,varname:node_1759,prsc:2|emission-981-OUT;n:type:ShaderForge.SFN_Lerp,id:981,x:32519,y:32779,varname:node_981,prsc:2|A-685-RGB,B-3712-OUT,T-5474-OUT;n:type:ShaderForge.SFN_Multiply,id:5474,x:32530,y:33012,varname:node_5474,prsc:2|A-3028-OUT,B-4165-OUT;n:type:ShaderForge.SFN_Color,id:685,x:32362,y:32672,ptovrint:False,ptlb:node_685,ptin:_node_685,varname:node_685,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Multiply,id:3712,x:32334,y:32839,varname:node_3712,prsc:2|A-2388-RGB,B-5853-OUT;n:type:ShaderForge.SFN_Color,id:2388,x:32162,y:32756,ptovrint:False,ptlb:node_2388,ptin:_node_2388,varname:node_2388,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:0,c4:1;n:type:ShaderForge.SFN_Vector1,id:5853,x:32177,y:32928,varname:node_5853,prsc:2,v1:2;n:type:ShaderForge.SFN_OneMinus,id:3028,x:32352,y:33012,varname:node_3028,prsc:2|IN-4198-OUT;n:type:ShaderForge.SFN_Slider,id:4165,x:32282,y:33184,ptovrint:False,ptlb:node_4165,ptin:_node_4165,varname:node_4165,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Fresnel,id:4198,x:32183,y:33006,varname:node_4198,prsc:2|NRM-2971-OUT,EXP-4990-OUT;n:type:ShaderForge.SFN_Vector1,id:4990,x:32030,y:33117,varname:node_4990,prsc:2,v1:2;n:type:ShaderForge.SFN_NormalVector,id:2971,x:32024,y:32929,prsc:2,pt:False;proporder:685-2388-4165;pass:END;sub:END;*/

Shader "Custom/lightG" {
    Properties {
        _node_685 ("node_685", Color) = (0.5,0.5,0.5,1)
        _node_2388 ("node_2388", Color) = (1,1,0,1)
        _node_4165 ("node_4165", Range(0, 1)) = 0
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _node_685;
            uniform float4 _node_2388;
            uniform float _node_4165;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 posWorld : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
                UNITY_FOG_COORDS(2)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
/////// Vectors:
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float3 emissive = lerp(_node_685.rgb,(_node_2388.rgb*2.0),((1.0 - pow(1.0-max(0,dot(i.normalDir, viewDirection)),2.0))*_node_4165));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
