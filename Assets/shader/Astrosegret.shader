// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:9798,x:34865,y:32888,varname:node_9798,prsc:2|emission-8979-OUT;n:type:ShaderForge.SFN_Tex2d,id:5322,x:32984,y:32786,ptovrint:False,ptlb:Diffuse/Alpha TEX_copy,ptin:_DiffuseAlphaTEX_copy,varname:_DiffuseAlphaTEX_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:05f8de7f980ec4f4b972c38ad3db0ebb,ntxv:0,isnm:False|UVIN-8540-UVOUT;n:type:ShaderForge.SFN_Parallax,id:2838,x:32338,y:32756,varname:node_2838,prsc:2|UVIN-2680-UVOUT,HEI-8802-OUT,DEP-6766-OUT;n:type:ShaderForge.SFN_Slider,id:6766,x:31921,y:32928,ptovrint:False,ptlb:DepR_copy,ptin:_DepR_copy,varname:_DepR_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-10,cur:10,max:10;n:type:ShaderForge.SFN_Slider,id:8802,x:31921,y:32784,ptovrint:False,ptlb:HeiR_copy,ptin:_HeiR_copy,varname:_HeiR_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-10,cur:0,max:10;n:type:ShaderForge.SFN_Tex2d,id:8375,x:32971,y:33102,ptovrint:False,ptlb:Diffuse/Alpha TEX_copy_copy,ptin:_DiffuseAlphaTEX_copy_copy,varname:_DiffuseAlphaTEX_copy_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:05f8de7f980ec4f4b972c38ad3db0ebb,ntxv:0,isnm:False|UVIN-2026-UVOUT;n:type:ShaderForge.SFN_Parallax,id:2632,x:32296,y:33291,varname:node_2632,prsc:2|UVIN-1669-UVOUT,HEI-968-OUT,DEP-3086-OUT;n:type:ShaderForge.SFN_TexCoord,id:1669,x:31879,y:33187,varname:node_1669,prsc:2,uv:0;n:type:ShaderForge.SFN_Slider,id:3086,x:31879,y:33444,ptovrint:False,ptlb:DepR_copy_copy,ptin:_DepR_copy_copy,varname:_DepR_copy_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-10,cur:8,max:10;n:type:ShaderForge.SFN_Slider,id:968,x:31879,y:33353,ptovrint:False,ptlb:HeiR_copy_copy,ptin:_HeiR_copy_copy,varname:_HeiR_copy_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-10,cur:0,max:10;n:type:ShaderForge.SFN_Tex2d,id:227,x:32971,y:33378,ptovrint:False,ptlb:Diffuse/Alpha TEX_copy_copy_copy,ptin:_DiffuseAlphaTEX_copy_copy_copy,varname:_DiffuseAlphaTEX_copy_copy_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:05f8de7f980ec4f4b972c38ad3db0ebb,ntxv:0,isnm:False|UVIN-9804-UVOUT;n:type:ShaderForge.SFN_Parallax,id:7508,x:32294,y:33644,varname:node_7508,prsc:2|UVIN-4778-UVOUT,HEI-4589-OUT,DEP-3496-OUT;n:type:ShaderForge.SFN_TexCoord,id:4778,x:31877,y:33540,varname:node_4778,prsc:2,uv:0;n:type:ShaderForge.SFN_Slider,id:3496,x:31877,y:33797,ptovrint:False,ptlb:DepR_copy_copy_copy,ptin:_DepR_copy_copy_copy,varname:_DepR_copy_copy_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-10,cur:7,max:10;n:type:ShaderForge.SFN_Slider,id:4589,x:31877,y:33706,ptovrint:False,ptlb:HeiR_copy_copy_copy,ptin:_HeiR_copy_copy_copy,varname:_HeiR_copy_copy_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-10,cur:0,max:10;n:type:ShaderForge.SFN_Tex2d,id:6232,x:32995,y:33761,ptovrint:False,ptlb:Diffuse/Alpha TEX_copy_copy_copy_copy,ptin:_DiffuseAlphaTEX_copy_copy_copy_copy,varname:_DiffuseAlphaTEX_copy_copy_copy_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:05f8de7f980ec4f4b972c38ad3db0ebb,ntxv:0,isnm:False|UVIN-1030-UVOUT;n:type:ShaderForge.SFN_Parallax,id:5933,x:32297,y:33993,varname:node_5933,prsc:2|UVIN-8373-UVOUT,HEI-4216-OUT,DEP-8242-OUT;n:type:ShaderForge.SFN_TexCoord,id:8373,x:31880,y:33889,varname:node_8373,prsc:2,uv:0;n:type:ShaderForge.SFN_Slider,id:8242,x:31880,y:34146,ptovrint:False,ptlb:DepR_copy_copy_copy_copy,ptin:_DepR_copy_copy_copy_copy,varname:_DepR_copy_copy_copy_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-10,cur:6,max:10;n:type:ShaderForge.SFN_Slider,id:4216,x:31880,y:34055,ptovrint:False,ptlb:HeiR_copy_copy_copy_copy,ptin:_HeiR_copy_copy_copy_copy,varname:_HeiR_copy_copy_copy_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-10,cur:0,max:10;n:type:ShaderForge.SFN_Add,id:4044,x:33249,y:33181,varname:node_4044,prsc:2|A-5322-R,B-8375-G,C-227-B,D-6232-A;n:type:ShaderForge.SFN_TexCoord,id:2680,x:32045,y:32500,varname:node_2680,prsc:2,uv:0;n:type:ShaderForge.SFN_Rotator,id:8540,x:32615,y:32768,varname:node_8540,prsc:2|UVIN-2838-UVOUT,SPD-7560-OUT;n:type:ShaderForge.SFN_Slider,id:7560,x:32419,y:32998,ptovrint:False,ptlb:rotator_2,ptin:_rotator_2,varname:_rotator_2,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-1,cur:0.4988696,max:1;n:type:ShaderForge.SFN_Rotator,id:2026,x:32626,y:33156,varname:node_2026,prsc:2|UVIN-2632-UVOUT,SPD-6868-OUT;n:type:ShaderForge.SFN_Slider,id:6868,x:32454,y:33352,ptovrint:False,ptlb:rotator_3,ptin:_rotator_3,varname:_rotator_3,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-1,cur:0,max:1;n:type:ShaderForge.SFN_Rotator,id:9804,x:32629,y:33533,varname:node_9804,prsc:2|UVIN-7508-UVOUT,SPD-5092-OUT;n:type:ShaderForge.SFN_Slider,id:5092,x:32454,y:33684,ptovrint:False,ptlb:rotator_4,ptin:_rotator_4,varname:_rotator_4,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-1,cur:0,max:1;n:type:ShaderForge.SFN_Rotator,id:1030,x:32703,y:33847,varname:node_1030,prsc:2|UVIN-5933-UVOUT,SPD-1331-OUT;n:type:ShaderForge.SFN_Slider,id:1331,x:32495,y:34055,ptovrint:False,ptlb:rotator_5,ptin:_rotator_5,varname:_rotator_5,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-1,cur:0,max:1;n:type:ShaderForge.SFN_Tex2d,id:8944,x:33599,y:32722,ptovrint:False,ptlb:node_7943_copy,ptin:_node_7943_copy,varname:_node_7943_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:600e185cf40947845b6220297740efbb,ntxv:0,isnm:False|UVIN-3601-UVOUT;n:type:ShaderForge.SFN_Add,id:5292,x:33650,y:33017,varname:node_5292,prsc:2|A-6836-OUT,B-4044-OUT;n:type:ShaderForge.SFN_Parallax,id:1702,x:33199,y:32506,varname:node_1702,prsc:2|UVIN-7673-UVOUT,HEI-9255-OUT,DEP-4641-OUT;n:type:ShaderForge.SFN_TexCoord,id:7673,x:32942,y:32448,varname:node_7673,prsc:2,uv:0;n:type:ShaderForge.SFN_Vector1,id:9255,x:32866,y:32639,varname:node_9255,prsc:2,v1:0.9;n:type:ShaderForge.SFN_Vector1,id:4641,x:32942,y:32698,varname:node_4641,prsc:2,v1:1;n:type:ShaderForge.SFN_Rotator,id:3601,x:33454,y:32578,varname:node_3601,prsc:2|UVIN-1702-UVOUT,SPD-5517-OUT;n:type:ShaderForge.SFN_Slider,id:5517,x:33175,y:32756,ptovrint:False,ptlb:node_3403_copy,ptin:_node_3403_copy,varname:_node_3403_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.02907673,max:1;n:type:ShaderForge.SFN_Multiply,id:6836,x:33781,y:32528,varname:node_6836,prsc:2|A-4622-OUT,B-8944-RGB;n:type:ShaderForge.SFN_Vector1,id:4622,x:33623,y:32470,varname:node_4622,prsc:2,v1:0.3;n:type:ShaderForge.SFN_SceneDepth,id:7308,x:33215,y:33652,varname:node_7308,prsc:2;n:type:ShaderForge.SFN_Depth,id:6768,x:33188,y:33792,varname:node_6768,prsc:2;n:type:ShaderForge.SFN_Subtract,id:1752,x:33448,y:33727,varname:node_1752,prsc:2|A-7308-OUT,B-6768-OUT;n:type:ShaderForge.SFN_Multiply,id:5307,x:33675,y:33707,varname:node_5307,prsc:2|A-2479-OUT,B-1752-OUT;n:type:ShaderForge.SFN_ValueProperty,id:2479,x:33448,y:33673,ptovrint:False,ptlb:density_copy_copy_copy,ptin:_density_copy_copy_copy,varname:_density_copy_copy_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Vector4Property,id:3124,x:33394,y:33850,ptovrint:False,ptlb:node_940_copy_copy_copy,ptin:_node_940_copy_copy_copy,varname:_node_940_copy_copy_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0,v2:0,v3:0,v4:-0.1;n:type:ShaderForge.SFN_Add,id:3206,x:33638,y:33874,varname:node_3206,prsc:2|A-3124-XYZ,B-3124-W;n:type:ShaderForge.SFN_RemapRangeAdvanced,id:3441,x:34050,y:33810,varname:node_3441,prsc:2|IN-5307-OUT,IMIN-3206-OUT,IMAX-8794-OUT,OMIN-1868-OUT,OMAX-2594-OUT;n:type:ShaderForge.SFN_Add,id:8794,x:33677,y:34031,varname:node_8794,prsc:2|A-7845-XYZ,B-7845-W;n:type:ShaderForge.SFN_Vector4Property,id:7845,x:33388,y:34123,ptovrint:False,ptlb:node_2263_copy_copy_copy,ptin:_node_2263_copy_copy_copy,varname:_node_2263_copy_copy_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.6172414,v2:1,v3:0.9324544,v4:0.8;n:type:ShaderForge.SFN_Vector1,id:1868,x:33832,y:33946,varname:node_1868,prsc:2,v1:1;n:type:ShaderForge.SFN_Vector1,id:2594,x:33852,y:34075,varname:node_2594,prsc:2,v1:0;n:type:ShaderForge.SFN_Clamp01,id:8752,x:34194,y:33767,varname:node_8752,prsc:2|IN-3441-OUT;n:type:ShaderForge.SFN_Power,id:731,x:34460,y:33727,varname:node_731,prsc:2|VAL-8752-OUT,EXP-7197-OUT;n:type:ShaderForge.SFN_ValueProperty,id:7197,x:34194,y:33968,ptovrint:False,ptlb:fade power_copy_copy_copy,ptin:_fadepower_copy_copy_copy,varname:_fadepower_copy_copy_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:5;n:type:ShaderForge.SFN_SceneColor,id:2707,x:33914,y:33650,varname:node_2707,prsc:2;n:type:ShaderForge.SFN_Multiply,id:5830,x:34493,y:33602,varname:node_5830,prsc:2|A-2707-RGB,B-731-OUT;n:type:ShaderForge.SFN_Add,id:8979,x:34584,y:33223,varname:node_8979,prsc:2|A-5292-OUT,B-5830-OUT;n:type:ShaderForge.SFN_ComponentMask,id:6185,x:34233,y:33083,varname:node_6185,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-5292-OUT;proporder:5322-6766-8802-8375-3086-968-227-3496-4589-6232-8242-4216-7560-6868-5092-1331-8944-5517-2479-3124-7845-7197;pass:END;sub:END;*/

Shader "Custom/Astrosegret" {
    Properties {
        _DiffuseAlphaTEX_copy ("Diffuse/Alpha TEX_copy", 2D) = "white" {}
        _DepR_copy ("DepR_copy", Range(-10, 10)) = 10
        _HeiR_copy ("HeiR_copy", Range(-10, 10)) = 0
        _DiffuseAlphaTEX_copy_copy ("Diffuse/Alpha TEX_copy_copy", 2D) = "white" {}
        _DepR_copy_copy ("DepR_copy_copy", Range(-10, 10)) = 8
        _HeiR_copy_copy ("HeiR_copy_copy", Range(-10, 10)) = 0
        _DiffuseAlphaTEX_copy_copy_copy ("Diffuse/Alpha TEX_copy_copy_copy", 2D) = "white" {}
        _DepR_copy_copy_copy ("DepR_copy_copy_copy", Range(-10, 10)) = 7
        _HeiR_copy_copy_copy ("HeiR_copy_copy_copy", Range(-10, 10)) = 0
        _DiffuseAlphaTEX_copy_copy_copy_copy ("Diffuse/Alpha TEX_copy_copy_copy_copy", 2D) = "white" {}
        _DepR_copy_copy_copy_copy ("DepR_copy_copy_copy_copy", Range(-10, 10)) = 6
        _HeiR_copy_copy_copy_copy ("HeiR_copy_copy_copy_copy", Range(-10, 10)) = 0
        _rotator_2 ("rotator_2", Range(-1, 1)) = 0.4988696
        _rotator_3 ("rotator_3", Range(-1, 1)) = 0
        _rotator_4 ("rotator_4", Range(-1, 1)) = 0
        _rotator_5 ("rotator_5", Range(-1, 1)) = 0
        _node_7943_copy ("node_7943_copy", 2D) = "white" {}
        _node_3403_copy ("node_3403_copy", Range(0, 1)) = 0.02907673
        _density_copy_copy_copy ("density_copy_copy_copy", Float ) = 1
        _node_940_copy_copy_copy ("node_940_copy_copy_copy", Vector) = (0,0,0,-0.1)
        _node_2263_copy_copy_copy ("node_2263_copy_copy_copy", Vector) = (0.6172414,1,0.9324544,0.8)
        _fadepower_copy_copy_copy ("fade power_copy_copy_copy", Float ) = 5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        LOD 200
        GrabPass{ }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform sampler2D _GrabTexture;
            uniform sampler2D _CameraDepthTexture;
            uniform float4 _TimeEditor;
            uniform sampler2D _DiffuseAlphaTEX_copy; uniform float4 _DiffuseAlphaTEX_copy_ST;
            uniform float _DepR_copy;
            uniform float _HeiR_copy;
            uniform sampler2D _DiffuseAlphaTEX_copy_copy; uniform float4 _DiffuseAlphaTEX_copy_copy_ST;
            uniform float _DepR_copy_copy;
            uniform float _HeiR_copy_copy;
            uniform sampler2D _DiffuseAlphaTEX_copy_copy_copy; uniform float4 _DiffuseAlphaTEX_copy_copy_copy_ST;
            uniform float _DepR_copy_copy_copy;
            uniform float _HeiR_copy_copy_copy;
            uniform sampler2D _DiffuseAlphaTEX_copy_copy_copy_copy; uniform float4 _DiffuseAlphaTEX_copy_copy_copy_copy_ST;
            uniform float _DepR_copy_copy_copy_copy;
            uniform float _HeiR_copy_copy_copy_copy;
            uniform float _rotator_2;
            uniform float _rotator_3;
            uniform float _rotator_4;
            uniform float _rotator_5;
            uniform sampler2D _node_7943_copy; uniform float4 _node_7943_copy_ST;
            uniform float _node_3403_copy;
            uniform float _density_copy_copy_copy;
            uniform float4 _node_940_copy_copy_copy;
            uniform float4 _node_2263_copy_copy_copy;
            uniform float _fadepower_copy_copy_copy;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 bitangentDir : TEXCOORD4;
                float4 screenPos : TEXCOORD5;
                float4 projPos : TEXCOORD6;
                UNITY_FOG_COORDS(7)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                o.projPos = ComputeScreenPos (o.pos);
                COMPUTE_EYEDEPTH(o.projPos.z);
                o.screenPos = o.pos;
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                #if UNITY_UV_STARTS_AT_TOP
                    float grabSign = -_ProjectionParams.x;
                #else
                    float grabSign = _ProjectionParams.x;
                #endif
                i.normalDir = normalize(i.normalDir);
                i.screenPos = float4( i.screenPos.xy / i.screenPos.w, 0, 0 );
                i.screenPos.y *= _ProjectionParams.x;
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float sceneZ = max(0,LinearEyeDepth (UNITY_SAMPLE_DEPTH(tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(i.projPos)))) - _ProjectionParams.g);
                float partZ = max(0,i.projPos.z - _ProjectionParams.g);
                float2 sceneUVs = float2(1,grabSign)*i.screenPos.xy*0.5+0.5;
                float4 sceneColor = tex2D(_GrabTexture, sceneUVs);
////// Lighting:
////// Emissive:
                float4 node_5906 = _Time + _TimeEditor;
                float node_3601_ang = node_5906.g;
                float node_3601_spd = _node_3403_copy;
                float node_3601_cos = cos(node_3601_spd*node_3601_ang);
                float node_3601_sin = sin(node_3601_spd*node_3601_ang);
                float2 node_3601_piv = float2(0.5,0.5);
                float2 node_3601 = (mul((1.0*(0.9 - 0.5)*mul(tangentTransform, viewDirection).xy + i.uv0).rg-node_3601_piv,float2x2( node_3601_cos, -node_3601_sin, node_3601_sin, node_3601_cos))+node_3601_piv);
                float4 _node_7943_copy_var = tex2D(_node_7943_copy,TRANSFORM_TEX(node_3601, _node_7943_copy));
                float node_8540_ang = node_5906.g;
                float node_8540_spd = _rotator_2;
                float node_8540_cos = cos(node_8540_spd*node_8540_ang);
                float node_8540_sin = sin(node_8540_spd*node_8540_ang);
                float2 node_8540_piv = float2(0.5,0.5);
                float2 node_8540 = (mul((_DepR_copy*(_HeiR_copy - 0.5)*mul(tangentTransform, viewDirection).xy + i.uv0).rg-node_8540_piv,float2x2( node_8540_cos, -node_8540_sin, node_8540_sin, node_8540_cos))+node_8540_piv);
                float4 _DiffuseAlphaTEX_copy_var = tex2D(_DiffuseAlphaTEX_copy,TRANSFORM_TEX(node_8540, _DiffuseAlphaTEX_copy));
                float node_2026_ang = node_5906.g;
                float node_2026_spd = _rotator_3;
                float node_2026_cos = cos(node_2026_spd*node_2026_ang);
                float node_2026_sin = sin(node_2026_spd*node_2026_ang);
                float2 node_2026_piv = float2(0.5,0.5);
                float2 node_2026 = (mul((_DepR_copy_copy*(_HeiR_copy_copy - 0.5)*mul(tangentTransform, viewDirection).xy + i.uv0).rg-node_2026_piv,float2x2( node_2026_cos, -node_2026_sin, node_2026_sin, node_2026_cos))+node_2026_piv);
                float4 _DiffuseAlphaTEX_copy_copy_var = tex2D(_DiffuseAlphaTEX_copy_copy,TRANSFORM_TEX(node_2026, _DiffuseAlphaTEX_copy_copy));
                float node_9804_ang = node_5906.g;
                float node_9804_spd = _rotator_4;
                float node_9804_cos = cos(node_9804_spd*node_9804_ang);
                float node_9804_sin = sin(node_9804_spd*node_9804_ang);
                float2 node_9804_piv = float2(0.5,0.5);
                float2 node_9804 = (mul((_DepR_copy_copy_copy*(_HeiR_copy_copy_copy - 0.5)*mul(tangentTransform, viewDirection).xy + i.uv0).rg-node_9804_piv,float2x2( node_9804_cos, -node_9804_sin, node_9804_sin, node_9804_cos))+node_9804_piv);
                float4 _DiffuseAlphaTEX_copy_copy_copy_var = tex2D(_DiffuseAlphaTEX_copy_copy_copy,TRANSFORM_TEX(node_9804, _DiffuseAlphaTEX_copy_copy_copy));
                float node_1030_ang = node_5906.g;
                float node_1030_spd = _rotator_5;
                float node_1030_cos = cos(node_1030_spd*node_1030_ang);
                float node_1030_sin = sin(node_1030_spd*node_1030_ang);
                float2 node_1030_piv = float2(0.5,0.5);
                float2 node_1030 = (mul((_DepR_copy_copy_copy_copy*(_HeiR_copy_copy_copy_copy - 0.5)*mul(tangentTransform, viewDirection).xy + i.uv0).rg-node_1030_piv,float2x2( node_1030_cos, -node_1030_sin, node_1030_sin, node_1030_cos))+node_1030_piv);
                float4 _DiffuseAlphaTEX_copy_copy_copy_copy_var = tex2D(_DiffuseAlphaTEX_copy_copy_copy_copy,TRANSFORM_TEX(node_1030, _DiffuseAlphaTEX_copy_copy_copy_copy));
                float3 node_5292 = ((0.3*_node_7943_copy_var.rgb)+(_DiffuseAlphaTEX_copy_var.r+_DiffuseAlphaTEX_copy_copy_var.g+_DiffuseAlphaTEX_copy_copy_copy_var.b+_DiffuseAlphaTEX_copy_copy_copy_copy_var.a));
                float3 node_3206 = (_node_940_copy_copy_copy.rgb+_node_940_copy_copy_copy.a);
                float node_1868 = 1.0;
                float3 emissive = (node_5292+(sceneColor.rgb*pow(saturate((node_1868 + ( ((_density_copy_copy_copy*(sceneZ-partZ)) - node_3206) * (0.0 - node_1868) ) / ((_node_2263_copy_copy_copy.rgb+_node_2263_copy_copy_copy.a) - node_3206))),_fadepower_copy_copy_copy)));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
