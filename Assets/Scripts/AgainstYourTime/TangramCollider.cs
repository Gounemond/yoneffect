﻿using UnityEngine;
using System;
using System.Collections;

[RequireComponent (typeof(BoxCollider))]
public class TangramCollider : MonoBehaviour
{

    public event Action onCollisionEnter;              // Called when a Tangram Object enters the collider
    public event Action onCollisionExit;               // Called when a Tangram Object enters the collider

    public GameObject feedbackIndicator;
    public Shader inactiveShader;
    private Shader _myShader;


    private int objectsInCollider = 0;

    // Use this for initialization
    void Awake ()
    {
        _myShader = feedbackIndicator.GetComponent<Renderer>().material.shader;
        feedbackIndicator.GetComponent<Renderer>().material.shader = inactiveShader;
    }

    // Update is called once per frame
    void Update () {
	
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.tag =="Tangram")
        {
            if (objectsInCollider == 0)
            {
                if (onCollisionEnter != null)
                {
                    feedbackIndicator.GetComponent<Renderer>().material.shader = _myShader;
                    onCollisionEnter();
                }
            }
            objectsInCollider++;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Tangram")
        {
            if (objectsInCollider == 1)
            {
                if (onCollisionExit != null)
                {
                    feedbackIndicator.GetComponent<Renderer>().material.shader = inactiveShader;
                    onCollisionExit();
                }
            }
            objectsInCollider--;
        }
    }
}
