﻿using UnityEngine;
using System.Collections;

public class ClonePoolManagerJR : MonoBehaviour 
{
	public YonEffectJRController gameState;							// Reference to gameState

	public const int MAX_NCLONES = 30;						// Defines the maximum number of clones

	public InputVCR[] cloneKaerb;							// Array of Kaerb clones VCR controller  
	public InputVCR[] cloneCamera;							// Array of clone's cameras VCR controller
	
	private Recording[] _cloneKaerbActions;					// Series of recorded keyboard inputs on Kaerb
	private Recording[] _cloneCameraActions;				// Series of mouse inputs on Kaerb
	
	private float[] _cloneStartTime;						// Actual time when the clones appear
	private float[] _cloneEndTime;							// Actual time when the clones disappear
	
	private bool[] _isCloneActive;							// Tells if the clone is actually playing
	
	private int _numClonesRecorded;						    // Number of clones with a recording

	// Use this for initialization
	void Awake () 
	{
		// Array initialization
		_cloneKaerbActions = new Recording[MAX_NCLONES];
		_cloneCameraActions = new Recording[MAX_NCLONES];
	
		_cloneStartTime = new float[MAX_NCLONES];
		_cloneEndTime = new float[MAX_NCLONES];
		
		// The first clone has startTime at the beginning of the game. Always.
		_cloneStartTime[0] = 0;
		
		_isCloneActive = new bool[MAX_NCLONES];
				
		// Get reference to GameState
        if (gameState == null)
        {
            gameState = GameObject.Find("___GamestateManager").GetComponent<YonEffectJRController>();
        }

        _numClonesRecorded = 0;

    }
	
	// Update is called once per frame
	void Update () 
	{
        
	}
		
	// This method gets called whenever Kaerb does a jump back in time, adding a new record of past actions 
	public IEnumerator addKaerbRecording(Recording kaerbRecording, Recording cameraRecording, float jumpTime)
	{
		//Store the recording done until now on Kaerb and camera
		Recording recKaerb = kaerbRecording;
		Recording recCamera = cameraRecording;

		_cloneKaerbActions[_numClonesRecorded] = recKaerb;
		_cloneCameraActions[_numClonesRecorded] = recCamera;

  
        //_cloneStartTime[_numClonesRecorded + 1] = gameState.worldTime - jumpTime;
        //_cloneEndTime[_numClonesRecorded] = gameState.worldTime;

        _cloneStartTime[_numClonesRecorded + 1] = 0;
        _cloneEndTime[_numClonesRecorded] = gameState.timeBetweenJumps;

        // Resets all clones (update will take care of new conditions)
        for (int i = 0; i < _numClonesRecorded; i++)
		{
			// Stops whatever reproduction was going on 
			cloneKaerb[i].Stop ();
			cloneCamera[i].Stop ();
			// Move the clone out of visible area
			// Set the clone not active
			_isCloneActive[i]=false;
		}
		_numClonesRecorded++;	// Incrementing the number of clones with a recording

        for (int i = 0; i < _numClonesRecorded; i++)
        {
            startKaerbClone(i);
        }

        yield return null;
		
	}

    public void startKaerbClone(int i)
    {
        // Getting the initial position (frame 0) of the clone
        cloneKaerb[i].transform.position = InputVCR.ParseVector3(_cloneKaerbActions[i].GetProperty(0, "position"));

        // Rotation is stored with euler angles and gives problems. So, I give it the starting rotation 
        // and let the sync do his job
        cloneKaerb[i].transform.rotation = new Quaternion();

        // Doing the same for the camera
        cloneCamera[i].transform.position = InputVCR.ParseVector3(_cloneCameraActions[i].GetProperty(0, "position"));
        cloneCamera[i].transform.rotation = new Quaternion();

        // Enabling clone components
        cloneKaerb[i].GetComponent<OVRCloneControllerVCR>().enabled = true;

        // The clone now is set at the correct position he should be. 
        // Now, I make the clone plays the set of actions recorded  

        cloneKaerb[i].Play(Recording.ParseRecording(_cloneKaerbActions[i].ToString()));
        cloneCamera[i].Play(Recording.ParseRecording(_cloneCameraActions[i].ToString()));

        _isCloneActive[i] = true;	//This clone nos is active			
    }

    public void PauseAllClones()
    {
        for (int i = 0; i < _numClonesRecorded; i++)
        {
            // Stops whatever reproduction was going on 
            cloneKaerb[i].Pause();
            cloneCamera[i].Pause();
        }
    }

    public void ResumeAllClones()
    {
        for (int i = 0; i < _numClonesRecorded; i++)
        {
            // Resume whatever reproduction was going on 
            cloneKaerb[i].Play();
            cloneCamera[i].Play();
        }
    }

    public void StopAllClones()
    {
        for (int i = 0; i < _numClonesRecorded; i++)
        {
            // Stops whatever reproduction was going on 
            cloneKaerb[i].Stop();
            cloneCamera[i].Stop();
        }
    }
	
	// This routine checks if there are clones that won't ever be able to be reproduced again
	// This means that, considering the maximum amount of time you can jump in the past, the clones who
	// appear and disappear earlier of that time can be disposed of, along with their recording
	/*private void _disposeOutdatedClones()
	{
		for (int i = 0; i < _numClonesRecorded; i++)
		{
			// If this clone is outside the reach of the maximum jump time
			if (gameState.worldTime - gameState.JUMP_TIME > _cloneEndTime[i])
			{
				// Shifts all the successive clones and their datas backward
				for (int j = i; j < _numClonesRecorded -1; j++)
				{
					_cloneKaerbActions[i] = _cloneKaerbActions[i+1];
					_cloneCameraActions[i] = _cloneCameraActions[i+1];
					_cloneStartTime[i] = _cloneStartTime[i+1];
					_cloneEndTime[i] = _cloneEndTime[i+1];
					
					// This is a little tricky. It may happens that this routine is called while some recording is in progress.
					// Setting the actives = false, means that the update will check again, ad reassigning to the clone the proper 
					// recording even after the shift.  
					_isCloneActive[i] = false;							
				}
				_numClonesRecorded--;
			}
		}
	}*/
}
