// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:2,rntp:3,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:1360,x:35984,y:34433,varname:node_1360,prsc:2|emission-5330-OUT,clip-9085-OUT;n:type:ShaderForge.SFN_Multiply,id:3145,x:34433,y:34628,varname:node_3145,prsc:2|A-1064-RGB,B-1799-OUT,C-2584-OUT;n:type:ShaderForge.SFN_Vector1,id:1799,x:34029,y:34620,varname:node_1799,prsc:2,v1:5;n:type:ShaderForge.SFN_Color,id:1064,x:33775,y:34481,ptovrint:False,ptlb:node_9323_copy_copy_copy,ptin:_node_9323_copy_copy_copy,varname:_node_9323_copy_copy_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0,c3:1,c4:1;n:type:ShaderForge.SFN_Power,id:2584,x:33904,y:34973,varname:node_2584,prsc:2|VAL-9138-OUT,EXP-6847-OUT;n:type:ShaderForge.SFN_Multiply,id:9138,x:33622,y:34778,varname:node_9138,prsc:2|A-3670-OUT,B-2758-OUT;n:type:ShaderForge.SFN_Vector1,id:6847,x:33430,y:34968,varname:node_6847,prsc:2,v1:2;n:type:ShaderForge.SFN_Abs,id:3670,x:33498,y:34623,varname:node_3670,prsc:2|IN-8208-OUT;n:type:ShaderForge.SFN_Vector1,id:2758,x:33370,y:34812,varname:node_2758,prsc:2,v1:1.5;n:type:ShaderForge.SFN_Subtract,id:8208,x:33279,y:34554,varname:node_8208,prsc:2|A-9868-OUT,B-8029-OUT;n:type:ShaderForge.SFN_Vector1,id:8029,x:33036,y:34741,varname:node_8029,prsc:2,v1:0.5;n:type:ShaderForge.SFN_Frac,id:9868,x:33036,y:34577,varname:node_9868,prsc:2|IN-4052-OUT;n:type:ShaderForge.SFN_ComponentMask,id:9984,x:32663,y:34386,varname:node_9984,prsc:2,cc1:1,cc2:-1,cc3:-1,cc4:-1|IN-5181-UVOUT;n:type:ShaderForge.SFN_Panner,id:5181,x:32415,y:34301,varname:node_5181,prsc:2,spu:0,spv:-0.25|UVIN-2063-UVOUT;n:type:ShaderForge.SFN_TexCoord,id:2063,x:31137,y:34468,varname:node_2063,prsc:2,uv:0;n:type:ShaderForge.SFN_Color,id:617,x:34448,y:34423,ptovrint:False,ptlb:node_2855_copy_copy,ptin:_node_2855_copy_copy,varname:_node_2855_copy_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0,c3:1,c4:1;n:type:ShaderForge.SFN_Lerp,id:1722,x:34767,y:34664,varname:node_1722,prsc:2|A-617-RGB,B-3145-OUT,T-2584-OUT;n:type:ShaderForge.SFN_Tex2d,id:1890,x:34898,y:34923,ptovrint:False,ptlb:node_4549_copy,ptin:_node_4549_copy,varname:_node_4549_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:c2cbe79f52c704d4e8947f02f07d0820,ntxv:0,isnm:False|UVIN-7435-OUT;n:type:ShaderForge.SFN_Multiply,id:9499,x:35167,y:34746,varname:node_9499,prsc:2|A-1722-OUT,B-1890-R;n:type:ShaderForge.SFN_ComponentMask,id:2219,x:35484,y:34703,varname:node_2219,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-9499-OUT;n:type:ShaderForge.SFN_Add,id:7435,x:34687,y:35068,varname:node_7435,prsc:2|A-3510-UVOUT,B-4631-OUT;n:type:ShaderForge.SFN_TexCoord,id:3510,x:34413,y:34933,varname:node_3510,prsc:2,uv:0;n:type:ShaderForge.SFN_Multiply,id:4631,x:34441,y:35178,varname:node_4631,prsc:2|A-3894-OUT,B-3434-OUT;n:type:ShaderForge.SFN_Multiply,id:3434,x:34472,y:35351,varname:node_3434,prsc:2|A-3510-UVOUT,B-5840-OUT;n:type:ShaderForge.SFN_ValueProperty,id:5840,x:34216,y:35395,ptovrint:False,ptlb:titling_copy,ptin:_titling_copy,varname:_titling_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Append,id:3894,x:34119,y:35198,varname:node_3894,prsc:2|A-9696-OUT,B-3850-OUT;n:type:ShaderForge.SFN_ValueProperty,id:9696,x:33752,y:35274,ptovrint:False,ptlb:u_copy,ptin:_u_copy,varname:_u_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_ValueProperty,id:3850,x:33764,y:35437,ptovrint:False,ptlb:v_copy,ptin:_v_copy,varname:_v_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.4;n:type:ShaderForge.SFN_Tex2d,id:4823,x:34945,y:34330,ptovrint:False,ptlb:node_4823,ptin:_node_4823,varname:node_4823,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:c2cbe79f52c704d4e8947f02f07d0820,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Color,id:5953,x:35263,y:34333,ptovrint:False,ptlb:node_5953,ptin:_node_5953,varname:node_5953,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0,c3:1,c4:1;n:type:ShaderForge.SFN_Lerp,id:5330,x:35683,y:34535,varname:node_5330,prsc:2|A-5953-RGB,B-9499-OUT,T-5826-OUT;n:type:ShaderForge.SFN_OneMinus,id:5826,x:35186,y:34488,varname:node_5826,prsc:2|IN-4823-R;n:type:ShaderForge.SFN_Add,id:9085,x:35793,y:34728,varname:node_9085,prsc:2|A-9499-OUT,B-1890-R;n:type:ShaderForge.SFN_SwitchProperty,id:4052,x:32868,y:34643,ptovrint:False,ptlb:node_4052,ptin:_node_4052,varname:node_4052,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:True|A-9984-OUT,B-3662-OUT;n:type:ShaderForge.SFN_ComponentMask,id:3662,x:32622,y:34760,varname:node_3662,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-6685-OUT;n:type:ShaderForge.SFN_Panner,id:5625,x:32187,y:34710,varname:node_5625,prsc:2,spu:0.25,spv:0|UVIN-9907-OUT;n:type:ShaderForge.SFN_Distance,id:4169,x:31327,y:34852,varname:node_4169,prsc:2|A-2063-UVOUT,B-3536-OUT;n:type:ShaderForge.SFN_Vector2,id:3536,x:30811,y:35112,varname:node_3536,prsc:2,v1:0.5,v2:0.5;n:type:ShaderForge.SFN_Multiply,id:4834,x:31670,y:34928,varname:node_4834,prsc:2|A-4169-OUT,B-3298-OUT;n:type:ShaderForge.SFN_Vector1,id:3298,x:30855,y:35237,varname:node_3298,prsc:2,v1:2;n:type:ShaderForge.SFN_OneMinus,id:6620,x:31873,y:35002,varname:node_6620,prsc:2|IN-4834-OUT;n:type:ShaderForge.SFN_Multiply,id:2230,x:32067,y:35113,varname:node_2230,prsc:2|A-6620-OUT,B-2116-OUT;n:type:ShaderForge.SFN_Vector1,id:2116,x:31755,y:35299,varname:node_2116,prsc:2,v1:2;n:type:ShaderForge.SFN_Add,id:6685,x:32401,y:34900,varname:node_6685,prsc:2|A-5625-UVOUT,B-2230-OUT;n:type:ShaderForge.SFN_Append,id:8621,x:31705,y:34659,varname:node_8621,prsc:2|A-2581-OUT,B-2100-OUT;n:type:ShaderForge.SFN_ValueProperty,id:2581,x:31514,y:34659,ptovrint:False,ptlb:u,ptin:_u,varname:node_2581,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_ValueProperty,id:2100,x:31520,y:34790,ptovrint:False,ptlb:v,ptin:_v,varname:node_2100,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Multiply,id:9907,x:31861,y:34611,varname:node_9907,prsc:2|A-2063-UVOUT,B-8621-OUT;proporder:1064-617-1890-5840-9696-3850-5953-4823-4052-2581-2100;pass:END;sub:END;*/

Shader "Custom/BlueBridge 1" {
    Properties {
        _node_9323_copy_copy_copy ("node_9323_copy_copy_copy", Color) = (1,0,1,1)
        _node_2855_copy_copy ("node_2855_copy_copy", Color) = (0,0,1,1)
        _node_4549_copy ("node_4549_copy", 2D) = "white" {}
        _titling_copy ("titling_copy", Float ) = 1
        _u_copy ("u_copy", Float ) = 1
        _v_copy ("v_copy", Float ) = 0.4
        _node_5953 ("node_5953", Color) = (0,0,1,1)
        _node_4823 ("node_4823", 2D) = "white" {}
        [MaterialToggle] _node_4052 ("node_4052", Float ) = -0.8284271
        _u ("u", Float ) = 1
        _v ("v", Float ) = 1
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "Queue"="AlphaTest"
            "RenderType"="TransparentCutout"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform float4 _node_9323_copy_copy_copy;
            uniform float4 _node_2855_copy_copy;
            uniform sampler2D _node_4549_copy; uniform float4 _node_4549_copy_ST;
            uniform float _titling_copy;
            uniform float _u_copy;
            uniform float _v_copy;
            uniform sampler2D _node_4823; uniform float4 _node_4823_ST;
            uniform float4 _node_5953;
            uniform fixed _node_4052;
            uniform float _u;
            uniform float _v;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                UNITY_FOG_COORDS(1)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float4 node_6884 = _Time + _TimeEditor;
                float node_2584 = pow((abs((frac(lerp( (i.uv0+node_6884.g*float2(0,-0.25)).g, (((i.uv0*float2(_u,_v))+node_6884.g*float2(0.25,0))+((1.0 - (distance(i.uv0,float2(0.5,0.5))*2.0))*2.0)).r, _node_4052 ))-0.5))*1.5),2.0);
                float2 node_7435 = (i.uv0+(float2(_u_copy,_v_copy)*(i.uv0*_titling_copy)));
                float4 _node_4549_copy_var = tex2D(_node_4549_copy,TRANSFORM_TEX(node_7435, _node_4549_copy));
                float3 node_9499 = (lerp(_node_2855_copy_copy.rgb,(_node_9323_copy_copy_copy.rgb*5.0*node_2584),node_2584)*_node_4549_copy_var.r);
                clip((node_9499+_node_4549_copy_var.r) - 0.5);
////// Lighting:
////// Emissive:
                float4 _node_4823_var = tex2D(_node_4823,TRANSFORM_TEX(i.uv0, _node_4823));
                float3 emissive = lerp(_node_5953.rgb,node_9499,(1.0 - _node_4823_var.r));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform float4 _node_9323_copy_copy_copy;
            uniform float4 _node_2855_copy_copy;
            uniform sampler2D _node_4549_copy; uniform float4 _node_4549_copy_ST;
            uniform float _titling_copy;
            uniform float _u_copy;
            uniform float _v_copy;
            uniform fixed _node_4052;
            uniform float _u;
            uniform float _v;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float4 node_769 = _Time + _TimeEditor;
                float node_2584 = pow((abs((frac(lerp( (i.uv0+node_769.g*float2(0,-0.25)).g, (((i.uv0*float2(_u,_v))+node_769.g*float2(0.25,0))+((1.0 - (distance(i.uv0,float2(0.5,0.5))*2.0))*2.0)).r, _node_4052 ))-0.5))*1.5),2.0);
                float2 node_7435 = (i.uv0+(float2(_u_copy,_v_copy)*(i.uv0*_titling_copy)));
                float4 _node_4549_copy_var = tex2D(_node_4549_copy,TRANSFORM_TEX(node_7435, _node_4549_copy));
                float3 node_9499 = (lerp(_node_2855_copy_copy.rgb,(_node_9323_copy_copy_copy.rgb*5.0*node_2584),node_2584)*_node_4549_copy_var.r);
                clip((node_9499+_node_4549_copy_var.r) - 0.5);
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
