// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:8323,x:35578,y:32100,varname:node_8323,prsc:2|emission-5631-OUT,voffset-9290-OUT,disp-9290-OUT;n:type:ShaderForge.SFN_TexCoord,id:1803,x:31190,y:32254,varname:node_1803,prsc:2,uv:0;n:type:ShaderForge.SFN_Multiply,id:9290,x:34292,y:32684,varname:node_9290,prsc:2|A-6006-OUT,B-5828-OUT,C-1594-OUT;n:type:ShaderForge.SFN_NormalVector,id:5828,x:33852,y:32797,prsc:2,pt:True;n:type:ShaderForge.SFN_Sin,id:1594,x:33379,y:32526,varname:node_1594,prsc:2|IN-6295-OUT;n:type:ShaderForge.SFN_Distance,id:3515,x:31524,y:32427,varname:node_3515,prsc:2|A-1803-UVOUT,B-5693-OUT;n:type:ShaderForge.SFN_Vector2,id:5693,x:31060,y:32553,varname:node_5693,prsc:2,v1:0.5,v2:0.5;n:type:ShaderForge.SFN_Multiply,id:2447,x:31717,y:32484,varname:node_2447,prsc:2|A-3515-OUT,B-8364-OUT;n:type:ShaderForge.SFN_Vector1,id:8364,x:31104,y:32678,varname:node_8364,prsc:2,v1:2;n:type:ShaderForge.SFN_OneMinus,id:8652,x:31868,y:32535,varname:node_8652,prsc:2|IN-2447-OUT;n:type:ShaderForge.SFN_Multiply,id:6295,x:33183,y:32380,varname:node_6295,prsc:2|A-8685-UVOUT,B-401-OUT;n:type:ShaderForge.SFN_Tau,id:401,x:32516,y:32766,varname:node_401,prsc:2;n:type:ShaderForge.SFN_Time,id:7483,x:31256,y:32779,varname:node_7483,prsc:2;n:type:ShaderForge.SFN_Multiply,id:1921,x:31530,y:32750,varname:node_1921,prsc:2|A-7483-T,B-9931-OUT;n:type:ShaderForge.SFN_Vector1,id:9931,x:31332,y:32969,varname:node_9931,prsc:2,v1:0;n:type:ShaderForge.SFN_Panner,id:4135,x:32469,y:32297,varname:node_4135,prsc:2,spu:1,spv:1|UVIN-1803-UVOUT,DIST-145-OUT;n:type:ShaderForge.SFN_Multiply,id:145,x:32171,y:32606,varname:node_145,prsc:2|A-8652-OUT,B-2444-OUT;n:type:ShaderForge.SFN_Vector1,id:2444,x:32004,y:32740,varname:node_2444,prsc:2,v1:2;n:type:ShaderForge.SFN_Rotator,id:8685,x:32737,y:32359,varname:node_8685,prsc:2|UVIN-4135-UVOUT;n:type:ShaderForge.SFN_Multiply,id:6498,x:31818,y:32743,varname:node_6498,prsc:2|A-2447-OUT,B-1921-OUT;n:type:ShaderForge.SFN_Tex2d,id:5975,x:34146,y:31827,ptovrint:False,ptlb:node_1888_copy,ptin:_node_1888_copy,varname:_node_1888_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:c221062867ae30f44ad77f6f65b2132d,ntxv:2,isnm:False|UVIN-336-UVOUT;n:type:ShaderForge.SFN_Add,id:5631,x:35071,y:32192,varname:node_5631,prsc:2|A-5975-RGB,B-7952-OUT;n:type:ShaderForge.SFN_Panner,id:7288,x:33617,y:31776,varname:node_7288,prsc:2,spu:2,spv:2|UVIN-7343-UVOUT,DIST-8254-OUT;n:type:ShaderForge.SFN_TexCoord,id:7343,x:32925,y:31907,varname:node_7343,prsc:2,uv:0;n:type:ShaderForge.SFN_Rotator,id:336,x:33878,y:31814,varname:node_336,prsc:2|UVIN-7288-UVOUT;n:type:ShaderForge.SFN_Distance,id:8254,x:33219,y:32033,varname:node_8254,prsc:2|A-7343-UVOUT,B-8661-OUT;n:type:ShaderForge.SFN_Vector2,id:8661,x:32925,y:32105,varname:node_8661,prsc:2,v1:0.5,v2:0.5;n:type:ShaderForge.SFN_SwitchProperty,id:8956,x:33752,y:32465,ptovrint:False,ptlb:node_8956,ptin:_node_8956,varname:node_8956,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:True|A-6295-OUT,B-1594-OUT;n:type:ShaderForge.SFN_ValueProperty,id:6006,x:34042,y:32665,ptovrint:False,ptlb:node_6006,ptin:_node_6006,varname:node_6006,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.1;n:type:ShaderForge.SFN_Desaturate,id:7506,x:34205,y:32371,varname:node_7506,prsc:2|COL-8956-OUT,DES-8462-OUT;n:type:ShaderForge.SFN_ComponentMask,id:5340,x:34373,y:32327,varname:node_5340,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-7506-OUT;n:type:ShaderForge.SFN_Vector1,id:8462,x:34060,y:32527,varname:node_8462,prsc:2,v1:1;n:type:ShaderForge.SFN_Color,id:1979,x:33845,y:32056,ptovrint:False,ptlb:node_1979,ptin:_node_1979,varname:node_1979,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0,c3:0.9310346,c4:1;n:type:ShaderForge.SFN_Color,id:6248,x:33832,y:32255,ptovrint:False,ptlb:node_6248,ptin:_node_6248,varname:node_6248,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:1,c3:0.7517242,c4:1;n:type:ShaderForge.SFN_Lerp,id:7952,x:34325,y:32114,varname:node_7952,prsc:2|A-1979-RGB,B-6248-RGB,T-5340-OUT;proporder:5975-8956-6006-1979-6248;pass:END;sub:END;*/

Shader "Custom/alberi" {
    Properties {
        _node_1888_copy ("node_1888_copy", 2D) = "black" {}
        [MaterialToggle] _node_8956 ("node_8956", Float ) = -0.9810169
        _node_6006 ("node_6006", Float ) = 0.1
        _node_1979 ("node_1979", Color) = (1,0,0.9310346,1)
        _node_6248 ("node_6248", Color) = (0,1,0.7517242,1)
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform sampler2D _node_1888_copy; uniform float4 _node_1888_copy_ST;
            uniform fixed _node_8956;
            uniform float _node_6006;
            uniform float4 _node_1979;
            uniform float4 _node_6248;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
                UNITY_FOG_COORDS(2)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                float4 node_3566 = _Time + _TimeEditor;
                float node_8685_ang = node_3566.g;
                float node_8685_spd = 1.0;
                float node_8685_cos = cos(node_8685_spd*node_8685_ang);
                float node_8685_sin = sin(node_8685_spd*node_8685_ang);
                float2 node_8685_piv = float2(0.5,0.5);
                float node_2447 = (distance(o.uv0,float2(0.5,0.5))*2.0);
                float2 node_8685 = (mul((o.uv0+((1.0 - node_2447)*2.0)*float2(1,1))-node_8685_piv,float2x2( node_8685_cos, -node_8685_sin, node_8685_sin, node_8685_cos))+node_8685_piv);
                float2 node_6295 = (node_8685*6.28318530718);
                float2 node_1594 = sin(node_6295);
                float3 node_9290 = (_node_6006*v.normal*float3(node_1594,0.0));
                v.vertex.xyz += node_9290;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float4 node_3566 = _Time + _TimeEditor;
                float node_336_ang = node_3566.g;
                float node_336_spd = 1.0;
                float node_336_cos = cos(node_336_spd*node_336_ang);
                float node_336_sin = sin(node_336_spd*node_336_ang);
                float2 node_336_piv = float2(0.5,0.5);
                float2 node_336 = (mul((i.uv0+distance(i.uv0,float2(0.5,0.5))*float2(2,2))-node_336_piv,float2x2( node_336_cos, -node_336_sin, node_336_sin, node_336_cos))+node_336_piv);
                float4 _node_1888_copy_var = tex2D(_node_1888_copy,TRANSFORM_TEX(node_336, _node_1888_copy));
                float node_8685_ang = node_3566.g;
                float node_8685_spd = 1.0;
                float node_8685_cos = cos(node_8685_spd*node_8685_ang);
                float node_8685_sin = sin(node_8685_spd*node_8685_ang);
                float2 node_8685_piv = float2(0.5,0.5);
                float node_2447 = (distance(i.uv0,float2(0.5,0.5))*2.0);
                float2 node_8685 = (mul((i.uv0+((1.0 - node_2447)*2.0)*float2(1,1))-node_8685_piv,float2x2( node_8685_cos, -node_8685_sin, node_8685_sin, node_8685_cos))+node_8685_piv);
                float2 node_6295 = (node_8685*6.28318530718);
                float2 node_1594 = sin(node_6295);
                float3 emissive = (_node_1888_copy_var.rgb+lerp(_node_1979.rgb,_node_6248.rgb,lerp(lerp( node_6295, node_1594, _node_8956 ),dot(lerp( node_6295, node_1594, _node_8956 ),float3(0.3,0.59,0.11)),1.0).r));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform float _node_6006;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                float4 node_6439 = _Time + _TimeEditor;
                float node_8685_ang = node_6439.g;
                float node_8685_spd = 1.0;
                float node_8685_cos = cos(node_8685_spd*node_8685_ang);
                float node_8685_sin = sin(node_8685_spd*node_8685_ang);
                float2 node_8685_piv = float2(0.5,0.5);
                float node_2447 = (distance(o.uv0,float2(0.5,0.5))*2.0);
                float2 node_8685 = (mul((o.uv0+((1.0 - node_2447)*2.0)*float2(1,1))-node_8685_piv,float2x2( node_8685_cos, -node_8685_sin, node_8685_sin, node_8685_cos))+node_8685_piv);
                float2 node_6295 = (node_8685*6.28318530718);
                float2 node_1594 = sin(node_6295);
                float3 node_9290 = (_node_6006*v.normal*float3(node_1594,0.0));
                v.vertex.xyz += node_9290;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 normalDirection = i.normalDir;
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
