// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:5546,x:33841,y:32750,varname:node_5546,prsc:2|emission-1193-OUT;n:type:ShaderForge.SFN_Color,id:3947,x:32229,y:32501,ptovrint:False,ptlb:node_3947,ptin:_node_3947,varname:node_3947,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:0,c4:1;n:type:ShaderForge.SFN_Power,id:5632,x:32835,y:32978,varname:node_5632,prsc:2|VAL-2394-OUT,EXP-3465-OUT;n:type:ShaderForge.SFN_Multiply,id:2394,x:32573,y:32936,varname:node_2394,prsc:2|A-9668-OUT,B-1857-OUT;n:type:ShaderForge.SFN_Panner,id:4349,x:31395,y:32502,varname:node_4349,prsc:2,spu:1,spv:1|UVIN-3927-OUT,DIST-9091-OUT;n:type:ShaderForge.SFN_TexCoord,id:871,x:31007,y:32350,varname:node_871,prsc:2,uv:0;n:type:ShaderForge.SFN_Vector1,id:1857,x:31966,y:33031,varname:node_1857,prsc:2,v1:1.3;n:type:ShaderForge.SFN_Vector1,id:3465,x:32558,y:33168,varname:node_3465,prsc:2,v1:15;n:type:ShaderForge.SFN_Subtract,id:3800,x:31921,y:32748,varname:node_3800,prsc:2|A-7646-OUT,B-5391-OUT;n:type:ShaderForge.SFN_Abs,id:9668,x:32122,y:32870,varname:node_9668,prsc:2|IN-3800-OUT;n:type:ShaderForge.SFN_Frac,id:2670,x:31648,y:32681,varname:node_2670,prsc:2|IN-3453-OUT;n:type:ShaderForge.SFN_ComponentMask,id:4660,x:31680,y:32514,varname:node_4660,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-4349-UVOUT;n:type:ShaderForge.SFN_Frac,id:3585,x:31603,y:32821,varname:node_3585,prsc:2|IN-2670-OUT;n:type:ShaderForge.SFN_Vector1,id:5391,x:31826,y:32949,varname:node_5391,prsc:2,v1:0.15;n:type:ShaderForge.SFN_Time,id:3042,x:31126,y:32678,varname:node_3042,prsc:2;n:type:ShaderForge.SFN_Multiply,id:9091,x:31386,y:32821,varname:node_9091,prsc:2|A-3042-T,B-9789-OUT;n:type:ShaderForge.SFN_Slider,id:9789,x:31047,y:32893,ptovrint:False,ptlb:timer,ptin:_timer,varname:node_9789,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-1,cur:-1,max:1;n:type:ShaderForge.SFN_Lerp,id:1193,x:32624,y:32485,varname:node_1193,prsc:2|A-3094-RGB,B-3947-RGB,T-7881-OUT;n:type:ShaderForge.SFN_Color,id:3094,x:32338,y:32661,ptovrint:False,ptlb:node_3094,ptin:_node_3094,varname:node_3094,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.07843138,c2:0.07843138,c3:0.2352941,c4:1;n:type:ShaderForge.SFN_Multiply,id:3927,x:31214,y:32380,varname:node_3927,prsc:2|A-871-UVOUT,B-9564-OUT;n:type:ShaderForge.SFN_ValueProperty,id:9564,x:30876,y:32552,ptovrint:False,ptlb:number,ptin:_number,varname:node_9564,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_SwitchProperty,id:3453,x:31934,y:32457,ptovrint:False,ptlb:direction,ptin:_direction,varname:node_3453,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:True|A-9113-OUT,B-4660-OUT;n:type:ShaderForge.SFN_ComponentMask,id:9113,x:31667,y:32335,varname:node_9113,prsc:2,cc1:1,cc2:-1,cc3:-1,cc4:-1|IN-4349-UVOUT;n:type:ShaderForge.SFN_ValueProperty,id:3303,x:32863,y:33172,ptovrint:False,ptlb:int opacity,ptin:_intopacity,varname:node_7762,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.25;n:type:ShaderForge.SFN_Multiply,id:7881,x:33081,y:33040,varname:node_7881,prsc:2|A-5632-OUT,B-3303-OUT;n:type:ShaderForge.SFN_OneMinus,id:3051,x:31603,y:33045,varname:node_3051,prsc:2|IN-3585-OUT;n:type:ShaderForge.SFN_SwitchProperty,id:7646,x:31921,y:33169,ptovrint:False,ptlb:opacitydirection,ptin:_opacitydirection,varname:node_7646,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:False|A-3585-OUT,B-3051-OUT;proporder:3947-9789-3094-9564-3453-3303-7646;pass:END;sub:END;*/

Shader "Custom/LightPath" {
    Properties {
        _node_3947 ("node_3947", Color) = (1,1,0,1)
        _timer ("timer", Range(-1, 1)) = -1
        _node_3094 ("node_3094", Color) = (0.07843138,0.07843138,0.2352941,1)
        _number ("number", Float ) = 1
        [MaterialToggle] _direction ("direction", Float ) = 0
        _intopacity ("int opacity", Float ) = 0.25
        [MaterialToggle] _opacitydirection ("opacitydirection", Float ) = 0
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform float4 _node_3947;
            uniform float _timer;
            uniform float4 _node_3094;
            uniform float _number;
            uniform fixed _direction;
            uniform float _intopacity;
            uniform fixed _opacitydirection;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                UNITY_FOG_COORDS(1)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
////// Lighting:
////// Emissive:
                float4 node_3042 = _Time + _TimeEditor;
                float2 node_4349 = ((i.uv0*_number)+(node_3042.g*_timer)*float2(1,1));
                float node_3585 = frac(frac(lerp( node_4349.g, node_4349.r, _direction )));
                float3 emissive = lerp(_node_3094.rgb,_node_3947.rgb,(pow((abs((lerp( node_3585, (1.0 - node_3585), _opacitydirection )-0.15))*1.3),15.0)*_intopacity));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
