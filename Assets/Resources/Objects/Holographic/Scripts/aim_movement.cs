﻿using UnityEngine;
using System.Collections;

public class aim_movement : MonoBehaviour {
	private float _rot;
	private float _i;
	private float _speed;
	
	// Use this for initialization
	void Start () 
	{
		_rot = Random.Range(-270f,270f);
		_i = Random.Range(0f,360f);
		_speed = Random.Range(0.6f,1f);
		//this.transform.Rotate(Vector3.up,rot);
		
	}
	
	public float getRot()
	{
		return _rot;
	}
	
	public void setRot()
	{
	    
	}
	
	
	// Update is called once per frame
	void Update () 
	{
		_i+=_speed*4f;
		this.transform.Rotate(Vector3.forward, Mathf.Sin(_i/180f)/6);
	}
}
