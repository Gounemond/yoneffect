﻿using UnityEngine;
using System.Collections;

public class StartMenuDifficultySelector : MonoBehaviour {

    public VRDifficultyButton normalDifficulty;
    public VRDifficultyButton hardDifficulty;
    public VRDifficultyButton impossibleDifficulty;

	// Use this for initialization
	void Awake ()
    {
        GameDifficulty.LoadDifficulty();
    }

    void Start()
    {
        switch (GameDifficulty.maxDifficulty)
        {
            case 0:
                normalDifficulty.UnlockDifficulty();
                normalDifficulty.onClicked += SelectNormalDifficulty;
                break;
            case 1:
                normalDifficulty.UnlockDifficulty();
                normalDifficulty.onClicked += SelectNormalDifficulty;
                hardDifficulty.UnlockDifficulty();
                hardDifficulty.onClicked += SelectHardDifficulty;
                break;
            case 2:
                normalDifficulty.UnlockDifficulty();
                normalDifficulty.onClicked += SelectNormalDifficulty;
                hardDifficulty.UnlockDifficulty();
                hardDifficulty.onClicked += SelectHardDifficulty;
                impossibleDifficulty.UnlockDifficulty();
                impossibleDifficulty.onClicked += SelectImpossibleDifficulty;
                break;
            default:
                normalDifficulty.UnlockDifficulty();
                normalDifficulty.onClicked += SelectNormalDifficulty;
                break;
        }
    }
	
	void SelectNormalDifficulty()
    {
        GameDifficulty.selectedDifficulty = 0;
        normalDifficulty.SelectedDifficulty();
        switch (GameDifficulty.maxDifficulty)
        {
            case 0:
                break;
            case 1:
                hardDifficulty.DeselectedDifficulty();
                break;
            case 2:
                hardDifficulty.DeselectedDifficulty();
                impossibleDifficulty.DeselectedDifficulty();
                break;
            default:
                break;
        }
    }

    void SelectHardDifficulty()
    {
        GameDifficulty.selectedDifficulty = 1;
        hardDifficulty.SelectedDifficulty();
        switch (GameDifficulty.maxDifficulty)
        {
            case 0:
                normalDifficulty.DeselectedDifficulty();
                break;
            case 1:
                normalDifficulty.DeselectedDifficulty();
                break;
            case 2:
                normalDifficulty.DeselectedDifficulty();
                impossibleDifficulty.DeselectedDifficulty();
                break;
            default:
                break;
        }
    }

    void SelectImpossibleDifficulty()
    {
        GameDifficulty.selectedDifficulty = 2;
        impossibleDifficulty.SelectedDifficulty();
        switch (GameDifficulty.maxDifficulty)
        {
            case 0:
                normalDifficulty.DeselectedDifficulty();
                break;
            case 1:
                normalDifficulty.DeselectedDifficulty();
                hardDifficulty.DeselectedDifficulty();
                break;
            case 2:
                normalDifficulty.DeselectedDifficulty();
                hardDifficulty.DeselectedDifficulty();
                break;
            default:
                break;
        }
    }
}
