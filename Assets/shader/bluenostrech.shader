// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:3528,x:33996,y:32735,varname:node_3528,prsc:2|emission-5976-OUT;n:type:ShaderForge.SFN_Color,id:7920,x:32194,y:32735,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_7920,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0.1310344,c3:1,c4:1;n:type:ShaderForge.SFN_Add,id:2152,x:32975,y:32791,varname:node_2152,prsc:2|A-9785-OUT,B-5030-OUT;n:type:ShaderForge.SFN_TexCoord,id:3264,x:31406,y:32402,varname:node_3264,prsc:2,uv:0;n:type:ShaderForge.SFN_Tex2d,id:3280,x:32506,y:32474,ptovrint:False,ptlb:Texture,ptin:_Texture,varname:node_3280,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:bd0e5eaacbb887f4998743d549c365e9,ntxv:0,isnm:False|UVIN-7846-UVOUT;n:type:ShaderForge.SFN_Add,id:5030,x:32594,y:32938,varname:node_5030,prsc:2|A-7920-RGB,B-6721-UVOUT;n:type:ShaderForge.SFN_Panner,id:7846,x:32194,y:32547,varname:node_7846,prsc:2,spu:1,spv:1|UVIN-6029-OUT,DIST-4835-OUT;n:type:ShaderForge.SFN_Time,id:819,x:31532,y:32719,varname:node_819,prsc:2;n:type:ShaderForge.SFN_Slider,id:4889,x:31375,y:32881,ptovrint:False,ptlb:Speed Panner,ptin:_SpeedPanner,varname:node_4889,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.04116169,max:1;n:type:ShaderForge.SFN_Multiply,id:4835,x:31765,y:32792,varname:node_4835,prsc:2|A-819-T,B-4889-OUT;n:type:ShaderForge.SFN_Multiply,id:6029,x:31663,y:32475,varname:node_6029,prsc:2|A-3264-UVOUT,B-6559-OUT;n:type:ShaderForge.SFN_ValueProperty,id:6559,x:31406,y:32592,ptovrint:False,ptlb:Tiling,ptin:_Tiling,varname:node_6559,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:2;n:type:ShaderForge.SFN_ScreenPos,id:6721,x:32278,y:33042,varname:node_6721,prsc:2,sctp:0;n:type:ShaderForge.SFN_Posterize,id:6566,x:32772,y:32580,varname:node_6566,prsc:2|IN-3280-RGB,STPS-641-OUT;n:type:ShaderForge.SFN_SwitchProperty,id:9785,x:32964,y:32513,ptovrint:False,ptlb:Posterize,ptin:_Posterize,varname:node_9785,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:False|A-3280-RGB,B-6566-OUT;n:type:ShaderForge.SFN_ValueProperty,id:641,x:32506,y:32682,ptovrint:False,ptlb:Posterize Value,ptin:_PosterizeValue,varname:node_641,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:5;n:type:ShaderForge.SFN_Fresnel,id:5528,x:32890,y:32972,varname:node_5528,prsc:2|EXP-4093-OUT;n:type:ShaderForge.SFN_Add,id:3384,x:33426,y:32872,varname:node_3384,prsc:2|A-2152-OUT,B-5414-OUT;n:type:ShaderForge.SFN_Multiply,id:5414,x:33142,y:33050,varname:node_5414,prsc:2|A-5528-OUT,B-5592-RGB;n:type:ShaderForge.SFN_Color,id:5592,x:32890,y:33144,ptovrint:False,ptlb:Fresnel Clr,ptin:_FresnelClr,varname:node_5592,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0.2352941,c3:1,c4:1;n:type:ShaderForge.SFN_ValueProperty,id:4093,x:32594,y:33123,ptovrint:False,ptlb:Fresnel Exp,ptin:_FresnelExp,varname:node_4093,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:5;n:type:ShaderForge.SFN_SwitchProperty,id:5976,x:33659,y:32783,ptovrint:False,ptlb:Fresnel,ptin:_Fresnel,varname:node_5976,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:False|A-2152-OUT,B-3384-OUT;proporder:7920-3280-4889-6559-9785-641-5592-4093-5976;pass:END;sub:END;*/

Shader "Custom/bluenostrech" {
    Properties {
        _Color ("Color", Color) = (0,0.1310344,1,1)
        _Texture ("Texture", 2D) = "white" {}
        _SpeedPanner ("Speed Panner", Range(0, 1)) = 0.04116169
        _Tiling ("Tiling", Float ) = 2
        [MaterialToggle] _Posterize ("Posterize", Float ) = 0
        _PosterizeValue ("Posterize Value", Float ) = 5
        _FresnelClr ("Fresnel Clr", Color) = (0,0.2352941,1,1)
        _FresnelExp ("Fresnel Exp", Float ) = 5
        [MaterialToggle] _Fresnel ("Fresnel", Float ) = 0
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform float4 _Color;
            uniform sampler2D _Texture; uniform float4 _Texture_ST;
            uniform float _SpeedPanner;
            uniform float _Tiling;
            uniform fixed _Posterize;
            uniform float _PosterizeValue;
            uniform float4 _FresnelClr;
            uniform float _FresnelExp;
            uniform fixed _Fresnel;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float4 screenPos : TEXCOORD3;
                UNITY_FOG_COORDS(4)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                o.screenPos = o.pos;
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                i.screenPos = float4( i.screenPos.xy / i.screenPos.w, 0, 0 );
                i.screenPos.y *= _ProjectionParams.x;
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float4 node_819 = _Time + _TimeEditor;
                float2 node_7846 = ((i.uv0*_Tiling)+(node_819.g*_SpeedPanner)*float2(1,1));
                float4 _Texture_var = tex2D(_Texture,TRANSFORM_TEX(node_7846, _Texture));
                float3 node_2152 = (lerp( _Texture_var.rgb, floor(_Texture_var.rgb * _PosterizeValue) / (_PosterizeValue - 1), _Posterize )+(_Color.rgb+float3(i.screenPos.rg,0.0)));
                float3 emissive = lerp( node_2152, (node_2152+(pow(1.0-max(0,dot(normalDirection, viewDirection)),_FresnelExp)*_FresnelClr.rgb)), _Fresnel );
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
