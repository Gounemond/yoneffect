// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:2,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:False,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:7797,x:34041,y:32674,varname:node_7797,prsc:2|emission-671-OUT,alpha-3948-OUT,voffset-7990-OUT;n:type:ShaderForge.SFN_Multiply,id:4158,x:32909,y:32955,varname:node_4158,prsc:2|A-2184-RGB,B-1602-OUT,C-3948-OUT;n:type:ShaderForge.SFN_Vector1,id:1602,x:32658,y:33110,varname:node_1602,prsc:2,v1:10;n:type:ShaderForge.SFN_Color,id:2184,x:32633,y:32885,ptovrint:False,ptlb:main,ptin:_main,varname:node_9323,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0,c3:0.06206894,c4:1;n:type:ShaderForge.SFN_Power,id:3948,x:32436,y:32985,varname:node_3948,prsc:2|VAL-8958-OUT,EXP-8457-OUT;n:type:ShaderForge.SFN_Color,id:2925,x:32654,y:32581,ptovrint:False,ptlb:glow,ptin:_glow,varname:node_1812,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0,c3:1,c4:1;n:type:ShaderForge.SFN_Vector1,id:4295,x:32920,y:32874,varname:node_4295,prsc:2,v1:1;n:type:ShaderForge.SFN_Lerp,id:5625,x:32803,y:32632,varname:node_5625,prsc:2|A-2925-RGB,B-865-OUT,T-3948-OUT;n:type:ShaderForge.SFN_Vector1,id:865,x:32599,y:32744,varname:node_865,prsc:2,v1:0.5;n:type:ShaderForge.SFN_Multiply,id:1556,x:32909,y:33206,varname:node_1556,prsc:2|A-3948-OUT,B-6954-OUT,C-782-OUT;n:type:ShaderForge.SFN_NormalVector,id:782,x:32683,y:33336,prsc:2,pt:False;n:type:ShaderForge.SFN_Multiply,id:8958,x:32244,y:32910,varname:node_8958,prsc:2|A-2964-OUT,B-8250-OUT;n:type:ShaderForge.SFN_Abs,id:2964,x:32103,y:32804,varname:node_2964,prsc:2|IN-8316-OUT;n:type:ShaderForge.SFN_Subtract,id:8316,x:31941,y:32769,varname:node_8316,prsc:2|A-1391-OUT,B-8974-OUT;n:type:ShaderForge.SFN_Frac,id:1391,x:31719,y:32661,varname:node_1391,prsc:2|IN-1604-OUT;n:type:ShaderForge.SFN_ComponentMask,id:1604,x:31500,y:32635,varname:node_1604,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-7442-UVOUT;n:type:ShaderForge.SFN_Panner,id:7442,x:31244,y:32545,varname:node_7442,prsc:2,spu:0.25,spv:0|UVIN-3044-OUT;n:type:ShaderForge.SFN_Frac,id:2424,x:33540,y:33082,varname:node_2424,prsc:2|IN-2878-XYZ;n:type:ShaderForge.SFN_FragmentPosition,id:2878,x:33185,y:33020,varname:node_2878,prsc:2;n:type:ShaderForge.SFN_Add,id:7990,x:33904,y:33202,varname:node_7990,prsc:2|A-2424-OUT,B-1556-OUT;n:type:ShaderForge.SFN_TexCoord,id:6576,x:30183,y:32377,varname:node_6576,prsc:2,uv:0;n:type:ShaderForge.SFN_ValueProperty,id:8457,x:32244,y:33137,ptovrint:False,ptlb:power,ptin:_power,varname:node_9258,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:2.5;n:type:ShaderForge.SFN_ValueProperty,id:8250,x:32045,y:33019,ptovrint:False,ptlb:abs,ptin:_abs,varname:node_9177,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1.5;n:type:ShaderForge.SFN_ValueProperty,id:8974,x:31734,y:32910,ptovrint:False,ptlb:substract,ptin:_substract,varname:node_5830,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.5;n:type:ShaderForge.SFN_ValueProperty,id:6954,x:32557,y:33252,ptovrint:False,ptlb:movimento,ptin:_movimento,varname:node_3180,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.2;n:type:ShaderForge.SFN_ValueProperty,id:6291,x:30738,y:32638,ptovrint:False,ptlb:quantiti,ptin:_quantiti,varname:node_276,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:3;n:type:ShaderForge.SFN_Multiply,id:2973,x:30564,y:32542,varname:node_2973,prsc:2|A-6576-V,B-2968-OUT;n:type:ShaderForge.SFN_Multiply,id:5540,x:30575,y:32313,varname:node_5540,prsc:2|A-6576-U,B-8201-OUT;n:type:ShaderForge.SFN_ValueProperty,id:8201,x:30326,y:32283,ptovrint:False,ptlb:U Scale,ptin:_UScale,varname:node_7593,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_ValueProperty,id:2968,x:30334,y:32596,ptovrint:False,ptlb:V Scale,ptin:_VScale,varname:node_7134,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Append,id:1436,x:30790,y:32423,varname:node_1436,prsc:2|A-5540-OUT,B-2973-OUT;n:type:ShaderForge.SFN_Lerp,id:5997,x:33254,y:32601,varname:node_5997,prsc:2|A-5625-OUT,B-8432-RGB,T-3948-OUT;n:type:ShaderForge.SFN_Color,id:8432,x:32958,y:32469,ptovrint:False,ptlb:node_4617,ptin:_node_4617,varname:node_4617,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Lerp,id:671,x:33639,y:32622,varname:node_671,prsc:2|A-5997-OUT,B-4158-OUT,T-8244-OUT;n:type:ShaderForge.SFN_Power,id:8244,x:33362,y:32874,varname:node_8244,prsc:2|VAL-3948-OUT,EXP-4403-OUT;n:type:ShaderForge.SFN_ValueProperty,id:4403,x:33185,y:32919,ptovrint:False,ptlb:node_299,ptin:_node_299,varname:node_299,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1.2;n:type:ShaderForge.SFN_Multiply,id:3044,x:31030,y:32441,varname:node_3044,prsc:2|A-1436-OUT,B-6291-OUT;proporder:2184-2925-8457-8250-8974-8432-4403-6954-8201-2968-6291;pass:END;sub:END;*/

Shader "Custom/greenfloor 1" {
    Properties {
        _main ("main", Color) = (1,0,0.06206894,1)
        _glow ("glow", Color) = (0,0,1,1)
        _power ("power", Float ) = 2.5
        _abs ("abs", Float ) = 1.5
        _substract ("substract", Float ) = 0.5
        _node_4617 ("node_4617", Color) = (1,1,1,1)
        _node_299 ("node_299", Float ) = 1.2
        _movimento ("movimento", Float ) = 0.2
        _UScale ("U Scale", Float ) = 1
        _VScale ("V Scale", Float ) = 1
        _quantiti ("quantiti", Float ) = 3
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            Cull Off
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform float4 _main;
            uniform float4 _glow;
            uniform float _power;
            uniform float _abs;
            uniform float _substract;
            uniform float _movimento;
            uniform float _quantiti;
            uniform float _UScale;
            uniform float _VScale;
            uniform float4 _node_4617;
            uniform float _node_299;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                UNITY_FOG_COORDS(3)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                float4 node_4323 = _Time + _TimeEditor;
                float node_3948 = pow((abs((frac(((float2((o.uv0.r*_UScale),(o.uv0.g*_VScale))*_quantiti)+node_4323.g*float2(0.25,0)).r)-_substract))*_abs),_power);
                v.vertex.xyz += (frac(mul(unity_ObjectToWorld, v.vertex).rgb)+(node_3948*_movimento*v.normal));
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                i.normalDir = normalize(i.normalDir);
                i.normalDir *= faceSign;
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float node_865 = 0.5;
                float4 node_4323 = _Time + _TimeEditor;
                float node_3948 = pow((abs((frac(((float2((i.uv0.r*_UScale),(i.uv0.g*_VScale))*_quantiti)+node_4323.g*float2(0.25,0)).r)-_substract))*_abs),_power);
                float3 emissive = lerp(lerp(lerp(_glow.rgb,float3(node_865,node_865,node_865),node_3948),_node_4617.rgb,node_3948),(_main.rgb*10.0*node_3948),pow(node_3948,_node_299));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,node_3948);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform float _power;
            uniform float _abs;
            uniform float _substract;
            uniform float _movimento;
            uniform float _quantiti;
            uniform float _UScale;
            uniform float _VScale;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
                float4 posWorld : TEXCOORD2;
                float3 normalDir : TEXCOORD3;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                float4 node_7935 = _Time + _TimeEditor;
                float node_3948 = pow((abs((frac(((float2((o.uv0.r*_UScale),(o.uv0.g*_VScale))*_quantiti)+node_7935.g*float2(0.25,0)).r)-_substract))*_abs),_power);
                v.vertex.xyz += (frac(mul(unity_ObjectToWorld, v.vertex).rgb)+(node_3948*_movimento*v.normal));
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                i.normalDir = normalize(i.normalDir);
                i.normalDir *= faceSign;
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
