﻿using UnityEngine;
using System.Collections;

namespace UI.StepsVR
{
    public class UI : MonoBehaviour
    {
        public GameObject header;
        public GameObject footer;


        private Vector3 tempVector;

        // Use this for initialization
        void OnEnable()
        {
            /*tempVector = Camera.main.transform.position;
            tempVector += Camera.main.transform.forward*5;

            this.transform.position = tempVector;

            this.transform.LookAt(Camera.main.transform.position);
            this.transform.Rotate(new Vector3(0,180,0));*/

            iTween.MoveTo(header, iTween.Hash("position", new Vector3(0, 5, 0),
                                              "time", 1,
                                              "islocal", true));

            iTween.MoveTo(footer, iTween.Hash("position", new Vector3(0, -4.6f, 0),
                                              "time", 1,
                                              "islocal", true));

        }


        // Update is called once per frame
        void Update()
        {

        }
    }
}