﻿using UnityEngine;
using System.Collections;
using System;

namespace UI.StepsVR
{
    public class MenuPrincipale : Pulsante
    {

        public override void azione_click()
        {
            Time.timeScale = 1.0f;
            Application.LoadLevel(0);
        }
    }
}