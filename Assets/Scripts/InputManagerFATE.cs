﻿using UnityEngine;
using UnityEngine.VR;
using System.Collections;
using System.Collections.Generic;

public class InputManagerFATE : MonoBehaviour
{
    protected static InputManagerFATE _self;
    public static InputManagerFATE Instance
    {
        get
        {
            if (_self == null)
                _self = FindObjectOfType(typeof(InputManagerFATE)) as InputManagerFATE;
            return _self;
        }
    }

    [Header("Input names")]
    public string[] forwardMovementAxis;
    public string[] sideMovementAxis;
    public string[] horizontalRotationAxis;
    public string[] verticalRotationAxis;
    public string[] sprintButton;
    public string[] grabButton;

    public int inputDeviceIndex = 0;

    [HideInInspector]
    public float forwardMovement;
    [HideInInspector]
    public float sideMovement;
    [HideInInspector]
    public float horizontalRotation;
    [HideInInspector]
    public float verticalRotation;
    [HideInInspector]
    public bool sprint;
    [HideInInspector]
    public bool grab;


    Dictionary<string, float> inputAxis = new Dictionary<string, float>();
    Dictionary<string, bool> inputButton = new Dictionary<string, bool>();


    // Use this for initialization
    void Start ()
    {
        if (forwardMovementAxis == null)
        {
            forwardMovementAxis = new string[] { "This is not gonna work" };
        }

        if (sideMovementAxis == null)
        {
            sideMovementAxis = new string[] { "This is not gonna work" };
        }

        if (horizontalRotationAxis == null)
        {
            horizontalRotationAxis = new string[] { "This is not gonna work" };
        }

        if (verticalRotationAxis == null)
        {
            verticalRotationAxis = new string[] { "This is not gonna work" };
        }

        if (sprintButton == null)
        {
            sprintButton = new string[] { "This is not gonna work" };
        }

        if (grabButton == null)
        {
            grabButton = new string[] { "This is not gonna work" };
        }

        // Adding those inputs to dictionaries
        inputAxis.Add(forwardMovementAxis[inputDeviceIndex], forwardMovement);
        inputAxis.Add(sideMovementAxis[inputDeviceIndex], sideMovement);
        inputAxis.Add(horizontalRotationAxis[inputDeviceIndex], horizontalRotation);
        inputAxis.Add(verticalRotationAxis[inputDeviceIndex], verticalRotation);
        inputButton.Add(sprintButton[inputDeviceIndex], sprint);
        inputButton.Add(grabButton[inputDeviceIndex], grab);


    }
	
	// Update is called once per frame
	void Update ()
    {
        forwardMovement =       Input.GetAxis(forwardMovementAxis[inputDeviceIndex]);
        sideMovement =          Input.GetAxis(sideMovementAxis[inputDeviceIndex]);
        horizontalRotation =    Input.GetAxis(horizontalRotationAxis[inputDeviceIndex]);
        verticalRotation =      Input.GetAxis(verticalRotationAxis[inputDeviceIndex]);
        sprint =                Input.GetButton(sprintButton[inputDeviceIndex]);
        grab =                  Input.GetButton(grabButton[inputDeviceIndex]);
    }

    public float GetAxis(string inputName)
    {
        float inputValue;
        inputAxis.TryGetValue(inputName, out inputValue);
        return inputValue;
    }

    public bool GetButton(string inputName)
    {
        bool inputValue;
        inputButton.TryGetValue(inputName, out inputValue);
        return inputValue;
    }
}
