using UnityEngine;
using System;
using System.Collections;

public class LaserTriggerOne : MonoBehaviour
{
    public event Action onTriggerEnter;
    public event Action onTriggerExit;
	
	public GameObject generator;
	public GameObject laserOne;
	
	private GenTwoLaserBehaviour genEngine;
	private LaserOnePosition scriptLaserOne;

    private int somethingEntered = 0;


	
	// Use this for initialization
	void Start ()
    {		
		genEngine = generator.GetComponent<GenTwoLaserBehaviour>();
		scriptLaserOne = laserOne.GetComponent<LaserOnePosition>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnTriggerEnter(Collider collision)
    {
		genEngine.deactiveGen();
        if (somethingEntered == 0)
        {
            onTriggerEnter();
        }
        somethingEntered++;

        if (collision.CompareTag("Player") || collision.CompareTag("ClonePlayer"))
        {

            //The player collided	
            scriptLaserOne.modifyPos(collision.transform.position);
        }
        else
        {
            //An object collided
            scriptLaserOne.modifyPos(collision.transform.position);
		}
		
	}
	
	void OnTriggerExit(Collider collision)
    {		
        if (somethingEntered == 1)
        {
            onTriggerExit();
        }
        somethingEntered--;
        genEngine.activeGen();
		scriptLaserOne.resetPos();
		
	}
	
}
