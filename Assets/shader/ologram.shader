// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:2592,x:34079,y:32037,varname:node_2592,prsc:2|emission-9866-OUT;n:type:ShaderForge.SFN_Blend,id:2699,x:32894,y:32468,varname:node_2699,prsc:2,blmd:14,clmp:True|SRC-7944-OUT,DST-2426-RGB;n:type:ShaderForge.SFN_Lerp,id:7944,x:32497,y:32534,varname:node_7944,prsc:2|A-1429-RGB,B-2655-OUT,T-1859-RGB;n:type:ShaderForge.SFN_Multiply,id:2655,x:32207,y:32650,varname:node_2655,prsc:2|A-5189-RGB,B-1859-RGB;n:type:ShaderForge.SFN_Color,id:1429,x:32067,y:32366,ptovrint:False,ptlb:node_1429,ptin:_node_1429,varname:node_1429,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0,c3:0.9310346,c4:1;n:type:ShaderForge.SFN_Tex2d,id:1859,x:31810,y:32535,ptovrint:False,ptlb:node_1859,ptin:_node_1859,varname:node_1859,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:ae0049a4a1783f946ac225627cf79c28,ntxv:0,isnm:False|UVIN-3010-OUT;n:type:ShaderForge.SFN_Color,id:5189,x:31771,y:32305,ptovrint:False,ptlb:node_5189,ptin:_node_5189,varname:node_5189,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0,c3:0,c4:1;n:type:ShaderForge.SFN_TexCoord,id:5401,x:31181,y:32012,varname:node_5401,prsc:2,uv:0;n:type:ShaderForge.SFN_Tex2d,id:2426,x:32528,y:32937,ptovrint:False,ptlb:node_2426,ptin:_node_2426,varname:node_2426,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:ed022fea07aefa64192fa64c637f2690,ntxv:0,isnm:False|UVIN-5717-OUT;n:type:ShaderForge.SFN_TexCoord,id:3940,x:31173,y:32618,varname:node_3940,prsc:2,uv:0;n:type:ShaderForge.SFN_Add,id:3010,x:31576,y:32057,varname:node_3010,prsc:2|A-5401-UVOUT,B-3596-OUT;n:type:ShaderForge.SFN_Append,id:7891,x:31243,y:32181,varname:node_7891,prsc:2|A-4168-OUT,B-6230-OUT;n:type:ShaderForge.SFN_ValueProperty,id:4168,x:30921,y:32191,ptovrint:False,ptlb:v,ptin:_v,varname:node_4168,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_ValueProperty,id:6230,x:30896,y:32283,ptovrint:False,ptlb:u,ptin:_u,varname:node_6230,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.5;n:type:ShaderForge.SFN_Multiply,id:3596,x:31474,y:32191,varname:node_3596,prsc:2|A-7891-OUT,B-6290-OUT;n:type:ShaderForge.SFN_Time,id:4431,x:31107,y:32301,varname:node_4431,prsc:2;n:type:ShaderForge.SFN_Multiply,id:6290,x:31342,y:32348,varname:node_6290,prsc:2|A-4431-T,B-5819-OUT;n:type:ShaderForge.SFN_Slider,id:5819,x:30896,y:32462,ptovrint:False,ptlb:time nube,ptin:_timenube,varname:node_5819,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.04579202,max:1;n:type:ShaderForge.SFN_Add,id:5717,x:32116,y:32962,varname:node_5717,prsc:2|A-1972-OUT,B-9761-OUT;n:type:ShaderForge.SFN_Multiply,id:9761,x:31846,y:33000,varname:node_9761,prsc:2|A-942-OUT,B-1460-OUT;n:type:ShaderForge.SFN_ValueProperty,id:3869,x:31096,y:32883,ptovrint:False,ptlb:rappend,ptin:_rappend,varname:node_3869,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.5;n:type:ShaderForge.SFN_ValueProperty,id:7131,x:31143,y:33009,ptovrint:False,ptlb:gappend,ptin:_gappend,varname:node_7131,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Append,id:942,x:31481,y:33080,varname:node_942,prsc:2|A-3869-OUT,B-7131-OUT;n:type:ShaderForge.SFN_Multiply,id:1460,x:31571,y:33272,varname:node_1460,prsc:2|A-5664-T,B-4970-OUT;n:type:ShaderForge.SFN_Time,id:5664,x:31189,y:33113,varname:node_5664,prsc:2;n:type:ShaderForge.SFN_Slider,id:4970,x:30968,y:33292,ptovrint:False,ptlb:time curl,ptin:_timecurl,varname:node_4970,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-1,cur:0.1532608,max:1;n:type:ShaderForge.SFN_Slider,id:4352,x:33177,y:31838,ptovrint:False,ptlb:sliderchiaro,ptin:_sliderchiaro,varname:node_4352,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Add,id:10,x:33456,y:32223,varname:node_10,prsc:2|A-3979-OUT,B-2699-OUT;n:type:ShaderForge.SFN_Multiply,id:3979,x:32794,y:31971,varname:node_3979,prsc:2|A-1231-OUT,B-9847-OUT;n:type:ShaderForge.SFN_OneMinus,id:4750,x:32451,y:32085,varname:node_4750,prsc:2|IN-9847-OUT;n:type:ShaderForge.SFN_Fresnel,id:9847,x:32148,y:31888,varname:node_9847,prsc:2|NRM-7159-OUT,EXP-6277-OUT;n:type:ShaderForge.SFN_NormalVector,id:7159,x:31797,y:31843,prsc:2,pt:False;n:type:ShaderForge.SFN_Multiply,id:1972,x:31606,y:32775,varname:node_1972,prsc:2|A-3940-UVOUT,B-6944-OUT;n:type:ShaderForge.SFN_ValueProperty,id:6944,x:31307,y:32839,ptovrint:False,ptlb:tile,ptin:_tile,varname:node_6944,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:2;n:type:ShaderForge.SFN_ValueProperty,id:6277,x:31901,y:32033,ptovrint:False,ptlb:fresnel,ptin:_fresnel,varname:node_6277,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.5;n:type:ShaderForge.SFN_Multiply,id:1231,x:32424,y:31847,varname:node_1231,prsc:2|A-2374-RGB,B-9847-OUT;n:type:ShaderForge.SFN_Color,id:2374,x:32174,y:31612,ptovrint:False,ptlb:2color,ptin:_2color,varname:node_2374,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0,c3:0,c4:1;n:type:ShaderForge.SFN_Lerp,id:9866,x:33787,y:31841,varname:node_9866,prsc:2|A-10-OUT,B-7584-RGB,T-4352-OUT;n:type:ShaderForge.SFN_Color,id:7584,x:33400,y:31617,ptovrint:False,ptlb:solid color,ptin:_solidcolor,varname:node_7584,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0,c3:0.5,c4:1;proporder:1429-1859-5189-2426-4168-6230-5819-3869-7131-4970-4352-6944-6277-2374-7584;pass:END;sub:END;*/

Shader "Custom/ologram" {
    Properties {
        _node_1429 ("node_1429", Color) = (1,0,0.9310346,1)
        _node_1859 ("node_1859", 2D) = "white" {}
        _node_5189 ("node_5189", Color) = (1,0,0,1)
        _node_2426 ("node_2426", 2D) = "white" {}
        _v ("v", Float ) = 1
        _u ("u", Float ) = 0.5
        _timenube ("time nube", Range(0, 1)) = 0.04579202
        _rappend ("rappend", Float ) = 0.5
        _gappend ("gappend", Float ) = 1
        _timecurl ("time curl", Range(-1, 1)) = 0.1532608
        _sliderchiaro ("sliderchiaro", Range(0, 1)) = 0
        _tile ("tile", Float ) = 2
        _fresnel ("fresnel", Float ) = 0.5
        _2color ("2color", Color) = (1,0,0,1)
        _solidcolor ("solid color", Color) = (0,0,0.5,1)
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform float4 _node_1429;
            uniform sampler2D _node_1859; uniform float4 _node_1859_ST;
            uniform float4 _node_5189;
            uniform sampler2D _node_2426; uniform float4 _node_2426_ST;
            uniform float _v;
            uniform float _u;
            uniform float _timenube;
            uniform float _rappend;
            uniform float _gappend;
            uniform float _timecurl;
            uniform float _sliderchiaro;
            uniform float _tile;
            uniform float _fresnel;
            uniform float4 _2color;
            uniform float4 _solidcolor;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                UNITY_FOG_COORDS(3)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float node_9847 = pow(1.0-max(0,dot(i.normalDir, viewDirection)),_fresnel);
                float4 node_4431 = _Time + _TimeEditor;
                float2 node_3010 = (i.uv0+(float2(_v,_u)*(node_4431.g*_timenube)));
                float4 _node_1859_var = tex2D(_node_1859,TRANSFORM_TEX(node_3010, _node_1859));
                float4 node_5664 = _Time + _TimeEditor;
                float2 node_5717 = ((i.uv0*_tile)+(float2(_rappend,_gappend)*(node_5664.g*_timecurl)));
                float4 _node_2426_var = tex2D(_node_2426,TRANSFORM_TEX(node_5717, _node_2426));
                float3 emissive = lerp((((_2color.rgb*node_9847)*node_9847)+saturate(( lerp(_node_1429.rgb,(_node_5189.rgb*_node_1859_var.rgb),_node_1859_var.rgb) > 0.5 ? (_node_2426_var.rgb + 2.0*lerp(_node_1429.rgb,(_node_5189.rgb*_node_1859_var.rgb),_node_1859_var.rgb) -1.0) : (_node_2426_var.rgb + 2.0*(lerp(_node_1429.rgb,(_node_5189.rgb*_node_1859_var.rgb),_node_1859_var.rgb)-0.5))))),_solidcolor.rgb,_sliderchiaro);
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
