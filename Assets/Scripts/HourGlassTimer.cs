﻿using UnityEngine;
using System.Collections;

public class HourGlassTimer : MonoBehaviour {

    public Material hourGlassSand;
    public string nameOfProperty;
    public Color switchColor1;
    public Color switchColor2;
    public Color switchColor3;
    public Color originalColor;

    // Use this for initialization
    void Start ()
    {
	    
	}

    void OnDisable()
    {
        hourGlassSand.SetColor("_color2", originalColor);
        hourGlassSand.SetFloat(nameOfProperty, 0);
    }
	
	// Update is called once per frame
	void Update ()
    {
        hourGlassSand.SetFloat(nameOfProperty, (YonEffectJRController.Self.worldTime / YonEffectJRController.Self.timeBetweenJumps) *0.5f);

	    if (YonEffectJRController.Self.worldTime <= YonEffectJRController.Self.timeBetweenJumps && YonEffectJRController.Self.worldTime > YonEffectJRController.Self.timeBetweenJumps * 0.75f)
        {
            hourGlassSand.SetColor("_color2", switchColor1);
        }
        if (YonEffectJRController.Self.worldTime <= YonEffectJRController.Self.timeBetweenJumps *0.75f && YonEffectJRController.Self.worldTime > YonEffectJRController.Self.timeBetweenJumps * 0.5f)
        {
            hourGlassSand.SetColor("_color2", switchColor2);
        }
        if (YonEffectJRController.Self.worldTime <= YonEffectJRController.Self.timeBetweenJumps * 0.5f && YonEffectJRController.Self.worldTime > YonEffectJRController.Self.timeBetweenJumps * 0.25f)
        {
            hourGlassSand.SetColor("_color2", switchColor3);
        }
        if (YonEffectJRController.Self.worldTime <= YonEffectJRController.Self.timeBetweenJumps * 0.25f && YonEffectJRController.Self.worldTime > 0)
        {
            hourGlassSand.SetColor("_color2", originalColor);
        }
    }
}
