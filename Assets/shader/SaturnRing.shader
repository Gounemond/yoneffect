// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:3,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:2,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:False,igpj:True,qofs:0,qpre:2,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:920,x:34280,y:32566,varname:node_920,prsc:2|emission-2606-OUT,alpha-9839-OUT,clip-2906-OUT;n:type:ShaderForge.SFN_Tex2d,id:539,x:32971,y:32796,ptovrint:False,ptlb:2ccc,ptin:_2ccc,varname:node_539,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:600e185cf40947845b6220297740efbb,ntxv:0,isnm:False|UVIN-7132-UVOUT;n:type:ShaderForge.SFN_Smoothstep,id:2906,x:33236,y:32993,varname:node_2906,prsc:2|A-9830-OUT,B-9153-OUT,V-539-A;n:type:ShaderForge.SFN_Vector1,id:9830,x:32697,y:32973,varname:node_9830,prsc:2,v1:0;n:type:ShaderForge.SFN_Vector1,id:9153,x:32776,y:33035,varname:node_9153,prsc:2,v1:1;n:type:ShaderForge.SFN_Vector1,id:9839,x:33191,y:32921,varname:node_9839,prsc:2,v1:0.5;n:type:ShaderForge.SFN_TexCoord,id:7051,x:32387,y:32713,varname:node_7051,prsc:2,uv:0;n:type:ShaderForge.SFN_Rotator,id:7132,x:32697,y:32739,varname:node_7132,prsc:2|UVIN-7051-UVOUT,SPD-3681-OUT;n:type:ShaderForge.SFN_Color,id:3930,x:33127,y:32152,ptovrint:False,ptlb:node_3930,ptin:_node_3930,varname:node_3930,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.6196079,c2:0.6196079,c3:0.9921569,c4:1;n:type:ShaderForge.SFN_Lerp,id:2606,x:33805,y:32414,varname:node_2606,prsc:2|A-7273-OUT,B-7183-OUT,T-5249-RGB;n:type:ShaderForge.SFN_Color,id:6901,x:33076,y:32391,ptovrint:False,ptlb:node_6901,ptin:_node_6901,varname:node_6901,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0.8313726,c3:0.8313726,c4:1;n:type:ShaderForge.SFN_Power,id:7273,x:33585,y:32179,varname:node_7273,prsc:2|VAL-3930-RGB,EXP-9066-OUT;n:type:ShaderForge.SFN_Vector1,id:9066,x:33280,y:32259,varname:node_9066,prsc:2,v1:3;n:type:ShaderForge.SFN_Multiply,id:7183,x:33401,y:32362,varname:node_7183,prsc:2|A-6901-RGB,B-6048-OUT;n:type:ShaderForge.SFN_Vector1,id:6048,x:33240,y:32504,varname:node_6048,prsc:2,v1:5;n:type:ShaderForge.SFN_ValueProperty,id:3681,x:32387,y:32882,ptovrint:False,ptlb:spdrotator,ptin:_spdrotator,varname:node_3681,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_Tex2d,id:5249,x:32890,y:32478,ptovrint:False,ptlb:node_5249,ptin:_node_5249,varname:node_5249,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:600e185cf40947845b6220297740efbb,ntxv:0,isnm:False|UVIN-3547-UVOUT;n:type:ShaderForge.SFN_Rotator,id:3547,x:32619,y:32380,varname:node_3547,prsc:2|UVIN-2774-UVOUT,SPD-3681-OUT;n:type:ShaderForge.SFN_TexCoord,id:6436,x:32213,y:32259,varname:node_6436,prsc:2,uv:0;n:type:ShaderForge.SFN_Parallax,id:2774,x:32426,y:32341,varname:node_2774,prsc:2|UVIN-6436-UVOUT,HEI-7715-A,DEP-4922-OUT;n:type:ShaderForge.SFN_Tex2d,id:7715,x:32156,y:32470,ptovrint:False,ptlb:node_7715,ptin:_node_7715,varname:node_7715,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:600e185cf40947845b6220297740efbb,ntxv:0,isnm:False;n:type:ShaderForge.SFN_ValueProperty,id:4922,x:32323,y:32557,ptovrint:False,ptlb:node_4922,ptin:_node_4922,varname:node_4922,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;proporder:539-3930-6901-3681-5249-7715-4922;pass:END;sub:END;*/

Shader "Custom/SaturnRing" {
    Properties {
        _2ccc ("2ccc", 2D) = "white" {}
        _node_3930 ("node_3930", Color) = (0.6196079,0.6196079,0.9921569,1)
        _node_6901 ("node_6901", Color) = (0,0.8313726,0.8313726,1)
        _spdrotator ("spdrotator", Float ) = 0
        _node_5249 ("node_5249", 2D) = "white" {}
        _node_7715 ("node_7715", 2D) = "white" {}
        _node_4922 ("node_4922", Float ) = 1
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="AlphaTest"
            "RenderType"="Transparent"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            Cull Off
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma multi_compile_fwdbase
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform sampler2D _2ccc; uniform float4 _2ccc_ST;
            uniform float4 _node_3930;
            uniform float4 _node_6901;
            uniform float _spdrotator;
            uniform sampler2D _node_5249; uniform float4 _node_5249_ST;
            uniform sampler2D _node_7715; uniform float4 _node_7715_ST;
            uniform float _node_4922;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 bitangentDir : TEXCOORD4;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                i.normalDir = normalize(i.normalDir);
                i.normalDir *= faceSign;
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
                float4 node_1694 = _Time + _TimeEditor;
                float node_7132_ang = node_1694.g;
                float node_7132_spd = _spdrotator;
                float node_7132_cos = cos(node_7132_spd*node_7132_ang);
                float node_7132_sin = sin(node_7132_spd*node_7132_ang);
                float2 node_7132_piv = float2(0.5,0.5);
                float2 node_7132 = (mul(i.uv0-node_7132_piv,float2x2( node_7132_cos, -node_7132_sin, node_7132_sin, node_7132_cos))+node_7132_piv);
                float4 _2ccc_var = tex2D(_2ccc,TRANSFORM_TEX(node_7132, _2ccc));
                clip(smoothstep( 0.0, 1.0, _2ccc_var.a ) - 0.5);
////// Lighting:
////// Emissive:
                float node_3547_ang = node_1694.g;
                float node_3547_spd = _spdrotator;
                float node_3547_cos = cos(node_3547_spd*node_3547_ang);
                float node_3547_sin = sin(node_3547_spd*node_3547_ang);
                float2 node_3547_piv = float2(0.5,0.5);
                float4 _node_7715_var = tex2D(_node_7715,TRANSFORM_TEX(i.uv0, _node_7715));
                float2 node_3547 = (mul((_node_4922*(_node_7715_var.a - 0.5)*mul(tangentTransform, viewDirection).xy + i.uv0).rg-node_3547_piv,float2x2( node_3547_cos, -node_3547_sin, node_3547_sin, node_3547_cos))+node_3547_piv);
                float4 _node_5249_var = tex2D(_node_5249,TRANSFORM_TEX(node_3547, _node_5249));
                float3 emissive = lerp(pow(_node_3930.rgb,3.0),(_node_6901.rgb*5.0),_node_5249_var.rgb);
                float3 finalColor = emissive;
                return fixed4(finalColor,0.5);
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform sampler2D _2ccc; uniform float4 _2ccc_ST;
            uniform float _spdrotator;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                float4 node_5267 = _Time + _TimeEditor;
                float node_7132_ang = node_5267.g;
                float node_7132_spd = _spdrotator;
                float node_7132_cos = cos(node_7132_spd*node_7132_ang);
                float node_7132_sin = sin(node_7132_spd*node_7132_ang);
                float2 node_7132_piv = float2(0.5,0.5);
                float2 node_7132 = (mul(i.uv0-node_7132_piv,float2x2( node_7132_cos, -node_7132_sin, node_7132_sin, node_7132_cos))+node_7132_piv);
                float4 _2ccc_var = tex2D(_2ccc,TRANSFORM_TEX(node_7132, _2ccc));
                clip(smoothstep( 0.0, 1.0, _2ccc_var.a ) - 0.5);
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
