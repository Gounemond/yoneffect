// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:8361,x:33319,y:32640,varname:node_8361,prsc:2|diff-892-OUT,spec-2132-OUT,emission-822-OUT,transm-3150-OUT,lwrap-3150-OUT,alpha-3150-OUT;n:type:ShaderForge.SFN_Multiply,id:822,x:32845,y:32891,varname:node_822,prsc:2|A-9323-RGB,B-101-OUT,C-3150-OUT;n:type:ShaderForge.SFN_Vector1,id:101,x:32594,y:33046,varname:node_101,prsc:2,v1:10;n:type:ShaderForge.SFN_Color,id:9323,x:32517,y:32761,ptovrint:False,ptlb:node_9323,ptin:_node_9323,varname:node_9323,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.2313726,c2:0.9803922,c3:0.7215686,c4:1;n:type:ShaderForge.SFN_Power,id:3150,x:32372,y:32921,varname:node_3150,prsc:2|VAL-7822-OUT,EXP-8829-OUT;n:type:ShaderForge.SFN_Color,id:1812,x:32590,y:32517,ptovrint:False,ptlb:node_1812,ptin:_node_1812,varname:node_1812,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0,c3:0.07450981,c4:1;n:type:ShaderForge.SFN_Vector1,id:2132,x:32856,y:32810,varname:node_2132,prsc:2,v1:1;n:type:ShaderForge.SFN_Lerp,id:892,x:32914,y:32585,varname:node_892,prsc:2|A-1812-RGB,B-7756-OUT,T-3150-OUT;n:type:ShaderForge.SFN_Vector1,id:7756,x:32545,y:32690,varname:node_7756,prsc:2,v1:1;n:type:ShaderForge.SFN_Multiply,id:744,x:32845,y:33142,varname:node_744,prsc:2|A-3150-OUT,B-7639-OUT,C-6607-OUT;n:type:ShaderForge.SFN_NormalVector,id:6607,x:32619,y:33272,prsc:2,pt:False;n:type:ShaderForge.SFN_Vector1,id:7639,x:32619,y:33196,varname:node_7639,prsc:2,v1:0.2;n:type:ShaderForge.SFN_Multiply,id:7822,x:32180,y:32846,varname:node_7822,prsc:2|A-1071-OUT,B-9584-OUT;n:type:ShaderForge.SFN_Vector1,id:8829,x:32180,y:33049,varname:node_8829,prsc:2,v1:2.5;n:type:ShaderForge.SFN_Abs,id:1071,x:32039,y:32740,varname:node_1071,prsc:2|IN-6097-OUT;n:type:ShaderForge.SFN_Vector1,id:9584,x:32039,y:32955,varname:node_9584,prsc:2,v1:1.5;n:type:ShaderForge.SFN_Subtract,id:6097,x:31877,y:32705,varname:node_6097,prsc:2|A-4592-OUT,B-155-OUT;n:type:ShaderForge.SFN_Vector1,id:155,x:31713,y:32831,varname:node_155,prsc:2,v1:0.5;n:type:ShaderForge.SFN_Frac,id:4592,x:31655,y:32597,varname:node_4592,prsc:2|IN-7731-OUT;n:type:ShaderForge.SFN_ComponentMask,id:7731,x:31385,y:32541,varname:node_7731,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-1260-UVOUT;n:type:ShaderForge.SFN_Panner,id:1260,x:31196,y:32464,varname:node_1260,prsc:2,spu:0.25,spv:0|UVIN-1008-UVOUT;n:type:ShaderForge.SFN_ScreenPos,id:7819,x:30314,y:32386,varname:node_7819,prsc:2,sctp:0;n:type:ShaderForge.SFN_Parallax,id:1008,x:30964,y:32333,varname:node_1008,prsc:2|UVIN-6082-OUT,HEI-4262-OUT;n:type:ShaderForge.SFN_Vector1,id:4262,x:30747,y:32397,varname:node_4262,prsc:2,v1:1;n:type:ShaderForge.SFN_Vector1,id:8695,x:30395,y:32581,varname:node_8695,prsc:2,v1:0.1;n:type:ShaderForge.SFN_OneMinus,id:6082,x:30761,y:32463,varname:node_6082,prsc:2|IN-166-UVOUT;n:type:ShaderForge.SFN_Rotator,id:166,x:30566,y:32439,varname:node_166,prsc:2|UVIN-7819-UVOUT,SPD-8695-OUT;proporder:9323-1812;pass:END;sub:END;*/

Shader "Custom/railway0.1" {
    Properties {
        _node_9323 ("node_9323", Color) = (0.2313726,0.9803922,0.7215686,1)
        _node_1812 ("node_1812", Color) = (0,0,0.07450981,1)
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform float4 _TimeEditor;
            uniform float4 _node_9323;
            uniform float4 _node_1812;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 posWorld : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
                float3 tangentDir : TEXCOORD2;
                float3 bitangentDir : TEXCOORD3;
                float4 screenPos : TEXCOORD4;
                UNITY_FOG_COORDS(5)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                o.screenPos = o.pos;
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                i.screenPos = float4( i.screenPos.xy / i.screenPos.w, 0, 0 );
                i.screenPos.y *= _ProjectionParams.x;
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = 1;
                float3 attenColor = attenuation * _LightColor0.xyz;
///////// Gloss:
                float gloss = 0.5;
                float specPow = exp2( gloss * 10.0+1.0);
////// Specular:
                float NdotL = max(0, dot( normalDirection, lightDirection ));
                float node_2132 = 1.0;
                float3 specularColor = float3(node_2132,node_2132,node_2132);
                float3 directSpecular = (floor(attenuation) * _LightColor0.xyz) * pow(max(0,dot(halfDirection,normalDirection)),specPow)*specularColor;
                float3 specular = directSpecular;
/////// Diffuse:
                NdotL = dot( normalDirection, lightDirection );
                float4 node_8930 = _Time + _TimeEditor;
                float node_166_ang = node_8930.g;
                float node_166_spd = 0.1;
                float node_166_cos = cos(node_166_spd*node_166_ang);
                float node_166_sin = sin(node_166_spd*node_166_ang);
                float2 node_166_piv = float2(0.5,0.5);
                float2 node_166 = (mul(i.screenPos.rg-node_166_piv,float2x2( node_166_cos, -node_166_sin, node_166_sin, node_166_cos))+node_166_piv);
                float node_3150 = pow((abs((frac(((0.05*(1.0 - 0.5)*mul(tangentTransform, viewDirection).xy + (1.0 - node_166)).rg+node_8930.g*float2(0.25,0)).r)-0.5))*1.5),2.5);
                float3 w = float3(node_3150,node_3150,node_3150)*0.5; // Light wrapping
                float3 NdotLWrap = NdotL * ( 1.0 - w );
                float3 forwardLight = max(float3(0.0,0.0,0.0), NdotLWrap + w );
                float3 backLight = max(float3(0.0,0.0,0.0), -NdotLWrap + w ) * float3(node_3150,node_3150,node_3150);
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = (forwardLight+backLight) * attenColor;
                float3 indirectDiffuse = float3(0,0,0);
                indirectDiffuse += UNITY_LIGHTMODEL_AMBIENT.rgb; // Ambient Light
                float node_7756 = 1.0;
                float3 diffuseColor = lerp(_node_1812.rgb,float3(node_7756,node_7756,node_7756),node_3150);
                float3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
////// Emissive:
                float3 emissive = (_node_9323.rgb*10.0*node_3150);
/// Final Color:
                float3 finalColor = diffuse + specular + emissive;
                fixed4 finalRGBA = fixed4(finalColor,node_3150);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdadd
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform float4 _TimeEditor;
            uniform float4 _node_9323;
            uniform float4 _node_1812;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 posWorld : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
                float3 tangentDir : TEXCOORD2;
                float3 bitangentDir : TEXCOORD3;
                float4 screenPos : TEXCOORD4;
                LIGHTING_COORDS(5,6)
                UNITY_FOG_COORDS(7)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                o.screenPos = o.pos;
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                i.screenPos = float4( i.screenPos.xy / i.screenPos.w, 0, 0 );
                i.screenPos.y *= _ProjectionParams.x;
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
///////// Gloss:
                float gloss = 0.5;
                float specPow = exp2( gloss * 10.0+1.0);
////// Specular:
                float NdotL = max(0, dot( normalDirection, lightDirection ));
                float node_2132 = 1.0;
                float3 specularColor = float3(node_2132,node_2132,node_2132);
                float3 directSpecular = attenColor * pow(max(0,dot(halfDirection,normalDirection)),specPow)*specularColor;
                float3 specular = directSpecular;
/////// Diffuse:
                NdotL = dot( normalDirection, lightDirection );
                float4 node_2054 = _Time + _TimeEditor;
                float node_166_ang = node_2054.g;
                float node_166_spd = 0.1;
                float node_166_cos = cos(node_166_spd*node_166_ang);
                float node_166_sin = sin(node_166_spd*node_166_ang);
                float2 node_166_piv = float2(0.5,0.5);
                float2 node_166 = (mul(i.screenPos.rg-node_166_piv,float2x2( node_166_cos, -node_166_sin, node_166_sin, node_166_cos))+node_166_piv);
                float node_3150 = pow((abs((frac(((0.05*(1.0 - 0.5)*mul(tangentTransform, viewDirection).xy + (1.0 - node_166)).rg+node_2054.g*float2(0.25,0)).r)-0.5))*1.5),2.5);
                float3 w = float3(node_3150,node_3150,node_3150)*0.5; // Light wrapping
                float3 NdotLWrap = NdotL * ( 1.0 - w );
                float3 forwardLight = max(float3(0.0,0.0,0.0), NdotLWrap + w );
                float3 backLight = max(float3(0.0,0.0,0.0), -NdotLWrap + w ) * float3(node_3150,node_3150,node_3150);
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = (forwardLight+backLight) * attenColor;
                float node_7756 = 1.0;
                float3 diffuseColor = lerp(_node_1812.rgb,float3(node_7756,node_7756,node_7756),node_3150);
                float3 diffuse = directDiffuse * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse + specular;
                fixed4 finalRGBA = fixed4(finalColor * node_3150,0);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
