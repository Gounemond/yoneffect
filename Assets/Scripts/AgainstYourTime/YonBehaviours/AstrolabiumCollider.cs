﻿using UnityEngine;
using System;
using System.Collections;

[RequireComponent (typeof(BoxCollider))]
public class AstrolabiumCollider : MonoBehaviour
{

    public event Action onTriggerEnter;              // Called when a Tangram Object enters the collider
    public event Action onTriggerExit;               // Called when a Tangram Object enters the collider
    public event Action elementActivated;

    public AudioSource roomAudio;

    private int objectsInCollider = 0;

    // Use this for initialization
    void Awake ()
    {

    }

    // Update is called once per frame
    void Update ()
    {
        if (objectsInCollider > 0)
        {
            elementActivated();
        }
	
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.tag =="Player" || other.tag == "ClonePlayer")
        {
            if (objectsInCollider == 0)
            {
                roomAudio.Play();
                if (onTriggerEnter != null)
                {

                    onTriggerEnter();
                }
            }
            objectsInCollider++;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player" || other.tag == "ClonePlayer")
        {
            if (objectsInCollider == 1)
            {
                roomAudio.Stop();
                if (onTriggerExit != null)
                {
                    onTriggerExit();
                }
                objectsInCollider--;
            }
        }
    }
}
