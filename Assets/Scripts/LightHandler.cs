using UnityEngine;
using System.Collections;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System;
using System.Linq;

public class LightHandler: YonBehaviour {
	
	public float speed = 0.1f;
	public float intensity = 0;
	public Light luce;
	
	private bool active = false;
	private bool moving = false;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

	}
	
	override public void Activate()
	{
		StartCoroutine (lettherebelight());
	}
	
	override public void deActivate()
	{
		StartCoroutine (lettherebedark());
	}
	
	private IEnumerator lettherebelight()
	{
		while (luce.intensity < intensity)
		{
			luce.intensity += speed;
			yield return 0;
		}
	}
	private IEnumerator lettherebedark()
	{
		while (luce.intensity >0)
		{
			luce.intensity -= speed;
			yield return 0;
		}
	}
}
