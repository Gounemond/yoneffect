// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:3245,x:32719,y:32712,varname:node_3245,prsc:2|emission-1026-OUT,olwid-6551-OUT,olcol-4663-RGB;n:type:ShaderForge.SFN_Lerp,id:1026,x:32458,y:32837,varname:node_1026,prsc:2|A-9726-RGB,B-992-OUT,T-3727-OUT;n:type:ShaderForge.SFN_Multiply,id:992,x:32249,y:32854,varname:node_992,prsc:2|A-9050-RGB,B-5034-OUT;n:type:ShaderForge.SFN_Vector1,id:5034,x:32076,y:32969,varname:node_5034,prsc:2,v1:2;n:type:ShaderForge.SFN_Color,id:9050,x:32058,y:32765,ptovrint:False,ptlb:color,ptin:_color,varname:node_9050,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0.9724138,c3:0,c4:1;n:type:ShaderForge.SFN_Color,id:9726,x:32321,y:32622,ptovrint:False,ptlb:base,ptin:_base,varname:node_9726,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0,c3:0.07450981,c4:1;n:type:ShaderForge.SFN_OneMinus,id:3727,x:32344,y:33054,varname:node_3727,prsc:2|IN-5760-OUT;n:type:ShaderForge.SFN_Fresnel,id:5760,x:32132,y:33056,varname:node_5760,prsc:2|NRM-5552-OUT,EXP-6071-OUT;n:type:ShaderForge.SFN_NormalVector,id:5552,x:31893,y:32995,prsc:2,pt:False;n:type:ShaderForge.SFN_Vector1,id:6071,x:32002,y:33161,varname:node_6071,prsc:2,v1:2;n:type:ShaderForge.SFN_Multiply,id:6551,x:32533,y:33066,varname:node_6551,prsc:2|A-9833-OUT,B-7175-OUT;n:type:ShaderForge.SFN_Slider,id:7175,x:32213,y:33264,ptovrint:False,ptlb:frecce2,ptin:_frecce2,varname:node_7175,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Vector1,id:9833,x:32292,y:33183,varname:node_9833,prsc:2,v1:0.1;n:type:ShaderForge.SFN_Color,id:4663,x:32533,y:33287,ptovrint:False,ptlb:outline,ptin:_outline,varname:node_4663,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;proporder:9050-9726-7175-4663;pass:END;sub:END;*/

Shader "Custom/astrofrecce2" {
    Properties {
        _color ("color", Color) = (1,0.9724138,0,1)
        _base ("base", Color) = (0,0,0.07450981,1)
        _frecce2 ("frecce2", Range(0, 1)) = 1
        _outline ("outline", Color) = (1,1,1,1)
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "Outline"
            Tags {
            }
            Cull Front
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float _frecce2;
            uniform float4 _outline;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                UNITY_FOG_COORDS(0)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.pos = mul(UNITY_MATRIX_MVP, float4(v.vertex.xyz + v.normal*(0.1*_frecce2),1) );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                return fixed4(_outline.rgb,0);
            }
            ENDCG
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _color;
            uniform float4 _base;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 posWorld : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
                UNITY_FOG_COORDS(2)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float3 emissive = lerp(_base.rgb,(_color.rgb*2.0),(1.0 - pow(1.0-max(0,dot(i.normalDir, viewDirection)),2.0)));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
