// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:872,x:33446,y:32473,varname:node_872,prsc:2|emission-7583-OUT,alpha-1083-OUT;n:type:ShaderForge.SFN_Color,id:9032,x:32674,y:32389,ptovrint:False,ptlb:wireframe,ptin:_wireframe,varname:node_9032,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.1172414,c2:1,c3:0,c4:1;n:type:ShaderForge.SFN_Multiply,id:6410,x:32809,y:32808,varname:node_6410,prsc:2|A-2859-OUT,B-5058-OUT,C-93-OUT,D-8877-OUT;n:type:ShaderForge.SFN_Blend,id:2859,x:32368,y:32691,varname:node_2859,prsc:2,blmd:16,clmp:True|SRC-9601-OUT,DST-474-OUT;n:type:ShaderForge.SFN_Blend,id:8877,x:32382,y:32851,varname:node_8877,prsc:2,blmd:16,clmp:True|SRC-9601-OUT,DST-6589-OUT;n:type:ShaderForge.SFN_Blend,id:5058,x:32380,y:32492,varname:node_5058,prsc:2,blmd:16,clmp:True|SRC-6181-OUT,DST-6643-OUT;n:type:ShaderForge.SFN_Blend,id:93,x:32392,y:33026,varname:node_93,prsc:2,blmd:16,clmp:True|SRC-6181-OUT,DST-3947-OUT;n:type:ShaderForge.SFN_OneMinus,id:6643,x:32133,y:32522,varname:node_6643,prsc:2|IN-474-OUT;n:type:ShaderForge.SFN_TexCoord,id:2580,x:31639,y:32714,varname:node_2580,prsc:2,uv:0;n:type:ShaderForge.SFN_ComponentMask,id:474,x:31865,y:32638,varname:node_474,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-2580-UVOUT;n:type:ShaderForge.SFN_ComponentMask,id:6589,x:31876,y:32880,varname:node_6589,prsc:2,cc1:1,cc2:-1,cc3:-1,cc4:-1|IN-2580-UVOUT;n:type:ShaderForge.SFN_OneMinus,id:3947,x:32110,y:33005,varname:node_3947,prsc:2|IN-6589-OUT;n:type:ShaderForge.SFN_Vector1,id:9601,x:32093,y:32802,varname:node_9601,prsc:2,v1:0.98;n:type:ShaderForge.SFN_Lerp,id:7583,x:33042,y:32603,varname:node_7583,prsc:2|A-9032-RGB,B-9317-RGB,T-6410-OUT;n:type:ShaderForge.SFN_Color,id:9317,x:32705,y:32601,ptovrint:False,ptlb:basecolor,ptin:_basecolor,varname:node_9317,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0,c3:0.07843138,c4:1;n:type:ShaderForge.SFN_Vector1,id:6181,x:32080,y:32719,varname:node_6181,prsc:2,v1:0.97;n:type:ShaderForge.SFN_Vector1,id:4766,x:32916,y:32905,varname:node_4766,prsc:2,v1:1;n:type:ShaderForge.SFN_Slider,id:9825,x:32809,y:33038,ptovrint:False,ptlb:opacityvalue,ptin:_opacityvalue,varname:node_9825,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.3162391,max:1;n:type:ShaderForge.SFN_Multiply,id:1083,x:33133,y:32918,varname:node_1083,prsc:2|A-4766-OUT,B-9825-OUT;proporder:9032-9317-9825;pass:END;sub:END;*/

Shader "Custom/timemachine_Scuro" {
    Properties {
        _wireframe ("wireframe", Color) = (0.1172414,1,0,1)
        _basecolor ("basecolor", Color) = (0,0,0.07843138,1)
        _opacityvalue ("opacityvalue", Range(0, 1)) = 0.3162391
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _wireframe;
            uniform float4 _basecolor;
            uniform float _opacityvalue;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                UNITY_FOG_COORDS(1)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
////// Lighting:
////// Emissive:
                float node_9601 = 0.98;
                float node_474 = i.uv0.r;
                float node_6181 = 0.97;
                float node_6589 = i.uv0.g;
                float3 emissive = lerp(_wireframe.rgb,_basecolor.rgb,(saturate(round( 0.5*(node_9601 + node_474)))*saturate(round( 0.5*(node_6181 + (1.0 - node_474))))*saturate(round( 0.5*(node_6181 + (1.0 - node_6589))))*saturate(round( 0.5*(node_9601 + node_6589)))));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,(1.0*_opacityvalue));
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
