﻿using UnityEngine;
using System.Collections;

public class TimeClockText : MonoBehaviour {


    private YonEffectJRController gamestate;

    private float timeReversed;
    // Use this for initialization
    void Start ()
    {
        gamestate = FindObjectOfType<YonEffectJRController>().GetComponent<YonEffectJRController>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        timeReversed = gamestate.timeBetweenJumps - gamestate.worldTime;
        GetComponent<TextMesh>().text = timeReversed.ToString("F1");
	
	}
}
