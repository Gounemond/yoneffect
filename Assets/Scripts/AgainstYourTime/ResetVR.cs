﻿using UnityEngine;
using System.Collections;
using Valve.VR;
using UnityEngine.VR;


public class ResetVR : MonoBehaviour {
    
    public Transform cameraVive;
    public Transform parentVive;

    private static bool isFirstTime = false;

    public ETrackingUniverseOrigin trackingSpace = ETrackingUniverseOrigin.TrackingUniverseSeated;

    // Use this for initialization
    IEnumerator Start ()
    {
        if (VRSettings.loadedDeviceName == "OpenVR")
        {
            OpenVR.Compositor.SetTrackingSpace(trackingSpace);
            yield return new WaitForEndOfFrame();
            if (!ResetVR.isFirstTime)
            {
                yield return new WaitForSeconds(2);
                ResetVR.isFirstTime = true;
            }
            Vector3 idealPosition = new Vector3(0f, 0f, -0.05f);
            if (OpenVR.IsHmdPresent())
            {
                SteamVR.instance.hmd.ResetSeatedZeroPose();

                /*while (cameraVive.localPosition == idealPosition)
                 yield return 0;*/
                parentVive.localPosition = parentVive.localPosition - idealPosition - cameraVive.localPosition;
                //parentVive.localPosition = parentVive.localPosition - cameraVive.localPosition;
                SteamVR.instance.hmd.ResetSeatedZeroPose();
            }



        }

        
    }

    void Update () {
        if (Input.GetButtonDown("ResetOrientation"))
        {
            if (VRSettings.loadedDeviceName == "OpenVR")
            {
                if (OpenVR.IsHmdPresent())
                {
                    SteamVR.instance.hmd.ResetSeatedZeroPose();
                }

            }
        }
	}
}
