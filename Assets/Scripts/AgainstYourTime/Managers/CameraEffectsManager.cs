using System.Collections;
using UnityEngine;
using UnityStandardAssets.ImageEffects;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using VRStandardAssets.Common;
using VRStandardAssets.Utils;
using VRStandardAssets.ShootingGallery;

public class CameraEffectsManager : MonoBehaviour
{
    // Singleton implementation
    protected static CameraEffectsManager _self;
    public static CameraEffectsManager Self
    {
        get
        {
            if (_self == null)
                _self = FindObjectOfType(typeof(CameraEffectsManager)) as CameraEffectsManager;
            return _self;
        }
    }

    //public VRCameraFade cameraFader;
    public Camera cameraWithEffects;
    public NoiseAndScratches noiseEffect;
    public BlurOptimized blurEffect;

    void Awake()
    {
        blurEffect = cameraWithEffects.GetComponent<BlurOptimized>();
        noiseEffect = cameraWithEffects.GetComponent<NoiseAndScratches>();
    }

    void Start()
    {
        blurEffect.enabled = false;
        noiseEffect.enabled = false;
    }

    public IEnumerator ClearNoiseGradually()
    {
        float amount = noiseEffect.grainIntensityMin;

        while (amount >= 0)
        {
            noiseEffect.grainIntensityMin -= Time.deltaTime * 3;
            amount -= Time.deltaTime * 3;
            yield return null;
        }

        noiseEffect.grainIntensityMin = 0;
    }


    public IEnumerator StartBlurEffect(float time = 1f)
    {
        float timeElapsed = 0f;

        blurEffect.enabled = true;

        while (timeElapsed < time)
        {
            timeElapsed += Time.deltaTime;

            // I choose 7 because that's the maximum amount of blur I want to achieve
            blurEffect.blurSize += Time.deltaTime* 5 / (time);
            //blurEffect.blurIterations = (int)(blurEffect.blurSize * 4 / 7);

            yield return null;
        }

        blurEffect.blurSize = 5;
        //blurEffect.blurIterations = 4;
    }

    public IEnumerator EndBlurEffect(float time = 1f)
    {
        float timeElapsed = 0f;

        while (timeElapsed < time)
        {
            timeElapsed += Time.deltaTime;

            // I choose 7 because that's the maximum amount of blur I want to achieve
            blurEffect.blurSize -= Time.deltaTime* 5 / (time);
            //blurEffect.blurIterations = (int)(blurEffect.blurSize * 4 / 7);

            yield return null;
        }

        blurEffect.blurSize = 0;
        //blurEffect.blurIterations = 1;

        blurEffect.enabled = false;
    }

}