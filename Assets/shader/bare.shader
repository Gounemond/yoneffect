// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:8232,x:33214,y:32721,varname:node_8232,prsc:2|emission-930-OUT;n:type:ShaderForge.SFN_Color,id:3979,x:32575,y:32491,ptovrint:False,ptlb:glow,ptin:_glow,varname:node_3979,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.7647059,c2:0.7647059,c3:0.7647059,c4:1;n:type:ShaderForge.SFN_Lerp,id:9993,x:32651,y:32690,varname:node_9993,prsc:2|A-3979-RGB,B-8784-OUT,T-9756-OUT;n:type:ShaderForge.SFN_Multiply,id:8784,x:32422,y:32680,varname:node_8784,prsc:2|A-5474-OUT,B-5221-OUT;n:type:ShaderForge.SFN_Vector1,id:5221,x:32137,y:32765,varname:node_5221,prsc:2,v1:2;n:type:ShaderForge.SFN_Color,id:946,x:32055,y:32418,ptovrint:False,ptlb:Main,ptin:_Main,varname:node_946,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0,c3:0.8068962,c4:1;n:type:ShaderForge.SFN_OneMinus,id:9756,x:32220,y:32870,varname:node_9756,prsc:2|IN-4300-OUT;n:type:ShaderForge.SFN_Fresnel,id:4300,x:32036,y:32892,varname:node_4300,prsc:2|NRM-5274-OUT,EXP-3852-OUT;n:type:ShaderForge.SFN_NormalVector,id:5274,x:31838,y:32793,prsc:2,pt:False;n:type:ShaderForge.SFN_Vector1,id:3852,x:31815,y:32979,varname:node_3852,prsc:2,v1:2;n:type:ShaderForge.SFN_Time,id:5425,x:32001,y:33010,varname:node_5425,prsc:2;n:type:ShaderForge.SFN_Sin,id:7395,x:32200,y:33010,varname:node_7395,prsc:2|IN-5425-T;n:type:ShaderForge.SFN_Multiply,id:930,x:32967,y:32774,varname:node_930,prsc:2|A-9993-OUT,B-1319-OUT;n:type:ShaderForge.SFN_Clamp,id:1319,x:32432,y:32997,varname:node_1319,prsc:2|IN-7395-OUT,MIN-23-OUT,MAX-7588-OUT;n:type:ShaderForge.SFN_Vector1,id:23,x:32171,y:33167,varname:node_23,prsc:2,v1:0.5;n:type:ShaderForge.SFN_Vector1,id:7588,x:32171,y:33238,varname:node_7588,prsc:2,v1:10;n:type:ShaderForge.SFN_Multiply,id:5474,x:32325,y:32470,varname:node_5474,prsc:2|A-946-RGB,B-5405-OUT;n:type:ShaderForge.SFN_Multiply,id:5405,x:32185,y:32616,varname:node_5405,prsc:2|A-1319-OUT,B-8018-OUT;n:type:ShaderForge.SFN_Vector1,id:8018,x:32036,y:32673,varname:node_8018,prsc:2,v1:1.5;proporder:3979-946;pass:END;sub:END;*/

Shader "Custom/bare" {
    Properties {
        _glow ("glow", Color) = (0.7647059,0.7647059,0.7647059,1)
        _Main ("Main", Color) = (1,0,0.8068962,1)
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform float4 _glow;
            uniform float4 _Main;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 posWorld : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
                UNITY_FOG_COORDS(2)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float4 node_5425 = _Time + _TimeEditor;
                float node_1319 = clamp(sin(node_5425.g),0.5,10.0);
                float3 emissive = (lerp(_glow.rgb,((_Main.rgb*(node_1319*1.5))*2.0),(1.0 - pow(1.0-max(0,dot(i.normalDir, viewDirection)),2.0)))*node_1319);
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
