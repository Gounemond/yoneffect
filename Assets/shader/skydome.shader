// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:2,rntp:3,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:769,x:35198,y:32616,varname:node_769,prsc:2|emission-9863-RGB,clip-6962-OUT;n:type:ShaderForge.SFN_Tex2dAsset,id:9469,x:34238,y:32501,ptovrint:False,ptlb:gradient,ptin:_gradient,varname:node_9469,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:f93e75cbf3415fb448054077b7023c87,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:9863,x:34545,y:32393,varname:node_9863,prsc:2,tex:f93e75cbf3415fb448054077b7023c87,ntxv:0,isnm:False|UVIN-8104-OUT,TEX-9469-TEX;n:type:ShaderForge.SFN_Append,id:8104,x:34174,y:32310,varname:node_8104,prsc:2|A-9546-OUT,B-6461-OUT;n:type:ShaderForge.SFN_Vector1,id:6461,x:33991,y:32451,varname:node_6461,prsc:2,v1:0;n:type:ShaderForge.SFN_Tex2d,id:4625,x:33162,y:33320,ptovrint:False,ptlb:noise,ptin:_noise,varname:node_4625,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:28c7aad1372ff114b90d330f8a2dd938,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Add,id:9393,x:33423,y:32803,varname:node_9393,prsc:2|A-316-OUT,B-4625-RGB;n:type:ShaderForge.SFN_RemapRange,id:316,x:33151,y:32777,varname:node_316,prsc:2,frmn:0,frmx:1,tomn:-0.1,tomx:0.1|IN-1775-OUT;n:type:ShaderForge.SFN_OneMinus,id:1775,x:32927,y:32777,varname:node_1775,prsc:2|IN-4022-OUT;n:type:ShaderForge.SFN_RemapRange,id:385,x:33665,y:32580,varname:node_385,prsc:2,frmn:0,frmx:1,tomn:-6,tomx:6|IN-9393-OUT;n:type:ShaderForge.SFN_Clamp01,id:1778,x:33655,y:32357,varname:node_1778,prsc:2|IN-385-OUT;n:type:ShaderForge.SFN_OneMinus,id:9546,x:33896,y:32290,varname:node_9546,prsc:2|IN-1778-OUT;n:type:ShaderForge.SFN_Tex2d,id:3795,x:34183,y:33303,ptovrint:False,ptlb:cable,ptin:_cable,varname:node_3795,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:c59fdc20aeaf2824d8e9b3d6a1c5aec7,ntxv:0,isnm:False|UVIN-1687-OUT;n:type:ShaderForge.SFN_Multiply,id:3027,x:34280,y:32964,varname:node_3027,prsc:2|A-9393-OUT,B-3795-RGB;n:type:ShaderForge.SFN_ComponentMask,id:6962,x:34841,y:32931,varname:node_6962,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-3027-OUT;n:type:ShaderForge.SFN_TexCoord,id:3922,x:33394,y:33331,varname:node_3922,prsc:2,uv:0;n:type:ShaderForge.SFN_Multiply,id:1687,x:33651,y:33260,varname:node_1687,prsc:2|A-3922-UVOUT,B-4534-OUT;n:type:ShaderForge.SFN_Vector1,id:4534,x:33382,y:33494,varname:node_4534,prsc:2,v1:20;n:type:ShaderForge.SFN_Time,id:9931,x:32109,y:32828,varname:node_9931,prsc:2;n:type:ShaderForge.SFN_Multiply,id:4521,x:32421,y:32913,varname:node_4521,prsc:2|A-9931-T,B-731-OUT;n:type:ShaderForge.SFN_Sin,id:5611,x:32594,y:33006,varname:node_5611,prsc:2|IN-4521-OUT;n:type:ShaderForge.SFN_Slider,id:291,x:32405,y:32802,ptovrint:False,ptlb:DoNoTouchPLZ,ptin:_DoNoTouchPLZ,varname:node_291,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-10,cur:-3,max:10;n:type:ShaderForge.SFN_Multiply,id:4022,x:32755,y:32926,varname:node_4022,prsc:2|A-291-OUT,B-5611-OUT;n:type:ShaderForge.SFN_Slider,id:731,x:32035,y:33144,ptovrint:False,ptlb:time,ptin:_time,varname:node_731,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.3,max:1;proporder:9469-4625-3795-291-731;pass:END;sub:END;*/

Shader "Custom/skydome" {
    Properties {
        _gradient ("gradient", 2D) = "white" {}
        _noise ("noise", 2D) = "white" {}
        _cable ("cable", 2D) = "white" {}
        _DoNoTouchPLZ ("DoNoTouchPLZ", Range(-10, 10)) = -3
        _time ("time", Range(0, 1)) = 0.3
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "Queue"="AlphaTest"
            "RenderType"="TransparentCutout"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform sampler2D _gradient; uniform float4 _gradient_ST;
            uniform sampler2D _noise; uniform float4 _noise_ST;
            uniform sampler2D _cable; uniform float4 _cable_ST;
            uniform float _DoNoTouchPLZ;
            uniform float _time;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                UNITY_FOG_COORDS(1)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float4 node_9931 = _Time + _TimeEditor;
                float4 _noise_var = tex2D(_noise,TRANSFORM_TEX(i.uv0, _noise));
                float3 node_9393 = (((1.0 - (_DoNoTouchPLZ*sin((node_9931.g*_time))))*0.2+-0.1)+_noise_var.rgb);
                float2 node_1687 = (i.uv0*20.0);
                float4 _cable_var = tex2D(_cable,TRANSFORM_TEX(node_1687, _cable));
                clip((node_9393*_cable_var.rgb).r - 0.5);
////// Lighting:
////// Emissive:
                float4 node_8104 = float4((1.0 - saturate((node_9393*12.0+-6.0))),0.0);
                float4 node_9863 = tex2D(_gradient,TRANSFORM_TEX(node_8104, _gradient));
                float3 emissive = node_9863.rgb;
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform sampler2D _noise; uniform float4 _noise_ST;
            uniform sampler2D _cable; uniform float4 _cable_ST;
            uniform float _DoNoTouchPLZ;
            uniform float _time;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float4 node_9931 = _Time + _TimeEditor;
                float4 _noise_var = tex2D(_noise,TRANSFORM_TEX(i.uv0, _noise));
                float3 node_9393 = (((1.0 - (_DoNoTouchPLZ*sin((node_9931.g*_time))))*0.2+-0.1)+_noise_var.rgb);
                float2 node_1687 = (i.uv0*20.0);
                float4 _cable_var = tex2D(_cable,TRANSFORM_TEX(node_1687, _cable));
                clip((node_9393*_cable_var.rgb).r - 0.5);
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
