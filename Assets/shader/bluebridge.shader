// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:2,rntp:3,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:9213,x:35581,y:33536,varname:node_9213,prsc:2|emission-3309-OUT,clip-6556-OUT;n:type:ShaderForge.SFN_Tex2d,id:7721,x:31827,y:32489,ptovrint:False,ptlb:node_7721,ptin:_node_7721,varname:node_7721,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:c59fdc20aeaf2824d8e9b3d6a1c5aec7,ntxv:0,isnm:False;n:type:ShaderForge.SFN_OneMinus,id:8674,x:32528,y:32944,varname:node_8674,prsc:2|IN-7721-A;n:type:ShaderForge.SFN_Multiply,id:4349,x:32665,y:32741,varname:node_4349,prsc:2|A-7721-B,B-4171-OUT;n:type:ShaderForge.SFN_Posterize,id:8711,x:32911,y:32854,varname:node_8711,prsc:2|IN-4349-OUT,STPS-8268-OUT;n:type:ShaderForge.SFN_Vector1,id:8268,x:32665,y:32902,varname:node_8268,prsc:2,v1:2;n:type:ShaderForge.SFN_Color,id:9975,x:32929,y:32642,ptovrint:False,ptlb:node_9975,ptin:_node_9975,varname:node_9975,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0,c3:1,c4:1;n:type:ShaderForge.SFN_Multiply,id:4383,x:33166,y:32769,varname:node_4383,prsc:2|A-9975-RGB,B-8711-OUT;n:type:ShaderForge.SFN_Power,id:2715,x:32621,y:33421,varname:node_2715,prsc:2|VAL-1956-OUT,EXP-2184-OUT;n:type:ShaderForge.SFN_Multiply,id:1956,x:32390,y:33373,varname:node_1956,prsc:2|A-8152-OUT,B-2440-OUT;n:type:ShaderForge.SFN_Panner,id:9947,x:31501,y:33129,varname:node_9947,prsc:2,spu:1,spv:1|UVIN-3930-OUT,DIST-3242-OUT;n:type:ShaderForge.SFN_TexCoord,id:1760,x:31113,y:32977,varname:node_1760,prsc:2,uv:0;n:type:ShaderForge.SFN_Vector1,id:2440,x:32027,y:33658,varname:node_2440,prsc:2,v1:1.3;n:type:ShaderForge.SFN_Vector1,id:2184,x:32390,y:33535,varname:node_2184,prsc:2,v1:15;n:type:ShaderForge.SFN_Subtract,id:3723,x:32064,y:33186,varname:node_3723,prsc:2|A-9000-OUT,B-4468-OUT;n:type:ShaderForge.SFN_Abs,id:8152,x:32236,y:33266,varname:node_8152,prsc:2|IN-3723-OUT;n:type:ShaderForge.SFN_Frac,id:354,x:31780,y:33255,varname:node_354,prsc:2|IN-7124-OUT;n:type:ShaderForge.SFN_ComponentMask,id:7124,x:31678,y:33058,varname:node_7124,prsc:2,cc1:1,cc2:-1,cc3:-1,cc4:-1|IN-9947-UVOUT;n:type:ShaderForge.SFN_Frac,id:9000,x:31780,y:33403,varname:node_9000,prsc:2|IN-354-OUT;n:type:ShaderForge.SFN_Vector1,id:4468,x:31955,y:33403,varname:node_4468,prsc:2,v1:0.15;n:type:ShaderForge.SFN_Time,id:4491,x:31232,y:33305,varname:node_4491,prsc:2;n:type:ShaderForge.SFN_Multiply,id:3242,x:31492,y:33448,varname:node_3242,prsc:2|A-4491-T,B-6518-OUT;n:type:ShaderForge.SFN_Slider,id:6518,x:31153,y:33520,ptovrint:False,ptlb:timer,ptin:_timer,varname:node_9789,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-1,cur:-0.1,max:1;n:type:ShaderForge.SFN_Multiply,id:3930,x:31320,y:33007,varname:node_3930,prsc:2|A-1760-UVOUT,B-7575-OUT;n:type:ShaderForge.SFN_ValueProperty,id:7575,x:30982,y:33179,ptovrint:False,ptlb:number,ptin:_number,varname:node_9564,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Lerp,id:4914,x:32969,y:33295,varname:node_4914,prsc:2|A-7721-R,B-234-RGB,T-2715-OUT;n:type:ShaderForge.SFN_Color,id:234,x:32576,y:33223,ptovrint:False,ptlb:node_234,ptin:_node_234,varname:node_234,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0,c3:0,c4:1;n:type:ShaderForge.SFN_OneMinus,id:4171,x:32315,y:32624,varname:node_4171,prsc:2|IN-7721-G;n:type:ShaderForge.SFN_Lerp,id:285,x:33363,y:33100,varname:node_285,prsc:2|A-4171-OUT,B-8674-OUT,T-4914-OUT;n:type:ShaderForge.SFN_ComponentMask,id:6556,x:34574,y:33573,varname:node_6556,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-5273-OUT;n:type:ShaderForge.SFN_Posterize,id:5010,x:34016,y:33347,varname:node_5010,prsc:2|IN-285-OUT,STPS-9018-OUT;n:type:ShaderForge.SFN_Vector1,id:9018,x:33559,y:33359,varname:node_9018,prsc:2,v1:2;n:type:ShaderForge.SFN_Multiply,id:2345,x:33171,y:32506,varname:node_2345,prsc:2|A-5685-RGB,B-4236-OUT;n:type:ShaderForge.SFN_Color,id:5685,x:32929,y:32392,ptovrint:False,ptlb:node_5685,ptin:_node_5685,varname:node_5685,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0.4620686,c3:1,c4:1;n:type:ShaderForge.SFN_Blend,id:3309,x:33635,y:32553,varname:node_3309,prsc:2,blmd:6,clmp:True|SRC-2345-OUT,DST-361-OUT;n:type:ShaderForge.SFN_Color,id:450,x:33057,y:32952,ptovrint:False,ptlb:node_450,ptin:_node_450,varname:node_450,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0,c3:0,c4:1;n:type:ShaderForge.SFN_Multiply,id:7689,x:32996,y:33166,varname:node_7689,prsc:2|A-450-RGB,B-2715-OUT;n:type:ShaderForge.SFN_Lerp,id:361,x:33582,y:32889,varname:node_361,prsc:2|A-7689-OUT,B-4383-OUT,T-8711-OUT;n:type:ShaderForge.SFN_Add,id:2139,x:31869,y:33015,varname:node_2139,prsc:2|A-2724-OUT,B-7124-OUT;n:type:ShaderForge.SFN_ComponentMask,id:2724,x:31657,y:32918,varname:node_2724,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-9947-UVOUT;n:type:ShaderForge.SFN_Multiply,id:6983,x:34149,y:33650,varname:node_6983,prsc:2|A-1086-RGB,B-5717-OUT;n:type:ShaderForge.SFN_Vector1,id:5717,x:33556,y:34079,varname:node_5717,prsc:2,v1:1.4;n:type:ShaderForge.SFN_Vector1,id:2485,x:34244,y:33182,varname:node_2485,prsc:2,v1:1;n:type:ShaderForge.SFN_Multiply,id:6836,x:33016,y:33915,varname:node_6836,prsc:2|A-3925-UVOUT,B-1538-OUT;n:type:ShaderForge.SFN_TexCoord,id:3925,x:32605,y:33577,varname:node_3925,prsc:2,uv:0;n:type:ShaderForge.SFN_ValueProperty,id:1538,x:32859,y:34016,ptovrint:False,ptlb:titling,ptin:_titling,varname:node_7795,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Append,id:6497,x:32800,y:33746,varname:node_6497,prsc:2|A-9623-OUT,B-4437-OUT;n:type:ShaderForge.SFN_ValueProperty,id:9623,x:32595,y:33803,ptovrint:False,ptlb:U,ptin:_U,varname:node_8977,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_ValueProperty,id:4437,x:32583,y:33934,ptovrint:False,ptlb:V,ptin:_V,varname:node_8525,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.4;n:type:ShaderForge.SFN_Add,id:2760,x:33228,y:33710,varname:node_2760,prsc:2|A-3925-UVOUT,B-4566-OUT;n:type:ShaderForge.SFN_Multiply,id:4566,x:33037,y:33756,varname:node_4566,prsc:2|A-6497-OUT,B-6836-OUT;n:type:ShaderForge.SFN_Tex2d,id:1086,x:33596,y:33650,ptovrint:False,ptlb:node_1398,ptin:_node_1398,varname:node_1398,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:c59fdc20aeaf2824d8e9b3d6a1c5aec7,ntxv:0,isnm:False|UVIN-2760-OUT;n:type:ShaderForge.SFN_Add,id:5273,x:34410,y:33573,varname:node_5273,prsc:2|A-6983-OUT,B-5010-OUT;n:type:ShaderForge.SFN_OneMinus,id:4236,x:32680,y:32547,varname:node_4236,prsc:2|IN-7721-R;proporder:7721-9975-6518-7575-234-5685-450-1538-9623-4437-1086-3644-290;pass:END;sub:END;*/

Shader "Custom/bluebridge" {
    Properties {
        _node_7721 ("node_7721", 2D) = "white" {}
        _node_9975 ("node_9975", Color) = (0,0,1,1)
        _timer ("timer", Range(-1, 1)) = -0.1
        _number ("number", Float ) = 1
        _node_234 ("node_234", Color) = (0,0,0,1)
        _node_5685 ("node_5685", Color) = (0,0.4620686,1,1)
        _node_450 ("node_450", Color) = (1,0,0,1)
        _titling ("titling", Float ) = 1
        _U ("U", Float ) = 1
        _V ("V", Float ) = 0.4
        _node_1398 ("node_1398", 2D) = "white" {}
        _node_3644 ("node_3644", Color) = (0.5,0.5,0.5,1)
        _node_290 ("node_290", Color) = (0.5,0.5,0.5,1)
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "Queue"="AlphaTest"
            "RenderType"="TransparentCutout"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform sampler2D _node_7721; uniform float4 _node_7721_ST;
            uniform float4 _node_9975;
            uniform float _timer;
            uniform float _number;
            uniform float4 _node_234;
            uniform float4 _node_5685;
            uniform float4 _node_450;
            uniform float _titling;
            uniform float _U;
            uniform float _V;
            uniform sampler2D _node_1398; uniform float4 _node_1398_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                UNITY_FOG_COORDS(1)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float2 node_2760 = (i.uv0+(float2(_U,_V)*(i.uv0*_titling)));
                float4 _node_1398_var = tex2D(_node_1398,TRANSFORM_TEX(node_2760, _node_1398));
                float4 _node_7721_var = tex2D(_node_7721,TRANSFORM_TEX(i.uv0, _node_7721));
                float node_4171 = (1.0 - _node_7721_var.g);
                float node_8674 = (1.0 - _node_7721_var.a);
                float4 node_4491 = _Time + _TimeEditor;
                float2 node_9947 = ((i.uv0*_number)+(node_4491.g*_timer)*float2(1,1));
                float node_7124 = node_9947.g;
                float node_2715 = pow((abs((frac(frac(node_7124))-0.15))*1.3),15.0);
                float node_9018 = 2.0;
                clip(((_node_1398_var.rgb*1.4)+floor(lerp(float3(node_4171,node_4171,node_4171),float3(node_8674,node_8674,node_8674),lerp(float3(_node_7721_var.r,_node_7721_var.r,_node_7721_var.r),_node_234.rgb,node_2715)) * node_9018) / (node_9018 - 1)).r - 0.5);
////// Lighting:
////// Emissive:
                float node_8268 = 2.0;
                float node_8711 = floor((_node_7721_var.b*node_4171) * node_8268) / (node_8268 - 1);
                float3 emissive = saturate((1.0-(1.0-(_node_5685.rgb*(1.0 - _node_7721_var.r)))*(1.0-lerp((_node_450.rgb*node_2715),(_node_9975.rgb*node_8711),node_8711))));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform sampler2D _node_7721; uniform float4 _node_7721_ST;
            uniform float _timer;
            uniform float _number;
            uniform float4 _node_234;
            uniform float _titling;
            uniform float _U;
            uniform float _V;
            uniform sampler2D _node_1398; uniform float4 _node_1398_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float2 node_2760 = (i.uv0+(float2(_U,_V)*(i.uv0*_titling)));
                float4 _node_1398_var = tex2D(_node_1398,TRANSFORM_TEX(node_2760, _node_1398));
                float4 _node_7721_var = tex2D(_node_7721,TRANSFORM_TEX(i.uv0, _node_7721));
                float node_4171 = (1.0 - _node_7721_var.g);
                float node_8674 = (1.0 - _node_7721_var.a);
                float4 node_4491 = _Time + _TimeEditor;
                float2 node_9947 = ((i.uv0*_number)+(node_4491.g*_timer)*float2(1,1));
                float node_7124 = node_9947.g;
                float node_2715 = pow((abs((frac(frac(node_7124))-0.15))*1.3),15.0);
                float node_9018 = 2.0;
                clip(((_node_1398_var.rgb*1.4)+floor(lerp(float3(node_4171,node_4171,node_4171),float3(node_8674,node_8674,node_8674),lerp(float3(_node_7721_var.r,_node_7721_var.r,_node_7721_var.r),_node_234.rgb,node_2715)) * node_9018) / (node_9018 - 1)).r - 0.5);
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
