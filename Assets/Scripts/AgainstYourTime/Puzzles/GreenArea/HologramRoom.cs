using UnityEngine;
using VRStandardAssets.Utils;
using System.Collections;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System;
using System.Linq;

public class HologramRoom : MonoBehaviour 
{
    public GameObject crystalBottom;
    public GameObject crystalTop;
    public GameObject specialEffects;
    private Material m_materialToChange;               // Shader used when the room is lightened up
    public AudioSource audioEmitter;

    private Color _myOriginalColor;

    void Start()
    {
        m_materialToChange = GetComponent<Renderer>().sharedMaterial;
        m_materialToChange.SetFloat("_sliderchiaro", 0f);
    }

    public void TurnOn()
    {
        m_materialToChange.SetFloat("_sliderchiaro", 1f);
        specialEffects.SetActive(true);
        audioEmitter.Play();
    }

    public void TurnOff()
    {
        specialEffects.SetActive(false);
        m_materialToChange.SetFloat("_sliderchiaro", 0f);
    }
}