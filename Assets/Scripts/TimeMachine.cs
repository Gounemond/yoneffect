using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System;
using System.Linq;

public class TimeMachine : MonoBehaviour 
{
	public const int MAX_NUM_CLONES = 21;				// Defines the maximum number of clones
	public const int MAX_JUMP_TIME = 180;				// Defines the maximum amount of seconds you can jump back in time
	
	#region objectsToRecord
	public InputVCR[] recordedKaerb;					// Array of Kaerb clones VCR controllers to use when going back in time 
	public InputVCR[] recordedCamera;					// Array of cameras of Kaerb clones VCR controllers to use when going back in time
	public InputVCR[] objVCR;							// Array of objects which are needed to keep track of when time-travelling
	#endregion
	
	public InputVCR playerVCR;							// Main player - instance of Kaerb VCR controller
	public InputVCR cameraVCR;							// Main player camera - instance of Maincamera VCR controller
	
	#region startingConditions
	private Vector3 recStartPos;						// Starting position of Kaerb
	private Quaternion recStartRot;						// Starting rotation of Kaerb	
	
	private Vector3 recStartPos2;						// Starting position of Kaerb's camera
	private Quaternion recStartRot2;					// Starting rotation of Kaerb's camera
	
	private Vector3[] recStartPosObj;					// Starting position of every object recorded
	private Quaternion[] recStartRotObj;				// Starting rotation of every object recorded
	#endregion
	
	#region actionStreams
	private Recording[] myActions;						// Series of recorded keyboard inputs on Kaerb
	private Recording[] myLooks;						// Series of mouse inputs on Kaerb
	private Recording[] recObj;							// Series of "positions" of objects
	#endregion
	
	private float maxTime;								// Keeps how much time you're able to jump back (never exceeds MAX_JUMP_TIME)
	
	private bool isRecording;							// If we are actually recording Kaerb or not
	
	private bool Pause = false;							// If the game is paused or not
	
	private bool isPlaying=false;						// Dunno yet
	
	private float[] startTime;							// Actual time when the clones appear
	private float[] endTime;							// Actual time when the clones disappear
	private bool[] attivo;								// Tells if the clone is actually playing
	
	private float WorldTime;							// Time in game, subject to time travel (different from unity time!)
	private Text worldTimeLabel;						// Reference to the label on NGUI for WorldTime
	
	private float timeLabeltime= 0.0f;					// Refers to the jump Time
	private Text timeLabel;							// Reference to the label on NGUI for timeLabeltime (jumptime)
	
	private int nextCloneAvailable = 0;					//	Keeps track of how many jumps in the past (so how many clones got a recording)
	
	void Start()
	{
		maxTime = 0f;									//	When starting, the time machine has to power-up, charging seconds
		
		Cursor.visible = false;
		Screen.lockCursor = true;
		timeLabel = GameObject.Find("TimeLabel").GetComponent<Text>();
		worldTimeLabel = GameObject.Find("ActualTimeLabel").GetComponent<Text>();
		// Vector size initialization
		startTime = new float[MAX_NUM_CLONES];
		endTime = new float[MAX_NUM_CLONES];
		myActions = new Recording[MAX_NUM_CLONES];
		myLooks = new Recording[MAX_NUM_CLONES];
		attivo = new bool[MAX_NUM_CLONES];
		
		recordedKaerb = new InputVCR[MAX_NUM_CLONES];
		recordedCamera = new InputVCR[MAX_NUM_CLONES];
		
		recStartPos = playerVCR.transform.position;			//	Initial Kaerb position
		recStartRot = playerVCR.transform.rotation;			//	Initial Kaerb rotation
		recStartPos2 = cameraVCR.transform.position;		//	Initial camera position
		recStartRot2 = cameraVCR.transform.rotation;		//	Initial camera rotation
		recStartPosObj = new Vector3[objVCR.Length];		//	Initial object[s] position
		recStartRotObj = new Quaternion[objVCR.Length];		//	Initial object[s] rotation
		
		startTime[0] = 0f;
		WorldTime = 0.0f;
		
		//Initial object position and rotation
		for (int i = 0; i < objVCR.Length; i++)
		{
			recStartPosObj[i] = objVCR[i].transform.position;
			recStartRotObj[i] = objVCR[i].transform.rotation;
		}
		
		//Get references to clones and cameras
		for (int i = 0; i < recordedKaerb.Length; i++) 
		{
			recordedKaerb[i] = GameObject.Find("KaerbRecorded" + i).GetComponent<InputVCR>();
			recordedCamera[i] = recordedKaerb[i].transform.Find("Head Joint/First Person Camera").GetComponent<InputVCR>();
		}
		
		//Start recording player's actions
		playerVCR.NewRecording ();
		cameraVCR.NewRecording ();
		//Start recording objects position
		for (int i = 0; i < objVCR.Length; i++)
		{
			objVCR[i].NewRecording();
		}
		isRecording = true;			//Recording is on!
	}
	
	//Handles pause / resume game, stopping or resuming recording (otherwise you're going to record your actions in the menu!)
	public void StartRecording()
	{
		
		if ( isRecording )	//If we were recording  (should check if the game has switched in pause mode)
		{
			//Stop recording for player
			playerVCR.Stop ();	
			cameraVCR.Stop ();
			
			//Stop recording for objects
			for (int i = 0; i < objVCR.Length; i ++)
			{
				objVCR[i].Stop ();
			}
		}
		else			//If we were paused or just not recording (who knows, maybe cutscene in the future!)
		{
			//Start again recording (Record will append the recording to the previous one, like the stop never happened!)
			playerVCR.Record ();
			cameraVCR.Record ();
			
			//Start again recording even for objects
			for (int i = 0; i < objVCR.Length; i ++)
			{
				objVCR[i].Record ();
			}
			
		}
		
		//Switching state
		isRecording = !isRecording;
	}
	
	//Update every frame
	void Update()
	{
		//Keeps track of the actual time		
		WorldTime += Time.deltaTime;
		worldTimeLabel.text = WorldTime.ToString ();
		
		//MAXTIME IS "charging" while the time passes, until reaching the maximum amount.
		if (maxTime <= MAX_JUMP_TIME)
		{
			maxTime +=Time.deltaTime;
		}
		
		//Should open the time machine. Gonna be moved somewhere else on GUIHandler, 
		
		//Checks for all the possible clones that may be active
		for (int i = 0; i < nextCloneAvailable; i++)
		{
			if (!attivo[i])		//If this clone isn't actually playing
			{
				if (WorldTime >= startTime[i] && WorldTime < endTime[i])	//If the clone should exists in this time
				{
					//I need to go to "Time 0" of the clone, the beginning. Time 0 of the clone may mean "second 34 in the world"(startTime), because, the clone appeared at the second 						//34. I'm getting the property "position" at the closest frame of time 0, and reposition my clone to that actual position
					recordedKaerb[i].transform.position = InputVCR.ParseVector3(myActions[i].GetProperty (myActions[i].GetClosestFrame((WorldTime - startTime[i])),"position"));
					
					//I should do the same with rotation, but it's going to be synced during reproduction
					recordedKaerb[i].transform.rotation = new Quaternion();
					//Same thing with the camera, though it should be a child component, I do not want surprises :)
					recordedCamera[i].transform.position = InputVCR.ParseVector3(myLooks[i].GetProperty (myLooks[i].GetClosestFrame((WorldTime - startTime[i])),"position"));
					recordedCamera[i].transform.rotation = new Quaternion();
					
					recordedKaerb[i].GetComponent<FirstPersonCharacter>().enabled = true;
					//recordedKaerb[i].GetComponent<SimpleMouseRotator>().enabled = true;
					recordedKaerb[i].GetComponent<FirstPersonHeadBob>().enabled = true;
					
					Debug.Log ("Qualcosa");
					//Start the recording on the clone at time 0
					recordedKaerb[i].Play (Recording.ParseRecording (myActions[i].ToString ()), (WorldTime - startTime[i]));
					recordedCamera[i].Play (Recording.ParseRecording (myLooks[i].ToString ()), (WorldTime - startTime[i]));
					
					attivo[i] = true;	//This clone now is active, because it's playing
				}
			}
			else			//If this clone was already playing
			{
				if (WorldTime > endTime[i])	//If the clone lifespan exceeded the endtime
				{	
					Debug.Log ("Più di qualcosa");
					//Stop the reproduction
					recordedKaerb[i].Stop ();
					recordedCamera[i].Stop ();
					recordedKaerb[i].GetComponent<FirstPersonCharacter>().enabled = false;
					//recordedKaerb[i].GetComponent<SimpleMouseRotator>().enabled = false;
					recordedKaerb[i].GetComponent<FirstPersonHeadBob>().enabled = false;
					
					//Take object and camera out of the visible space
					recordedKaerb[i].transform.position= new Vector3(1000f,1000f+10*i,1000f);
					//recordedCamera[i].transform.position= new Vector3(1000f,1000f+10*i,1000f);
					
					//Clone is no more active
					attivo[i]=false;
				}
			}
		}
		
		if (Input.GetButtonUp("TimeTravel"))
		{
			Pause = !Pause;
			if (Pause)
			{
				Time.timeScale = 0;
				Cursor.visible = true;
				Screen.lockCursor = false;
			}
			else
			{
				Time.timeScale = 1;
				Cursor.visible = false;
				Screen.lockCursor = true;
			}
			GameObject.Find("CameraTimeMachine").GetComponent<Camera>().enabled = !GameObject.Find("CameraTimeMachine").GetComponent<Camera>().enabled; 	// Enables the NGUI Camera (TODO: shouldn't be handled here)
		}
	}
	
	public void increaseLabelTime()
	{
		if ((timeLabeltime +2f)<= maxTime)
		{
			timeLabeltime += 2f;
		}
		timeLabel.text = timeLabeltime.ToString();
		
	}
	
	public void decreaseLabelTime()
	{
		if ((timeLabeltime -2f)>= 0.0f)
		{
			timeLabeltime -= 2f;
		}
		timeLabel.text = timeLabeltime.ToString();
	}
	
	//Called from NGUI to begin time travel
	public void TimeTravel()
	{
		Time.timeScale = 1f;															// Game is back to normal flow
		Pause = false;																	// No more pause
		
		timeLabel.text = timeLabeltime.ToString();;
		StartCoroutine(PlayerTest(timeLabeltime));										// Start the coroutine which handles saving and storing recording
		Cursor.visible = false;														// Hide cursor (TODO: shouldn't be handled here)
		Screen.lockCursor = true;
		
		GameObject.Find("CameraTimeMachine").GetComponent<Camera>().enabled = false; 	// Disables the NGUI Camera, shouldn't be handled here now
	}
	
	private IEnumerator PlayerTest(float time)
	{
		//Store the recording done until now on Kaerb and camera
		Recording recPlayer = playerVCR.GetRecording ();
		Recording recCamera = cameraVCR.GetRecording ();
		
		//Store the recordings done until now on the objects
		for (int i = 0; i < objVCR.Length; i ++)
		{
			recObj[i] = objVCR[i].GetRecording ();
		}
		//If somehow there's no recording
		if ( null == recPlayer)
		{
			yield break;
		}
		
		//Adds new set of recording
		myActions[nextCloneAvailable] = recPlayer;
		myLooks[nextCloneAvailable] = recCamera;
		
		//The new start time (that will be used for the next clone) will be actual time less the amount asked to go back
		startTime[nextCloneAvailable+1] = WorldTime - time;
		
		//The end time for the actual clone will be the time you started the time travel, means, now
		endTime[nextCloneAvailable] = WorldTime;
		
		//Adjust the time displayed 
		WorldTime = WorldTime - time;
		
		//Time machine has used some power!
		maxTime = maxTime - time;
		
		//Starts a new recording for the actual player
		playerVCR.NewRecording ();
		cameraVCR.NewRecording ();
		
		nextCloneAvailable++;	//Next clone ready to get recording
		
		for (int i = 0; i < MAX_NUM_CLONES; i++)
		{
			//Stops whatever reproduction was going on 
			recordedKaerb[i].Stop ();
			recordedCamera[i].Stop ();
			
			//Throws all of them somewhere not visible
			//recordedKaerb[i].transform.position= new Vector3(1000,1000,1000);
			//recordedCamera[i].transform.position= new Vector3(1000,1000,1000);
			
			//Sets all clones deactivated
			attivo[i]=false;
		}
		
		//	Reposition all the objects
		for (int i = 0; i < objVCR.Length; i++)
		{
			objVCR[i].transform.position = InputVCR.ParseVector3(recObj[i].GetProperty (recObj[i].GetClosestFrame((WorldTime - time)),"position"));	
		}
	}	
}