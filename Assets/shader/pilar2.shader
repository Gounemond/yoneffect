// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:8361,x:33523,y:32638,varname:node_8361,prsc:2|spec-2132-OUT,emission-5518-OUT,transm-3150-OUT,lwrap-3150-OUT,alpha-3150-OUT;n:type:ShaderForge.SFN_Multiply,id:822,x:32845,y:32891,varname:node_822,prsc:2|A-9323-RGB,B-101-OUT,C-3150-OUT;n:type:ShaderForge.SFN_Vector1,id:101,x:32594,y:33046,varname:node_101,prsc:2,v1:5;n:type:ShaderForge.SFN_Color,id:9323,x:32517,y:32808,ptovrint:False,ptlb:glow,ptin:_glow,varname:node_9323,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.2313726,c2:0.9803922,c3:0.7215686,c4:1;n:type:ShaderForge.SFN_Power,id:3150,x:32372,y:32921,varname:node_3150,prsc:2|VAL-7822-OUT,EXP-8829-OUT;n:type:ShaderForge.SFN_Color,id:1812,x:32590,y:32517,ptovrint:False,ptlb:main,ptin:_main,varname:node_1812,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0,c3:0.07450981,c4:1;n:type:ShaderForge.SFN_Vector1,id:2132,x:32856,y:32810,varname:node_2132,prsc:2,v1:1;n:type:ShaderForge.SFN_Lerp,id:892,x:32856,y:32682,varname:node_892,prsc:2|A-1812-RGB,B-7756-OUT,T-3150-OUT;n:type:ShaderForge.SFN_Vector1,id:7756,x:32530,y:32692,varname:node_7756,prsc:2,v1:0.1;n:type:ShaderForge.SFN_Multiply,id:7822,x:32180,y:32846,varname:node_7822,prsc:2|A-1071-OUT,B-9584-OUT;n:type:ShaderForge.SFN_Vector1,id:8829,x:32176,y:33047,varname:node_8829,prsc:2,v1:5;n:type:ShaderForge.SFN_Abs,id:1071,x:32039,y:32740,varname:node_1071,prsc:2|IN-6097-OUT;n:type:ShaderForge.SFN_Vector1,id:9584,x:32039,y:32955,varname:node_9584,prsc:2,v1:1.5;n:type:ShaderForge.SFN_Subtract,id:6097,x:31877,y:32705,varname:node_6097,prsc:2|A-4592-OUT,B-155-OUT;n:type:ShaderForge.SFN_Vector1,id:155,x:31713,y:32831,varname:node_155,prsc:2,v1:0.5;n:type:ShaderForge.SFN_Frac,id:4592,x:31655,y:32597,varname:node_4592,prsc:2|IN-6531-OUT;n:type:ShaderForge.SFN_ComponentMask,id:7731,x:31370,y:32494,varname:node_7731,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-1260-UVOUT;n:type:ShaderForge.SFN_Panner,id:1260,x:31196,y:32464,varname:node_1260,prsc:2,spu:0.25,spv:0|UVIN-1420-OUT,DIST-5993-OUT;n:type:ShaderForge.SFN_Time,id:2388,x:30617,y:32635,varname:node_2388,prsc:2;n:type:ShaderForge.SFN_Multiply,id:5993,x:30914,y:32664,varname:node_5993,prsc:2|A-2388-T,B-6798-OUT;n:type:ShaderForge.SFN_Slider,id:6798,x:30550,y:32774,ptovrint:False,ptlb:gottagofast,ptin:_gottagofast,varname:node_6798,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:5.323946,max:10;n:type:ShaderForge.SFN_TexCoord,id:9773,x:30722,y:32428,varname:node_9773,prsc:2,uv:0;n:type:ShaderForge.SFN_Multiply,id:1420,x:30912,y:32337,varname:node_1420,prsc:2|A-5288-OUT,B-9773-UVOUT;n:type:ShaderForge.SFN_Slider,id:5288,x:30513,y:32283,ptovrint:False,ptlb:numberoflight,ptin:_numberoflight,varname:node_5288,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:1,cur:5,max:10;n:type:ShaderForge.SFN_Add,id:5518,x:33033,y:32705,varname:node_5518,prsc:2|A-892-OUT,B-822-OUT;n:type:ShaderForge.SFN_SwitchProperty,id:6531,x:31517,y:32623,ptovrint:False,ptlb:switch direction,ptin:_switchdirection,varname:node_6531,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:True|A-7731-OUT,B-5943-OUT;n:type:ShaderForge.SFN_ComponentMask,id:5943,x:31358,y:32651,varname:node_5943,prsc:2,cc1:1,cc2:-1,cc3:-1,cc4:-1|IN-2547-UVOUT;n:type:ShaderForge.SFN_Panner,id:2547,x:31177,y:32651,varname:node_2547,prsc:2,spu:0,spv:0.25|UVIN-1420-OUT,DIST-5993-OUT;proporder:9323-1812-6798-5288-6531;pass:END;sub:END;*/

Shader "Custom/Pilar2" {
    Properties {
        _glow ("glow", Color) = (0.2313726,0.9803922,0.7215686,1)
        _main ("main", Color) = (0,0,0.07450981,1)
        _gottagofast ("gottagofast", Range(0, 10)) = 5.323946
        _numberoflight ("numberoflight", Range(1, 10)) = 5
        [MaterialToggle] _switchdirection ("switch direction", Float ) = 0
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform float4 _TimeEditor;
            uniform float4 _glow;
            uniform float4 _main;
            uniform float _gottagofast;
            uniform float _numberoflight;
            uniform fixed _switchdirection;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                UNITY_FOG_COORDS(3)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = 1;
                float3 attenColor = attenuation * _LightColor0.xyz;
///////// Gloss:
                float gloss = 0.5;
                float specPow = exp2( gloss * 10.0+1.0);
////// Specular:
                float NdotL = max(0, dot( normalDirection, lightDirection ));
                float node_2132 = 1.0;
                float3 specularColor = float3(node_2132,node_2132,node_2132);
                float3 directSpecular = (floor(attenuation) * _LightColor0.xyz) * pow(max(0,dot(halfDirection,normalDirection)),specPow)*specularColor;
                float3 specular = directSpecular;
////// Emissive:
                float node_7756 = 0.1;
                float4 node_2388 = _Time + _TimeEditor;
                float node_5993 = (node_2388.g*_gottagofast);
                float2 node_1420 = (_numberoflight*i.uv0);
                float node_3150 = pow((abs((frac(lerp( (node_1420+node_5993*float2(0.25,0)).r, (node_1420+node_5993*float2(0,0.25)).g, _switchdirection ))-0.5))*1.5),5.0);
                float3 emissive = (lerp(_main.rgb,float3(node_7756,node_7756,node_7756),node_3150)+(_glow.rgb*5.0*node_3150));
/// Final Color:
                float3 finalColor = specular + emissive;
                fixed4 finalRGBA = fixed4(finalColor,node_3150);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdadd
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform float4 _TimeEditor;
            uniform float4 _glow;
            uniform float4 _main;
            uniform float _gottagofast;
            uniform float _numberoflight;
            uniform fixed _switchdirection;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                LIGHTING_COORDS(3,4)
                UNITY_FOG_COORDS(5)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
///////// Gloss:
                float gloss = 0.5;
                float specPow = exp2( gloss * 10.0+1.0);
////// Specular:
                float NdotL = max(0, dot( normalDirection, lightDirection ));
                float node_2132 = 1.0;
                float3 specularColor = float3(node_2132,node_2132,node_2132);
                float3 directSpecular = attenColor * pow(max(0,dot(halfDirection,normalDirection)),specPow)*specularColor;
                float3 specular = directSpecular;
/// Final Color:
                float3 finalColor = specular;
                float4 node_2388 = _Time + _TimeEditor;
                float node_5993 = (node_2388.g*_gottagofast);
                float2 node_1420 = (_numberoflight*i.uv0);
                float node_3150 = pow((abs((frac(lerp( (node_1420+node_5993*float2(0.25,0)).r, (node_1420+node_5993*float2(0,0.25)).g, _switchdirection ))-0.5))*1.5),5.0);
                fixed4 finalRGBA = fixed4(finalColor * node_3150,0);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
