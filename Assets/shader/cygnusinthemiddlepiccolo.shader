// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:1,bsrc:0,bdst:1,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:8351,x:35946,y:32104,varname:node_8351,prsc:2|emission-7822-OUT;n:type:ShaderForge.SFN_Lerp,id:7877,x:33978,y:32352,varname:node_7877,prsc:2|A-1427-RGB,B-1757-UVOUT,T-6283-OUT;n:type:ShaderForge.SFN_Color,id:1427,x:33095,y:32207,ptovrint:False,ptlb:node_1429_copy_copy,ptin:_node_1429_copy_copy,varname:_node_1429_copy_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0,c3:0.9310346,c4:1;n:type:ShaderForge.SFN_Tex2d,id:6420,x:32838,y:32958,ptovrint:False,ptlb:node_1859_copy_copy,ptin:_node_1859_copy_copy,varname:_node_1859_copy_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:ae0049a4a1783f946ac225627cf79c28,ntxv:0,isnm:False|UVIN-5366-UVOUT;n:type:ShaderForge.SFN_Multiply,id:5478,x:33630,y:31984,varname:node_5478,prsc:2|A-1545-OUT,B-1545-OUT;n:type:ShaderForge.SFN_OneMinus,id:1099,x:33438,y:32199,varname:node_1099,prsc:2|IN-1545-OUT;n:type:ShaderForge.SFN_Fresnel,id:1545,x:33135,y:32002,varname:node_1545,prsc:2|NRM-8203-OUT,EXP-2457-OUT;n:type:ShaderForge.SFN_NormalVector,id:8203,x:32784,y:31957,prsc:2,pt:False;n:type:ShaderForge.SFN_ValueProperty,id:2457,x:32888,y:32147,ptovrint:False,ptlb:fresnel_copy_copy,ptin:_fresnel_copy_copy,varname:_fresnel_copy_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.5;n:type:ShaderForge.SFN_Add,id:723,x:34290,y:32044,varname:node_723,prsc:2|A-5478-OUT,B-7877-OUT;n:type:ShaderForge.SFN_ScreenPos,id:1757,x:33324,y:32666,varname:node_1757,prsc:2,sctp:0;n:type:ShaderForge.SFN_Panner,id:5366,x:32588,y:32993,varname:node_5366,prsc:2,spu:1,spv:1|UVIN-8705-UVOUT,DIST-3093-OUT;n:type:ShaderForge.SFN_TexCoord,id:8705,x:32295,y:32829,varname:node_8705,prsc:2,uv:0;n:type:ShaderForge.SFN_Time,id:690,x:31974,y:32837,varname:node_690,prsc:2;n:type:ShaderForge.SFN_Multiply,id:3093,x:32438,y:33086,varname:node_3093,prsc:2|A-690-T,B-949-OUT;n:type:ShaderForge.SFN_ValueProperty,id:949,x:32117,y:33118,ptovrint:False,ptlb:time_copy,ptin:_time_copy,varname:_time_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_Power,id:4443,x:33282,y:33052,varname:node_4443,prsc:2|VAL-6420-RGB,EXP-2037-OUT;n:type:ShaderForge.SFN_Vector1,id:2037,x:33001,y:33130,varname:node_2037,prsc:2,v1:3;n:type:ShaderForge.SFN_OneMinus,id:4054,x:33568,y:32902,varname:node_4054,prsc:2|IN-4443-OUT;n:type:ShaderForge.SFN_SwitchProperty,id:6283,x:33819,y:32738,ptovrint:False,ptlb:switchnubi_copy,ptin:_switchnubi_copy,varname:_switchnubi_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:False|A-6420-RGB,B-4054-OUT;n:type:ShaderForge.SFN_SceneDepth,id:2888,x:34039,y:32886,varname:node_2888,prsc:2;n:type:ShaderForge.SFN_Depth,id:4412,x:34012,y:33026,varname:node_4412,prsc:2;n:type:ShaderForge.SFN_Subtract,id:4446,x:34276,y:32886,varname:node_4446,prsc:2|A-2888-OUT,B-4412-OUT;n:type:ShaderForge.SFN_Multiply,id:3162,x:34464,y:32727,varname:node_3162,prsc:2|A-7072-OUT,B-4446-OUT;n:type:ShaderForge.SFN_ValueProperty,id:7072,x:34229,y:32727,ptovrint:False,ptlb:density_copy,ptin:_density_copy,varname:_density_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Vector4Property,id:7209,x:34218,y:33084,ptovrint:False,ptlb:node_940_copy,ptin:_node_940_copy,varname:_node_940_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0,v2:0,v3:0,v4:-0.1;n:type:ShaderForge.SFN_Add,id:6170,x:34462,y:33108,varname:node_6170,prsc:2|A-7209-XYZ,B-7209-W;n:type:ShaderForge.SFN_RemapRangeAdvanced,id:1713,x:34874,y:33044,varname:node_1713,prsc:2|IN-3162-OUT,IMIN-6170-OUT,IMAX-7261-OUT,OMIN-6028-OUT,OMAX-5975-OUT;n:type:ShaderForge.SFN_Add,id:7261,x:34501,y:33265,varname:node_7261,prsc:2|A-5777-XYZ,B-5777-W;n:type:ShaderForge.SFN_Vector4Property,id:5777,x:34212,y:33357,ptovrint:False,ptlb:node_2263_copy,ptin:_node_2263_copy,varname:_node_2263_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0,v2:1,v3:0.8235295,v4:0.8;n:type:ShaderForge.SFN_Vector1,id:6028,x:34656,y:33180,varname:node_6028,prsc:2,v1:1;n:type:ShaderForge.SFN_Vector1,id:5975,x:34676,y:33309,varname:node_5975,prsc:2,v1:0;n:type:ShaderForge.SFN_Clamp01,id:7909,x:35018,y:33001,varname:node_7909,prsc:2|IN-1713-OUT;n:type:ShaderForge.SFN_Power,id:2331,x:35284,y:32961,varname:node_2331,prsc:2|VAL-7909-OUT,EXP-2366-OUT;n:type:ShaderForge.SFN_ValueProperty,id:2366,x:35018,y:33202,ptovrint:False,ptlb:fade power_copy,ptin:_fadepower_copy,varname:_fadepower_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:3;n:type:ShaderForge.SFN_SceneColor,id:4675,x:34892,y:32722,varname:node_4675,prsc:2;n:type:ShaderForge.SFN_Multiply,id:7822,x:35533,y:32200,varname:node_7822,prsc:2|A-1830-OUT,B-9142-OUT;n:type:ShaderForge.SFN_Vector1,id:4727,x:34644,y:31956,varname:node_4727,prsc:2,v1:2;n:type:ShaderForge.SFN_Multiply,id:1830,x:34846,y:31594,varname:node_1830,prsc:2|A-723-OUT,B-4727-OUT;n:type:ShaderForge.SFN_Multiply,id:9142,x:35397,y:32667,varname:node_9142,prsc:2|A-4675-RGB,B-2331-OUT;proporder:1427-6420-2457-6283-7072-7209-5777-2366-949;pass:END;sub:END;*/

Shader "Custom/cygnusinthemiddlepiccolo" {
    Properties {
        _node_1429_copy_copy ("node_1429_copy_copy", Color) = (1,0,0.9310346,1)
        _node_1859_copy_copy ("node_1859_copy_copy", 2D) = "white" {}
        _fresnel_copy_copy ("fresnel_copy_copy", Float ) = 0.5
        [MaterialToggle] _switchnubi_copy ("switchnubi_copy", Float ) = 0
        _density_copy ("density_copy", Float ) = 1
        _node_940_copy ("node_940_copy", Vector) = (0,0,0,-0.1)
        _node_2263_copy ("node_2263_copy", Vector) = (0,1,0.8235295,0.8)
        _fadepower_copy ("fade power_copy", Float ) = 3
        _time_copy ("time_copy", Float ) = 0
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        LOD 200
        GrabPass{ }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Cull Front
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform sampler2D _GrabTexture;
            uniform sampler2D _CameraDepthTexture;
            uniform float4 _TimeEditor;
            uniform float4 _node_1429_copy_copy;
            uniform sampler2D _node_1859_copy_copy; uniform float4 _node_1859_copy_copy_ST;
            uniform float _fresnel_copy_copy;
            uniform float _time_copy;
            uniform fixed _switchnubi_copy;
            uniform float _density_copy;
            uniform float4 _node_940_copy;
            uniform float4 _node_2263_copy;
            uniform float _fadepower_copy;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float4 screenPos : TEXCOORD3;
                float4 projPos : TEXCOORD4;
                UNITY_FOG_COORDS(5)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(-v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                o.projPos = ComputeScreenPos (o.pos);
                COMPUTE_EYEDEPTH(o.projPos.z);
                o.screenPos = o.pos;
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                #if UNITY_UV_STARTS_AT_TOP
                    float grabSign = -_ProjectionParams.x;
                #else
                    float grabSign = _ProjectionParams.x;
                #endif
                i.normalDir = normalize(i.normalDir);
                i.screenPos = float4( i.screenPos.xy / i.screenPos.w, 0, 0 );
                i.screenPos.y *= _ProjectionParams.x;
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float sceneZ = max(0,LinearEyeDepth (UNITY_SAMPLE_DEPTH(tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(i.projPos)))) - _ProjectionParams.g);
                float partZ = max(0,i.projPos.z - _ProjectionParams.g);
                float2 sceneUVs = float2(1,grabSign)*i.screenPos.xy*0.5+0.5;
                float4 sceneColor = tex2D(_GrabTexture, sceneUVs);
////// Lighting:
////// Emissive:
                float node_1545 = pow(1.0-max(0,dot(i.normalDir, viewDirection)),_fresnel_copy_copy);
                float4 node_690 = _Time + _TimeEditor;
                float2 node_5366 = (i.uv0+(node_690.g*_time_copy)*float2(1,1));
                float4 _node_1859_copy_copy_var = tex2D(_node_1859_copy_copy,TRANSFORM_TEX(node_5366, _node_1859_copy_copy));
                float3 node_6170 = (_node_940_copy.rgb+_node_940_copy.a);
                float node_6028 = 1.0;
                float3 emissive = ((((node_1545*node_1545)+lerp(_node_1429_copy_copy.rgb,float3(i.screenPos.rg,0.0),lerp( _node_1859_copy_copy_var.rgb, (1.0 - pow(_node_1859_copy_copy_var.rgb,3.0)), _switchnubi_copy )))*2.0)*(sceneColor.rgb*pow(saturate((node_6028 + ( ((_density_copy*(sceneZ-partZ)) - node_6170) * (0.0 - node_6028) ) / ((_node_2263_copy.rgb+_node_2263_copy.a) - node_6170))),_fadepower_copy)));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
