using UnityEngine;

[RequireComponent(typeof(ThirdPersonCharacter))]
public class ThirdPersonUserControl : MonoBehaviour 
{
	
	public bool walkByDefault = false;                  // toggle for walking state
	public bool lookInCameraDirection = true;           // should the character be looking in the same direction that the camera is facing
	
	private Vector3 lookPos;                            // The position that the character should be looking towards
    private ThirdPersonCharacter character;             // A reference to the ThirdPersonCharacter on the object
	private Transform cam;                              // A reference to the main camera in the scenes transform
	private Vector3 camForward;							// The current forward direction of the camera
	private Vector3 move;								// the world-relative desired move direction, calculated from the camForward and user input.
	private float directionLength;

	// Use this for initialization
	void Start ()
	{
        // get the transform of the main camera
		if (Camera.main != null)
		{
			cam = Camera.main.transform;
		} else {
			Debug.LogWarning("Warning: no main camera found. Third person character needs a Camera tagged \"MainCamera\", for camera-relative controls.");
			// we use self-relative controls in this case, which probably isn't what the user wants, but hey, we warned them!
		}

        // get the third person character ( this should never be null due to require component )
		character = GetComponent<ThirdPersonCharacter>();
	}

	// Fixed update is called in sync with physics
	void FixedUpdate ()
	{
		// read inputs
		bool crouch = Input.GetButton("Crouch");

		#if CROSS_PLATFORM_INPUT
		bool jump = CrossPlatformInput.GetButton("Jump");
		float h = CrossPlatformInput.GetAxis("Horizontal");
		float v = CrossPlatformInput.GetAxis("Vertical");
		#else
		bool jump = Input.GetButton("Jump");
		float h = Input.GetAxis("Horizontal");
		float v = Input.GetAxis("Vertical");
		#endif

		// calculate move direction to pass to character
		if (cam != null) 
		{
			move = new Vector3 (h, 0, v);

			if (move.magnitude > 1) 
			{
				move.Normalize ();
				}

				if (move != Vector3.zero) {
						// Get the length of the directon vector and then normalize it
						// Dividing by the length is cheaper than normalizing when we already have the length anyway
						directionLength = move.magnitude;
						move = move / directionLength;
		
						// Make sure the length is no bigger than 1
						directionLength = Mathf.Min (1, directionLength);
		
						// Make the input vector more sensitive towards the extremes and less sensitive in the middle
						// This makes it easier to control slow speeds when using analog sticks
						directionLength = directionLength * directionLength;
		
						// Multiply the normalized direction vector by the modified length
						move = move * directionLength;
				}
		}
		#if !MOBILE_INPUT
		// On non-mobile builds, walk/run speed is modified by a key press.
		bool walkToggle = Input.GetKey(KeyCode.LeftShift);
		// We select appropriate speed based on whether we're walking by default, and whether the walk/run toggle button is pressed:
		float walkMultiplier = (walkByDefault ? walkToggle ? 1 : 2f : walkToggle ? 2f : 1);
		move *= walkMultiplier;
		#endif

		// On mobile, walk/run speed is controlled in analogue fashion by the v input value, and therefore needs no special handling.
		// *hence no code here!*



		// calculate the head look target position
			lookPos = transform.rotation * move;

	    // pass all parameters to the character control script
		character.Move( move, crouch, jump, lookPos );
	}


}
