﻿using UnityEngine;
using System.Collections;

public class YonGearText : MonoBehaviour {


    private YonEffectJRController gamestate;
    // Use this for initialization
    void Start ()
    {
        gamestate = FindObjectOfType<YonEffectJRController>().GetComponent<YonEffectJRController>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        GetComponent<TextMesh>().text = gamestate.numberGEARSCollected.ToString();
	
	}
}
