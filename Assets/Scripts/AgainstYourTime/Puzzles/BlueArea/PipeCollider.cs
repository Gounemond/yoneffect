﻿using UnityEngine;
using System.Collections;

public class PipeCollider : MonoBehaviour
{
    public Pipe parentPipe;
    public Pipe connectedPipe;
    public PipeCollider connectedCollider;
    public Material turnOnMaterial;
    public Material turnOffMaterial;
    private int m_objInTrigger = 0;

    public bool exhausted = false;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Pipe")
        {
            if (m_objInTrigger == 0)
            {
                connectedCollider = other.gameObject.GetComponent<PipeCollider>();
                connectedPipe = connectedCollider.parentPipe;
            }
            m_objInTrigger++;
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.tag == "Pipe")
        {
            if (m_objInTrigger == 1)
            {
                connectedCollider = null;
                connectedPipe = null;
            }
            m_objInTrigger--;
        }
    }

    public void TurnOn()
    {
        if (connectedCollider != null)
        {
            GetComponent<Renderer>().material = turnOnMaterial;
        }
    }

    public void TurnOff()
    {
        GetComponent<Renderer>().material = turnOffMaterial;
    }

    public void PropagateSignal()
    {
        if (!exhausted)
        {
            exhausted = true;
            if (connectedPipe != null && !connectedCollider.exhausted)
            {
                if (parentPipe.isPowered)
                {
                    connectedPipe.TurnOn();
                    connectedPipe.Propagate();
                    connectedCollider.Exhaust();
                }
            }
        }
    }

    public void Exhaust()
    {
        exhausted = true;
    }

    public void Recharge()
    {
        exhausted = false;
    }
}
