using UnityEngine;
using VRStandardAssets.Utils;
using System.Collections;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System;
using System.Linq;

[RequireComponent (typeof(VRInteractiveItem))]
[RequireComponent (typeof(Rigidbody))]
public class VRTangramGrab : MonoBehaviour 
{
    [Header ("Grabbing parameters")]
	public float  correctionforce = 50.0f;      // Correction force which determines the speed of the object when grabbed
	public Transform beacon;                    // The "grabity beacon" that will be used then grabbing this object
    private Material m_MyMaterial;              // The custom material used for tangram objects

    public TangramPuzzle tangramManager;

	private bool  grabbed  = false;
    private bool  onHover = false;

    [Header("AudioSfx")]
    public AudioSource myAudioSource;
    public AudioClip hoverInAudio;
    public AudioClip hoverOutAudio;
    public AudioClip grabAudio;

    private Transform selectedBeacon;
    private Transform selectedPlacement = null;

    private Rigidbody _myRigidbody;
    private VRInteractiveItem _myVRitem;

    void OnEnable()
    {
        _myVRitem.OnClick += GrabSwitchState;
        _myVRitem.OnOver += HoverIn;
        _myVRitem.OnOut += HoverOut;
        //_myVRitem.OnPadBDown += RotateRight;
        //_myVRitem.OnPadXDown += RotateLeft;
    }

    void OnDisable()
    {
        _myVRitem.OnClick -= GrabSwitchState;
        _myVRitem.OnOver -= HoverIn;
        _myVRitem.OnOut -= HoverOut;
        //_myVRitem.OnPadBDown -= RotateRight;
        //_myVRitem.OnPadXDown -= RotateLeft;
    }

    void Awake () 
	{
        _myRigidbody = GetComponent<Rigidbody>();
        _myVRitem = GetComponent<VRInteractiveItem>();
        m_MyMaterial = GetComponent<Renderer>().material;
    }

    void Update () 
	{
        if (grabbed && !onHover && Input.GetButton("Fire1"))
        {
            GrabSwitchState();
            tangramManager.switchObjectGrabbed(null);     // Tells the manager to set the object grabbed as null
        }

        // Update gravity force around the GrabBeacon if you actually have grabbed an object
        // The grab method is inspired from the Half Life series
        if (grabbed)
		{
            selectedBeacon = tangramManager.GetClosestSnapPosition(beacon);
            selectedPlacement = tangramManager.GetCorrectPlacementPosition();

            //Vector3 force = new Vector3(selectedBeacon.transform.position.x - transform.position.x, beacon.transform.position.y - transform.position.y, selectedBeacon.transform.position.z - transform.position.z);
            Vector3 force = new Vector3(selectedBeacon.transform.position.x - transform.position.x, selectedBeacon.transform.position.y - transform.position.y, selectedBeacon.transform.position.z - transform.position.z);
            //Vector3 force = beacon.transform.position - transform.position;
            _myRigidbody.velocity = force.normalized * _myRigidbody.velocity.magnitude;
            _myRigidbody.AddForce(force * correctionforce);
            _myRigidbody.velocity *= Mathf.Min(1.0f, force.magnitude / 2 );
		}
        else if (selectedPlacement != null)
        {
            Vector3 force = new Vector3(selectedPlacement.transform.position.x - transform.position.x, selectedPlacement.transform.position.y - transform.position.y, selectedPlacement.transform.position.z - transform.position.z);
            _myRigidbody.velocity = force.normalized * _myRigidbody.velocity.magnitude;
            _myRigidbody.AddForce(force * correctionforce);
            _myRigidbody.velocity *= Mathf.Min(1.0f, force.magnitude / 2);
        }

    }

    public void GrabSwitchState()
    {
        if (onHover)
        {
            if (!grabbed)
            {
                grabbed = true;
                myAudioSource.PlayOneShot(grabAudio);
                m_MyMaterial.SetFloat("_Trasparent", 1);
                tangramManager.switchObjectGrabbed(this.gameObject);
            }
            else
            {
                grabbed = false;
                m_MyMaterial.SetFloat("_Trasparent", 0);
                tangramManager.switchObjectGrabbed(null);
            }
        }
        else
        {
            if (grabbed)
            {
                grabbed = false;
                m_MyMaterial.SetFloat("_Trasparent", 0);
            }
        }
    }

    public void HoverIn()
    {
        m_MyMaterial.SetFloat("_maintoselection", 1);
        myAudioSource.PlayOneShot(hoverInAudio);

        onHover = true;
    }

    public void HoverOut()
    {
        m_MyMaterial.SetFloat("_maintoselection", 0);
        myAudioSource.PlayOneShot(hoverOutAudio);
        onHover = false;
    }

    /*public void RotateRight()
    {
        _myRigidbody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
        transform.Rotate(0, 0, Time.deltaTime * rotationSpeed);
        _myRigidbody.constraints = RigidbodyConstraints.FreezeRotation;

    }*/

    /*public void RotateLeft()
    {
        _myRigidbody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
        transform.Rotate(0, 0, -Time.deltaTime * rotationSpeed);
        _myRigidbody.constraints = RigidbodyConstraints.FreezeRotation;
    }*/
}