// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:False,qofs:0,qpre:2,rntp:3,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:3138,x:32719,y:32712,varname:node_3138,prsc:2|emission-8445-RGB,clip-9377-OUT,olwid-8264-OUT,olcol-2907-OUT;n:type:ShaderForge.SFN_Color,id:7241,x:32455,y:33211,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_7241,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0.172549,c3:1,c4:1;n:type:ShaderForge.SFN_Tex2d,id:8445,x:32345,y:32516,ptovrint:False,ptlb:node_8445,ptin:_node_8445,varname:node_8445,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:70a1dca6b55e86b41809734ee6f4d635,ntxv:0,isnm:False;n:type:ShaderForge.SFN_ValueProperty,id:8264,x:32401,y:33097,ptovrint:False,ptlb:outline,ptin:_outline,varname:node_8264,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.02;n:type:ShaderForge.SFN_Noise,id:6455,x:31992,y:32717,varname:node_6455,prsc:2|XY-8654-UVOUT;n:type:ShaderForge.SFN_TexCoord,id:8654,x:31778,y:32717,varname:node_8654,prsc:2,uv:0;n:type:ShaderForge.SFN_Tex2d,id:3134,x:31967,y:32977,ptovrint:False,ptlb:node_3134,ptin:_node_3134,varname:node_3134,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:122722ed3a82a264c86c60ba82600fbd,ntxv:0,isnm:False|UVIN-5729-OUT;n:type:ShaderForge.SFN_Add,id:7820,x:32176,y:32953,varname:node_7820,prsc:2|A-6455-OUT,B-3134-RGB;n:type:ShaderForge.SFN_Multiply,id:2907,x:32630,y:33257,varname:node_2907,prsc:2|A-7241-RGB,B-1333-OUT;n:type:ShaderForge.SFN_Vector1,id:1333,x:32486,y:33391,varname:node_1333,prsc:2,v1:1;n:type:ShaderForge.SFN_Vector4Property,id:4443,x:29973,y:33277,ptovrint:False,ptlb:vector 6,ptin:_vector6,varname:_vector6,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:3,v2:3,v3:4,v4:0;n:type:ShaderForge.SFN_Relay,id:986,x:30401,y:33225,varname:node_986,prsc:2|IN-4443-X;n:type:ShaderForge.SFN_Relay,id:2390,x:30270,y:33465,varname:node_2390,prsc:2|IN-4443-Y;n:type:ShaderForge.SFN_Relay,id:3632,x:30024,y:33602,varname:node_3632,prsc:2|IN-4443-Z;n:type:ShaderForge.SFN_Multiply,id:8251,x:30211,y:33620,varname:node_8251,prsc:2|A-3632-OUT,B-8810-OUT;n:type:ShaderForge.SFN_Time,id:4234,x:29798,y:33671,varname:node_4234,prsc:2;n:type:ShaderForge.SFN_Frac,id:1999,x:30444,y:33730,varname:node_1999,prsc:2|IN-8251-OUT;n:type:ShaderForge.SFN_Multiply,id:7273,x:30406,y:33536,varname:node_7273,prsc:2|A-986-OUT,B-2390-OUT;n:type:ShaderForge.SFN_Multiply,id:2190,x:30626,y:33587,varname:node_2190,prsc:2|A-7273-OUT,B-1999-OUT;n:type:ShaderForge.SFN_Round,id:708,x:30783,y:33710,varname:node_708,prsc:2|IN-2190-OUT;n:type:ShaderForge.SFN_Divide,id:6827,x:30897,y:33562,varname:node_6827,prsc:2|A-708-OUT,B-986-OUT;n:type:ShaderForge.SFN_Divide,id:3178,x:31052,y:33147,varname:node_3178,prsc:2|A-7000-OUT,B-986-OUT;n:type:ShaderForge.SFN_Divide,id:8855,x:31097,y:33401,varname:node_8855,prsc:2|A-2135-OUT,B-2390-OUT;n:type:ShaderForge.SFN_Floor,id:2135,x:31085,y:33562,varname:node_2135,prsc:2|IN-6827-OUT;n:type:ShaderForge.SFN_Fmod,id:7000,x:31080,y:33266,varname:node_7000,prsc:2|A-708-OUT,B-986-OUT;n:type:ShaderForge.SFN_OneMinus,id:2544,x:31326,y:33367,varname:node_2544,prsc:2|IN-8855-OUT;n:type:ShaderForge.SFN_Append,id:2106,x:31505,y:33256,varname:node_2106,prsc:2|A-3178-OUT,B-2544-OUT;n:type:ShaderForge.SFN_Append,id:6906,x:31430,y:33073,varname:node_6906,prsc:2|A-986-OUT,B-2390-OUT;n:type:ShaderForge.SFN_TexCoord,id:6144,x:31639,y:32916,varname:node_6144,prsc:2,uv:0;n:type:ShaderForge.SFN_Divide,id:9697,x:31895,y:33218,varname:node_9697,prsc:2|A-6144-UVOUT,B-6906-OUT;n:type:ShaderForge.SFN_Add,id:5729,x:31789,y:33355,varname:node_5729,prsc:2|A-9697-OUT,B-2106-OUT;n:type:ShaderForge.SFN_Multiply,id:8810,x:30078,y:33764,varname:node_8810,prsc:2|A-4234-T,B-7944-OUT;n:type:ShaderForge.SFN_Slider,id:7944,x:29730,y:33902,ptovrint:False,ptlb:time_copy_copy,ptin:_time_copy_copy,varname:_time_copy_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Multiply,id:1951,x:32289,y:32838,varname:node_1951,prsc:2|A-7636-OUT,B-7820-OUT;n:type:ShaderForge.SFN_ComponentMask,id:9377,x:32454,y:32858,varname:node_9377,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-1951-OUT;n:type:ShaderForge.SFN_Multiply,id:7636,x:32150,y:32616,varname:node_7636,prsc:2|A-862-OUT,B-6455-OUT;n:type:ShaderForge.SFN_Vector1,id:862,x:31968,y:32604,varname:node_862,prsc:2,v1:1;proporder:7241-8445-8264-3134-4443-7944;pass:END;sub:END;*/

Shader "Shader Forge/mainCharacter" {
    Properties {
        _Color ("Color", Color) = (0,0.172549,1,1)
        _node_8445 ("node_8445", 2D) = "white" {}
        _outline ("outline", Float ) = 0.02
        _node_3134 ("node_3134", 2D) = "white" {}
        _vector6 ("vector 6", Vector) = (3,3,4,0)
        _time_copy_copy ("time_copy_copy", Range(0, 1)) = 1
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "Queue"="AlphaTest"
            "RenderType"="TransparentCutout"
        }
        Pass {
            Name "Outline"
            Tags {
            }
            Cull Front
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform float4 _Color;
            uniform float _outline;
            uniform sampler2D _node_3134; uniform float4 _node_3134_ST;
            uniform float4 _vector6;
            uniform float _time_copy_copy;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, float4(v.vertex.xyz + v.normal*_outline,1) );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float2 node_6455_skew = i.uv0 + 0.2127+i.uv0.x*0.3713*i.uv0.y;
                float2 node_6455_rnd = 4.789*sin(489.123*(node_6455_skew));
                float node_6455 = frac(node_6455_rnd.x*node_6455_rnd.y*(1+node_6455_skew.x));
                float node_986 = _vector6.r;
                float node_2390 = _vector6.g;
                float4 node_4234 = _Time + _TimeEditor;
                float node_708 = round(((node_986*node_2390)*frac((_vector6.b*(node_4234.g*_time_copy_copy)))));
                float2 node_5729 = ((i.uv0/float2(node_986,node_2390))+float2((fmod(node_708,node_986)/node_986),(1.0 - (floor((node_708/node_986))/node_2390))));
                float4 _node_3134_var = tex2D(_node_3134,TRANSFORM_TEX(node_5729, _node_3134));
                clip(((1.0*node_6455)*(node_6455+_node_3134_var.rgb)).r - 0.5);
                return fixed4((_Color.rgb*1.0),0);
            }
            ENDCG
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform sampler2D _node_8445; uniform float4 _node_8445_ST;
            uniform sampler2D _node_3134; uniform float4 _node_3134_ST;
            uniform float4 _vector6;
            uniform float _time_copy_copy;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float2 node_6455_skew = i.uv0 + 0.2127+i.uv0.x*0.3713*i.uv0.y;
                float2 node_6455_rnd = 4.789*sin(489.123*(node_6455_skew));
                float node_6455 = frac(node_6455_rnd.x*node_6455_rnd.y*(1+node_6455_skew.x));
                float node_986 = _vector6.r;
                float node_2390 = _vector6.g;
                float4 node_4234 = _Time + _TimeEditor;
                float node_708 = round(((node_986*node_2390)*frac((_vector6.b*(node_4234.g*_time_copy_copy)))));
                float2 node_5729 = ((i.uv0/float2(node_986,node_2390))+float2((fmod(node_708,node_986)/node_986),(1.0 - (floor((node_708/node_986))/node_2390))));
                float4 _node_3134_var = tex2D(_node_3134,TRANSFORM_TEX(node_5729, _node_3134));
                clip(((1.0*node_6455)*(node_6455+_node_3134_var.rgb)).r - 0.5);
////// Lighting:
////// Emissive:
                float4 _node_8445_var = tex2D(_node_8445,TRANSFORM_TEX(i.uv0, _node_8445));
                float3 emissive = _node_8445_var.rgb;
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform sampler2D _node_3134; uniform float4 _node_3134_ST;
            uniform float4 _vector6;
            uniform float _time_copy_copy;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float2 node_6455_skew = i.uv0 + 0.2127+i.uv0.x*0.3713*i.uv0.y;
                float2 node_6455_rnd = 4.789*sin(489.123*(node_6455_skew));
                float node_6455 = frac(node_6455_rnd.x*node_6455_rnd.y*(1+node_6455_skew.x));
                float node_986 = _vector6.r;
                float node_2390 = _vector6.g;
                float4 node_4234 = _Time + _TimeEditor;
                float node_708 = round(((node_986*node_2390)*frac((_vector6.b*(node_4234.g*_time_copy_copy)))));
                float2 node_5729 = ((i.uv0/float2(node_986,node_2390))+float2((fmod(node_708,node_986)/node_986),(1.0 - (floor((node_708/node_986))/node_2390))));
                float4 _node_3134_var = tex2D(_node_3134,TRANSFORM_TEX(node_5729, _node_3134));
                clip(((1.0*node_6455)*(node_6455+_node_3134_var.rgb)).r - 0.5);
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
