﻿using UnityEngine;
using System.Collections;

public class SecretWallUnlocker : MonoBehaviour {

    public int reqDifficultyToUnlock = 1;

	// Use this for initialization
	void Start ()
    {
        if (GameDifficulty.selectedDifficulty == reqDifficultyToUnlock)
        {
            this.gameObject.SetActive(false);
        }	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
