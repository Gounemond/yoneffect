// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:2565,x:33762,y:32386,varname:node_2565,prsc:2|emission-4437-OUT;n:type:ShaderForge.SFN_TexCoord,id:2647,x:31871,y:32601,varname:node_2647,prsc:2,uv:0;n:type:ShaderForge.SFN_RemapRange,id:9083,x:32075,y:32601,varname:node_9083,prsc:2,frmn:0,frmx:1,tomn:-1,tomx:1|IN-2647-UVOUT;n:type:ShaderForge.SFN_Multiply,id:7348,x:32264,y:32601,varname:node_7348,prsc:2|A-9083-OUT,B-9083-OUT;n:type:ShaderForge.SFN_ComponentMask,id:9718,x:32421,y:32629,varname:node_9718,prsc:2,cc1:0,cc2:1,cc3:-1,cc4:-1|IN-7348-OUT;n:type:ShaderForge.SFN_Add,id:9553,x:32640,y:32629,varname:node_9553,prsc:2|A-9718-R,B-9718-G;n:type:ShaderForge.SFN_Slider,id:4963,x:32157,y:32904,ptovrint:False,ptlb:u,ptin:_u,varname:node_4963,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Panner,id:5962,x:32704,y:32467,varname:node_5962,prsc:2,spu:1,spv:0|UVIN-3798-UVOUT,DIST-4963-OUT;n:type:ShaderForge.SFN_Slider,id:657,x:32561,y:33048,ptovrint:False,ptlb:v,ptin:_v,varname:node_657,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:7.410256,max:10;n:type:ShaderForge.SFN_Panner,id:7620,x:32877,y:32467,varname:node_7620,prsc:2,spu:0,spv:1|UVIN-5962-UVOUT,DIST-657-OUT;n:type:ShaderForge.SFN_TexCoord,id:3798,x:32421,y:32454,varname:node_3798,prsc:2,uv:0;n:type:ShaderForge.SFN_Multiply,id:919,x:33043,y:32611,varname:node_919,prsc:2|A-7620-UVOUT,B-9553-OUT;n:type:ShaderForge.SFN_Color,id:7210,x:33091,y:32358,ptovrint:False,ptlb:node_7210,ptin:_node_7210,varname:node_7210,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:1,c3:0.1310346,c4:1;n:type:ShaderForge.SFN_Multiply,id:4638,x:33333,y:32449,varname:node_4638,prsc:2|A-7210-RGB,B-919-OUT;n:type:ShaderForge.SFN_Add,id:4437,x:33537,y:32358,varname:node_4437,prsc:2|A-7648-RGB,B-4638-OUT;n:type:ShaderForge.SFN_Color,id:7648,x:33318,y:32159,ptovrint:False,ptlb:node_7648,ptin:_node_7648,varname:node_7648,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.7867647,c2:0.02892515,c3:0.02892515,c4:1;proporder:4963-657-7210-7648;pass:END;sub:END;*/

Shader "Custom/ygbbase" {
    Properties {
        _u ("u", Range(0, 1)) = 0
        _v ("v", Range(0, 10)) = 7.410256
        _node_7210 ("node_7210", Color) = (0,1,0.1310346,1)
        _node_7648 ("node_7648", Color) = (0.7867647,0.02892515,0.02892515,1)
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float _u;
            uniform float _v;
            uniform float4 _node_7210;
            uniform float4 _node_7648;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                UNITY_FOG_COORDS(1)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
////// Lighting:
////// Emissive:
                float2 node_9083 = (i.uv0*2.0+-1.0);
                float2 node_9718 = (node_9083*node_9083).rg;
                float3 emissive = (_node_7648.rgb+(_node_7210.rgb*float3((((i.uv0+_u*float2(1,0))+_v*float2(0,1))*(node_9718.r+node_9718.g)),0.0)));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
