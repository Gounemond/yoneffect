// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:1954,x:33530,y:32674,varname:node_1954,prsc:2|emission-9186-OUT;n:type:ShaderForge.SFN_Tex2d,id:4114,x:33057,y:32984,ptovrint:False,ptlb:Distorsion TEX,ptin:_DistorsionTEX,varname:node_4114,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:ed022fea07aefa64192fa64c637f2690,ntxv:0,isnm:False|UVIN-1012-UVOUT;n:type:ShaderForge.SFN_Rotator,id:4018,x:32665,y:32950,varname:node_4018,prsc:2|UVIN-9465-UVOUT,ANG-6354-OUT,SPD-638-OUT;n:type:ShaderForge.SFN_TexCoord,id:9465,x:32118,y:32823,varname:node_9465,prsc:2,uv:0;n:type:ShaderForge.SFN_Multiply,id:1331,x:32118,y:32983,varname:node_1331,prsc:2|A-5868-OUT,B-8224-OUT;n:type:ShaderForge.SFN_Multiply,id:6354,x:32372,y:33123,varname:node_6354,prsc:2|A-1331-OUT,B-4756-OUT;n:type:ShaderForge.SFN_Tau,id:4756,x:32151,y:33113,varname:node_4756,prsc:2;n:type:ShaderForge.SFN_Multiply,id:8224,x:31899,y:33077,varname:node_8224,prsc:2|A-7166-OUT,B-4055-OUT;n:type:ShaderForge.SFN_Slider,id:4055,x:31474,y:33155,ptovrint:False,ptlb:time,ptin:_time,varname:node_4055,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:1,cur:1,max:10;n:type:ShaderForge.SFN_OneMinus,id:5868,x:31899,y:32944,varname:node_5868,prsc:2|IN-641-OUT;n:type:ShaderForge.SFN_Multiply,id:641,x:31646,y:32828,varname:node_641,prsc:2|A-370-OUT,B-2597-OUT;n:type:ShaderForge.SFN_Vector1,id:2597,x:31386,y:32878,varname:node_2597,prsc:2,v1:2;n:type:ShaderForge.SFN_Distance,id:370,x:31386,y:32715,varname:node_370,prsc:2|A-3456-UVOUT,B-7137-OUT;n:type:ShaderForge.SFN_Vector2,id:7137,x:31172,y:32814,varname:node_7137,prsc:2,v1:0.5,v2:0.5;n:type:ShaderForge.SFN_TexCoord,id:3456,x:31172,y:32653,varname:node_3456,prsc:2,uv:0;n:type:ShaderForge.SFN_Color,id:7233,x:32610,y:32472,ptovrint:False,ptlb:Color Nube Light,ptin:_ColorNubeLight,varname:node_7233,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0,c3:0.8078432,c4:1;n:type:ShaderForge.SFN_Blend,id:9186,x:33250,y:32709,varname:node_9186,prsc:2,blmd:14,clmp:True|SRC-2963-OUT,DST-4114-RGB;n:type:ShaderForge.SFN_Tex2d,id:6470,x:32610,y:32665,ptovrint:False,ptlb:Nube TEX,ptin:_NubeTEX,varname:node_6470,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:ae0049a4a1783f946ac225627cf79c28,ntxv:0,isnm:False|UVIN-6873-UVOUT;n:type:ShaderForge.SFN_Multiply,id:3372,x:32840,y:32741,varname:node_3372,prsc:2|A-7233-RGB,B-6470-RGB;n:type:ShaderForge.SFN_Lerp,id:2963,x:33098,y:32593,varname:node_2963,prsc:2|A-5779-RGB,B-3372-OUT,T-6470-RGB;n:type:ShaderForge.SFN_Color,id:5779,x:32812,y:32478,ptovrint:False,ptlb:Color Nube Dark,ptin:_ColorNubeDark,varname:node_5779,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5490196,c2:0,c3:0.4431373,c4:1;n:type:ShaderForge.SFN_Rotator,id:6873,x:32394,y:32472,varname:node_6873,prsc:2|UVIN-6806-UVOUT,SPD-2484-OUT;n:type:ShaderForge.SFN_TexCoord,id:6806,x:31923,y:32426,varname:node_6806,prsc:2,uv:0;n:type:ShaderForge.SFN_Slider,id:2484,x:31923,y:32611,ptovrint:False,ptlb:Rotaor Nube,ptin:_RotaorNube,varname:node_2484,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-0.1,cur:-0.05,max:1;n:type:ShaderForge.SFN_Slider,id:638,x:32215,y:33290,ptovrint:False,ptlb:SPEED,ptin:_SPEED,varname:node_638,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.01,max:1;n:type:ShaderForge.SFN_Vector1,id:7166,x:31631,y:33043,varname:node_7166,prsc:2,v1:20;n:type:ShaderForge.SFN_Panner,id:1012,x:32862,y:32984,varname:node_1012,prsc:2,spu:0.1,spv:0.1|UVIN-4018-UVOUT;proporder:4114-4055-7233-6470-5779-2484-638;pass:END;sub:END;*/

Shader "Custom/skydome2" {
    Properties {
        _DistorsionTEX ("Distorsion TEX", 2D) = "white" {}
        _time ("time", Range(1, 10)) = 1
        _ColorNubeLight ("Color Nube Light", Color) = (1,0,0.8078432,1)
        _NubeTEX ("Nube TEX", 2D) = "white" {}
        _ColorNubeDark ("Color Nube Dark", Color) = (0.5490196,0,0.4431373,1)
        _RotaorNube ("Rotaor Nube", Range(-0.1, 1)) = -0.05
        _SPEED ("SPEED", Range(0, 1)) = 0.01
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform sampler2D _DistorsionTEX; uniform float4 _DistorsionTEX_ST;
            uniform float _time;
            uniform float4 _ColorNubeLight;
            uniform sampler2D _NubeTEX; uniform float4 _NubeTEX_ST;
            uniform float4 _ColorNubeDark;
            uniform float _RotaorNube;
            uniform float _SPEED;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                UNITY_FOG_COORDS(1)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
////// Lighting:
////// Emissive:
                float4 node_1528 = _Time + _TimeEditor;
                float node_6873_ang = node_1528.g;
                float node_6873_spd = _RotaorNube;
                float node_6873_cos = cos(node_6873_spd*node_6873_ang);
                float node_6873_sin = sin(node_6873_spd*node_6873_ang);
                float2 node_6873_piv = float2(0.5,0.5);
                float2 node_6873 = (mul(i.uv0-node_6873_piv,float2x2( node_6873_cos, -node_6873_sin, node_6873_sin, node_6873_cos))+node_6873_piv);
                float4 _NubeTEX_var = tex2D(_NubeTEX,TRANSFORM_TEX(node_6873, _NubeTEX));
                float node_4018_ang = (((1.0 - (distance(i.uv0,float2(0.5,0.5))*2.0))*(20.0*_time))*6.28318530718);
                float node_4018_spd = _SPEED;
                float node_4018_cos = cos(node_4018_spd*node_4018_ang);
                float node_4018_sin = sin(node_4018_spd*node_4018_ang);
                float2 node_4018_piv = float2(0.5,0.5);
                float2 node_4018 = (mul(i.uv0-node_4018_piv,float2x2( node_4018_cos, -node_4018_sin, node_4018_sin, node_4018_cos))+node_4018_piv);
                float2 node_1012 = (node_4018+node_1528.g*float2(0.1,0.1));
                float4 _DistorsionTEX_var = tex2D(_DistorsionTEX,TRANSFORM_TEX(node_1012, _DistorsionTEX));
                float3 emissive = saturate(( lerp(_ColorNubeDark.rgb,(_ColorNubeLight.rgb*_NubeTEX_var.rgb),_NubeTEX_var.rgb) > 0.5 ? (_DistorsionTEX_var.rgb + 2.0*lerp(_ColorNubeDark.rgb,(_ColorNubeLight.rgb*_NubeTEX_var.rgb),_NubeTEX_var.rgb) -1.0) : (_DistorsionTEX_var.rgb + 2.0*(lerp(_ColorNubeDark.rgb,(_ColorNubeLight.rgb*_NubeTEX_var.rgb),_NubeTEX_var.rgb)-0.5))));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
