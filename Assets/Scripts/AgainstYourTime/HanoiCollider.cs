﻿using UnityEngine;
using System;
using VRStandardAssets.Utils;
using System.Collections;

[RequireComponent (typeof(BoxCollider))]
public class HanoiCollider : MonoBehaviour
{

    public event Action onCollisionEnter;              // Called when a Hanoi Object enters the collider
    public event Action onCollisionExit;               // Called when a Hanoi Object enters the collider
    public event Action successfullyPositioned;        // Called when the Hanoi Object is positioned in the ending zone
    public event Action successfullyRemoved;        // Called when the Hanoi Object is positioned in the ending zone

    public Transform targetZone;

    private int objectsInCollider = 0;

    // Use this for initialization
    void Awake ()
    {

    }

    // Update is called once per frame
    void Update () {
	
	}

    void OnCollisionEnter(Collision collision)
    { 
        if (collision.collider.tag =="Hanoi")
        {
            if (collision.gameObject.transform.position.y < transform.position.y)
            {
                if (collision.gameObject.transform.localScale.x < transform.localScale.x)
                {
                    if (onCollisionEnter != null)
                    {
                        onCollisionEnter();
                    }
                }
                else
                {
                    collision.gameObject.GetComponent<VRHanoiGrab>().enabled = false;
                }
                
            }
        }
    }

    void OnCollisionExit(Collision collision)
    {
        if (collision.collider.tag == "Hanoi")
        {
            if (collision.gameObject.transform.position.y < transform.position.y)
            {
                if (collision.gameObject.transform.localScale.x > transform.localScale.x)
                {
                    collision.gameObject.GetComponent<VRHanoiGrab>().enabled = true;
                }
            }
        }
    }
}
