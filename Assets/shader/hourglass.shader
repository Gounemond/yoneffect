// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.16 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.16;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:8626,x:33267,y:32597,varname:node_8626,prsc:2|emission-2974-OUT,olwid-5238-OUT,olcol-1700-RGB;n:type:ShaderForge.SFN_Tex2d,id:3340,x:32238,y:32847,ptovrint:False,ptlb:textureHourglass,ptin:_textureHourglass,varname:node_3340,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:38bcaafa59d8ca042a970aae3a65f871,ntxv:0,isnm:False|UVIN-5800-UVOUT;n:type:ShaderForge.SFN_Panner,id:5800,x:32000,y:32826,varname:node_5800,prsc:2,spu:1,spv:1|DIST-3112-OUT;n:type:ShaderForge.SFN_Slider,id:5563,x:31442,y:32836,ptovrint:False,ptlb:sand_slider,ptin:_sand_slider,varname:node_5563,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:0.5;n:type:ShaderForge.SFN_Lerp,id:9133,x:32523,y:32632,varname:node_9133,prsc:2|A-5645-RGB,B-3091-RGB,T-3340-RGB;n:type:ShaderForge.SFN_Color,id:5645,x:32037,y:32382,ptovrint:False,ptlb:color1,ptin:_color1,varname:node_5645,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.3647059,c2:0.4470589,c3:0.6705883,c4:1;n:type:ShaderForge.SFN_Color,id:3091,x:32020,y:32642,ptovrint:False,ptlb:color2,ptin:_color2,varname:node_3091,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.6705883,c2:0.8666667,c3:0.1333333,c4:1;n:type:ShaderForge.SFN_Fresnel,id:9624,x:32348,y:33112,varname:node_9624,prsc:2|NRM-5362-OUT,EXP-4744-OUT;n:type:ShaderForge.SFN_NormalVector,id:5362,x:31859,y:33053,prsc:2,pt:False;n:type:ShaderForge.SFN_Vector1,id:4744,x:31906,y:33206,varname:node_4744,prsc:2,v1:2;n:type:ShaderForge.SFN_OneMinus,id:5371,x:32572,y:33112,varname:node_5371,prsc:2|IN-9624-OUT;n:type:ShaderForge.SFN_Color,id:1986,x:32561,y:32411,ptovrint:False,ptlb:glow,ptin:_glow,varname:node_1986,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.9632353,c2:0.9632353,c3:0.9632353,c4:1;n:type:ShaderForge.SFN_Lerp,id:2974,x:32714,y:32629,varname:node_2974,prsc:2|A-1986-RGB,B-9133-OUT,T-5371-OUT;n:type:ShaderForge.SFN_OneMinus,id:3112,x:31772,y:32848,varname:node_3112,prsc:2|IN-5563-OUT;n:type:ShaderForge.SFN_Color,id:1700,x:33055,y:33072,ptovrint:False,ptlb:coloroutline,ptin:_coloroutline,varname:node_1700,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:1,c3:0.9172413,c4:1;n:type:ShaderForge.SFN_Slider,id:5238,x:32810,y:32900,ptovrint:False,ptlb:slideroutlinehourglass,ptin:_slideroutlinehourglass,varname:node_5238,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0.01,cur:0.01,max:1;proporder:3340-5563-5645-3091-1986-1700-5238;pass:END;sub:END;*/

Shader "Custom/hourglass" {
    Properties {
        _textureHourglass ("textureHourglass", 2D) = "white" {}
        _sand_slider ("sand_slider", Range(0, 0.5)) = 0
        _color1 ("color1", Color) = (0.3647059,0.4470589,0.6705883,1)
        _color2 ("color2", Color) = (0.6705883,0.8666667,0.1333333,1)
        _glow ("glow", Color) = (0.9632353,0.9632353,0.9632353,1)
        _coloroutline ("coloroutline", Color) = (0,1,0.9172413,1)
        _slideroutlinehourglass ("slideroutlinehourglass", Range(0.01, 1)) = 0.01
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "Outline"
            Tags {
            }
            Cull Front
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _coloroutline;
            uniform float _slideroutlinehourglass;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                UNITY_FOG_COORDS(0)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.pos = mul(UNITY_MATRIX_MVP, float4(v.vertex.xyz + v.normal*_slideroutlinehourglass,1));
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
                return fixed4(_coloroutline.rgb,0);
            }
            ENDCG
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform sampler2D _textureHourglass; uniform float4 _textureHourglass_ST;
            uniform float _sand_slider;
            uniform float4 _color1;
            uniform float4 _color2;
            uniform float4 _glow;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                UNITY_FOG_COORDS(3)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
/////// Vectors:
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float2 node_5800 = (i.uv0+(1.0 - _sand_slider)*float2(1,1));
                float4 _textureHourglass_var = tex2D(_textureHourglass,TRANSFORM_TEX(node_5800, _textureHourglass));
                float3 emissive = lerp(_glow.rgb,lerp(_color1.rgb,_color2.rgb,_textureHourglass_var.rgb),(1.0 - pow(1.0-max(0,dot(i.normalDir, viewDirection)),2.0)));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
