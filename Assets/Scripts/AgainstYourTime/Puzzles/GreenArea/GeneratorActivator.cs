using UnityEngine;
using VRStandardAssets.Utils;
using System.Collections;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System;
using System.Linq;

public class GeneratorActivator : MonoBehaviour 
{
    public Animator generatorAnimator;                  // The animator of the generator
    public VRInteractiveItem myVRitem;                  // The Interactive Item (where the collider which will react to the user gaze is located
    public GameObject generatorCrystal;                 // The object which will need to apply another shader
    public GameObject crystalLight;                     

    public event Action onGeneratorActivate;            // Action triggered when the generator is activated

    public AudioSource audioEmitter;                    // AudioSource of the generator
    public AudioClip soundCorrectActivation;            // Audioclip for correct activation
    public AudioClip soundWrongActivation;              // Audioclip for wrong activation
    public AudioClip soundHoverIn;                      // Audioclip for looking on the generator
    public AudioClip soundHoverOut;                     // Audioclip for looking out the generator

    public Transform spawnYonGear;                      // The initial position where the yonGear spawns
    public Transform destionationYonGear1;              // The destination position of the gear1
    public Transform destionationYonGear2;              // The destination position of the gear2

    private bool onHover = false;                       // Flag used when looking on the generator                      

    private Material _crystalMaterial;                  // The material of the crystal to edit



    public void EnableGenerator()
    {
        myVRitem.OnClick += ActivateGenerator;
        myVRitem.OnOver += HoverIn;
        myVRitem.OnOut += HoverOut;
    }

    public void DisableGenerator()
    {
        myVRitem.OnClick -= ActivateGenerator;
        myVRitem.OnOver -= HoverIn;
        myVRitem.OnOut -= HoverOut;
    }

    void Awake () 
	{
        _crystalMaterial = generatorCrystal.GetComponent<Renderer>().material;
        _crystalMaterial.SetFloat("_fresnel", 0.5f);
    }

    public void ActivateGenerator()
    {
        StartCoroutine(ActivateGeneratorRoutine());
    }

    public IEnumerator ActivateGeneratorRoutine()
    {
        if (onHover)
        {
            DisableGenerator();
            _crystalMaterial.SetFloat("_fresnel", 0.5f);
            if (onGeneratorActivate != null)
            {
                // It will set corret or wrong, based if the generator is the right one in the sequence
                onGeneratorActivate();
            }
            yield return new WaitForSeconds(3);
            generatorAnimator.SetBool("Correct", false);
            generatorAnimator.SetBool("Wrong", false);
            yield return new WaitForSeconds(1);
            EnableGenerator();

        }
    }

    public void HoverIn()
    {
        _crystalMaterial.SetFloat("_fresnel", 0.15f);
        audioEmitter.PlayOneShot(soundHoverIn);
        onHover = true;
    }

    public void HoverOut()
    {
        _crystalMaterial.SetFloat("_fresnel", 0.5f);
        audioEmitter.PlayOneShot(soundHoverOut);
        onHover = false;
    }

    public void CorrectActivation()
    {
        generatorAnimator.SetBool("Correct", true);
        _crystalMaterial.SetFloat("_you_win", 1f);
        crystalLight.SetActive(true);
        StartCoroutine(WaitAndRestoreMaterial(3));
        HoverOut();
        audioEmitter.volume = 0.4f;        
        audioEmitter.PlayOneShot(soundCorrectActivation);
    }

    public void WrongActivation()
    {
        generatorAnimator.SetBool("Wrong", true);
        _crystalMaterial.SetFloat("_You_Wrong_VO", 1);
        _crystalMaterial.SetFloat("_You_Wrong_Clr", 1);
        StartCoroutine(WaitAndRestoreMaterial(2));
        HoverOut();
        audioEmitter.volume = 0.6f;
        audioEmitter.PlayOneShot(soundWrongActivation);
    }

    public IEnumerator WaitAndRestoreMaterial(float time)
    {
        float timer = 0;

        while (timer < time)
        {
            timer += Time.deltaTime;
            yield return null;
        }

        crystalLight.SetActive(false);
        _crystalMaterial.SetFloat("_you_win", 0f);
        _crystalMaterial.SetFloat("_You_Wrong_VO", 0);
        _crystalMaterial.SetFloat("_You_Wrong_Clr", 0);
    }
}