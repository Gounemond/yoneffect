﻿using UnityEngine;
using System;
using System.Collections;
using UI.StepsVR;
using UnityEngine.VR;

public class CameraUI : MonoBehaviour
{

    private bool isOculus;

    private Pulsante over = null;

    private Vector3 tempVector = new Vector3();

    public GameObject cursore;

    public TextMesh prova;

    public Camera cameraUi;

    public bool checkArgomenti = true;

    void Awake()
    {
        isOculus = VRSettings.loadedDevice == VRDeviceType.None ? false : true;
    }

    void Start()
    {

        

        if (checkArgomenti == true)
        {
            changeOption();
        }
    }

    // Use this for initialization
    void OnEnable ()
    {
        
        if (isOculus == true)
        {
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }
        else
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }

        
        

        tempVector.x = cameraUi.pixelWidth / 2;
        tempVector.y = cameraUi.pixelHeight / 2;

        cursore.SetActive(isOculus);
        
    }

    void changeOption()
    {
        string[] argomenti = System.Environment.GetCommandLineArgs();

        if (argomenti.Length == 0)
        {
            return;
        }

        try
        {
            //prova.text = "" + argomenti[1];

            PlayerPrefs.SetString("masterPath", argomenti[5]);
            
            int quality = int.Parse(argomenti[1].Trim());

            QualitySettings.SetQualityLevel(quality);

            if (isOculus == false)
            {
                //prova.text = "oculus False";

                bool fullScreen = bool.Parse(argomenti[2]);
                int width = int.Parse(argomenti[3]);
                int height = int.Parse(argomenti[4]);

                bool riavvia = false;

                if (Screen.width != width
                    || Screen.height != height
                    || Screen.fullScreen != fullScreen)
                {
                    riavvia = true;
                }

                Screen.SetResolution(width, height, fullScreen);

                //Application.LoadLevel(Application.loadedLevel);

                if(riavvia==true)
                {
//                    System.Diagnostics.Process.Start(argomenti[0]);
                    Application.Quit();
                }
                
            }
            else
            {
                //prova.text = "oculus true";
            }

            //prova.text = "ok";

        }
        catch (Exception e)
        {
            prova.text = ""+e.Message+"_____"+ argomenti[0]+"____";

        }
    }

    private bool oldFireValue = false;

    public LayerMask layer;

    // Update is called once per frame
    void Update ()
    {
        

        if (isOculus == true)
        {

            RaycastHit hit;

            //Debug.DrawRay(Camera.main.ScreenToWorldPoint(tempVector), Camera.main.transform.forward*20, Color.red);

            //if (Physics.Raycast(cameraUi.ScreenToWorldPoint(tempVector), cameraUi.transform.forward,out hit, 20))
            if (Physics.Raycast(cameraUi.ScreenToWorldPoint(tempVector), cameraUi.transform.forward, out hit, 20, layer))
            {
                Pulsante pulsante = hit.collider.GetComponent<Pulsante>();

                if (pulsante != null)
                {
                    if (over == null)
                    {
                        over = pulsante;
                        pulsante.OnMouseEnter();
                    }

                    if (Input.GetButtonDown("Fire1")
                        || (oldFireValue == false
                                && Input.GetAxis("Fire1") > 0f)
                        || Input.GetButtonDown("Jump"))
                    {
                        pulsante.azione_click();
                    }

                    oldFireValue = Input.GetAxis("Fire1") > 0f;
                    return;
                }

            }

            if (over != null)
            {
                over.OnMouseExit();
            }
            over = null;

        }

        oldFireValue = Input.GetAxis("Fire1") > 0f;
    }
}
