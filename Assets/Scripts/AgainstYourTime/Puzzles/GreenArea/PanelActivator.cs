﻿using UnityEngine;
using System.Collections;
using VRStandardAssets.Utils;
using DG.Tweening;
using System;

public class PanelActivator : MonoBehaviour
{
    public GameObject[] number;
    public GameObject[] crystal;
    public GameObject[] yonGears;
    public int IndexPlaying = 0;

    private Material m_MyMaterial;

    public Color HoverInColor;
    private Color m_OriginalColor;

    public Color yonGearInactiveColor;
    private Color m_YonGearActiveColor;

    public VRInteractiveItem myVRItem;

    void Awake()
    {
        m_MyMaterial = myVRItem.GetComponent<Renderer>().material;
        m_OriginalColor = m_MyMaterial.GetColor("_node_3094");
        m_YonGearActiveColor = yonGears[0].GetComponent<Renderer>().materials[1].GetColor("_node_4897");
    }

    // Use this for initialization
    void Start ()
    {
        for (int i = 0; i < yonGears.Length; i++)
        {
            yonGears[i].GetComponent<Renderer>().materials[1].SetColor("_node_4897", yonGearInactiveColor);
        }
        for (int i = 0; i < number.Length; i++)
        {
            number[i].SetActive(false);
            crystal[i].SetActive(false);
        }
        EnableInteractivity();
    }

    public void EnableInteractivity()
    {
        myVRItem.OnOver += HoverIn;
        myVRItem.OnOut += HoverOut;
    }

    public void DisableInteractivity()
    {
        myVRItem.OnOver -= HoverIn;
        HoverOut();
        myVRItem.OnOut -= HoverOut;
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void HoverIn()
    {
        myVRItem.GetComponent<Renderer>().material.SetColor("_node_3094", HoverInColor);
    }

    public void HoverOut()
    {
        myVRItem.GetComponent<Renderer>().material.SetColor("_node_3094", m_OriginalColor);
    }

    public void TurnOnNumber()
    {
        number[IndexPlaying].SetActive(true);
        crystal[IndexPlaying].SetActive(true);
    }

    public void TurnOffNumber()
    {
        crystal[IndexPlaying].SetActive(false);
    }

    public void ResetStatus()
    {
        for (int i = 0; i < number.Length; i++)
        {
            number[i].SetActive(false);
            crystal[i].SetActive(false);
        }

    }

    public void Disappear()
    {
        myVRItem.transform.DOMoveZ(-1.5f, 1);
        this.transform.DOMoveY(-1.5f, 1);
    }

    public void SetActiveGears(int number)
    {
        for (int i = 0; i < number; i++)
        {
            yonGears[i].GetComponent<Renderer>().materials[1].SetColor("_node_4897", m_YonGearActiveColor);
        }
    }
}
