// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:127,x:33717,y:32739,varname:node_127,prsc:2|emission-5800-OUT;n:type:ShaderForge.SFN_Color,id:6426,x:32244,y:32567,ptovrint:False,ptlb:Green,ptin:_Green,varname:node_6426,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:1,c3:0,c4:1;n:type:ShaderForge.SFN_Color,id:4149,x:32262,y:32820,ptovrint:False,ptlb:Red,ptin:_Red,varname:node_4149,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0,c3:0,c4:1;n:type:ShaderForge.SFN_Color,id:904,x:32262,y:33036,ptovrint:False,ptlb:Blue,ptin:_Blue,varname:node_904,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0,c3:1,c4:1;n:type:ShaderForge.SFN_Color,id:9095,x:32475,y:32727,ptovrint:False,ptlb:orange,ptin:_orange,varname:node_9095,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0.5490196,c3:0,c4:1;n:type:ShaderForge.SFN_Color,id:2648,x:32489,y:33158,ptovrint:False,ptlb:Fabulous,ptin:_Fabulous,varname:node_2648,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0,c3:1,c4:1;n:type:ShaderForge.SFN_Color,id:6286,x:32249,y:33282,ptovrint:False,ptlb:Yellow,ptin:_Yellow,varname:node_6286,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:0,c4:1;n:type:ShaderForge.SFN_Color,id:400,x:32475,y:32938,ptovrint:False,ptlb:background,ptin:_background,varname:node_400,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0,c3:0,c4:1;n:type:ShaderForge.SFN_SwitchProperty,id:3245,x:33002,y:32567,ptovrint:False,ptlb:Green_switch,ptin:_Green_switch,varname:node_3245,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:False|A-400-RGB,B-6426-RGB;n:type:ShaderForge.SFN_SwitchProperty,id:6314,x:33002,y:32752,ptovrint:False,ptlb:Orange_switch,ptin:_Orange_switch,varname:node_6314,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:False|A-400-RGB,B-9095-RGB;n:type:ShaderForge.SFN_SwitchProperty,id:5341,x:33002,y:32940,ptovrint:False,ptlb:Red_switch,ptin:_Red_switch,varname:node_5341,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:False|A-400-RGB,B-4149-RGB;n:type:ShaderForge.SFN_SwitchProperty,id:9166,x:33002,y:33312,ptovrint:False,ptlb:Fabulous_switch,ptin:_Fabulous_switch,varname:node_9166,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:False|A-400-RGB,B-2648-RGB;n:type:ShaderForge.SFN_SwitchProperty,id:6145,x:33002,y:33510,ptovrint:False,ptlb:Yellow_switch,ptin:_Yellow_switch,varname:node_6145,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:False|A-400-RGB,B-6286-RGB;n:type:ShaderForge.SFN_SwitchProperty,id:3579,x:33002,y:33110,ptovrint:False,ptlb:Blue_switch,ptin:_Blue_switch,varname:node_3579,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:False|A-400-RGB,B-904-RGB;n:type:ShaderForge.SFN_Add,id:4985,x:33249,y:32668,varname:node_4985,prsc:2|A-3245-OUT,B-6314-OUT,C-5341-OUT,D-3579-OUT;n:type:ShaderForge.SFN_Add,id:1920,x:33320,y:32993,varname:node_1920,prsc:2|A-9166-OUT,B-6145-OUT,C-8238-OUT;n:type:ShaderForge.SFN_Add,id:5800,x:33472,y:32796,varname:node_5800,prsc:2|A-4985-OUT,B-1920-OUT;n:type:ShaderForge.SFN_Color,id:1332,x:32516,y:33475,ptovrint:False,ptlb:main,ptin:_main,varname:node_1332,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5254902,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_SwitchProperty,id:8238,x:33025,y:33680,ptovrint:False,ptlb:main_switch,ptin:_main_switch,varname:node_8238,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:False|A-400-RGB,B-1332-RGB;proporder:6426-4149-904-9095-2648-6286-400-3245-6314-5341-9166-6145-3579-1332-8238;pass:END;sub:END;*/

Shader "Custom/crystalselection" {
    Properties {
        _Green ("Green", Color) = (0,1,0,1)
        _Red ("Red", Color) = (1,0,0,1)
        _Blue ("Blue", Color) = (0,0,1,1)
        _orange ("orange", Color) = (1,0.5490196,0,1)
        _Fabulous ("Fabulous", Color) = (1,0,1,1)
        _Yellow ("Yellow", Color) = (1,1,0,1)
        _background ("background", Color) = (0,0,0,1)
        [MaterialToggle] _Green_switch ("Green_switch", Float ) = 0
        [MaterialToggle] _Orange_switch ("Orange_switch", Float ) = 0
        [MaterialToggle] _Red_switch ("Red_switch", Float ) = 0
        [MaterialToggle] _Fabulous_switch ("Fabulous_switch", Float ) = 0
        [MaterialToggle] _Yellow_switch ("Yellow_switch", Float ) = 0
        [MaterialToggle] _Blue_switch ("Blue_switch", Float ) = 0
        _main ("main", Color) = (0.5254902,1,1,1)
        [MaterialToggle] _main_switch ("main_switch", Float ) = 0
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _Green;
            uniform float4 _Red;
            uniform float4 _Blue;
            uniform float4 _orange;
            uniform float4 _Fabulous;
            uniform float4 _Yellow;
            uniform float4 _background;
            uniform fixed _Green_switch;
            uniform fixed _Orange_switch;
            uniform fixed _Red_switch;
            uniform fixed _Fabulous_switch;
            uniform fixed _Yellow_switch;
            uniform fixed _Blue_switch;
            uniform float4 _main;
            uniform fixed _main_switch;
            struct VertexInput {
                float4 vertex : POSITION;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                UNITY_FOG_COORDS(0)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
////// Lighting:
////// Emissive:
                float3 emissive = ((lerp( _background.rgb, _Green.rgb, _Green_switch )+lerp( _background.rgb, _orange.rgb, _Orange_switch )+lerp( _background.rgb, _Red.rgb, _Red_switch )+lerp( _background.rgb, _Blue.rgb, _Blue_switch ))+(lerp( _background.rgb, _Fabulous.rgb, _Fabulous_switch )+lerp( _background.rgb, _Yellow.rgb, _Yellow_switch )+lerp( _background.rgb, _main.rgb, _main_switch )));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
