// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:1627,x:33768,y:32641,varname:node_1627,prsc:2|emission-235-OUT;n:type:ShaderForge.SFN_Color,id:3111,x:32463,y:32652,ptovrint:False,ptlb:node_3947_copy,ptin:_node_3947_copy,varname:_node_3947_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0.8000001,c3:0.8313726,c4:1;n:type:ShaderForge.SFN_Power,id:202,x:32963,y:33106,varname:node_202,prsc:2|VAL-6169-OUT,EXP-5633-OUT;n:type:ShaderForge.SFN_Multiply,id:6169,x:32701,y:33064,varname:node_6169,prsc:2|A-7322-OUT,B-8131-OUT;n:type:ShaderForge.SFN_Panner,id:7311,x:31523,y:32630,varname:node_7311,prsc:2,spu:1,spv:1|UVIN-6100-OUT,DIST-1683-OUT;n:type:ShaderForge.SFN_TexCoord,id:1821,x:30966,y:32422,varname:node_1821,prsc:2,uv:0;n:type:ShaderForge.SFN_Vector1,id:8131,x:32049,y:33159,varname:node_8131,prsc:2,v1:1.3;n:type:ShaderForge.SFN_Vector1,id:5633,x:32686,y:33296,varname:node_5633,prsc:2,v1:15;n:type:ShaderForge.SFN_Subtract,id:1228,x:32049,y:32876,varname:node_1228,prsc:2|A-9865-OUT,B-2611-OUT;n:type:ShaderForge.SFN_Abs,id:7322,x:32167,y:33002,varname:node_7322,prsc:2|IN-1228-OUT;n:type:ShaderForge.SFN_Frac,id:6514,x:31776,y:32809,varname:node_6514,prsc:2|IN-3089-OUT;n:type:ShaderForge.SFN_ComponentMask,id:8287,x:31808,y:32642,varname:node_8287,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-7311-UVOUT;n:type:ShaderForge.SFN_Frac,id:9865,x:31802,y:32946,varname:node_9865,prsc:2|IN-6514-OUT;n:type:ShaderForge.SFN_Vector1,id:2611,x:31954,y:33077,varname:node_2611,prsc:2,v1:0.15;n:type:ShaderForge.SFN_Time,id:3405,x:31254,y:32806,varname:node_3405,prsc:2;n:type:ShaderForge.SFN_Multiply,id:1683,x:31514,y:32949,varname:node_1683,prsc:2|A-3405-T,B-753-OUT;n:type:ShaderForge.SFN_Slider,id:753,x:31175,y:33021,ptovrint:False,ptlb:timer_copy,ptin:_timer_copy,varname:_timer_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-1,cur:0.4059818,max:1;n:type:ShaderForge.SFN_Lerp,id:798,x:32977,y:32652,varname:node_798,prsc:2|A-3647-RGB,B-3111-RGB,T-8683-OUT;n:type:ShaderForge.SFN_Color,id:3647,x:32672,y:32413,ptovrint:False,ptlb:node_3094_copy,ptin:_node_3094_copy,varname:_node_3094_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.07843138,c2:0.07843138,c3:0.2352941,c4:1;n:type:ShaderForge.SFN_Multiply,id:6100,x:31342,y:32508,varname:node_6100,prsc:2|A-1821-UVOUT,B-564-OUT;n:type:ShaderForge.SFN_ValueProperty,id:564,x:31004,y:32680,ptovrint:False,ptlb:number_copy,ptin:_number_copy,varname:_number_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_SwitchProperty,id:3089,x:32062,y:32585,ptovrint:False,ptlb:direction_copy,ptin:_direction_copy,varname:_direction_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:False|A-452-OUT,B-8287-OUT;n:type:ShaderForge.SFN_ComponentMask,id:452,x:31795,y:32463,varname:node_452,prsc:2,cc1:1,cc2:-1,cc3:-1,cc4:-1|IN-7311-UVOUT;n:type:ShaderForge.SFN_ValueProperty,id:6220,x:32991,y:33300,ptovrint:False,ptlb:int opacity_copy,ptin:_intopacity_copy,varname:_intopacity_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.25;n:type:ShaderForge.SFN_Multiply,id:8683,x:33209,y:33168,varname:node_8683,prsc:2|A-202-OUT,B-6220-OUT;n:type:ShaderForge.SFN_Tex2d,id:8195,x:33297,y:32730,ptovrint:False,ptlb:node_8195,ptin:_node_8195,varname:node_8195,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:c221062867ae30f44ad77f6f65b2132d,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Lerp,id:235,x:33359,y:32441,varname:node_235,prsc:2|A-3647-RGB,B-798-OUT,T-8195-RGB;proporder:3111-3647-3089-6220-564-753-8195;pass:END;sub:END;*/

Shader "Custom/cvringpath" {
    Properties {
        _node_3947_copy ("node_3947_copy", Color) = (0,0.8000001,0.8313726,1)
        _node_3094_copy ("node_3094_copy", Color) = (0.07843138,0.07843138,0.2352941,1)
        [MaterialToggle] _direction_copy ("direction_copy", Float ) = 0
        _intopacity_copy ("int opacity_copy", Float ) = 0.25
        _number_copy ("number_copy", Float ) = 1
        _timer_copy ("timer_copy", Range(-1, 1)) = 0.4059818
        _node_8195 ("node_8195", 2D) = "white" {}
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform float4 _node_3947_copy;
            uniform float _timer_copy;
            uniform float4 _node_3094_copy;
            uniform float _number_copy;
            uniform fixed _direction_copy;
            uniform float _intopacity_copy;
            uniform sampler2D _node_8195; uniform float4 _node_8195_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                UNITY_FOG_COORDS(1)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
////// Lighting:
////// Emissive:
                float4 node_3405 = _Time + _TimeEditor;
                float2 node_7311 = ((i.uv0*_number_copy)+(node_3405.g*_timer_copy)*float2(1,1));
                float4 _node_8195_var = tex2D(_node_8195,TRANSFORM_TEX(i.uv0, _node_8195));
                float3 emissive = lerp(_node_3094_copy.rgb,lerp(_node_3094_copy.rgb,_node_3947_copy.rgb,(pow((abs((frac(frac(lerp( node_7311.g, node_7311.r, _direction_copy )))-0.15))*1.3),15.0)*_intopacity_copy)),_node_8195_var.rgb);
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
