// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:2,rntp:3,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:360,x:34358,y:32727,varname:node_360,prsc:2|emission-7407-OUT,clip-9398-OUT;n:type:ShaderForge.SFN_Color,id:8773,x:32471,y:32537,ptovrint:False,ptlb:main,ptin:_main,varname:node_8773,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0,c3:0.07450981,c4:1;n:type:ShaderForge.SFN_Color,id:3832,x:32466,y:32800,ptovrint:False,ptlb:selection,ptin:_selection,varname:node_3832,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.1193771,c2:0.5247557,c3:0.7058823,c4:1;n:type:ShaderForge.SFN_SwitchProperty,id:7407,x:33049,y:32690,ptovrint:False,ptlb:maintoselection,ptin:_maintoselection,varname:node_7407,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:False|A-9302-OUT,B-3412-OUT;n:type:ShaderForge.SFN_Multiply,id:9302,x:32833,y:32591,varname:node_9302,prsc:2|A-8773-RGB,B-4396-OUT;n:type:ShaderForge.SFN_Vector1,id:4396,x:32501,y:32692,varname:node_4396,prsc:2,v1:2;n:type:ShaderForge.SFN_Multiply,id:3412,x:32750,y:32805,varname:node_3412,prsc:2|A-3832-RGB,B-4475-OUT;n:type:ShaderForge.SFN_Vector1,id:4475,x:32466,y:32958,varname:node_4475,prsc:2,v1:4;n:type:ShaderForge.SFN_Blend,id:1043,x:32841,y:33536,varname:node_1043,prsc:2,blmd:16,clmp:True|SRC-8515-OUT,DST-5415-OUT;n:type:ShaderForge.SFN_TexCoord,id:6170,x:32083,y:33370,varname:node_6170,prsc:2,uv:0;n:type:ShaderForge.SFN_ComponentMask,id:6814,x:32330,y:33282,varname:node_6814,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-6170-UVOUT;n:type:ShaderForge.SFN_ComponentMask,id:4008,x:32334,y:33543,varname:node_4008,prsc:2,cc1:1,cc2:-1,cc3:-1,cc4:-1|IN-6170-UVOUT;n:type:ShaderForge.SFN_OneMinus,id:5436,x:32547,y:33106,varname:node_5436,prsc:2|IN-6814-OUT;n:type:ShaderForge.SFN_OneMinus,id:5415,x:32522,y:33623,varname:node_5415,prsc:2|IN-4008-OUT;n:type:ShaderForge.SFN_Blend,id:1489,x:32892,y:33316,varname:node_1489,prsc:2,blmd:16,clmp:True|SRC-8515-OUT,DST-4008-OUT;n:type:ShaderForge.SFN_Blend,id:142,x:32812,y:33148,varname:node_142,prsc:2,blmd:16,clmp:True|SRC-8515-OUT,DST-6814-OUT;n:type:ShaderForge.SFN_Blend,id:2531,x:32812,y:33000,varname:node_2531,prsc:2,blmd:16,clmp:True|SRC-8515-OUT,DST-5436-OUT;n:type:ShaderForge.SFN_Vector1,id:8515,x:32563,y:33318,varname:node_8515,prsc:2,v1:0.9;n:type:ShaderForge.SFN_Multiply,id:2873,x:33143,y:33207,varname:node_2873,prsc:2|A-2531-OUT,B-142-OUT,C-1489-OUT,D-1043-OUT;n:type:ShaderForge.SFN_OneMinus,id:9806,x:33416,y:33251,varname:node_9806,prsc:2|IN-2873-OUT;n:type:ShaderForge.SFN_ComponentMask,id:684,x:33673,y:33327,varname:node_684,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-9806-OUT;n:type:ShaderForge.SFN_SwitchProperty,id:6401,x:33865,y:33230,ptovrint:False,ptlb:Trasparent,ptin:_Trasparent,varname:node_6401,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:False|A-7460-OUT,B-684-OUT;n:type:ShaderForge.SFN_ComponentMask,id:9398,x:34003,y:33068,varname:node_9398,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-6401-OUT;n:type:ShaderForge.SFN_Vector1,id:7460,x:33673,y:33251,varname:node_7460,prsc:2,v1:1;proporder:8773-3832-7407-6401;pass:END;sub:END;*/

Shader "Custom/tangram" {
    Properties {
        _main ("main", Color) = (0,0,0.07450981,1)
        _selection ("selection", Color) = (0.1193771,0.5247557,0.7058823,1)
        [MaterialToggle] _maintoselection ("maintoselection", Float ) = 0
        [MaterialToggle] _Trasparent ("Trasparent", Float ) = 1
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "Queue"="AlphaTest"
            "RenderType"="TransparentCutout"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _main;
            uniform float4 _selection;
            uniform fixed _maintoselection;
            uniform fixed _Trasparent;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                UNITY_FOG_COORDS(1)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float node_8515 = 0.9;
                float node_6814 = i.uv0.r;
                float node_4008 = i.uv0.g;
                clip(lerp( 1.0, (1.0 - (saturate(round( 0.5*(node_8515 + (1.0 - node_6814))))*saturate(round( 0.5*(node_8515 + node_6814)))*saturate(round( 0.5*(node_8515 + node_4008)))*saturate(round( 0.5*(node_8515 + (1.0 - node_4008)))))).r, _Trasparent ).r - 0.5);
////// Lighting:
////// Emissive:
                float3 emissive = lerp( (_main.rgb*2.0), (_selection.rgb*4.0), _maintoselection );
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform fixed _Trasparent;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float node_8515 = 0.9;
                float node_6814 = i.uv0.r;
                float node_4008 = i.uv0.g;
                clip(lerp( 1.0, (1.0 - (saturate(round( 0.5*(node_8515 + (1.0 - node_6814))))*saturate(round( 0.5*(node_8515 + node_6814)))*saturate(round( 0.5*(node_8515 + node_4008)))*saturate(round( 0.5*(node_8515 + (1.0 - node_4008)))))).r, _Trasparent ).r - 0.5);
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
