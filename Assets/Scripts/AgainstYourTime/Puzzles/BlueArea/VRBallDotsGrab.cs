using UnityEngine;
using VRStandardAssets.Utils;
using System.Collections;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System;
using System.Linq;

[RequireComponent (typeof(VRInteractiveItem))]
[RequireComponent (typeof(Rigidbody))]
[RequireComponent (typeof(Collider))]
public class VRBallDotsGrab : MonoBehaviour 
{
	public float  correctionforce = 50.0f;              // The intensity of the gravity force toward the grabbing beacon
	public Transform beacon;                            // The beacon used as centre of gravity to grab and move around the object
    public ConnectTheDotsPuzzle connectTheDotsManager;     // Reference to the connectTheDotsManager

    public Material checkPointReachedMaterial;          // The material that will be set for a reached checkpoint

    [Header ("AudioSfx")]
    public AudioSource myAudioSource;
    public AudioClip reachedCheckpointAudio;
    public AudioClip resetPosAudio;
    public AudioClip hoverInAudio;
    public AudioClip hoverOutAudio;
    public AudioClip grabAudio;

    private Material m_MyMaterial;                      // The current material which will be modified upon various conditions
    private Color m_OriginalColor;                      // The original color of the material

    private Vector3 m_ResetPosition;                    // When exiting from the path, the ball will be sent back to this position
    private int m_PathCollisions = 0;                   // Keeps track of how many path colliders the ball is touching

	private bool  grabbed  = false;                     // Flag that indicates if the object is grabbed
    private bool  onHover = false;                      // Flag that indicates if the object is currently targeted by the player

    private Rigidbody m_MyRigidbody;                    // Reference to own rigidbody
    private VRInteractiveItem m_VRitem;                // Reference to the interactable script

	void Awake () 
	{
        m_MyRigidbody = GetComponent<Rigidbody>();
        m_VRitem = GetComponent<VRInteractiveItem>();
        m_MyMaterial = GetComponent<Renderer>().material;
        m_OriginalColor = m_MyMaterial.GetColor("_2color");
        m_ResetPosition = transform.position;
    }

    void OnEnable()
    {
        m_VRitem.OnClick += GrabSwitchState;
        m_VRitem.OnOver += HoverIn;
        m_VRitem.OnOut += HoverOut;
    }

    void OnDisable()
    {
        m_VRitem.OnClick -= GrabSwitchState;
        m_VRitem.OnOver -= HoverIn;
        m_VRitem.OnOut -= HoverOut;
    }

    void Update () 
	{
        if (grabbed && !onHover && Input.GetButton("Fire1"))
        {
            GrabSwitchState();
            connectTheDotsManager.switchObjectGrabbed(null);     // Tells the manager to set the object grabbed as null
        }
        // Update gravity force around the GrabBeacon if you actually have grabbed an object
        // The grab method is inspired from the Half Life series
        if (grabbed)
		{
			Vector3 force = new Vector3(beacon.transform.position.x - transform.position.x, beacon.transform.position.y - transform.position.y, beacon.transform.position.z - transform.position.z);
            m_MyRigidbody.velocity = force.normalized * m_MyRigidbody.velocity.magnitude;
            m_MyRigidbody.AddForce(force * correctionforce);
            m_MyRigidbody.velocity *= Mathf.Min(1.0f, force.magnitude / 2 );
		}
    }

    public void GrabSwitchState()
    {
        if (onHover)
        {
            if (!grabbed)
            {
                connectTheDotsManager.switchObjectGrabbed(this.gameObject);       // Tells the manages to set this object as grabbed
                myAudioSource.PlayOneShot(grabAudio);
                grabbed = true;                                                     // Sets grabbed state at true
            }
            else
            {
                connectTheDotsManager.switchObjectGrabbed(null);                  // Tells the manager to set the object grabbed as null
                grabbed = false;                                                    // The object is no more grabbed
            }
        }
        else
        {
            if (grabbed)
            {
                grabbed = false;                                                    // The object is no more grabbed
            }
        }
    }

    public void HoverIn()
    {
        m_MyMaterial.SetColor("_2color", Color.white);
        myAudioSource.PlayOneShot(hoverInAudio);
        onHover = true; 
    }

    public void HoverOut()
    {
        m_MyMaterial.SetColor("_2color", m_OriginalColor);
        myAudioSource.PlayOneShot(hoverOutAudio);
        onHover = false;
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "ConnectTheDotPath")
        {
            m_PathCollisions++;
        }

        if (other.tag == "ConnectTheDotCheckpoint")
        {
            m_ResetPosition = other.transform.position;
            myAudioSource.PlayOneShot(reachedCheckpointAudio);
            other.GetComponent<Renderer>().material = checkPointReachedMaterial;
        }
        
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.tag == "ConnectTheDotPath")
        {
            m_PathCollisions--;
        }

        if (m_PathCollisions == 0)
        {
            ResetPosition();
            myAudioSource.PlayOneShot(resetPosAudio);
        }
    }

    public void ResetPosition()
    {
        GrabSwitchState();
        transform.position = m_ResetPosition;
    }
}