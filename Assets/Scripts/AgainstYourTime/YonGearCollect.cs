﻿using UnityEngine;
using System.Collections;

public class YonGearCollect : MonoBehaviour {

    public AudioSource pickupAudio;                 // AudioSource used to play the pickUp sound
    private YonEffectJRController gamestate;        // Reference to the main gameController

	// Use this for initialization
	void Start ()
    {
        // If, for some reason, I forgot to assign the variable, goes and gets it by himself
        if (pickupAudio == null)
        {
            pickupAudio = GameObject.Find("PickUpYonGearAudioSource").GetComponent<AudioSource>();
        }

        // Binds to the controller
        gamestate = FindObjectOfType<YonEffectJRController>().GetComponent<YonEffectJRController>();
	}


    public void OnTriggerEnter(Collider other)
    {
        // If the player picked up the YonGear
        if (other.tag == "Player")
        {
            gamestate.numberGEARSCollected++;       // Increases the YonGear count
            pickupAudio.Play();                     // Plays the pick up audio
            transform.gameObject.SetActive(false);  // Make the YonGear disappear
        }        
    }
}
