﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using VRStandardAssets.Utils;

public class VRExitButton : MonoBehaviour {

    private VRInteractiveItem _myVRitem;

    public VRCameraFade cameraFader;
    public Material eyeHoverMaterial;               // The shader used when hovering on the object
    public AudioClip soundClick;
    public AudioClip soundHoverIn;
    public AudioClip soundHoverOut;
    private Material _myMaterial;
    private bool m_onHover = true;

    // Use this for initialization
    void Awake()
    {
        _myVRitem = GetComponent<VRInteractiveItem>();
        _myMaterial = GetComponent<Renderer>().material;
    }

    void Start ()
    {
        _myVRitem.OnClick += Clicked;
        _myVRitem.OnOver += HoverIn;
        _myVRitem.OnOut += HoverOut;
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void HoverIn()
    {
        GetComponent<Renderer>().material = eyeHoverMaterial;
        GetComponent<AudioSource>().PlayOneShot(soundHoverIn);
        m_onHover = true;
    }

    public void HoverOut()
    {
        GetComponent<Renderer>().material = _myMaterial;
        GetComponent<AudioSource>().PlayOneShot(soundHoverOut);
        m_onHover = false;
    }

    public void Clicked()
    {
        if (m_onHover)
        {
            _myVRitem.OnClick -= Clicked;
            _myVRitem.OnOver -= HoverIn;
            _myVRitem.OnOut -= HoverOut;
            cameraFader.OnFadeComplete += LoadGameScene;
            GetComponent<AudioSource>().PlayOneShot(soundClick);
            StartCoroutine(cameraFader.BeginFadeOut(false));
        }
    }

    public void LoadGameScene()
    {
        Application.Quit();
    }
}
