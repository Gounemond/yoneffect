﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/// <summary>
/// This class handles methods to show the correct icon of a command needed, being cross-platform
/// (so, shows the gamepad button if using the gamepad, or keyboard / mouse if using the latter.
/// </summary>
public class ReticleGUIInputHelper : MonoBehaviour {


    public Image fillerImage;
    public Image helperImage;                       // Reference to the Image object

    [Header ("Action buttons")]
    public Sprite xbox_A_Button;                 
    public Sprite mouse_LClick_Button;

    public enum eInputController
    {
        MouseKeyboard,
        Controller,
        OculusTouch,
        HTCWand,
        LeapMotion
    };

    private eInputController m_InputController = eInputController.MouseKeyboard;


    // Use this for initialization
    void Start ()
    {
        fillerImage.enabled = false;
        fillerImage.GetComponent<Animator>().enabled = false;
        helperImage.enabled = false;
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    /// <summary>
    /// Call to display the suggestion for the action command
    /// </summary>
    public void ShowActionSuggestion()
    {
        switch (GetInputState())
        {
            case eInputController.MouseKeyboard:
                helperImage.sprite = mouse_LClick_Button;
                break;
            case eInputController.Controller:
                helperImage.sprite = xbox_A_Button;
                break;
        }
        fillerImage.enabled = true;
        //fillerImage.GetComponent<Animator>().enabled = true;
        helperImage.enabled = true;
    }

    public void HideSuggestion()
    {
        fillerImage.enabled = false;
        //fillerImage.GetComponent<Animator>().enabled = false;
        helperImage.enabled = false;
    }

    /// <summary>
    /// Returns the type of controller currently used
    /// </summary>
    /// <returns></returns>
    public eInputController GetInputState()
    {
        if(isControllerInput())
        {
            m_InputController = eInputController.Controller;
        }
        else
        {
            m_InputController = eInputController.MouseKeyboard;
        }
        return m_InputController;
    }


    /// <summary>
    /// Checks wether the last Input is from keyboard/mouse
    /// </summary>
    /// <returns></returns>
    private bool isMouseKeyboard()
    {
        // mouse & keyboard buttons
        if (Event.current.isKey ||
            Event.current.isMouse)
        {
            return true;
        }

        // mouse movement
        if (Input.GetAxis("Mouse X") != 0.0f ||
            Input.GetAxis("Mouse Y") != 0.0f)
        {
            return true;
        }
        return false;
    }

    /// <summary>
    /// Checks wether the current Input is from the joystick controller
    /// </summary>
    /// <returns></returns>
    private bool isControllerInput()
    {
        // joystick buttons
        if (Input.GetKey(KeyCode.Joystick1Button0) ||
           Input.GetKey(KeyCode.Joystick1Button1) ||
           Input.GetKey(KeyCode.Joystick1Button2) ||
           Input.GetKey(KeyCode.Joystick1Button3) ||
           Input.GetKey(KeyCode.Joystick1Button4) ||
           Input.GetKey(KeyCode.Joystick1Button5) ||
           Input.GetKey(KeyCode.Joystick1Button6) ||
           Input.GetKey(KeyCode.Joystick1Button7) ||
           Input.GetKey(KeyCode.Joystick1Button8) ||
           Input.GetKey(KeyCode.Joystick1Button9) ||
           Input.GetKey(KeyCode.Joystick1Button10) ||
           Input.GetKey(KeyCode.Joystick1Button11) ||
           Input.GetKey(KeyCode.Joystick1Button12) ||
           Input.GetKey(KeyCode.Joystick1Button13) ||
           Input.GetKey(KeyCode.Joystick1Button14) ||
           Input.GetKey(KeyCode.Joystick1Button15) ||
           Input.GetKey(KeyCode.Joystick1Button16) ||
           Input.GetKey(KeyCode.Joystick1Button17) ||
           Input.GetKey(KeyCode.Joystick1Button18) ||
           Input.GetKey(KeyCode.Joystick1Button19))
        {
            return true;
        }

        // joystick axis
        if (Input.GetAxis("Desktop_Right_X_Axis") != 0.0f ||
           Input.GetAxis("Desktop_Right_Y_Axis") != 0.0f ||
           Input.GetAxis("Sprint") != 0.0f ||
           Input.GetAxis("Desktop_Left_X_Axis") != 0.0f ||
           Input.GetAxis("Desktop_Left_Y_Axis") != 0.0f)
        {
            return true;
        }

        return false;
    }
}
