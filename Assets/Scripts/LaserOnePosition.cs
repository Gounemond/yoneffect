using UnityEngine;
using System.Collections;

public class LaserOnePosition : MonoBehaviour {
	
	public GameObject laserBoxOne;
	public GameObject generator;
	
	private LineRenderer laserOne;
	
	private Vector3 initialPos;
	private Vector3 finalPos;
	
	// Use this for initialization
	void Start () {
		
		//Modifico la Y delle posizioni così il laser è posizionato meglio (al centro, dato che la scala dei cubi è 1)
		finalPos = new Vector3(laserBoxOne.transform.position.x, laserBoxOne.transform.position.y - 0.5F, laserBoxOne.transform.position.z);
		initialPos = new Vector3(generator.transform.position.x, generator.transform.position.y - 0.5F, generator.transform.position.z);
		
		//This sets the position and the color of the lineRenderer
		laserOne = this.GetComponent<LineRenderer>();
		laserOne.SetPosition(0, initialPos);
		laserOne.SetPosition(1, finalPos);
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	//Function which modify the position of the laser when there is a collision
	public void modifyPos(Vector3 position) {
		
		//Modification of the x position of the laser
		Vector3 pos = new Vector3(finalPos.x, position.y + 5.5f, finalPos.z);
		laserOne.SetPosition(1, pos);
		
	}
	
	//Reset of the position of the laser when the collision is over
	public void resetPos() {
		
		laserOne.SetPosition(1, finalPos);
		
	}
	
}
