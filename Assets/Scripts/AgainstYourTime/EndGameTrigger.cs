using UnityEngine;
using System.Collections;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System;
using System.Linq;

public class EndGameTrigger : MonoBehaviour {

	public YonEffectJRController gameState;
    public AudioSource levelCompleted;
	private int triggerCounter = 0;
	// Use this for initialization
	void Start ()
    {
	
	}
	
	// Update is called once per frame
	void Update () {
	}
	
	void OnTriggerEnter(Collider ObjectCollision)
	{
        if (ObjectCollision.tag == "Player" && triggerCounter == 0)
        {
            triggerCounter++;
            gameState.winGame();
            levelCompleted.Play();
        }
	}
	
	void OnTriggerExit(Collider ObjectCollision)
	{
        if (ObjectCollision.tag == "Player")
        {
            triggerCounter--;
        }
	}
	
}
