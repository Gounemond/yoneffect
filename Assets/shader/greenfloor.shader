// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:7386,x:34084,y:32145,varname:node_7386,prsc:2|emission-7035-OUT;n:type:ShaderForge.SFN_Color,id:8946,x:32450,y:32192,ptovrint:False,ptlb:node_8946,ptin:_node_8946,varname:node_8946,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:1,c3:0.08965516,c4:1;n:type:ShaderForge.SFN_Tex2d,id:5163,x:32488,y:32575,ptovrint:False,ptlb:10s,ptin:_10s,varname:node_5163,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:cc1cad8f411069f49a730228d9d3031f,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Time,id:8189,x:31350,y:32699,varname:node_8189,prsc:2;n:type:ShaderForge.SFN_Multiply,id:7374,x:32011,y:32967,varname:node_7374,prsc:2|A-8189-T,B-3678-OUT;n:type:ShaderForge.SFN_Vector1,id:3678,x:31836,y:33015,varname:node_3678,prsc:2,v1:0.3;n:type:ShaderForge.SFN_Multiply,id:3899,x:32975,y:32338,varname:node_3899,prsc:2|A-5163-R,B-7997-OUT;n:type:ShaderForge.SFN_Multiply,id:959,x:32022,y:32726,varname:node_959,prsc:2|A-8189-T,B-4491-OUT;n:type:ShaderForge.SFN_Vector1,id:4491,x:31823,y:32778,varname:node_4491,prsc:2,v1:0.25;n:type:ShaderForge.SFN_Multiply,id:2521,x:33064,y:32663,varname:node_2521,prsc:2|A-5163-G,B-220-OUT;n:type:ShaderForge.SFN_Multiply,id:5821,x:32030,y:32444,varname:node_5821,prsc:2|A-8189-T,B-2739-OUT;n:type:ShaderForge.SFN_Multiply,id:8536,x:32955,y:32532,varname:node_8536,prsc:2|A-582-OUT,B-5163-B;n:type:ShaderForge.SFN_Vector1,id:2739,x:31834,y:32501,varname:node_2739,prsc:2,v1:0.2;n:type:ShaderForge.SFN_Add,id:2604,x:33347,y:32565,varname:node_2604,prsc:2|A-2521-OUT,B-8536-OUT;n:type:ShaderForge.SFN_Add,id:2821,x:33466,y:32355,varname:node_2821,prsc:2|A-3899-OUT,B-2604-OUT;n:type:ShaderForge.SFN_Color,id:5226,x:32992,y:32127,ptovrint:False,ptlb:node_5226,ptin:_node_5226,varname:node_5226,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.07843138,c2:0.07843138,c3:0.2352941,c4:1;n:type:ShaderForge.SFN_Lerp,id:7035,x:33755,y:32306,varname:node_7035,prsc:2|A-5226-RGB,B-8946-RGB,T-2821-OUT;n:type:ShaderForge.SFN_Frac,id:9558,x:32303,y:32721,varname:node_9558,prsc:2|IN-959-OUT;n:type:ShaderForge.SFN_Frac,id:6073,x:32238,y:32392,varname:node_6073,prsc:2|IN-5821-OUT;n:type:ShaderForge.SFN_Frac,id:679,x:32216,y:32915,varname:node_679,prsc:2|IN-7374-OUT;n:type:ShaderForge.SFN_RemapRange,id:5969,x:32396,y:33059,varname:node_5969,prsc:2,frmn:0,frmx:1,tomn:-1,tomx:1|IN-679-OUT;n:type:ShaderForge.SFN_Abs,id:7997,x:32635,y:32980,varname:node_7997,prsc:2|IN-5969-OUT;n:type:ShaderForge.SFN_Abs,id:582,x:32632,y:32349,varname:node_582,prsc:2|IN-7637-OUT;n:type:ShaderForge.SFN_RemapRange,id:7637,x:32488,y:32374,varname:node_7637,prsc:2,frmn:0,frmx:1,tomn:-1,tomx:1|IN-6073-OUT;n:type:ShaderForge.SFN_RemapRange,id:1644,x:32523,y:32764,varname:node_1644,prsc:2,frmn:0,frmx:1,tomn:-1,tomx:1|IN-9558-OUT;n:type:ShaderForge.SFN_Abs,id:220,x:32832,y:32750,varname:node_220,prsc:2|IN-1644-OUT;proporder:8946-5163-5226;pass:END;sub:END;*/

Shader "Custom/greenfloor" {
    Properties {
        _node_8946 ("node_8946", Color) = (0,1,0.08965516,1)
        _10s ("10s", 2D) = "white" {}
        _node_5226 ("node_5226", Color) = (0.07843138,0.07843138,0.2352941,1)
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform float4 _node_8946;
            uniform sampler2D _10s; uniform float4 _10s_ST;
            uniform float4 _node_5226;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                UNITY_FOG_COORDS(1)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
////// Lighting:
////// Emissive:
                float4 _10s_var = tex2D(_10s,TRANSFORM_TEX(i.uv0, _10s));
                float4 node_8189 = _Time + _TimeEditor;
                float node_679 = frac((node_8189.g*0.3));
                float node_959 = (node_8189.g*0.25);
                float node_6073 = frac((node_8189.g*0.2));
                float3 emissive = lerp(_node_5226.rgb,_node_8946.rgb,((_10s_var.r*abs((node_679*2.0+-1.0)))+((_10s_var.g*abs((frac(node_959)*2.0+-1.0)))+(abs((node_6073*2.0+-1.0))*_10s_var.b))));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
