﻿using UnityEngine;
using System.Collections;

public class OutlineCheMandanoAMale : MonoBehaviour
{

    public Material outlineMaterial;

    private float r;
    private float g;
    private float b;

	// Use this for initialization
	void Start ()
    {
        r = outlineMaterial.GetColor("_OutlineColor").r;
        g = outlineMaterial.GetColor("_OutlineColor").g;
        b = outlineMaterial.GetColor("_OutlineColor").b;
    }
	
	// Update is called once per frame
	void Update ()
    {
        Color colorchemandaMale = new Color(Mathf.Sin(Time.time+r), Mathf.Cos(Time.time+g), Mathf.Sin((Time.time+b)/2));
        outlineMaterial.SetColor("_OutlineColor", colorchemandaMale);
    }
}
