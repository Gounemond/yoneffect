using UnityEngine;
using VRStandardAssets.Utils;
using System.Collections;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System;
using System.Linq;

[RequireComponent (typeof(VRInteractiveItem))]
[RequireComponent (typeof(Rigidbody))]
public class VRGrab : MonoBehaviour 
{
	public float  correctionforce = 50.0f;
	public Transform beacon;
    private Material m_MyMaterial;

    [Header("AudioSfx")]
    public AudioSource myAudioSource;
    public AudioClip hoverInAudio;
    public AudioClip hoverOutAudio;
    public AudioClip grabAudio;

    private bool  grabbed  = false;
    private bool  onHover = false;

    private Rigidbody _myRigidbody;
    private VRInteractiveItem _myVRitem;




    void OnEnable()
    {
        _myVRitem.OnClick += GrabSwitchState;
        _myVRitem.OnOver += HoverIn;
        _myVRitem.OnOut += HoverOut;
    }

    void OnDisable()
    {
        _myVRitem.OnClick -= GrabSwitchState;
        _myVRitem.OnOver -= HoverIn;
        _myVRitem.OnOut -= HoverOut;
    }

	void Awake () 
	{
        _myRigidbody = GetComponent<Rigidbody>();
        _myVRitem = GetComponent<VRInteractiveItem>();
        m_MyMaterial = GetComponent<Renderer>().material;
    }

    void Update () 
	{
        if (grabbed && !onHover && Input.GetButton("Fire1"))
        {
            GrabSwitchState();
        }

        // Update gravity force around the GrabBeacon if you actually have grabbed an object
        // The grab method is inspired from the Half Life series
        if (grabbed)
		{
			Vector3 force = beacon.transform.position - transform.position;
            _myRigidbody.velocity = force.normalized * _myRigidbody.velocity.magnitude;
            _myRigidbody.AddForce(force * correctionforce);
            _myRigidbody.velocity *= Mathf.Min(1.0f, force.magnitude / 2 );
		}

        
	}

    public void GrabSwitchState()
    {
        if (grabbed)
        {
            grabbed = false;
            _myRigidbody.useGravity = true;
            _myRigidbody.constraints = RigidbodyConstraints.FreezeRotation;
        }
        else
        {
            grabbed = true;
            myAudioSource.PlayOneShot(grabAudio);
            _myRigidbody.useGravity = false;
            _myRigidbody.constraints = RigidbodyConstraints.FreezeRotation;

        }
    }

    public void HoverIn()
    {
        m_MyMaterial.SetFloat("_grab", 1);
        myAudioSource.PlayOneShot(hoverInAudio);
        onHover = true;
    }

    public void HoverOut()
    {
        m_MyMaterial.SetFloat("_grab", 0);
        myAudioSource.PlayOneShot(hoverOutAudio);
        onHover = false;
    }
}