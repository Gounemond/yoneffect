using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;
using VRStandardAssets.Common;
using VRStandardAssets.Utils;
using VRStandardAssets.ShootingGallery;

public class YonEffectJRController : MonoBehaviour
{
    // Singleton implementation
    protected static YonEffectJRController _self;
    public static YonEffectJRController Self
    {
        get
        {
            if (_self == null)
                _self = FindObjectOfType(typeof(YonEffectJRController)) as YonEffectJRController;
            return _self;
        }
    }

    public VRCameraFade cameraFader;

    public ClonePoolManagerJR clonePool;				            // Reference to the clone pool manager
    public InputVCR           playerController;                     // Main player - instance of InputVCR on the player body
    public InputVCR           cameraController;                     // Main player camera - isntance of InputVCR on the player camera

    public AudioSource        timeMachineAudio;
    public AudioSource        timeParadoxAudio;
    public AudioMixerSnapshot calmBeginning;
    public AudioMixerSnapshot timeRunning;
    public AudioMixerSnapshot closeIsClose;
    public Animator blackWaveAnimator;

    public bool IsPlaying { get; private set; }                     // Whether or not the game is currently playing.
    public bool hasPlayerWon { get; private set; }
    public bool IsPause   { get; set; }                     // Whether or not the game is currently paused
    public bool IsGameBegun = false;
    public bool timerStarted { get; set; }
    private bool m_WaveExpanding;

    public float worldTime = 0;                                     // Time in game, subject to time travel (different from unity time!)
    public float timeBetweenJumps = 20;				                // How much time passes between one jump and another

    private bool _isRecording;                                      // Wether or not the game is currently recording the player
    public int numberGEARSCollected = 0;                            // Amount of YonGear collected

    public Coroutine paradoxRoutine;
    private int m_clonesLookingAtYou = 0;
    private int m_clonesCloseToYou = 0;
    private float m_Timer;
    public float timeToParadox = 2.0f;







    private IEnumerator Start()
    {
        // Continue looping through all the phases.
        while (true)
        {
            yield return StartCoroutine (StartPhase ());
            yield return StartCoroutine (PlayPhase ());
            yield return StartCoroutine (EndPhase ());
        }
    }


    private IEnumerator StartPhase ()
    {
        timerStarted = false;
        switch (GameDifficulty.selectedDifficulty)
        {
            case 0:
                break;
            case 1:
                timeBetweenJumps = 90;
                timeToParadox = 0.7f;
                break;
            case 2:
                timeBetweenJumps = 70;
                timeToParadox = 0.2f;
                break;
        }
        calmBeginning.TransitionTo(0.1f);
        // Wait for the intro UI to fade in.
        //yield return StartCoroutine (m_UIController.ShowIntroUI ());

        // Show the reticle (since there is now a selection slider) and hide the radial.
        //m_Reticle.Show ();
        //m_SelectionRadial.Hide ();

        // Turn on the tap warnings for the selection slider.
        //m_InputWarnings.TurnOnDoubleTapWarnings ();
        //m_InputWarnings.TurnOnSingleTapWarnings ();

        // Wait for the selection slider to finish filling.
        //yield return StartCoroutine (m_SelectionSlider.WaitForBarToFill ());

        // Turn off the tap warnings since it will now be tap to fire.
        //m_InputWarnings.TurnOffDoubleTapWarnings ();
        //m_InputWarnings.TurnOffSingleTapWarnings ();

        // Wait for the intro UI to fade out.
        //yield return StartCoroutine (m_UIController.HideIntroUI ());
        yield return null;
    }


    private IEnumerator PlayPhase ()
    {
        // Wait for the UI on the player's gun to fade in.
        //yield return StartCoroutine(m_UIController.ShowPlayerUI());

        yield return StartCoroutine(cameraFader.BeginFadeIn(2, false));

        worldTime         = 0.0f;                         // Setting the world time to 0 (the beginning)
        
        IsPlaying = true;                                   // The game is now playing.
        IsPause = false;                                    // The game is not paused
        _isRecording = true;								// Recording is on!


        // Make sure the reticle is being shown.
        // m_Reticle.Show ();

        // Reset the score.
        // SessionData.Restart ();

        // Wait for the play updates to finish.
        yield return StartCoroutine (PlayUpdate ());

        // Wait for the gun's UI to fade.
        //yield return StartCoroutine(m_UIController.HidePlayerUI());

        // The game is no longer playing.
        IsPlaying = false;
    }


    private IEnumerator EndPhase ()
    {
        if (!hasPlayerWon)
        {
            timeParadoxAudio.Play();
            yield return StartCoroutine(cameraFader.BeginFadeOut(5, false));

            yield return new WaitForSeconds(3);

            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
        else
        {
            timeMachineAudio.Play();
            yield return StartCoroutine(cameraFader.BeginFadeOut(1, false));

            GameDifficulty.UnlockNextDifficulty();
            GameDifficulty.SaveDifficulty();
            SceneManager.LoadScene("Credits");
            yield return null;
        }


        yield return null;
        // Hide the reticle since the radial is about to be used.
        //m_Reticle.Hide ();

        // In order, wait for the outro UI to fade in then wait for an additional delay.
        //yield return StartCoroutine (m_UIController.ShowOutroUI ());
        //yield return new WaitForSeconds(m_EndDelay);

        // Turn on the tap warnings.
        //m_InputWarnings.TurnOnDoubleTapWarnings();
        //m_InputWarnings.TurnOnSingleTapWarnings();

        // Wait for the radial to fill (this will show and hide the radial automatically).
        //yield return StartCoroutine(m_SelectionRadial.WaitForSelectionRadialToFill());

        // The radial is now filled so stop the warnings.
        //m_InputWarnings.TurnOffDoubleTapWarnings();
        //m_InputWarnings.TurnOffSingleTapWarnings();

        // Wait for the outro UI to fade out.
        //yield return StartCoroutine(m_UIController.HideOutroUI());
    }


    private IEnumerator PlayUpdate ()
    {
        // While player is alive or hasn't completed the game...3
        while (IsPlaying)
        {
            if (!IsPause)
            {
                if (timerStarted)
                {
                    if (worldTime < timeBetweenJumps)
                    {
                        if (worldTime > timeBetweenJumps -3 && !m_WaveExpanding)
                        {
                            m_WaveExpanding = true;
                            blackWaveAnimator.SetBool("Play", true);
                        }
                        worldTime += Time.deltaTime;

                    }
                    else
                    {
                        m_WaveExpanding = true;
                        blackWaveAnimator.SetBool("Play", false);
                        yield return StartCoroutine(TimeTravel());
                    }
                }
            }
            // Wait for the next frame.
            yield return null;
        }
    }

    // Does a time travel in the past
    public IEnumerator TimeTravel()
    {
        // Should do timeTravel Effect

        // Fade to black
        yield return StartCoroutine(cameraFader.BeginFadeOut(false));
        playerController.GetComponent<OVRPlayerControllerVCR>().enabled = false;
        timeMachineAudio.Play();

        yield return new WaitForSeconds(0.5f);
        // Add this current recording to clone pool
        yield return StartCoroutine(clonePool.addKaerbRecording(playerController.GetRecording(), cameraController.GetRecording(), worldTime));

        resetWorldTime();

        // Starting to record our actions, to reproduce them when going back in the past
        playerController.NewRecording();
        cameraController.NewRecording();

        yield return new WaitForSeconds(1);
        // When all heavy resource loading is finished, fadeIn again
        playerController.GetComponent<OVRPlayerControllerVCR>().enabled = true;
        yield return StartCoroutine(cameraFader.BeginFadeIn(false));

    }

    public void OnCloneHoverInAtYou()
    {
        m_clonesLookingAtYou++;
    }

    public void OnCloneHoverOutAtYou()
    {
        m_clonesLookingAtYou--;
    }

    public void OnCloneRangeIn()
    {
        m_clonesCloseToYou++;
        if (m_clonesCloseToYou == 1)
        {
            closeIsClose.TransitionTo(1f);
        }
    }

    public void OnCloneRangeOut()
    {
        m_clonesCloseToYou--;
        if (m_clonesCloseToYou == 0)
        {
            timeRunning.TransitionTo(1f);
        }
    }

    public IEnumerator BeginParadox()
    {
        m_Timer = 0;
        CameraEffectsManager.Self.noiseEffect.enabled = true;
        while (m_Timer < timeToParadox)
        {
            // ... add to the timer the difference between frames.
            m_Timer += Time.deltaTime;
            CameraEffectsManager.Self.noiseEffect.grainIntensityMin += Time.deltaTime * (5 / timeToParadox);

            yield return null;

            if (m_clonesLookingAtYou > 0)
            {
                continue;
            }
            else
            {
                m_Timer = 0f;
                paradoxRoutine = null;
                StartCoroutine(CameraEffectsManager.Self.ClearNoiseGradually());
                yield break;
            }

        }

        // Ok here you lose the game
        StartCoroutine(CameraEffectsManager.Self.ClearNoiseGradually());
        loseGame();
    }

    public void resetWorldTime()
    {
        worldTime = 0.0f;
    }

    public void startPlayerRecording()
    {
        playerController.NewRecording();
        cameraController.NewRecording();
        IsGameBegun = true;
    }

    public void stopClones()
    {
        clonePool.StopAllClones();
    }

    public void PausePlayerRecording()
    {
        if (IsGameBegun)
        {
            playerController.Pause();
            cameraController.Pause();
            clonePool.PauseAllClones();
        }
        
        IsPause = true;
    }

    public void ResumePlayerRecording()
    {
        if (IsGameBegun)
        {
            playerController.Record();
            cameraController.Record();
            clonePool.ResumeAllClones();
        }
        IsPause = false;
    }


    public void loseGame()
    {
        IsPlaying = false;
        hasPlayerWon = false;
    }

    public void winGame()
    {
        IsPlaying = false;
        hasPlayerWon = true;
    }

}