﻿using UnityEngine;
using System.Collections;
using Valve.VR;
using UnityEngine.VR;

public class ViveStartRotation : MonoBehaviour {
    
    // Use this for initialization
    void Start ()
    {

        if (VRSettings.loadedDeviceName == "OpenVR")
        {
            transform.Rotate(new Vector3(0, 180, 0));
        }      
    }

    void Update () {
	}
}
