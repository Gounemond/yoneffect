using UnityEngine;
using System.Collections;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System;
using System.Linq;


public class AudioSourceHandler : YonBehaviour {
	
	public AudioSource speaker;
	public AudioClip active;
	public AudioClip deactive;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	override public void Activate()
	{
		speaker.PlayOneShot (active);
	}
	
	override public void deActivate()
	{
		speaker.PlayOneShot (deactive);
	}
}
