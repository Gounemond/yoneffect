﻿using UnityEngine;
using System.Collections;

public class ClonePoolManager : MonoBehaviour 
{
	public GameState gameState;								// Reference to gameState

	public const int MAX_NCLONES = 21;						// Defines the maximum number of clones

	public InputVCR[] cloneKaerb;							// Array of Kaerb clones VCR controller  
	public InputVCR[] cloneCamera;							// Array of clone's cameras VCR controller
	
	private Recording[] _cloneKaerbActions;					// Series of recorded keyboard inputs on Kaerb
	private Recording[] _cloneCameraActions;				// Series of mouse inputs on Kaerb
	
	private float[] _cloneStartTime;						// Actual time when the clones appear
	private float[] _cloneEndTime;							// Actual time when the clones disappear
	
	private bool[] _isCloneActive;							// Tells if the clone is actually playing
	
	private int _numClonesRecorded;							// Number of clones with a recording

	// Use this for initialization
	void Awake () 
	{
		// Array initialization
		cloneKaerb = new InputVCR[MAX_NCLONES];
		cloneCamera = new InputVCR[MAX_NCLONES];
	
		_cloneKaerbActions = new Recording[MAX_NCLONES];
		_cloneCameraActions = new Recording[MAX_NCLONES];
	
		_cloneStartTime = new float[MAX_NCLONES];
		_cloneEndTime = new float[MAX_NCLONES];
		
		// The first clone has startTime at the beginning of the game. Always.
		_cloneStartTime[0] = 0;
		
		_isCloneActive = new bool[MAX_NCLONES];
		
		// Get references to clones and cameras
		for (int i = 0; i < MAX_NCLONES; i++) 
		{
			cloneKaerb[i] = GameObject.Find("KaerbRecorded" + i).GetComponent<InputVCR>();
			cloneCamera[i] = cloneKaerb[i].transform.Find("Head Joint/First Person Camera").GetComponent<InputVCR>();
		}
		
		// Get reference to GameState
		gameState = GameObject.Find("GameState").GetComponent<GameState>();		
		
		_numClonesRecorded = 0;
	}
	
	// Update is called once per frame
	void FixedUpdate () 
	{
		// Loop through all the clones with a recording
		for (int i = 0; i < _numClonesRecorded; i++)
		{
			// Checks if the clone isn't actually playing
			if (!_isCloneActive[i])
			{
				// Checks if the clone should be playing at this time
				if (gameState.worldTime >= _cloneStartTime[i] && gameState.worldTime < _cloneEndTime[i])
				{
					// I need to go to "Time 0" of the clone, the beginning. Time 0 of the clone may mean "second 34 in the world" (_cloneStartTime), 
					// because, the clone appeared at the second 34. I'm getting the property "position" at the closest frame of time 0, 
					// and reposition my clone to that actual position
					cloneKaerb[i].transform.position = InputVCR.ParseVector3(_cloneKaerbActions[i].GetProperty(
						_cloneKaerbActions[i].GetClosestFrame((gameState.worldTime - _cloneStartTime[i])), "position"));
					
					// Rotation is stored with euler angles and gives problems. So, I give it the starting rotation 
					// and let the sync do his job
					cloneKaerb[i].transform.rotation = new Quaternion();
					
					cloneCamera[i].transform.position = InputVCR.ParseVector3(_cloneCameraActions[i].GetProperty (
					_cloneCameraActions[i].GetClosestFrame((gameState.worldTime - _cloneStartTime[i])), "position"));
					cloneCamera[i].transform.rotation = new Quaternion();
					
					// Enabling clone components
					cloneKaerb[i].GetComponent<FirstPersonCharacter>().enabled = true;
					cloneKaerb[i].GetComponent<FirstPersonHeadBob>().enabled = true;

					// The clone now is set at the correct position he should be. 
					// Now, I make the clone plays the set of actions recorded  
					cloneKaerb[i].Play (Recording.ParseRecording (_cloneKaerbActions[i].ToString ()), (gameState.worldTime - _cloneStartTime[i]));
					cloneCamera[i].Play (Recording.ParseRecording (_cloneCameraActions[i].ToString ()), (gameState.worldTime - _cloneStartTime[i]));
					
					_isCloneActive[i] = true;	//This clone nos is active					
				}
			}
			else			// If this clone was already playing
			{
				if (gameState.worldTime > _cloneEndTime[i])	//If the clone lifespan exceeded the endtime
				{	
					// ---- All of this should be handled in a method, except the _isCloneActive[i] = false
					// Stop clone
					cloneKaerb[i].Stop ();
					cloneCamera[i].Stop ();
					
					// Disable clone components
					cloneKaerb[i].GetComponent<FirstPersonCharacter>().enabled = false;
					cloneKaerb[i].GetComponent<FirstPersonHeadBob>().enabled = false;
					
					// Take object and camera out of the visible space
					cloneKaerb[i].transform.position= new Vector3(1000f,1000f+10*i,1000f);
					cloneCamera[i].transform.position= new Vector3(1000f,1000f+10*i,1000f);
					
					// Clone is no more active
					_isCloneActive[i]=false;
				}
			}
		}
	}
		
	// This method gets called whenever Kaerb does a jump back in time, adding a new record of past actions 
	public void addKaerbRecording(Recording kaerbRecording, Recording cameraRecording, float jumpTime)
	{
		//Store the recording done until now on Kaerb and camera
		Recording recKaerb = kaerbRecording;
		Recording recCamera = cameraRecording;
		
		_cloneKaerbActions[_numClonesRecorded] = recKaerb;
		_cloneCameraActions[_numClonesRecorded] = recCamera;
		
		_cloneStartTime[_numClonesRecorded+1] = gameState.worldTime - jumpTime;
		_cloneEndTime[_numClonesRecorded] = gameState.worldTime;
			
		// Resets all clones (update will take care of new conditions)
		for (int i = 0; i < _numClonesRecorded; i++)
		{
			// Stops whatever reproduction was going on 
			cloneKaerb[i].Stop ();
			cloneCamera[i].Stop ();
				// Move the clone out of visible area
			cloneKaerb[i].transform.position= new Vector3(1000,1000+i*5,1000+i*5);
			cloneCamera[i].transform.position= new Vector3(1000,1000+i*5,1000+i*5);
				// Set the clone not active
			_isCloneActive[i]=false;
		}
		
		_numClonesRecorded++;	// Incrementing the number of clones with a recording
		
	}
	
	// This routine checks if there are clones that won't ever be able to be reproduced again
	// This means that, considering the maximum amount of time you can jump in the past, the clones who
	// appear and disappear earlier of that time can be disposed of, along with their recording
	private void _disposeOutdatedClones()
	{
		for (int i = 0; i < _numClonesRecorded; i++)
		{
			// If this clone is outside the reach of the maximum jump time
			if (gameState.worldTime - gameState.MAX_JUMP_TIME > _cloneEndTime[i])
			{
				// Shifts all the successive clones and their datas backward
				for (int j = i; j < _numClonesRecorded -1; j++)
				{
					_cloneKaerbActions[i] = _cloneKaerbActions[i+1];
					_cloneCameraActions[i] = _cloneCameraActions[i+1];
					_cloneStartTime[i] = _cloneStartTime[i+1];
					_cloneEndTime[i] = _cloneEndTime[i+1];
					
					// This is a little tricky. It may happens that this routine is called while some recording is in progress.
					// Setting the actives = false, means that the update will check again, ad reassigning to the clone the proper 
					// recording even after the shift.  
					_isCloneActive[i] = false;							
				}
				_numClonesRecorded--;
			}
		}
	}
}
