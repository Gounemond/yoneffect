using UnityEngine;
using System.Collections;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System;
using System.Linq;


public class StartMenu : MonoBehaviour {
	
	public Texture2D backdrop;
	public Texture startbutton;
	public Texture quitbutton;
	
	// Use this for initialization
	void Start () {
		Cursor.visible=true;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnGUI()
	{
		GUIStyle backgroundStyle = new GUIStyle();
		backgroundStyle.normal.background = backdrop;
		backgroundStyle.stretchWidth = true;
		backgroundStyle.stretchHeight = true;
		//GUI.Label(new Rect((float)((Screen.width - (Screen.height * 2)) * 0.75), 0f,(float) Screen.height * 2,(float)Screen.height), "", backgroundStyle);
		//GUI.Label(new Rect((float)((Screen.width/2 - (Screen.height * 2)) * 0.75), 0f,(float) Screen.height * 2,(float)Screen.height), "", backgroundStyle);
		GUI.Label (new Rect(0,0,Screen.width,Screen.height),"",backgroundStyle);
		
		if(GUI.Button (new Rect(Screen.width/2-75,Screen.height/2-250,150,100),startbutton))
		{
			Application.LoadLevel("Tutorial1");
		}
		if(GUI.Button (new Rect(Screen.width/2-75,Screen.height/2+200,150,100),quitbutton))
		{
			Application.Quit();
		}
	}
}
