// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:1003,x:36783,y:31636,varname:node_1003,prsc:2|emission-9023-OUT,voffset-4479-OUT;n:type:ShaderForge.SFN_Lerp,id:6289,x:33228,y:32392,varname:node_6289,prsc:2|A-9170-RGB,B-3155-OUT,T-4940-RGB;n:type:ShaderForge.SFN_Multiply,id:3155,x:32938,y:32508,varname:node_3155,prsc:2|A-9617-RGB,B-4940-RGB;n:type:ShaderForge.SFN_Color,id:9170,x:32798,y:32224,ptovrint:False,ptlb:node_1429,ptin:_node_1429,varname:node_1429,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0,c3:0.9310346,c4:1;n:type:ShaderForge.SFN_Tex2d,id:4940,x:32541,y:32393,ptovrint:False,ptlb:node_1859,ptin:_node_1859,varname:node_1859,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:ae0049a4a1783f946ac225627cf79c28,ntxv:0,isnm:False|UVIN-8239-OUT;n:type:ShaderForge.SFN_Color,id:9617,x:32632,y:32082,ptovrint:False,ptlb:node_5189,ptin:_node_5189,varname:node_5189,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0,c3:0,c4:1;n:type:ShaderForge.SFN_TexCoord,id:179,x:31789,y:31929,varname:node_179,prsc:2,uv:0;n:type:ShaderForge.SFN_Add,id:8239,x:32244,y:32068,varname:node_8239,prsc:2|A-179-UVOUT,B-864-OUT;n:type:ShaderForge.SFN_Time,id:9339,x:31743,y:32152,varname:node_9339,prsc:2;n:type:ShaderForge.SFN_Multiply,id:864,x:32008,y:32143,varname:node_864,prsc:2|A-9339-T,B-2157-OUT;n:type:ShaderForge.SFN_Slider,id:2157,x:31608,y:32383,ptovrint:False,ptlb:time nube,ptin:_timenube,varname:node_5819,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.04200784,max:1;n:type:ShaderForge.SFN_Add,id:4938,x:33675,y:31886,varname:node_4938,prsc:2|A-4414-OUT,B-6289-OUT;n:type:ShaderForge.SFN_Multiply,id:4414,x:33426,y:31600,varname:node_4414,prsc:2|A-3374-OUT,B-7216-OUT;n:type:ShaderForge.SFN_OneMinus,id:9291,x:33182,y:31943,varname:node_9291,prsc:2|IN-7216-OUT;n:type:ShaderForge.SFN_Fresnel,id:7216,x:32879,y:31746,varname:node_7216,prsc:2|NRM-8608-OUT,EXP-7040-OUT;n:type:ShaderForge.SFN_NormalVector,id:8608,x:32528,y:31701,prsc:2,pt:False;n:type:ShaderForge.SFN_ValueProperty,id:7040,x:32632,y:31891,ptovrint:False,ptlb:fresnel,ptin:_fresnel,varname:node_6277,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.5;n:type:ShaderForge.SFN_Multiply,id:3374,x:33137,y:31687,varname:node_3374,prsc:2|A-7510-RGB,B-7216-OUT;n:type:ShaderForge.SFN_Color,id:7510,x:32869,y:31416,ptovrint:False,ptlb:2color,ptin:_2color,varname:node_2374,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0,c3:0,c4:1;n:type:ShaderForge.SFN_Code,id:5323,x:34521,y:32409,varname:node_5323,prsc:2,code:IAAgAGYAbABvAGEAdAAgAHIAZQB0ACAAPQAgADAAOwANACAAIABpAG4AdAAgAGkAdABlAHIAYQB0AGkAbwBuAHMAIAA9ACAANAA7AA0ACgAgACAAZgBvAHIAIAAoAGkAbgB0ACAAaQAgAD0AIAAwADsAIABpACAAPAAgAGkAdABlAHIAYQB0AGkAbwBuAHMAOwAgACsAKwBpACkADQAKACAAIAB7AA0ACgAgACAAIAAgACAAZgBsAG8AYQB0ADIAIABwACAAPQAgAGYAbABvAG8AcgAoAFUAVgAgACoAIAAoAGkAKwAxACkAKQA7AA0ACgAgACAAIAAgACAAZgBsAG8AYQB0ADIAIABmACAAPQAgAGYAcgBhAGMAKABVAFYAIAAqACAAKABpACsAMQApACkAOwANAAoAIAAgACAAIAAgAGYAIAA9ACAAZgAgACoAIABmACAAKgAgACgAMwAuADAAIAAtACAAMgAuADAAIAAqACAAZgApADsADQAKACAAIAAgACAAIABmAGwAbwBhAHQAIABuACAAPQAgAHAALgB4ACAAKwAgAHAALgB5ACAAKgAgADUANwAuADAAOwANAAoAIAAgACAAIAAgAGYAbABvAGEAdAA0ACAAbgBvAGkAcwBlACAAPQAgAGYAbABvAGEAdAA0ACgAbgAsACAAbgAgACsAIAAxACwAIABuACAAKwAgADUANwAuADAALAAgAG4AIAArACAANQA4AC4AMAApADsADQAKACAAIAAgACAAIABuAG8AaQBzAGUAIAA9ACAAZgByAGEAYwAoAHMAaQBuACgAbgBvAGkAcwBlACkAKgA0ADMANwAuADUAOAA1ADQANQAzACkAOwANAAoAIAAgACAAIAAgAHIAZQB0ACAAKwA9ACAAbABlAHIAcAAoAGwAZQByAHAAKABuAG8AaQBzAGUALgB4ACwAIABuAG8AaQBzAGUALgB5ACwAIABmAC4AeAApACwAIABsAGUAcgBwACgAbgBvAGkAcwBlAC4AegAsACAAbgBvAGkAcwBlAC4AdwAsACAAZgAuAHgAKQAsACAAZgAuAHkAKQAgACoAIAAoACAAaQB0AGUAcgBhAHQAaQBvAG4AcwAgAC8AIAAoAGkAKwAxACkAKQA7AA0ACgAgACAAfQANAAoAIAAgAHIAZQB0AHUAcgBuACAAcgBlAHQALwBpAHQAZQByAGEAdABpAG8AbgBzADsA,output:2,fname:noise,width:247,height:112,input:1,input_1_label:UV|A-6849-OUT;n:type:ShaderForge.SFN_Clamp01,id:8625,x:34910,y:32394,varname:node_8625,prsc:2|IN-5323-OUT;n:type:ShaderForge.SFN_OneMinus,id:9772,x:35234,y:32479,varname:node_9772,prsc:2|IN-8625-OUT;n:type:ShaderForge.SFN_RemapRange,id:8632,x:35111,y:32256,varname:node_8632,prsc:2,frmn:0,frmx:1,tomn:-2,tomx:2|IN-8625-OUT;n:type:ShaderForge.SFN_Clamp01,id:243,x:35371,y:32157,varname:node_243,prsc:2|IN-8632-OUT;n:type:ShaderForge.SFN_Hue,id:4931,x:35637,y:32217,varname:node_4931,prsc:2|IN-243-OUT;n:type:ShaderForge.SFN_Lerp,id:1257,x:35892,y:31972,varname:node_1257,prsc:2|A-3006-OUT,B-4938-OUT,T-243-OUT;n:type:ShaderForge.SFN_Panner,id:244,x:33876,y:32278,varname:node_244,prsc:2,spu:0.4,spv:0.4|UVIN-9875-UVOUT,DIST-6789-OUT;n:type:ShaderForge.SFN_TexCoord,id:9875,x:33633,y:32278,varname:node_9875,prsc:2,uv:0;n:type:ShaderForge.SFN_Rotator,id:8204,x:34081,y:32375,varname:node_8204,prsc:2|UVIN-244-UVOUT,SPD-6789-OUT;n:type:ShaderForge.SFN_Tau,id:801,x:33948,y:32570,varname:node_801,prsc:2;n:type:ShaderForge.SFN_Multiply,id:6849,x:34311,y:32406,varname:node_6849,prsc:2|A-8204-UVOUT,B-801-OUT;n:type:ShaderForge.SFN_OneMinus,id:1953,x:35719,y:32524,varname:node_1953,prsc:2|IN-9772-OUT;n:type:ShaderForge.SFN_SwitchProperty,id:5330,x:36099,y:32448,ptovrint:False,ptlb:You_Wrong_VO,ptin:_You_Wrong_VO,varname:node_5330,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:True|A-3674-OUT,B-9772-OUT;n:type:ShaderForge.SFN_Vector1,id:3674,x:35700,y:32415,varname:node_3674,prsc:2,v1:0;n:type:ShaderForge.SFN_SwitchProperty,id:9441,x:36023,y:31861,ptovrint:False,ptlb:You_Wrong_Clr,ptin:_You_Wrong_Clr,varname:_YouWromg_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:False|A-4938-OUT,B-1257-OUT;n:type:ShaderForge.SFN_Color,id:568,x:35817,y:31602,ptovrint:False,ptlb:color,ptin:_color,varname:node_568,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.8206897,c2:0,c3:1,c4:1;n:type:ShaderForge.SFN_Slider,id:8738,x:35660,y:31788,ptovrint:False,ptlb:you_win,ptin:_you_win,varname:node_8738,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Lerp,id:9023,x:36294,y:31718,varname:node_9023,prsc:2|A-9441-OUT,B-568-RGB,T-8738-OUT;n:type:ShaderForge.SFN_Multiply,id:4479,x:36517,y:32075,varname:node_4479,prsc:2|A-3613-UVOUT,B-5330-OUT,C-5330-OUT;n:type:ShaderForge.SFN_TexCoord,id:3613,x:36182,y:32127,varname:node_3613,prsc:2,uv:0;n:type:ShaderForge.SFN_Multiply,id:6789,x:33767,y:32476,varname:node_6789,prsc:2|A-4868-T,B-7071-OUT;n:type:ShaderForge.SFN_Time,id:4868,x:33443,y:32502,varname:node_4868,prsc:2;n:type:ShaderForge.SFN_ValueProperty,id:7071,x:33636,y:32601,ptovrint:False,ptlb:time,ptin:_time,varname:node_7071,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_ScreenPos,id:8224,x:35384,y:31953,varname:node_8224,prsc:2,sctp:0;n:type:ShaderForge.SFN_Add,id:3006,x:35672,y:32037,varname:node_3006,prsc:2|A-8224-UVOUT,B-4931-OUT;proporder:9170-4940-9617-7040-7510-2157-5330-9441-568-8738-7071;pass:END;sub:END;*/

Shader "Custom/Cygnus" {
    Properties {
        _node_1429 ("node_1429", Color) = (1,0,0.9310346,1)
        _node_1859 ("node_1859", 2D) = "white" {}
        _node_5189 ("node_5189", Color) = (1,0,0,1)
        _fresnel ("fresnel", Float ) = 0.5
        _2color ("2color", Color) = (1,0,0,1)
        _timenube ("time nube", Range(0, 1)) = 0.04200784
        [MaterialToggle] _You_Wrong_VO ("You_Wrong_VO", Float ) = 1
        [MaterialToggle] _You_Wrong_Clr ("You_Wrong_Clr", Float ) = 0
        _color ("color", Color) = (0.8206897,0,1,1)
        _you_win ("you_win", Range(0, 1)) = 0
        _time ("time", Float ) = 1
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform float4 _node_1429;
            uniform sampler2D _node_1859; uniform float4 _node_1859_ST;
            uniform float4 _node_5189;
            uniform float _timenube;
            uniform float _fresnel;
            uniform float4 _2color;
            float3 noise( float2 UV ){
              float ret = 0;  int iterations = 4;
              for (int i = 0; i < iterations; ++i)
              {
                 float2 p = floor(UV * (i+1));
                 float2 f = frac(UV * (i+1));
                 f = f * f * (3.0 - 2.0 * f);
                 float n = p.x + p.y * 57.0;
                 float4 noise = float4(n, n + 1, n + 57.0, n + 58.0);
                 noise = frac(sin(noise)*437.585453);
                 ret += lerp(lerp(noise.x, noise.y, f.x), lerp(noise.z, noise.w, f.x), f.y) * ( iterations / (i+1));
              }
              return ret/iterations;
            }
            
            uniform fixed _You_Wrong_VO;
            uniform fixed _You_Wrong_Clr;
            uniform float4 _color;
            uniform float _you_win;
            uniform float _time;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float4 screenPos : TEXCOORD3;
                UNITY_FOG_COORDS(4)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                float4 node_7407 = _Time + _TimeEditor;
                float4 node_4868 = _Time + _TimeEditor;
                float node_6789 = (node_4868.g*_time);
                float node_8204_ang = node_7407.g;
                float node_8204_spd = node_6789;
                float node_8204_cos = cos(node_8204_spd*node_8204_ang);
                float node_8204_sin = sin(node_8204_spd*node_8204_ang);
                float2 node_8204_piv = float2(0.5,0.5);
                float2 node_8204 = (mul((o.uv0+node_6789*float2(0.4,0.4))-node_8204_piv,float2x2( node_8204_cos, -node_8204_sin, node_8204_sin, node_8204_cos))+node_8204_piv);
                float3 node_8625 = saturate(noise( (node_8204*6.28318530718) ));
                float3 node_9772 = (1.0 - node_8625);
                float3 _You_Wrong_VO_var = lerp( 0.0, node_9772, _You_Wrong_VO );
                v.vertex.xyz += (float3(o.uv0,0.0)*_You_Wrong_VO_var*_You_Wrong_VO_var);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                o.screenPos = o.pos;
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                i.screenPos = float4( i.screenPos.xy / i.screenPos.w, 0, 0 );
                i.screenPos.y *= _ProjectionParams.x;
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float node_7216 = pow(1.0-max(0,dot(i.normalDir, viewDirection)),_fresnel);
                float4 node_9339 = _Time + _TimeEditor;
                float2 node_8239 = (i.uv0+(node_9339.g*_timenube));
                float4 _node_1859_var = tex2D(_node_1859,TRANSFORM_TEX(node_8239, _node_1859));
                float3 node_4938 = (((_2color.rgb*node_7216)*node_7216)+lerp(_node_1429.rgb,(_node_5189.rgb*_node_1859_var.rgb),_node_1859_var.rgb));
                float4 node_7407 = _Time + _TimeEditor;
                float4 node_4868 = _Time + _TimeEditor;
                float node_6789 = (node_4868.g*_time);
                float node_8204_ang = node_7407.g;
                float node_8204_spd = node_6789;
                float node_8204_cos = cos(node_8204_spd*node_8204_ang);
                float node_8204_sin = sin(node_8204_spd*node_8204_ang);
                float2 node_8204_piv = float2(0.5,0.5);
                float2 node_8204 = (mul((i.uv0+node_6789*float2(0.4,0.4))-node_8204_piv,float2x2( node_8204_cos, -node_8204_sin, node_8204_sin, node_8204_cos))+node_8204_piv);
                float3 node_8625 = saturate(noise( (node_8204*6.28318530718) ));
                float3 node_243 = saturate((node_8625*4.0+-2.0));
                float3 emissive = lerp(lerp( node_4938, lerp((float3(i.screenPos.rg,0.0)+saturate(3.0*abs(1.0-2.0*frac(node_243+float3(0.0,-1.0/3.0,1.0/3.0)))-1)),node_4938,node_243), _You_Wrong_Clr ),_color.rgb,_you_win);
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            float3 noise( float2 UV ){
              float ret = 0;  int iterations = 4;
              for (int i = 0; i < iterations; ++i)
              {
                 float2 p = floor(UV * (i+1));
                 float2 f = frac(UV * (i+1));
                 f = f * f * (3.0 - 2.0 * f);
                 float n = p.x + p.y * 57.0;
                 float4 noise = float4(n, n + 1, n + 57.0, n + 58.0);
                 noise = frac(sin(noise)*437.585453);
                 ret += lerp(lerp(noise.x, noise.y, f.x), lerp(noise.z, noise.w, f.x), f.y) * ( iterations / (i+1));
              }
              return ret/iterations;
            }
            
            uniform fixed _You_Wrong_VO;
            uniform float _time;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                float4 node_7149 = _Time + _TimeEditor;
                float4 node_4868 = _Time + _TimeEditor;
                float node_6789 = (node_4868.g*_time);
                float node_8204_ang = node_7149.g;
                float node_8204_spd = node_6789;
                float node_8204_cos = cos(node_8204_spd*node_8204_ang);
                float node_8204_sin = sin(node_8204_spd*node_8204_ang);
                float2 node_8204_piv = float2(0.5,0.5);
                float2 node_8204 = (mul((o.uv0+node_6789*float2(0.4,0.4))-node_8204_piv,float2x2( node_8204_cos, -node_8204_sin, node_8204_sin, node_8204_cos))+node_8204_piv);
                float3 node_8625 = saturate(noise( (node_8204*6.28318530718) ));
                float3 node_9772 = (1.0 - node_8625);
                float3 _You_Wrong_VO_var = lerp( 0.0, node_9772, _You_Wrong_VO );
                v.vertex.xyz += (float3(o.uv0,0.0)*_You_Wrong_VO_var*_You_Wrong_VO_var);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
