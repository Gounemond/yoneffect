// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:6688,x:34227,y:32653,varname:node_6688,prsc:2|emission-9844-OUT;n:type:ShaderForge.SFN_Color,id:6337,x:33448,y:32344,ptovrint:False,ptlb:node_3947,ptin:_node_3947,varname:node_3947,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0.7352943,c3:1,c4:1;n:type:ShaderForge.SFN_Power,id:1979,x:33387,y:32951,varname:node_1979,prsc:2|VAL-8587-OUT,EXP-7657-OUT;n:type:ShaderForge.SFN_Multiply,id:8587,x:33190,y:32879,varname:node_8587,prsc:2|A-8620-OUT,B-9878-OUT;n:type:ShaderForge.SFN_Panner,id:3151,x:31459,y:32566,varname:node_3151,prsc:2,spu:1,spv:1|UVIN-3981-OUT,DIST-3564-OUT;n:type:ShaderForge.SFN_TexCoord,id:4858,x:31023,y:32434,varname:node_4858,prsc:2,uv:0;n:type:ShaderForge.SFN_Vector1,id:9878,x:32874,y:32999,varname:node_9878,prsc:2,v1:1.3;n:type:ShaderForge.SFN_Vector1,id:7657,x:33012,y:33229,varname:node_7657,prsc:2,v1:15;n:type:ShaderForge.SFN_Subtract,id:1648,x:32720,y:32785,varname:node_1648,prsc:2|A-9762-OUT,B-7813-OUT;n:type:ShaderForge.SFN_Abs,id:8620,x:32940,y:32785,varname:node_8620,prsc:2|IN-1648-OUT;n:type:ShaderForge.SFN_Frac,id:5415,x:31712,y:32745,varname:node_5415,prsc:2|IN-7876-OUT;n:type:ShaderForge.SFN_ComponentMask,id:4639,x:31744,y:32578,varname:node_4639,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-3151-UVOUT;n:type:ShaderForge.SFN_Frac,id:2244,x:32041,y:32911,varname:node_2244,prsc:2|IN-5415-OUT;n:type:ShaderForge.SFN_Vector1,id:7813,x:32570,y:32911,varname:node_7813,prsc:2,v1:0.15;n:type:ShaderForge.SFN_Time,id:8052,x:31190,y:32742,varname:node_8052,prsc:2;n:type:ShaderForge.SFN_Lerp,id:6068,x:33890,y:32728,varname:node_6068,prsc:2|A-3599-RGB,B-6337-RGB,T-9744-OUT;n:type:ShaderForge.SFN_Color,id:3599,x:33395,y:32546,ptovrint:False,ptlb:node_3094,ptin:_node_3094,varname:node_3094,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.07843138,c2:0.07843138,c3:0.2352941,c4:1;n:type:ShaderForge.SFN_Multiply,id:3981,x:31278,y:32444,varname:node_3981,prsc:2|A-4858-UVOUT,B-7625-OUT;n:type:ShaderForge.SFN_ValueProperty,id:7625,x:30940,y:32616,ptovrint:False,ptlb:number,ptin:_number,varname:node_9564,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_SwitchProperty,id:7876,x:32054,y:32529,ptovrint:False,ptlb:direction,ptin:_direction,varname:node_3453,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:True|A-4289-OUT,B-4639-OUT;n:type:ShaderForge.SFN_ComponentMask,id:4289,x:31731,y:32399,varname:node_4289,prsc:2,cc1:1,cc2:-1,cc3:-1,cc4:-1|IN-3151-UVOUT;n:type:ShaderForge.SFN_ValueProperty,id:8596,x:33347,y:33229,ptovrint:False,ptlb:int opacity,ptin:_intopacity,varname:node_7762,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.25;n:type:ShaderForge.SFN_Multiply,id:9744,x:33644,y:32987,varname:node_9744,prsc:2|A-1979-OUT,B-8596-OUT;n:type:ShaderForge.SFN_OneMinus,id:7675,x:32107,y:33090,varname:node_7675,prsc:2|IN-2244-OUT;n:type:ShaderForge.SFN_SwitchProperty,id:9762,x:32439,y:32774,ptovrint:False,ptlb:opacitydirection,ptin:_opacitydirection,varname:node_7646,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:False|A-2244-OUT,B-7675-OUT;n:type:ShaderForge.SFN_SwitchProperty,id:9844,x:33864,y:32448,ptovrint:False,ptlb:on/off,ptin:_onoff,varname:node_9844,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:True|A-3599-RGB,B-6068-OUT;n:type:ShaderForge.SFN_Multiply,id:3564,x:31347,y:32764,varname:node_3564,prsc:2|A-8052-T,B-8670-OUT;n:type:ShaderForge.SFN_ValueProperty,id:8670,x:31222,y:32892,ptovrint:False,ptlb:time,ptin:_time,varname:node_8670,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;proporder:6337-3599-7876-8596-9762-7625-9844-8670;pass:END;sub:END;*/

Shader "Custom/menucable" {
    Properties {
        _node_3947 ("node_3947", Color) = (0,0.7352943,1,1)
        _node_3094 ("node_3094", Color) = (0.07843138,0.07843138,0.2352941,1)
        [MaterialToggle] _direction ("direction", Float ) = 0
        _intopacity ("int opacity", Float ) = 0.25
        [MaterialToggle] _opacitydirection ("opacitydirection", Float ) = 0
        _number ("number", Float ) = 1
        [MaterialToggle] _onoff ("on/off", Float ) = 0
        _time ("time", Float ) = 1
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform float4 _node_3947;
            uniform float4 _node_3094;
            uniform float _number;
            uniform fixed _direction;
            uniform float _intopacity;
            uniform fixed _opacitydirection;
            uniform fixed _onoff;
            uniform float _time;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                UNITY_FOG_COORDS(1)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
////// Lighting:
////// Emissive:
                float4 node_8052 = _Time + _TimeEditor;
                float node_3564 = (node_8052.g*_time);
                float2 node_3981 = (i.uv0*_number);
                float2 node_3151 = (node_3981+node_3564*float2(1,1));
                float node_4289 = node_3151.g;
                float node_4639 = node_3151.r;
                float node_5415 = frac(lerp( node_4289, node_4639, _direction ));
                float node_2244 = frac(node_5415);
                float3 emissive = lerp( _node_3094.rgb, lerp(_node_3094.rgb,_node_3947.rgb,(pow((abs((lerp( node_2244, (1.0 - node_2244), _opacitydirection )-0.15))*1.3),15.0)*_intopacity)), _onoff );
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
