// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:8211,x:32719,y:32712,varname:node_8211,prsc:2|emission-8850-OUT;n:type:ShaderForge.SFN_Tex2d,id:4102,x:31857,y:32583,ptovrint:False,ptlb:node_4102,ptin:_node_4102,varname:node_4102,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:28c7aad1372ff114b90d330f8a2dd938,ntxv:0,isnm:False|UVIN-6891-UVOUT;n:type:ShaderForge.SFN_Tex2d,id:4518,x:32389,y:32912,ptovrint:False,ptlb:node_4518,ptin:_node_4518,varname:node_4518,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:62e76d73fdb5fbe43862dd7ab2baab75,ntxv:0,isnm:False|UVIN-5106-UVOUT;n:type:ShaderForge.SFN_Blend,id:8850,x:32452,y:32586,varname:node_8850,prsc:2,blmd:14,clmp:True|SRC-2323-OUT,DST-4518-RGB;n:type:ShaderForge.SFN_Lerp,id:2323,x:32261,y:32484,varname:node_2323,prsc:2|A-2468-RGB,B-7469-OUT,T-4102-RGB;n:type:ShaderForge.SFN_Multiply,id:7469,x:32094,y:32624,varname:node_7469,prsc:2|A-7911-RGB,B-4102-RGB;n:type:ShaderForge.SFN_Color,id:7911,x:31949,y:32391,ptovrint:False,ptlb:node_7911,ptin:_node_7911,varname:node_7911,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Color,id:2468,x:32113,y:32281,ptovrint:False,ptlb:node_2468,ptin:_node_2468,varname:node_2468,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0.6241379,c3:0.6241379,c4:1;n:type:ShaderForge.SFN_Rotator,id:6891,x:31601,y:32519,varname:node_6891,prsc:2|UVIN-2756-UVOUT,SPD-2118-OUT;n:type:ShaderForge.SFN_Slider,id:2118,x:31315,y:32633,ptovrint:False,ptlb:node_2118,ptin:_node_2118,varname:node_2118,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-0.1,cur:-0.05,max:1;n:type:ShaderForge.SFN_TexCoord,id:2756,x:31348,y:32383,varname:node_2756,prsc:2,uv:0;n:type:ShaderForge.SFN_Rotator,id:5106,x:32153,y:32869,varname:node_5106,prsc:2|UVIN-6727-UVOUT,ANG-2144-OUT,SPD-6501-OUT;n:type:ShaderForge.SFN_TexCoord,id:6727,x:31898,y:32835,varname:node_6727,prsc:2,uv:0;n:type:ShaderForge.SFN_Multiply,id:2144,x:31953,y:33066,varname:node_2144,prsc:2|A-6946-OUT,B-4303-OUT;n:type:ShaderForge.SFN_Tau,id:4303,x:31745,y:33112,varname:node_4303,prsc:2;n:type:ShaderForge.SFN_OneMinus,id:1268,x:31555,y:32883,varname:node_1268,prsc:2|IN-3155-OUT;n:type:ShaderForge.SFN_Multiply,id:3155,x:31413,y:32837,varname:node_3155,prsc:2|A-7966-OUT,B-6631-OUT;n:type:ShaderForge.SFN_Vector1,id:6631,x:31247,y:32997,varname:node_6631,prsc:2,v1:2;n:type:ShaderForge.SFN_TexCoord,id:549,x:31042,y:32787,varname:node_549,prsc:2,uv:0;n:type:ShaderForge.SFN_Vector2,id:1055,x:30991,y:32960,varname:node_1055,prsc:2,v1:0.5,v2:0.5;n:type:ShaderForge.SFN_Multiply,id:2770,x:31478,y:33112,varname:node_2770,prsc:2|A-2447-T,B-2640-OUT;n:type:ShaderForge.SFN_Distance,id:7966,x:31213,y:32837,varname:node_7966,prsc:2|A-549-UVOUT,B-1055-OUT;n:type:ShaderForge.SFN_Time,id:2447,x:31167,y:33092,varname:node_2447,prsc:2;n:type:ShaderForge.SFN_Vector1,id:5132,x:31133,y:33283,varname:node_5132,prsc:2,v1:0.8;n:type:ShaderForge.SFN_Multiply,id:6946,x:31643,y:32987,varname:node_6946,prsc:2|A-1268-OUT,B-2770-OUT;n:type:ShaderForge.SFN_Vector1,id:6501,x:32003,y:32980,varname:node_6501,prsc:2,v1:0.25;n:type:ShaderForge.SFN_Slider,id:2640,x:31199,y:33425,ptovrint:False,ptlb:node_2640,ptin:_node_2640,varname:node_2640,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.8,max:1;proporder:4518-4102-7911-2468-2118-2640;pass:END;sub:END;*/

Shader "Custom/Start-sphere" {
    Properties {
        _node_4518 ("node_4518", 2D) = "white" {}
        _node_4102 ("node_4102", 2D) = "white" {}
        _node_7911 ("node_7911", Color) = (0,1,1,1)
        _node_2468 ("node_2468", Color) = (0,0.6241379,0.6241379,1)
        _node_2118 ("node_2118", Range(-0.1, 1)) = -0.05
        _node_2640 ("node_2640", Range(0, 1)) = 0.8
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform sampler2D _node_4102; uniform float4 _node_4102_ST;
            uniform sampler2D _node_4518; uniform float4 _node_4518_ST;
            uniform float4 _node_7911;
            uniform float4 _node_2468;
            uniform float _node_2118;
            uniform float _node_2640;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                UNITY_FOG_COORDS(1)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
////// Lighting:
////// Emissive:
                float4 node_9528 = _Time + _TimeEditor;
                float node_6891_ang = node_9528.g;
                float node_6891_spd = _node_2118;
                float node_6891_cos = cos(node_6891_spd*node_6891_ang);
                float node_6891_sin = sin(node_6891_spd*node_6891_ang);
                float2 node_6891_piv = float2(0.5,0.5);
                float2 node_6891 = (mul(i.uv0-node_6891_piv,float2x2( node_6891_cos, -node_6891_sin, node_6891_sin, node_6891_cos))+node_6891_piv);
                float4 _node_4102_var = tex2D(_node_4102,TRANSFORM_TEX(node_6891, _node_4102));
                float4 node_2447 = _Time + _TimeEditor;
                float node_5106_ang = (((1.0 - (distance(i.uv0,float2(0.5,0.5))*2.0))*(node_2447.g*_node_2640))*6.28318530718);
                float node_5106_spd = 0.25;
                float node_5106_cos = cos(node_5106_spd*node_5106_ang);
                float node_5106_sin = sin(node_5106_spd*node_5106_ang);
                float2 node_5106_piv = float2(0.5,0.5);
                float2 node_5106 = (mul(i.uv0-node_5106_piv,float2x2( node_5106_cos, -node_5106_sin, node_5106_sin, node_5106_cos))+node_5106_piv);
                float4 _node_4518_var = tex2D(_node_4518,TRANSFORM_TEX(node_5106, _node_4518));
                float3 emissive = saturate(( lerp(_node_2468.rgb,(_node_7911.rgb*_node_4102_var.rgb),_node_4102_var.rgb) > 0.5 ? (_node_4518_var.rgb + 2.0*lerp(_node_2468.rgb,(_node_7911.rgb*_node_4102_var.rgb),_node_4102_var.rgb) -1.0) : (_node_4518_var.rgb + 2.0*(lerp(_node_2468.rgb,(_node_7911.rgb*_node_4102_var.rgb),_node_4102_var.rgb)-0.5))));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
