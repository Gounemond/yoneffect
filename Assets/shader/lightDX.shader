// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:8201,x:32719,y:32712,varname:node_8201,prsc:2|emission-8301-OUT;n:type:ShaderForge.SFN_Lerp,id:8301,x:32498,y:32760,varname:node_8301,prsc:2|A-6165-RGB,B-8759-OUT,T-8708-OUT;n:type:ShaderForge.SFN_Multiply,id:8759,x:32300,y:32739,varname:node_8759,prsc:2|A-1156-RGB,B-4370-OUT;n:type:ShaderForge.SFN_Color,id:1156,x:32094,y:32721,ptovrint:False,ptlb:node_1156,ptin:_node_1156,varname:node_1156,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:0,c4:1;n:type:ShaderForge.SFN_Vector1,id:4370,x:32111,y:32873,varname:node_4370,prsc:2,v1:2;n:type:ShaderForge.SFN_Color,id:6165,x:32396,y:32595,ptovrint:False,ptlb:node_6165,ptin:_node_6165,varname:node_6165,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Multiply,id:8708,x:32529,y:32956,varname:node_8708,prsc:2|A-2943-OUT,B-7216-OUT;n:type:ShaderForge.SFN_Slider,id:7216,x:32223,y:33104,ptovrint:False,ptlb:SliderDX,ptin:_SliderDX,varname:node_7216,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_OneMinus,id:2943,x:32324,y:32913,varname:node_2943,prsc:2|IN-1992-OUT;n:type:ShaderForge.SFN_Fresnel,id:1992,x:32149,y:32927,varname:node_1992,prsc:2|NRM-2468-OUT,EXP-3454-OUT;n:type:ShaderForge.SFN_NormalVector,id:2468,x:31969,y:32906,prsc:2,pt:False;n:type:ShaderForge.SFN_Vector1,id:3454,x:32011,y:33088,varname:node_3454,prsc:2,v1:2;proporder:1156-6165-7216;pass:END;sub:END;*/

Shader "Custom/lightDX" {
    Properties {
        _node_1156 ("node_1156", Color) = (1,1,0,1)
        _node_6165 ("node_6165", Color) = (0.5,0.5,0.5,1)
        _SliderDX ("SliderDX", Range(0, 1)) = 0
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _node_1156;
            uniform float4 _node_6165;
            uniform float _SliderDX;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 posWorld : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
                UNITY_FOG_COORDS(2)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float3 emissive = lerp(_node_6165.rgb,(_node_1156.rgb*2.0),((1.0 - pow(1.0-max(0,dot(i.normalDir, viewDirection)),2.0))*_SliderDX));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
