// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:5511,x:32719,y:32712,varname:node_5511,prsc:2|emission-4656-OUT,olwid-5734-OUT,olcol-478-RGB;n:type:ShaderForge.SFN_Lerp,id:4656,x:32506,y:32866,varname:node_4656,prsc:2|A-3298-RGB,B-4188-OUT,T-6011-OUT;n:type:ShaderForge.SFN_Multiply,id:4188,x:32285,y:32804,varname:node_4188,prsc:2|A-7021-RGB,B-9148-OUT;n:type:ShaderForge.SFN_Vector1,id:9148,x:32066,y:32875,varname:node_9148,prsc:2,v1:2;n:type:ShaderForge.SFN_Color,id:7021,x:32016,y:32629,ptovrint:False,ptlb:color,ptin:_color,varname:_color,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.1102941,c2:0.1077661,c3:0.01865268,c4:1;n:type:ShaderForge.SFN_Color,id:3298,x:32376,y:32636,ptovrint:False,ptlb:base,ptin:_base,varname:_base,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0,c3:0.07450981,c4:1;n:type:ShaderForge.SFN_OneMinus,id:6011,x:32285,y:32980,varname:node_6011,prsc:2|IN-3038-OUT;n:type:ShaderForge.SFN_Fresnel,id:3038,x:32066,y:32953,varname:node_3038,prsc:2|NRM-9799-OUT,EXP-8696-OUT;n:type:ShaderForge.SFN_NormalVector,id:9799,x:31786,y:32799,prsc:2,pt:False;n:type:ShaderForge.SFN_Vector1,id:8696,x:31913,y:32987,varname:node_8696,prsc:2,v1:2;n:type:ShaderForge.SFN_Vector1,id:643,x:32514,y:33033,varname:node_643,prsc:2,v1:0.1;n:type:ShaderForge.SFN_Slider,id:2302,x:32242,y:33302,ptovrint:False,ptlb:frecce1,ptin:_frecce1,varname:_frecce1,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Color,id:478,x:32242,y:33128,ptovrint:False,ptlb:node_478,ptin:_node_478,varname:_node_478,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Multiply,id:5734,x:32552,y:33134,varname:node_5734,prsc:2|A-643-OUT,B-2302-OUT;proporder:7021-3298-2302-478;pass:END;sub:END;*/

Shader "Custom/astrofrecce1" {
    Properties {
        _color ("color", Color) = (0.1102941,0.1077661,0.01865268,1)
        _base ("base", Color) = (0,0,0.07450981,1)
        _frecce1 ("frecce1", Range(0, 1)) = 1
        _node_478 ("node_478", Color) = (1,1,1,1)
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "Outline"
            Tags {
            }
            Cull Front
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float _frecce1;
            uniform float4 _node_478;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                UNITY_FOG_COORDS(0)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.pos = mul(UNITY_MATRIX_MVP, float4(v.vertex.xyz + v.normal*(0.1*_frecce1),1) );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                return fixed4(_node_478.rgb,0);
            }
            ENDCG
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _color;
            uniform float4 _base;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 posWorld : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
                UNITY_FOG_COORDS(2)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float3 emissive = lerp(_base.rgb,(_color.rgb*2.0),(1.0 - pow(1.0-max(0,dot(i.normalDir, viewDirection)),2.0)));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
