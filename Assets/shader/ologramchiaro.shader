// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:6142,x:33979,y:32502,varname:node_6142,prsc:2|emission-6888-OUT,alpha-1341-OUT,clip-5349-OUT;n:type:ShaderForge.SFN_Vector1,id:6888,x:33597,y:32550,varname:node_6888,prsc:2,v1:0.4;n:type:ShaderForge.SFN_Multiply,id:5349,x:33700,y:32821,varname:node_5349,prsc:2|A-1398-RGB,B-4082-OUT;n:type:ShaderForge.SFN_Vector1,id:4082,x:33099,y:33025,varname:node_4082,prsc:2,v1:1.4;n:type:ShaderForge.SFN_Vector1,id:1341,x:33675,y:32651,varname:node_1341,prsc:2,v1:0.1;n:type:ShaderForge.SFN_Multiply,id:981,x:32559,y:32861,varname:node_981,prsc:2|A-1653-UVOUT,B-7795-OUT;n:type:ShaderForge.SFN_TexCoord,id:1653,x:32148,y:32523,varname:node_1653,prsc:2,uv:0;n:type:ShaderForge.SFN_ValueProperty,id:7795,x:32402,y:32962,ptovrint:False,ptlb:titling,ptin:_titling,varname:node_7795,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Append,id:8815,x:32343,y:32692,varname:node_8815,prsc:2|A-8977-OUT,B-8525-OUT;n:type:ShaderForge.SFN_ValueProperty,id:8977,x:32138,y:32749,ptovrint:False,ptlb:U,ptin:_U,varname:node_8977,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_ValueProperty,id:8525,x:32126,y:32880,ptovrint:False,ptlb:V,ptin:_V,varname:node_8525,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.4;n:type:ShaderForge.SFN_Add,id:4224,x:32771,y:32656,varname:node_4224,prsc:2|A-1653-UVOUT,B-7140-OUT;n:type:ShaderForge.SFN_Multiply,id:7140,x:32580,y:32702,varname:node_7140,prsc:2|A-8815-OUT,B-981-OUT;n:type:ShaderForge.SFN_Tex2d,id:1398,x:33139,y:32596,ptovrint:False,ptlb:node_1398,ptin:_node_1398,varname:node_1398,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:28d41caf4ea956b4d9f7eb2f98c54828,ntxv:0,isnm:False|UVIN-4224-OUT;proporder:7795-8977-8525-1398;pass:END;sub:END;*/

Shader "Custom/ologramchiaro" {
    Properties {
        _titling ("titling", Float ) = 1
        _U ("U", Float ) = 1
        _V ("V", Float ) = 0.4
        _node_1398 ("node_1398", 2D) = "white" {}
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float _titling;
            uniform float _U;
            uniform float _V;
            uniform sampler2D _node_1398; uniform float4 _node_1398_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                UNITY_FOG_COORDS(1)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float2 node_4224 = (i.uv0+(float2(_U,_V)*(i.uv0*_titling)));
                float4 _node_1398_var = tex2D(_node_1398,TRANSFORM_TEX(node_4224, _node_1398));
                clip((_node_1398_var.rgb*1.4) - 0.5);
////// Lighting:
////// Emissive:
                float node_6888 = 0.4;
                float3 emissive = float3(node_6888,node_6888,node_6888);
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,0.1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float _titling;
            uniform float _U;
            uniform float _V;
            uniform sampler2D _node_1398; uniform float4 _node_1398_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float2 node_4224 = (i.uv0+(float2(_U,_V)*(i.uv0*_titling)));
                float4 _node_1398_var = tex2D(_node_1398,TRANSFORM_TEX(node_4224, _node_1398));
                clip((_node_1398_var.rgb*1.4) - 0.5);
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
