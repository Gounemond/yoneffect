﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (CapsuleCollider))]
public class VR_FirstPersonCharacter : MonoBehaviour
{
    [Header("Movement settings")]
    public float forwardSpeed = 5f;                                    // The speed at which we want the character to move
    public float backwardSpeed = 3f;                                   // The speed at which we want the character to be able to strafe
    public float sideSpeed = 2f;
    public float sprintMultiplier = 2f;                                // The speed at which we want the character to move

    public bool HmdRotatesY = true;
    public Camera playerCamera;

    [Tooltip("Amount of degrees you turn when pressing GamePad Shoulders")]
    [Range(0, 180)]
    public float RotationRatchet = 45.0f;			                   // Amount of degrees for instant turns with Oculus (with Q/E)


    [SerializeField]
    private AdvancedSettings advanced = new AdvancedSettings();        // The container for the advanced settings ( done this way so that the advanced setting are exposed under a foldout

    [System.Serializable]
    public class AdvancedSettings                                                       // The advanced settings
    {
        public PhysicMaterial zeroFrictionMaterial;                                     // Material used for zero friction simulation
        public PhysicMaterial highFrictionMaterial;                                     // Material used for high friction ( can stop character sliding down slopes )
        public float groundStickyEffect = 5f;                                           // power of 'stick to ground' effect - prevents bumping down slopes.
    }

    Animator animator;		                                                            // The animator for the character
    private float speed = 1;
    private float animatorSpeed = 1;

    private Vector2 input;
    private IComparer rayHitComparer;

    private InputVCR vcrInput;                                                          // VCR Input Manager (responsible to record your inputs)														


    void Awake()
    {
        animator = GetComponentInChildren<Animator>();

        vcrInput = GetComponent<InputVCR>();

        rayHitComparer = new RayHitComparer();

        Cursor.visible = false;

        SetUpAnimator();

        
    }

    public void FixedUpdate()
    {
        RotateView();

        speed = InputManagerFATE.Instance.sprint ? sprintMultiplier : 1;
        animatorSpeed = InputManagerFATE.Instance.sprint ? sprintMultiplier : 1;

        Quaternion ort = (HmdRotatesY) ? playerCamera.transform.rotation : transform.rotation;
        Vector3 ortEuler = ort.eulerAngles;
        ortEuler.z = ortEuler.x = 0f;
        ort = Quaternion.Euler(ortEuler);

        Vector3 euler = transform.rotation.eulerAngles;

        if (Input.GetButtonUp("Desktop_LeftShoulder"))
        {
            euler.y -= RotationRatchet;
        }

        if (Input.GetButtonUp("Desktop_RightShoulder"))
        {
            euler.y += RotationRatchet;
        }

        // Input control to instantly rotate from keyboard
        if (Input.GetKeyDown(KeyCode.Q))
        {
            euler.y -= RotationRatchet;
        }
        // Input control to instantly rotate from keyboard
        if (Input.GetKeyDown(KeyCode.E))
        {
            euler.y += RotationRatchet;
        }

  
        float rightAxisX = Input.GetAxis("Desktop_Right_X_Axis");

        euler.y += rightAxisX * Time.deltaTime;

        euler.y += Input.GetAxis("Mouse X") * 3.25f * Time.deltaTime;


        transform.rotation = Quaternion.Euler(euler);

        //Vector2 input = GetInput(IsGrounded());
        
        transform.rotation = playerCamera.transform.localRotation;


        input = new Vector2(vcrInput.GetAxis(InputManagerFATE.Instance.sideMovementAxis[InputManagerFATE.Instance.inputDeviceIndex]),
                            vcrInput.GetAxis(InputManagerFATE.Instance.forwardMovementAxis[InputManagerFATE.Instance.inputDeviceIndex]));

        // normalize input if it exceeds 1 in combined length:
        if (input.sqrMagnitude > 1) input.Normalize();

        // always move along the camera forward as it is the direction that it being aimed at
        Vector3 desiredMove;
        if (HmdRotatesY)
        {
            desiredMove = playerCamera.transform.forward * input.y + playerCamera.transform.right * input.x;
            desiredMove = Vector3.ProjectOnPlane(desiredMove, Vector3.up).normalized;
        }
        else
        {
            desiredMove = transform.forward * input.y + transform.right * input.x;
            desiredMove = Vector3.ProjectOnPlane(desiredMove, Vector3.up).normalized;
        }

        // Get a vector which is desired move as a world-relative direction, including speeds
        desiredMove = Vector3.ProjectOnPlane(desiredMove, Vector3.up).normalized;

        // Set the rigidbody's velocity according to the ground angle and desired move
        GetComponent<Rigidbody>().velocity = desiredMove * 5;

        // Use low/high friction depending on whether we're moving or not
        if (desiredMove.magnitude > 0)
        {
            GetComponent<Collider>().material = advanced.zeroFrictionMaterial;
        }
        else
        {
            GetComponent<Collider>().material = advanced.highFrictionMaterial;
        }

        UpdateAnimator();
    }

    void SetUpAnimator()
    {
        // this is a ref to the animator component on the root.
        animator = GetComponent<Animator>();

        // we use avatar from a child animator component if present
        // (this is to enable easy swapping of the character model as a child node)
        foreach (var childAnimator in GetComponentsInChildren<Animator>())
        {
            if (childAnimator != animator)
            {
                animator.avatar = childAnimator.avatar;
                Destroy(childAnimator);
                break;
            }
        }
    }

    void UpdateAnimator()
    {
        // Here we tell the animator what to do based on the current states and inputs.

        // update the animator parameters
        animator.SetLookAtPosition(transform.forward * input.y * forwardSpeed + transform.right * input.x * sideSpeed);
        if (vcrInput.GetAxis(InputManagerFATE.Instance.forwardMovementAxis[InputManagerFATE.Instance.inputDeviceIndex]) < 0)
        {
            animator.SetFloat("Forward", -vcrInput.GetAxis(InputManagerFATE.Instance.forwardMovementAxis[InputManagerFATE.Instance.inputDeviceIndex]));
            animator.speed = -animatorSpeed;
        }
        else
        {
            animator.SetFloat("Forward", vcrInput.GetAxis(InputManagerFATE.Instance.forwardMovementAxis[InputManagerFATE.Instance.inputDeviceIndex]));
            animator.speed = animatorSpeed;
        }
        //animator.SetFloat ("Turn", turnAmount, 0.1f, Time.deltaTime);
        //animator.SetBool ("Crouch", crouchInput);
        animator.SetBool("OnGround", true);
        animator.SetBool("Crouch", false);

    }

    //used for comparing distances
    class RayHitComparer : IComparer
    {
        public int Compare(object x, object y)
        {
            return ((RaycastHit)x).distance.CompareTo(((RaycastHit)y).distance);
        }
    }

    // Get a Vector2 Input, adjusting movement speed according to direction
    private Vector2 GetInput(bool isGrounded)
    {
        Vector2 input;

        input = new Vector2
        {
            x = Input.GetAxis("Horizontal"),
            y = Input.GetAxis("Vertical")
        };

        return input;
    }

    private void RotateView()
    {
        //avoids the mouse looking if the game is effectively paused
        if (Mathf.Abs(Time.timeScale) < float.Epsilon) return;

        // get the rotation before it's changed
        float oldYRotation = transform.eulerAngles.y;

        // Rotate the rigidbody velocity to match the new direction that the character is looking
        Quaternion velRotation = Quaternion.AngleAxis(transform.eulerAngles.y - oldYRotation, Vector3.up);
        GetComponent<Rigidbody>().velocity = velRotation * GetComponent<Rigidbody>().velocity;
    }

}
