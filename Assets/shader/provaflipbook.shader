// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:2,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:2,rntp:3,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:2097,x:34776,y:32854,varname:node_2097,prsc:2|emission-5619-OUT,clip-5032-OUT;n:type:ShaderForge.SFN_Vector4Property,id:2878,x:31138,y:32981,ptovrint:False,ptlb:vector 4,ptin:_vector4,varname:node_2878,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:3,v2:3,v3:1,v4:0;n:type:ShaderForge.SFN_Relay,id:4645,x:31566,y:32929,varname:node_4645,prsc:2|IN-2878-X;n:type:ShaderForge.SFN_Relay,id:9257,x:31435,y:33169,varname:node_9257,prsc:2|IN-2878-Y;n:type:ShaderForge.SFN_Relay,id:9695,x:31189,y:33306,varname:node_9695,prsc:2|IN-2878-Z;n:type:ShaderForge.SFN_Multiply,id:3688,x:31376,y:33324,varname:node_3688,prsc:2|A-9695-OUT,B-5187-OUT;n:type:ShaderForge.SFN_Time,id:2872,x:30963,y:33375,varname:node_2872,prsc:2;n:type:ShaderForge.SFN_Frac,id:6808,x:31609,y:33434,varname:node_6808,prsc:2|IN-3688-OUT;n:type:ShaderForge.SFN_Multiply,id:5541,x:31571,y:33240,varname:node_5541,prsc:2|A-4645-OUT,B-9257-OUT;n:type:ShaderForge.SFN_Multiply,id:6920,x:31791,y:33291,varname:node_6920,prsc:2|A-5541-OUT,B-6808-OUT;n:type:ShaderForge.SFN_Round,id:7465,x:31948,y:33414,varname:node_7465,prsc:2|IN-6920-OUT;n:type:ShaderForge.SFN_Divide,id:6161,x:32062,y:33266,varname:node_6161,prsc:2|A-7465-OUT,B-4645-OUT;n:type:ShaderForge.SFN_Divide,id:3702,x:32217,y:32851,varname:node_3702,prsc:2|A-7457-OUT,B-4645-OUT;n:type:ShaderForge.SFN_Divide,id:6563,x:32262,y:33105,varname:node_6563,prsc:2|A-799-OUT,B-9257-OUT;n:type:ShaderForge.SFN_Floor,id:799,x:32262,y:33266,varname:node_799,prsc:2|IN-6161-OUT;n:type:ShaderForge.SFN_Fmod,id:7457,x:32245,y:32970,varname:node_7457,prsc:2|A-7465-OUT,B-4645-OUT;n:type:ShaderForge.SFN_OneMinus,id:8884,x:32491,y:33071,varname:node_8884,prsc:2|IN-6563-OUT;n:type:ShaderForge.SFN_Append,id:717,x:32670,y:32960,varname:node_717,prsc:2|A-3702-OUT,B-8884-OUT;n:type:ShaderForge.SFN_Append,id:5967,x:32595,y:32777,varname:node_5967,prsc:2|A-4645-OUT,B-9257-OUT;n:type:ShaderForge.SFN_TexCoord,id:3205,x:32804,y:32620,varname:node_3205,prsc:2,uv:0;n:type:ShaderForge.SFN_Divide,id:9385,x:33060,y:32922,varname:node_9385,prsc:2|A-3205-UVOUT,B-5967-OUT;n:type:ShaderForge.SFN_Add,id:4851,x:32954,y:33059,varname:node_4851,prsc:2|A-9385-OUT,B-717-OUT;n:type:ShaderForge.SFN_OneMinus,id:5032,x:33796,y:33247,varname:node_5032,prsc:2|IN-7768-RGB;n:type:ShaderForge.SFN_Color,id:5635,x:33764,y:32154,ptovrint:False,ptlb:node_4194_copy,ptin:_node_4194_copy,varname:_node_4194_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0,c3:1,c4:1;n:type:ShaderForge.SFN_TexCoord,id:6295,x:33320,y:32388,varname:node_6295,prsc:2,uv:0;n:type:ShaderForge.SFN_Distance,id:5100,x:33576,y:32364,varname:node_5100,prsc:2|A-6295-UVOUT,B-6364-OUT;n:type:ShaderForge.SFN_Vector2,id:6364,x:33320,y:32594,varname:node_6364,prsc:2,v1:0.5,v2:0.9;n:type:ShaderForge.SFN_OneMinus,id:4517,x:33995,y:32728,varname:node_4517,prsc:2|IN-2454-OUT;n:type:ShaderForge.SFN_Multiply,id:2454,x:33873,y:32483,varname:node_2454,prsc:2|A-5100-OUT,B-9141-OUT;n:type:ShaderForge.SFN_Vector1,id:9141,x:33696,y:32612,varname:node_9141,prsc:2,v1:1;n:type:ShaderForge.SFN_Lerp,id:5619,x:34203,y:32503,varname:node_5619,prsc:2|A-1818-RGB,B-5635-RGB,T-4517-OUT;n:type:ShaderForge.SFN_Color,id:1818,x:34038,y:32126,ptovrint:False,ptlb:node_4405_copy,ptin:_node_4405_copy,varname:_node_4405_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:1,c3:0.7921569,c4:1;n:type:ShaderForge.SFN_Multiply,id:5187,x:31243,y:33468,varname:node_5187,prsc:2|A-2872-T,B-9962-OUT;n:type:ShaderForge.SFN_Slider,id:9962,x:30895,y:33606,ptovrint:False,ptlb:time,ptin:_time,varname:node_9962,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.3323896,max:1;n:type:ShaderForge.SFN_Tex2d,id:7768,x:33443,y:33102,ptovrint:False,ptlb:node_7768,ptin:_node_7768,varname:node_7768,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:5e43576f8dd18494697b0b826fcb2c06,ntxv:0,isnm:False|UVIN-4851-OUT;proporder:2878-5635-1818-9962-7768;pass:END;sub:END;*/

Shader "Custom/provaflipbook" {
    Properties {
        _vector4 ("vector 4", Vector) = (3,3,1,0)
        _node_4194_copy ("node_4194_copy", Color) = (0,0,1,1)
        _node_4405_copy ("node_4405_copy", Color) = (0,1,0.7921569,1)
        _time ("time", Range(0, 1)) = 0.3323896
        _node_7768 ("node_7768", 2D) = "white" {}
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "Queue"="AlphaTest"
            "RenderType"="TransparentCutout"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Cull Off
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform float4 _vector4;
            uniform float4 _node_4194_copy;
            uniform float4 _node_4405_copy;
            uniform float _time;
            uniform sampler2D _node_7768; uniform float4 _node_7768_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                UNITY_FOG_COORDS(1)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                float node_4645 = _vector4.r;
                float node_9257 = _vector4.g;
                float4 node_2872 = _Time + _TimeEditor;
                float node_7465 = round(((node_4645*node_9257)*frac((_vector4.b*(node_2872.g*_time)))));
                float2 node_4851 = ((i.uv0/float2(node_4645,node_9257))+float2((fmod(node_7465,node_4645)/node_4645),(1.0 - (floor((node_7465/node_4645))/node_9257))));
                float4 _node_7768_var = tex2D(_node_7768,TRANSFORM_TEX(node_4851, _node_7768));
                clip((1.0 - _node_7768_var.rgb) - 0.5);
////// Lighting:
////// Emissive:
                float3 emissive = lerp(_node_4405_copy.rgb,_node_4194_copy.rgb,(1.0 - (distance(i.uv0,float2(0.5,0.9))*1.0)));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform float4 _vector4;
            uniform float _time;
            uniform sampler2D _node_7768; uniform float4 _node_7768_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                float node_4645 = _vector4.r;
                float node_9257 = _vector4.g;
                float4 node_2872 = _Time + _TimeEditor;
                float node_7465 = round(((node_4645*node_9257)*frac((_vector4.b*(node_2872.g*_time)))));
                float2 node_4851 = ((i.uv0/float2(node_4645,node_9257))+float2((fmod(node_7465,node_4645)/node_4645),(1.0 - (floor((node_7465/node_4645))/node_9257))));
                float4 _node_7768_var = tex2D(_node_7768,TRANSFORM_TEX(node_4851, _node_7768));
                clip((1.0 - _node_7768_var.rgb) - 0.5);
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
