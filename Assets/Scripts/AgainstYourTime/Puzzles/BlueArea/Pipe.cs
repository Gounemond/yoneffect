﻿
using UnityEngine;
using System.Collections;
using VRStandardAssets.Utils;
using DG.Tweening;

public class Pipe : MonoBehaviour
{
    public PipePuzzle pipePuzzleManager;
    public PipeCollider[] pipeColliders;
    public bool isPowered = false;

    public VRInteractiveItem pipeActivator;                // Interactable component
    public Color HoverInColor;
    private Color m_OriginalColor;

    public void EnableInteractivity()
    {
        pipeActivator.OnOver += HoverIn;
        pipeActivator.OnOut += HoverOut;
        pipeActivator.OnClick += RotateClick;
    }

    public void DisableInteractivity()
    {
        HoverOut();

        pipeActivator.OnOver -= HoverIn;
        pipeActivator.OnOut -= HoverOut;
        pipeActivator.OnClick -= RotateClick;
    }
    // Use this for initialization
    void Start ()
    {
        m_OriginalColor = GetComponent<Renderer>().materials[2].GetColor("_node_4897");
        EnableInteractivity();
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Propagate()
    {
        for (int i = 0; i < pipeColliders.Length; i++)
        {
            pipeColliders[i].PropagateSignal();
        }
    }

    public void RechargeAll()
    {
        for (int i = 0; i < pipeColliders.Length; i++)
        {
            pipeColliders[i].Recharge();
        }
    }

    public void TurnOn()
    {
        isPowered = true;
        for (int i = 0; i < pipeColliders.Length; i++)
        {
            pipeColliders[i].TurnOn();
        }
    }

    public void TurnOff()
    {
        isPowered = false;
        for (int i = 0; i < pipeColliders.Length; i++)
        {
            pipeColliders[i].TurnOff();
        }
    }

    public void HoverIn()
    {
        GetComponent<Renderer>().materials[2].SetColor("_node_4897", HoverInColor);
    }

    public void HoverOut()
    {
        GetComponent<Renderer>().materials[2].SetColor("_node_4897", m_OriginalColor);
    }

    public void RotateNotClick()
    {
        this.transform.DORotate(new Vector3(0, 0, 90), 1f, RotateMode.LocalAxisAdd);
    }

    public void RotateClick()
    {
        this.transform.DORotate(new Vector3(0, 0, 90), 1f, RotateMode.LocalAxisAdd);
        StartCoroutine(WaitAndRestoreInteractivity());
    }

    public IEnumerator WaitAndRestoreInteractivity()
    {
        DisableInteractivity();
        pipePuzzleManager.ResetPower();
        yield return new WaitForSeconds(1.2f);
        EnableInteractivity();
        pipePuzzleManager.Propagate();
    }
}
