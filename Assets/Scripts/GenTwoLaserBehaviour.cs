using UnityEngine;
using System.Collections;

public class GenTwoLaserBehaviour : MonoBehaviour {
	
	private Color active = Color.red;
	private Color semiactivated = Color.yellow;
	private Color deactivated = Color.black;
	
	// Variable that indicates how many laser are active
	private int numLaser;
	
	//Array of all elements thata are bounded to this generator
	public YonBehaviour[] boundedObjects;
	
	// Use this for initialization
	void Start ()
    {		
		numLaser = 1;
	}
	
	// Update is called once per frame
	void Update ()
    {
	
	}
	
	//Function which activete the generator
	public void activeGen()
    {	
		//Increment of the laser that are active
		numLaser++;
		
		if(numLaser == 1) {
			
			//GetComponent<Renderer>().material.color = semiactivated;
			
			//Call the deactivate for all the objects bounded to this generator
			for(int i = 0; i < boundedObjects.Length; i++) {
				
				boundedObjects[i].deActivate();
				
			}
			
		}
		else if(numLaser == 2) {
			
			//GetComponent<Renderer>().material.color = active;
			
		}
		else {
		
			print("Errore, laser attivi: ");
			print(numLaser);		
		}
		
	}
	
	//Function which disactivet the generator
	public void deactiveGen() {
	
		//Decrement of the laser count
		numLaser--;
		
		if(numLaser == 1)
        {		
			//GetComponent<Renderer>().material.color = semiactivated;
		}
		else if(numLaser == 0)
        {		
			//GetComponent<Renderer>().material.color = deactivated;
			
			//Call the activate for all objects bounded to this generator
			for(int i = 0; i < boundedObjects.Length; i++)
            {				
				boundedObjects[i].Activate();
			}
			
		}
		else
        {	
			//print("Errore, laser attivi: ");
			//print(numLaser);	
		}
		
	}
	
}
