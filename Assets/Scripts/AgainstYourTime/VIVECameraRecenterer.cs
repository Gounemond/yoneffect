﻿using UnityEngine;
using UnityEngine.VR;
using System.Collections;

public class VIVECameraRecenterer : MonoBehaviour {

    public Transform cameraVive;
    public Transform parentVive;

	// Use this for initialization
	IEnumerator Start ()
    {
        if (VRSettings.loadedDeviceName == "OpenVR")
        {
            while (cameraVive.localPosition == Vector3.zero)
            {
                yield return 0;
            }
            parentVive.localPosition = parentVive.localPosition - cameraVive.localPosition;
        }
        yield return null;
    }
	
	// Update is called once per frame
}
