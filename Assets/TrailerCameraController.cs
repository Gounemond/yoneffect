﻿using UnityEngine;
using System.Collections;

public class TrailerCameraController : MonoBehaviour {

    public float speedMultiplier = 3.0f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetAxis("Horizontal") < 0)
        {
            transform.position += speedMultiplier * transform.right * -Time.deltaTime;
        }
        if (Input.GetAxis("Vertical") < 0)
        {
            transform.position += speedMultiplier * transform.forward * Time.deltaTime;
        }
        if (Input.GetAxis("Vertical") > 0)
        {
            transform.position += speedMultiplier * transform.forward * -Time.deltaTime;
        }
        if (Input.GetAxis("Horizontal") > 0)
        {
            transform.position += speedMultiplier * transform.right * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.LeftShift))
        {
            transform.position += speedMultiplier/ 1.3f * transform.up * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.LeftControl))
        {
            transform.position += speedMultiplier/ 1.3f * transform.up * -Time.deltaTime;
        }

        if (Input.GetKeyDown(KeyCode.I))
        {
            speedMultiplier += 1;
        }
        if (Input.GetKeyDown(KeyCode.K))
        {
            speedMultiplier -= 1;
        }
    }
}
