﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class FloatingCollectible : MonoBehaviour {

    public float verticalSpeed = 1f;
    public float horizontalSpeed = 1f;
    public float verticalDistance = 1f;
    public float horizontalDistance = 1f;
    public float spinningSpeed = 90;

    private bool switchSin = false;
    private float sine = 0.0f;


    // Use this for initialization
    void Start ()
    {
        GetComponent<Rigidbody>().AddRelativeTorque(new Vector3(0, spinningSpeed, 0));    //function to actually add the turning on all three axis
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (sine < Mathf.PI && !switchSin)
        {   //sine variable is fluctuating between 0 and Pi causing an up and down motion simulating floating, think sine curve
            sine += Time.deltaTime;
        }
        if (sine >= Mathf.PI)
        {
            switchSin = true;
        }
        if (sine <= 0)
        {
            switchSin = false;
        }
        if (sine >= 0 && switchSin)
        {
            sine = 0;
        }

        GetComponent<Rigidbody>().velocity = new Vector3(Mathf.Sin(2 * sine * horizontalSpeed) * horizontalDistance, Mathf.Sin(2 * sine * verticalSpeed) * verticalDistance, 0);
    }
}
