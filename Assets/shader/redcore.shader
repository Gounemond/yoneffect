// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:1,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:3248,x:32719,y:32712,varname:node_3248,prsc:2|emission-3575-RGB,voffset-4318-OUT;n:type:ShaderForge.SFN_Multiply,id:4318,x:32543,y:32995,varname:node_4318,prsc:2|A-8255-XYZ,B-8117-OUT;n:type:ShaderForge.SFN_Transform,id:8255,x:32372,y:32866,varname:node_8255,prsc:2,tffrom:0,tfto:1|IN-6833-OUT;n:type:ShaderForge.SFN_Multiply,id:6833,x:32199,y:32868,varname:node_6833,prsc:2|A-1456-XYZ,B-8351-OUT;n:type:ShaderForge.SFN_ObjectScale,id:1456,x:32139,y:32623,varname:node_1456,prsc:2,rcp:False;n:type:ShaderForge.SFN_Power,id:8351,x:32052,y:32915,varname:node_8351,prsc:2|VAL-35-OUT,EXP-3887-OUT;n:type:ShaderForge.SFN_Sin,id:35,x:31866,y:32814,varname:node_35,prsc:2|IN-2256-T;n:type:ShaderForge.SFN_Vector1,id:3887,x:31918,y:32987,varname:node_3887,prsc:2,v1:1;n:type:ShaderForge.SFN_Time,id:2256,x:31736,y:32702,varname:node_2256,prsc:2;n:type:ShaderForge.SFN_Multiply,id:8117,x:32394,y:33070,varname:node_8117,prsc:2|A-388-OUT,B-7673-OUT;n:type:ShaderForge.SFN_Multiply,id:388,x:32196,y:33096,varname:node_388,prsc:2|A-3979-OUT,B-114-OUT;n:type:ShaderForge.SFN_NormalVector,id:114,x:32045,y:33196,prsc:2,pt:False;n:type:ShaderForge.SFN_Noise,id:3979,x:32007,y:33080,varname:node_3979,prsc:2|XY-5091-UVOUT;n:type:ShaderForge.SFN_TexCoord,id:5091,x:31852,y:33053,varname:node_5091,prsc:2,uv:0;n:type:ShaderForge.SFN_Vector1,id:7673,x:32230,y:33265,varname:node_7673,prsc:2,v1:0.2;n:type:ShaderForge.SFN_Color,id:3575,x:32441,y:32609,ptovrint:False,ptlb:node_3575,ptin:_node_3575,varname:node_3575,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.6901961,c2:0.02745098,c3:0.09019608,c4:1;proporder:3575;pass:END;sub:END;*/

Shader "Custom/redcore" {
    Properties {
        _node_3575 ("node_3575", Color) = (0.6901961,0.02745098,0.09019608,1)
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Cull Front
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform float4 _node_3575;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
                UNITY_FOG_COORDS(2)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(-v.normal);
                float3 recipObjScale = float3( length(unity_WorldToObject[0].xyz), length(unity_WorldToObject[1].xyz), length(unity_WorldToObject[2].xyz) );
                float3 objScale = 1.0/recipObjScale;
                float4 node_2256 = _Time + _TimeEditor;
                float2 node_3979_skew = o.uv0 + 0.2127+o.uv0.x*0.3713*o.uv0.y;
                float2 node_3979_rnd = 4.789*sin(489.123*(node_3979_skew));
                float node_3979 = frac(node_3979_rnd.x*node_3979_rnd.y*(1+node_3979_skew.x));
                v.vertex.xyz += (mul( unity_WorldToObject, float4((objScale*pow(sin(node_2256.g),1.0)),0) ).xyz.rgb*((node_3979*v.normal)*0.2));
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float3 recipObjScale = float3( length(unity_WorldToObject[0].xyz), length(unity_WorldToObject[1].xyz), length(unity_WorldToObject[2].xyz) );
                float3 objScale = 1.0/recipObjScale;
                i.normalDir = normalize(i.normalDir);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float3 emissive = _node_3575.rgb;
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(-v.normal);
                float3 recipObjScale = float3( length(unity_WorldToObject[0].xyz), length(unity_WorldToObject[1].xyz), length(unity_WorldToObject[2].xyz) );
                float3 objScale = 1.0/recipObjScale;
                float4 node_2256 = _Time + _TimeEditor;
                float2 node_3979_skew = o.uv0 + 0.2127+o.uv0.x*0.3713*o.uv0.y;
                float2 node_3979_rnd = 4.789*sin(489.123*(node_3979_skew));
                float node_3979 = frac(node_3979_rnd.x*node_3979_rnd.y*(1+node_3979_skew.x));
                v.vertex.xyz += (mul( unity_WorldToObject, float4((objScale*pow(sin(node_2256.g),1.0)),0) ).xyz.rgb*((node_3979*v.normal)*0.2));
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float3 recipObjScale = float3( length(unity_WorldToObject[0].xyz), length(unity_WorldToObject[1].xyz), length(unity_WorldToObject[2].xyz) );
                float3 objScale = 1.0/recipObjScale;
                i.normalDir = normalize(i.normalDir);
                float3 normalDirection = i.normalDir;
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
