using UnityEngine;
using System.Collections;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System;
using System.Linq;

public class Grab : MonoBehaviour 
{
	public float  correctionforce = 50.0f;
	public GameObject beacon;
	public bool useVCR;
  	public InputVCR vcr;
	private GameObject target = null;
	private bool  grabbed  = false;
	void Start () 
	{

    }
    bool drag = false;
    void Update () 
	{
        RaycastHit hit;
        Ray ray = new Ray();
        float maxDistance = 8;

        ray.origin = transform.position;
        ray.direction = transform.TransformDirection(Vector3.forward);
		if (useVCR)
		{
			if (target != null)
			{
                // If you have a target, but you're looking away from it (looking at any other object / wall)
				if(Physics.Raycast (ray ,out hit, 10000) && (hit.collider.gameObject != target))
				{
                    // Return the grabbable target object to his original shader
					target.GetComponent<Renderer>().material.shader = Shader.Find ("Toon/Lighted Outline");
				}
			}
            // If you have actually a target object and you're currently grabbing it, and you press again the grab button
			if(grabbed && Input.GetButtonDown("Desktop_Button X") && target != null)
			{
				grabbed = false;                                            // Release the object
				target.GetComponent<Rigidbody>().useGravity = true;         // Enable gravity again
				target.GetComponent<Rigidbody>().freezeRotation = false;    // Enable rotation again
				target = null;	                                            // Lose reference to grabbable target
			}
			else
			{
                // If it happens to see a grabbable object in your distance range
				if(Physics.Raycast (ray ,out hit, maxDistance) && hit.collider.gameObject.tag.Equals ("Grabbable") && !grabbed)
            	{		
					target = hit.collider.gameObject;           // Save the reference for the actual grabbable target     	
					target.GetComponent<Renderer>().material.shader = Shader.Find ("Self-Illumin/Diffuse"); // Change the shader
            	}
				
                // If it happens to see a grabbable object in you distance range, and you press the grab button
				if(!grabbed && Physics.Raycast (ray ,out hit, maxDistance) && hit.collider.gameObject.tag.Equals ("Grabbable") && Input.GetButtonDown("Desktop_Button X"))
				{
					grabbed = true;	// Grabs the object
				}

                // Debugging the raycast to grab objects
            	/*if(Physics.Raycast (ray ,out hit, maxDistance))
            	{
					Debug.DrawRay(ray.origin,ray.direction*maxDistance, Color.red);

            	}*/
        	}	
		}

        // Update gravity force around the GrabBeacon if you actually have grabbed an object
        // The grab method is inspired from the Half Life series
		if (target != null && grabbed == true)
		{
			Vector3 force = beacon.transform.position - target.transform.position;
			target.GetComponent<Rigidbody>().freezeRotation = true;
			target.GetComponent<Rigidbody>().useGravity = false;
            target.GetComponent<Rigidbody>().velocity = force.normalized * target.GetComponent<Rigidbody>().velocity.magnitude;
			target.GetComponent<Rigidbody>().AddForce(force * correctionforce);  
    		target.GetComponent<Rigidbody>().velocity *= Mathf.Min(1.0f, force.magnitude / 2);
		}
	}
}